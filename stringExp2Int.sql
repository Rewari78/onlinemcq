 DROP FUNCTION stringExp2Int;
DELIMITER $$

CREATE
    /*[DEFINER = { user | CURRENT_USER }]*/
    FUNCTION `admin_online`.`stringExp2Int` (expression VARCHAR(20))
    RETURNS INT
    /*LANGUAGE SQL
    | [NOT] DETERMINISTIC
    | { CONTAINS SQL | NO SQL | READS SQL DATA | MODIFIES SQL DATA }
    | SQL SECURITY { DEFINER | INVOKER }
    | COMMENT 'string'*/
    BEGIN
    
      DECLARE val INT DEFAULT 0;
      DECLARE ct INT DEFAULT 1;
      DECLARE pos INT DEFAULT 0;
      DECLARE sep CHAR(1) DEFAULT "/";

      WHILE ct <= LENGTH(expression) DO
         SET pos = LOCATE(sep,SUBSTR(expression,ct,LENGTH(expression)-ct+1));
         IF pos >= 1 THEN 
            BEGIN -- when pos = 1 : add a null value
                SET val = val + SUBSTR(expression,ct,pos-1);
                 RETURN val;
                SET ct = ct + pos;
            END;
        ELSE 
            BEGIN
                SET val = val + SUBSTR(expression,ct,LENGTH(expression)-ct+1);
                 RETURN val;
                SET ct = ct + LENGTH(expression)-ct+1;
            END;
        END IF;
     END WHILE;
     RETURN val;
    END$$

DELIMITER ;