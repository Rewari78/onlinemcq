<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 3/31/19
 * Time: 10:03 AM
 */

date_default_timezone_set('Asia/Kolkata');


define('DS', DIRECTORY_SEPARATOR);
define('FLD_ROOT', dirname(dirname(__FILE__)));
define('FLD_INC', FLD_ROOT  . DS .  'inc');

include FLD_INC . DS . 'autoloader.php';
include FLD_INC . DS . 'config.php';
include FLD_INC. DS  . 'define.php';

Autoloader::init();

$table = DB_TABLE_EMAIL_SMS_QUEUE;
$userTable = DB_TABLE_USERS;
$userExtra = DB_TABLE_USER_EXTRA_INFO;
$userPackageRelation = DB_TABLE_PACKAGE_USER_REL;
$time = time();
$SQL = "SELECT * FROM `{$table}` WHERE `type` = 'sms' AND `scheduleStart` < :time";
$database = Db::getInstance();
$stmt = $database->prepare($SQL);
Db::bindValues(array(
    ':time' => $time
), $stmt);
$stmt->execute();
$result = $stmt->fetch(PDO::FETCH_ASSOC);

if ( empty($result) ) exit;

$scheduleId = $result['id'];

$obj = json_decode($result['object'], true);
$to  = json_decode($result['to'], true);

$users = array();
if ( $to['type'] == 'package' )
{
    $SQL = "SELECT `ut`.`firstName`, `ut`.`lastName`, `ut`.`phone` FROM `{$userTable}` AS `ut`
            INNER JOIN `{$userPackageRelation}` AS `upr`
            ON ( `upr`.`userId`  = `ut`.`id`) 
            WHERE `packageId` = :pId";

    $database = Db::getInstance();
    $stmt = $database->prepare($SQL);
    DB::bindValues(array(
        'pId' => $to['value']
    ), $stmt);
    $stmt->execute();

    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $users = $result;
}

if ( empty($users) ) exit;

foreach ( $users as $user )
{
	echo $user['firstName']; 
    $msg = str_ireplace(array(
        '{$firstName}','{$lastName}'
    ), array(
        $user['firstName'],
        $user['lastName']
    ), $obj['msg']);
    $vars = array(
        'loginID' => 'neetguidance',
        'password' => '12345',
        'mobile' => $user['phone'],
        'text' => $msg,
        'senderid' => $obj['senderId'],
        'route_id' => 3
    );
	$url = 'http://184.95.37.226/API/pushsms.aspx?' . http_build_query($vars);
    file_get_contents($url);
    
	echo $user['lastName']; 

}

// delete sending.
$SQL = "DELETE FROM `{$table}` WHERE `id` = :id";
$database = Db::getInstance();
$stmt = $database->prepare($SQL);
Db::bindValues(array(
    ':id' => $scheduleId
), $stmt);
$stmt->execute();
