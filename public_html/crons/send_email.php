<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 3/31/19
 * Time: 10:03 AM
 */

date_default_timezone_set('Asia/Kolkata');


define('DS', DIRECTORY_SEPARATOR);
define('FLD_ROOT', dirname(dirname(__FILE__)));
define('FLD_INC', FLD_ROOT  . DS .  'inc');
define('FLD_LIB', FLD_ROOT  . DS .  'libs');

include FLD_INC . DS . 'autoloader.php';
include FLD_INC . DS . 'config.php';
include FLD_INC. DS  . 'define.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

include FLD_LIB . DS . 'PHPMailer' . DS . 'src' . DS . 'Exception.php';
include FLD_LIB . DS . 'PHPMailer' . DS . 'src' . DS . 'PHPMailer.php';
include FLD_LIB . DS . 'PHPMailer' . DS . 'src' . DS . 'SMTP.php';

$mail = new PHPMailer(true);

Autoloader::init();

$table = DB_TABLE_EMAIL_SMS_QUEUE;
$userTable = DB_TABLE_USERS;
$userExtra = DB_TABLE_USER_EXTRA_INFO;
$userPackageRelation = DB_TABLE_PACKAGE_USER_REL;
$time = time();
$SQL = "SELECT * FROM `{$table}` WHERE `type` = 'email' AND `scheduleStart` < :time";
$database = Db::getInstance();
$stmt = $database->prepare($SQL);
Db::bindValues(array(
    ':time' => $time
), $stmt);
$stmt->execute();
$result = $stmt->fetch(PDO::FETCH_ASSOC);

if ( empty($result) ) exit;

$scheduleId = $result['id'];

$obj = json_decode($result['object'], true);
$to  = json_decode($result['to'], true);

$users = array();
if ( $to['type'] == 'package' )
{
    $SQL = "SELECT `ut`.`firstName`, `ut`.`lastName`, `ut`.`email` FROM `{$userTable}` AS `ut`
            INNER JOIN `{$userPackageRelation}` AS `upr`
            ON ( `upr`.`userId`  = `ut`.`id`) 
            WHERE `packageId` = :pId";

    $database = Db::getInstance();
    $stmt = $database->prepare($SQL);
    DB::bindValues(array(
        'pId' => $to['value']
    ), $stmt);
    $stmt->execute();

    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $users = $result;
}

if ( empty($users) ) exit;
echo "Awesome1";
foreach ( $users as $user )
{
	echo $user['email'];
    $msg = str_ireplace(array(
        '{$firstName}','{$lastName}'
    ), array(
        $user['firstName'],
        $user['lastName']
    ), $obj['msg']);
  	
	// use wordwrap() if lines are longer than 70 characters
	$msg = wordwrap($msg,70);

	try {
		$mail->SMTPDebug = 2;                                       // Enable verbose debug output
		$mail->isSMTP();                                            // Set mailer to use SMTP
		$mail->Host       = 'mail.neetguidance.com';  // Specify main and backup SMTP servers
		$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
		$mail->Username   = 'support@neetguidance.com';                     // SMTP username
		$mail->Password   = 'password@124';                               // SMTP password
		$mail->Port       = 465;        
		
		//Recipients
		$mail->setFrom('noreply@neetguidance.com', 'Neet Guidance');
		$mail->addAddress($user['email']);     // Add a recipient
		$mail->Subject = 'You have an email from Neet Guidance!';
		$mail->Body    = $msg;
		$mail->AltBody = $msg;
		echo "Awesome";
	} catch( Exception $ex ) {
		echo $ex->getMessage();
		continue;
	} 
		
}

// delete sending.
$SQL = "DELETE FROM `{$table}` WHERE `id` = :id";
$database = Db::getInstance();
$stmt = $database->prepare($SQL);
Db::bindValues(array(
    ':id' => $scheduleId
), $stmt);
$stmt->execute();
