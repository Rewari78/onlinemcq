<?php

/**
 *
 * Description of Pages
 *
 * @package
 * @subpackage
 * @author Akash Bose <bose@ullashnetwork.com>
 * @category
 * @since 07 16, 2016 [12:51 PM]
 */
class Page
{

    private static $_user = null;

    private $_path = null;
    private $_title = null;
    private $_pageType = null;
    private $_staticsName = null;
    private $_headerPath = null;
    private $_footerPath = null;
    private $_js = array();
    private $_data = null;

    public function __construct($path, $pageTitle, $pageType, $staticsName)
    {
        $this->_path = $path;
        $this->_title = $pageTitle;
        $this->_pageType = $pageType;
        $this->_staticsName = $staticsName;
        $this->_headerPath = FLD_PAGES . DS . $this->_pageType . '.header.php';
        $this->_footerPath = FLD_PAGES . DS . $this->_pageType . '.footer.php';

        // if (self::$_user == null) {
        //     // First check if the user is logged in or not.
        //     if (UserManager::getInstance()->isLoggedIn()) {
        //         // create the user information data.
        //         $userId = UserManager::getInstance()->getUserId();
        //         self::$_user = new User($userId);

        //         if (
        //             empty(self::$_user->getExtraInfo()) &&
        //             ($staticsName !== 'complete_registration' &&
        //                 $staticsName !== 'validate_complete_profile')
        //         )
        //             throw new RenderCompleteRegistration;
        //     }
        // }
    }

    public function load($data)
    {
        $this->_data = $data;

        if ($this->_pageType == Requests::PAGE_TYPE_AJAX)
            $this->_handleAjaxPage();
        else
            $this->_handlePage();
    }

    private function _addHeader()
    {
        @include $this->_headerPath;
    }

    private function _addFooter()
    {
        @include $this->_footerPath;
    }

    private function _handleAjaxPage()
    {
        include $this->_path;
    }

    private function _handlePage()
    {
        include $this->_path;
    }

    public function getUser()
    {
        return self::$_user;
    }

    /**
     * @param string $allowUserType
     * @param int $featureRequired
     *
     * @throws RenderLogin
     * @throws RenderPermissionError
     * @throws RenderUpgradePackage
     */
    public function onlyLoggedInAllowed($allowUserType = UserManager::USER_TYPE_TEACHER, $featureRequired = null)
    {
        $isAjax = $this->_pageType === Requests::PAGE_TYPE_AJAX;

        if (!UserManager::getInstance()->isLoggedIn()) {
            if ($isAjax) $this->json('info', 'userIsNotOnline');

            throw new RenderLogin;
        }

        // If admin check is true.
        // We throw Permission error.
        $havePermission = false;
        // switch ($allowUserType) {
        //     case UserManager::USER_TYPE_ADMIN:
        //         $havePermission = self::$_user->isAdmin();
        //         break;
        //     case UserManager::USER_TYPE_MODERATOR:
        //         $havePermission = self::$_user->isModerator();
        //         break;
        //     default:
        //         $havePermission = true;
        // }


        switch ($allowUserType) {
            case UserManager::USER_TYPE_TEACHER:
                $havePermission = UserManager::getInstance()->isTeacher();
                break;
            case UserManager::USER_TYPE_STUDENT:
                $havePermission = UserManager::getInstance()->isStudent();
                break;
            default:
                $havePermission = false;
        }


        if ($havePermission === false) {
            if ($isAjax) $this->json('info', 'permissionError');

            throw new RenderPermissionError;
        }

        // Now check for package
        // if (is_numeric($featureRequired) && !self::$_user->isFeatureEnabled($featureRequired)) {
        //     $feature = PackageManager::getInstance()->getFeaturesById($featureRequired);
        //     if (!empty($feature)) {
        //         if ($isAjax) $this->json('info', 'permissionError');

        //         throw new RenderUpgradePackage(
        //             $feature['featureName'],
        //             "Your current package do not support this feature. 
        //             You need to upgrade your package."
        //         );
        //     }
        // }
    }

    public function redirect($route, $sessionMessage = null)
    {
        $_SESSION['sessionMessage'] = $sessionMessage;
        header('Location: ' . SITE_URL . DS . $route);
        exit;
    }

    public function getSessionMessage()
    {
        $m = isset($_SESSION['sessionMessage']) ?
            $_SESSION['sessionMessage'] : null;
        unset($_SESSION['sessionMessage']);
        return $m;
    }

    public function isPost()
    {
        return $_SERVER['REQUEST_METHOD'] === 'POST';
    }

    public function getRootUrl()
    {
        return SITE_URL . DS . $this->_pageType;
    }

    public function getPath()
    {
        return FLD_PAGES . DS . $this->_pageType;
    }

    public function onLoadJs($js)
    {
        $this->_js[count($this->_js)] = $js;
    }

    public function json($info, $message = null, $shouldExits = true)
    {
        $data = array(
            'info' => $info,
            'message' => $message,
        );
        header('Content-type: application/json');
        echo json_encode($data);
        if ($shouldExits) exit;
    }
}
