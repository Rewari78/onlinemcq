<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 1/9/18
 * Time: 5:47 PM
 */

class RenderException extends Exception
{

    private $_route;
    private $_pageType;
    private $_extraData;

    public function __construct( $pageType, $route, $extraData = null )
    {
        parent::__construct("", 0, null);
        $this->_route = $route;
        $this->_pageType = $pageType;
        $this->_extraData = $extraData;
    }

    public function getRoute()
    {
        return $this->_route;
    }

    public function getPageType()
    {
        return $this->_pageType;
    }

    public function getExtraData()
    {
        return $this->_extraData;
    }
}

class RenderLogin extends RenderException
{

    public function __construct()
    {
        parent::__construct(Requests::PAGE_TYPE_NORMAL, 'login');
    }
}

class RenderCompleteRegistration extends RenderException
{

    public function __construct()
    {
        parent::__construct(Requests::PAGE_TYPE_NORMAL, 'complete-registration');
    }
};

class RenderPermissionError extends RenderException
{
    public function __construct()
    {
        parent::__construct(Requests::PAGE_TYPE_NORMAL, 'permission-error');
    }
}

class RenderUpgradePackage extends RenderException
{
    public function __construct( $featureName, $desc )
    {
        parent::__construct(Requests::PAGE_TYPE_NORMAL, 'upgrade-package', array(
            'featureName' => $featureName,
            'desc' => $desc
        ));
    }
}

class Error404 extends  Exception {}

