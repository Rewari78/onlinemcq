<?php


/**
 *
 * Description of Requests
 *
 * @package
 * @subpackage
 * @author Akash Bose <bose@ullashnetwork.com>
 * @category
 * @since 07 16, 2016 [12:06 PM]
 */
class Requests
{
    const PAGE_TYPE_NORMAL = 'normal';
    const PAGE_TYPE_AJAX = 'ajax';

    private static $instance;

    public static function init()
    {
        if ( self::$instance === null )
        {
            self::$instance = new self;
        }

        return self::$instance;
    }

    public static $_routes = array(
        self::PAGE_TYPE_NORMAL => array(),
        self::PAGE_TYPE_AJAX => array()
    );

    private static $_defaults = array(

        // Default route to load when no uri route is available
        // what to render.
        'defaultRoute' => array(
            self::PAGE_TYPE_AJAX => 'index',
            self::PAGE_TYPE_NORMAL => 'home'
        )
    );

    private $_pageToRender = null;

    private function __construct()
    {
        //Get the requested uri.
        $uris = $this->_splitRequest($_SERVER["REQUEST_URI"]);

        try {

            $pageType = self::PAGE_TYPE_NORMAL;
            switch ( $uris[0] )
            {
                case self::PAGE_TYPE_AJAX:
                    $pageType = $uris[0];
                    unset($uris[0]);

                    $actualRoute = '';
                    if ( !empty($uris[1]) ) {
                        $actualRoute = $uris[1];
                        unset($uris[1]);
                    }
                    break;
                default:
                    $actualRoute = !empty($uris[0]) ? $uris[0] : '';
                    unset($uris[0]);
            }

            $this->_setRoute($actualRoute, $pageType);

            // If page to render is null.
            if ( $this->_pageToRender == null ) throw new Error404;

            $pagePath = FLD_PAGES . DS . $pageType . DS . $this->_pageToRender[0] . '.php';

            if ( !file_exists($pagePath) ) throw new Error404;

            // Now its time to call the page.
            $params = array_values($uris);
            $params['pageName'] = $this->_pageToRender[0];
            $pageTitle = $this->_pageToRender[1];
            $staticsName = $this->_pageToRender[0];

            call_user_func(array(new Page($pagePath, $pageTitle, $pageType, $staticsName), 'load'), $params);


        } catch ( Error404 $ex )
        {

            header('HTTP/1.0 404 Not Found');
            @include FLD_PAGES . DS . '404.php';

        } catch ( RenderException $ex )
        {
            $this->_handleRenderException($ex);
        }

    }

    private function _splitRequest($uri )
    {
        $uri = preg_replace('/\?.*/i', '', $uri);
        return explode('/', trim(trim(preg_replace('/\/+/', '/', $uri), '/')));
    }

    /**
     * @param $route
     * @param $pageType
     * @throws Error404
     */
    private function _setRoute( $route, $pageType )
    {
        if ( !isset(self::$_routes[$pageType][$route]) && !empty($route) ) throw new Error404();
        if ( empty($route) ) $route = self::$_defaults['defaultRoute'][$pageType];
        $this->_pageToRender = self::$_routes[$pageType][$route];

    }

    /**
     * @used-by Requests::__construct()
     *
     * @param RenderException $ex
     */
    private function _handleRenderException( RenderException $ex )
    {
        $routeI = self::$_routes[$ex->getPageType()][$ex->getRoute()];
        $pagePath = FLD_PAGES . DS . $ex->getPageType() . DS . $routeI[0] . '.php';
        $params = array(
            'pageName' => $routeI[0],
            'others' => $ex->getExtraData()
        );
        call_user_func(array(new Page($pagePath, $routeI[1], $ex->getPageType(), $routeI[0]), 'load'), $params);
    }

    /**
     * @param string     $page
     * @param string    $route
     * @param string    $title
     */
    public static function addRoute( $page, $route, $title )
    {
        self::$_routes['normal'][$route] =  array( $page, $title );
    }

    /**
     * @param string $page
     * @param string $route
     */
    public static function addAjaxRoute( $page, $route )
    {
        self::$_routes['ajax'][$route] = array($page, null);
    }

}

