<?php

/**
 * Created by PhpStorm.
 * User: akash
 * Date: 13/4/19
 * Time: 2:49 AM
 */


if (!UserManager::getInstance()->isTeacher()) {
    $this->onlyLoggedInAllowed(UserManager::USER_TYPE_STUDENT);
}

$testId = filter_input(INPUT_POST, 'testId', FILTER_SANITIZE_NUMBER_INT);
$ip = filter_input(INPUT_POST, 'ipaddress');
$userBrowser = filter_input(INPUT_POST, 'userBrowser');

$testId = empty($testId) ? 0 : $testId;
$testInfo = TestManager::getInstance()->getTestInfo($testId);

if (empty($testInfo)) $this->json("error", "invalidTestId");

$questions = TestManager::getInstance()->getAllQuestions($testId);

$totalCorr = 0;
$totalWrong = 0;
$totalSkip = 0;
$total = 0;
$totalQuestions = count($questions);
$totalAttemptedQuestions = count($_POST) - 3;

$data = array(
    'testId' => $testId,
    'testName' => $testInfo['description'],
    'questions' => [],
);

foreach ($questions as $question) {
    $ans = isset($_POST['ans' . $question['id']]) ? $_POST['ans' . $question['id']] : 0;

    $data['questions'][$question['id']]['question'] = $question['question'];
    $data['questions'][$question['id']]['exp'] = $question['exp'];

    if ($ans == $question['correctAns']) {
        $totalCorr += $question['weight'];
        $data['questions'][$question['id']]['answered'] = true;
    } else {
        $totalWrong += $question['weight'];
        $data['questions'][$question['id']]['answered'] = false;
    }

    $totalSkip = $ans == 0 ? $totalSkip + 1 : $totalSkip;

    $total += $question['weight'];
}

// $percent = $totalCorr - floor($totalWrong / 3);


// $subject = "Physics";
// switch ($testInfo['subject']) {
//     case TestManager::SUBJECT_CHEM:
//         $subject = "Chemistry";
//         break;
//     case TestManager::SUBJECT_BIO:
//         $subject = "Biology";
//         break;
//     case TestManager::SUBJECT_PHY:
//         $subject = "Physics";
//         break;
//     default:
//         $subject = "Unknown";
// }

// $phone = $user->getPhone();

// $data['msg'] = "New Score: <strong>" . $percent . " Marks</strong> on <strong>{$testInfo['title']}</strong> ({$phone})";
// $data['score'] = $percent;

// var_dump($_SESSION);
// exit;



if (UserManager::getInstance()->isStudent()) {
    $student = UserManager::getInstance();
    $resultId = TestManager::getInstance()->createTestResult(
        $ip,
        $userBrowser,
        $student->getUserId(),
        $testInfo['id'],
        $_SESSION['roll_no'],
        $_SESSION['first_name'],
        $_SESSION['last_name'],
        $_SESSION['className'],
        $_SESSION['divName'],
        "{$totalCorr}/{$total}",
        $totalAttemptedQuestions,
        json_encode($data)
    );

    $this->json("success", $resultId);
} else {
    $_SESSION['result'] = array(
        'score' => "{$totalCorr}/{$total}",
        'totalAttempted' => $totalAttemptedQuestions,
        'data' => json_encode($data)
    );
    $this->json("success");
}
