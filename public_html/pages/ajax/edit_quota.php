<?php
$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );

$q = filter_input(INPUT_POST, 'quota-name');
$qId = filter_input(INPUT_POST, 'quota-id');

if ( empty($q) || empty($qId) )
    $this->json('error', 'Please fill the form properly.');

Quotas::getInstance()->updateQuota($qId, $q);
$this->json('success');