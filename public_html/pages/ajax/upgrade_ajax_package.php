<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 5/3/19
 * Time: 2:07 PM
 */

$this->onlyLoggedInAllowed( UserManager::USER_TYPE_SUBSCRIBER );

$page = $this;

$packageId = filter_input(INPUT_POST, 'packageId', FILTER_SANITIZE_NUMBER_INT);
$packageId = empty($packageId) ? 0 : $packageId;

/** @var User $user */
$user = $this->getUser();

$package = PackageManager::getInstance()->getAPackageById($packageId);
if ( empty($package) ) $page->json('error', "Package mismatch.");

$randNumber = Util::genRandomNumber(60);
$amount = $package['packageInfo']['price'] - $package['packageInfo']['offPrice'];
$pname = $package['packageInfo']['packageName'];
$fname = $user->getFirstName();
$email = $user->getEmail();
$phone = $user->getPhone();

// before doing everything save tran id.
if ( 0 === $id = TranManager::getInstance()
        ->createTran(
            $randNumber,
            TranManager::TRAN_TYPE_UPGRADE_PACKAGE,
            $amount,
            "Upgrading Package - " . $pname,
            array(
                'packageId' => $packageId
            )
        ) ) $page->json('error', "Can not create a valid transaction.");

$page->json('success', array(
    'hash' => $hash=hash('sha512', PAYU_MKEY.'|'.$id.'|'.$amount.'|'.$pname.'|'.$fname.'|'.$email.'|||||'.PAYU_UDF5.'||||||'.PAYU_SALT),
    'key' => PAYU_MKEY,
    'txnid' => $id,
    'amount' => $amount,
    'firstname' => $fname,
    'email' => $email,
    'phone' => $phone,
    'productinfo' => $pname,
    'udf5' => PAYU_UDF5,
    'surl' => SITE_URL . DS . 'upgrade-package?packageId=' . $packageId . '&tranId=' . $id . '&hash=' . $hash . '&tranHash=' . $randNumber,
    'furl' => SITE_URL . DS . 'upgrade-package?packageId=' . $packageId . '&tranFail=1',
    'tranHash' => $randNumber
));
