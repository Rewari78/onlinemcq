<?php
$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );

$sType = filter_input(INPUT_POST, 'study-type');
$sTypeId = filter_input(INPUT_POST, 'study-type-id');
$collegeTypeId = filter_input(INPUT_POST, 'college-type-id');

if ( empty($sType) || empty($sTypeId) || empty($collegeTypeId) )
    $this->json('error', 'Please fill the form properly.');

// Now validate the college type id.
if ( empty(CollegeManager::getInstance()->getCollegeTypeById($collegeTypeId)) )
    $this->json('error', 'College type is not valid.');

CollegeManager::getInstance()
    ->updateStudyType($sTypeId, $collegeTypeId, $sType);

$this->json('success' );