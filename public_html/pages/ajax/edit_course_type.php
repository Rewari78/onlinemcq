<?php
$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );

$cType = filter_input(INPUT_POST, 'course-type');
$cTypeId = filter_input(INPUT_POST, 'course-type-id');
$sTypeId = filter_input(INPUT_POST, 'study-type-id');


if ( empty($cType) || empty($cTypeId) || empty($sTypeId) )
    $this->json('error', 'Please fill the form properly.');

CollegeManager::getInstance()->updateCourseType($cTypeId, $sTypeId, $cType);

$this->json('success');
