<?php
$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );

$disPrice = filter_input(INPUT_POST, 'discount-price', FILTER_SANITIZE_NUMBER_INT);
$packagePrice = filter_input(INPUT_POST, 'package-price', FILTER_SANITIZE_NUMBER_INT);
$packageName = filter_input(INPUT_POST, 'package-name');

if ( empty($packageName) )
{
    $this->json('error', "Please fill the form correctly.");
}

if ( empty($packagePrice) ) $packagePrice = 0;
if ( empty($disPrice) ) $disPrice = 0;

if ( !is_numeric($disPrice) ) $this->json("error", "Discount price should be a valid price number.");

if ( !PackageManager::getInstance()
    ->addAPackage($packageName, $packagePrice, $disPrice) )
    $this->json("error", "internalError");

$this->json('success');
