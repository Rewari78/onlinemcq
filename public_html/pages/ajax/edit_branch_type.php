<?php
$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );

$bType = filter_input(INPUT_POST, 'branch-type');
$bTypeId = filter_input(INPUT_POST, 'branch-type-id');

if ( empty($bType) || empty($bTypeId) )
    $this->json('error', 'Please fill the form properly.');

CollegeManager::getInstance()->updateBranchType($bTypeId, $bType);

$this->json('success');
