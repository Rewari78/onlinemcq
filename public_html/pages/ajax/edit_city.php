<?php
$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );

$city       = filter_input(INPUT_POST, 'city');
$cityId     = filter_input(INPUT_POST, 'city-id');

if ( empty($city) )
    $this->json('error', 'Please fill the form properly.');

// get the city info.
$cityInfo = StateManager::getInstance()->getCityById($cityId);

if ( empty($cityInfo) ) $this->json('error', "Invalid data.");

StateManager::getInstance()
    ->updateACity($cityInfo['id'], $cityInfo['stateId'],
        $city);

$this->json('success' );