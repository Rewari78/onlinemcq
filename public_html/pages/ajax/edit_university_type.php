<?php
$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );

$uTId = filter_input(INPUT_POST, 'university-type-id', FILTER_SANITIZE_NUMBER_INT);
$uT = filter_input(INPUT_POST, 'university-type');

if ( empty($uT) || empty($uTId) ) $this->json('error', "Please provide a valid login id.");

CollegeManager::getInstance()->updateUniversityType($uTId, $uT);

$this->json('success');