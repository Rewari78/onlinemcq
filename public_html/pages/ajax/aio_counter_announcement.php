<?php
$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );

function _getStudyType( $page )
{
    $colTypeId = filter_input(INPUT_POST, 'colTypeId');
    $colTypeId = empty($colTypeId) ? 0 : $colTypeId;

    $colType = CollegeManager::getInstance()->getCollegeTypeById($colTypeId);
    if ( empty($colType) ) $page->json('error', "College type is not valid.");

    // Now get the study type.
    $studyType = CollegeManager::getInstance()->getAvlStudyType();

    $page->json('success', $studyType);
}

function _getCourseType( $page )
{
    $studyTypeId = filter_input(INPUT_POST, 'studyTypeId');
    $studyTypeId = empty($studyTypeId) ? 0 : $studyTypeId;

    $studyType = CollegeManager::getInstance()->getStudyTypeById($studyTypeId);
    if ( empty($studyType) ) $page->json('error', "Study type is not valid.");

    // Get the course type now.
    $courseType = CollegeManager::getInstance()->getAvlCourseType($studyTypeId);

    $page->json('success', $courseType);
}

function _getBranchType( $page )
{
    $courseTypeId = filter_input(INPUT_POST, 'courseTypeId');
    $courseTypeId = empty($courseTypeId) ? 0 : $courseTypeId;

    $courseType = CollegeManager::getInstance()->getCourseTypeById( $courseTypeId );
    if ( empty($courseType) ) $page->json('error', "Course type is not valid.");

    $quotas = CollegeManager::getInstance()->getAvlBranchType($courseTypeId);
    $page->json('success', $quotas);
}

// Now validate the request.
$rq = filter_input(INPUT_POST, 'rq');
$rq = !empty($rq) ? $rq : null;

if ( empty($rq) || !function_exists('_' . $rq)) $this->json('error', 'invalidRequest');

call_user_func('_' . $rq, $this);