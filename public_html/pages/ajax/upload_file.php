<?php

/**
 * Created by PhpStorm.
 * User: akash
 * Date: 7/5/19
 * Time: 1:02 PM
 */

$this->onlyLoggedInAllowed(UserManager::USER_TYPE_TEACHER);

if (!isset($_FILES['upload'])) return;

$file = $_FILES['upload'];

if (file_exists($file['tmp_name']) && ($file['type'] == 'image/png' || $file['type'] == 'image/jpg' || $file['type'] == 'image/jpeg' || $file['type'] == 'image/gif')) {
    $name = time() . '.jpg';
    $uploadPath = FLD_IMAGES . DS  . $name;
    if (move_uploaded_file($file['tmp_name'], $uploadPath)) {
        header("content-type: application/json");
        echo json_encode(
            array(
                "uploaded" => 1,
                "filename" => $name,
                "url" => "/images/" . $name
            )
        );
    } else {
        header("content-type: application/json");
        echo json_encode(
            array(
                "uploaded" => 0,
                "error" => array(
                    "message" => "Internal server error."
                )
            )
        );
    }
} else {
    header("content-type: application/json");
    echo json_encode(
        array(
            "uploaded" => 0,
            "error" => array(
                "message" => "Please upload only image files."
            )
        )
    );
}
