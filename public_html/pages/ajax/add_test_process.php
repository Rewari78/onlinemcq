<?php

function isPostValid($key)
{
    return isset($_POST[$key]) && !empty($_POST[$key]);
}

// var_dump($_POST);
// exit;

if (isPostValid('test-subject') && isPostValid('test-class') && isPostValid('test-divs') && isPostValid('test-desc') && isPostValid('test-time')) {

    $subject = filter_input(INPUT_POST, 'test-subject', FILTER_SANITIZE_STRING);
    $class = filter_input(INPUT_POST, 'test-class', FILTER_SANITIZE_STRING);
    $divisions = '[' . implode(',', $_POST['test-divs']) . ']';
    $description = filter_input(INPUT_POST, 'test-desc', FILTER_SANITIZE_STRING);
    $start = filter_input(INPUT_POST, 'test-start', FILTER_SANITIZE_STRING);
    $time = filter_input(INPUT_POST, 'test-time', FILTER_SANITIZE_NUMBER_INT);
    $start = filter_input(INPUT_POST, 'test-start', FILTER_SANITIZE_STRING);

    $testId = TestManager::getInstance()->createTest($_SESSION['schoolId'], $_SESSION['username'], $subject, $class, $divisions, $description, $start, $_SESSION['name'], $time);
    echo json_encode(array('success' => true, 'testId' => $testId));
} else {
    echo json_encode(array('success' => false));
}
