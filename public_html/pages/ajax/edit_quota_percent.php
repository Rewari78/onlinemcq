<?php

$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );

$id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
$percent = filter_input(INPUT_POST, 'quota-percentage', FILTER_SANITIZE_NUMBER_INT);

if ( empty($id) || empty($percent) ) $this->json('error', "Please input valid form data.");
if ( $percent < 0 || $percent > 100 ) $this->json('error', "Enter a valid percentage.");

Quotas::getInstance()->updateQuotaPercentage($id, abs($percent));

$this->json('success');