<?php
if ( UserManager::getInstance()->isLoggedIn() ) $this->json('error', "userLoggedIn");

$validate = trim(filter_input(INPUT_POST,'validate'));

if ( $validate === '1' )
{
    _validate($this);

} else {
    _login($this);
}

function _validate( $page )
{
    $mobile = trim(filter_input(INPUT_POST, 'mobile'));
    $pass = trim(filter_input(INPUT_POST, 'password'));

    if ( empty($mobile) || empty($pass) ) $page->json('invalidParameters');

    // Now check if the email is available or not.
    $userInfo = UserDatabase::getInstance()->getUserInfoByPhone($mobile);

    if ( empty($userInfo) ) $page->json('error', "Mobile/password is wrong.");

    if ( !Validator::isPasswordValid($pass, $userInfo['password']) )
        $page->json('error', "Mobile/password is wrong.");

    if ( $userInfo['isActive'] != 1 ) $page->json('error', "Your account is not active anymore contact site admin to restore your account.");

    $seq = OtpManager::getInstance()
        ->createAnOtpObject(OtpManager::OBJECT_TYPE_LOGIN, array(
        'userId' => $userInfo['id'],
        'type' => $userInfo['userType']
    ));

    $otpObject = OtpManager::getInstance()->getAnOtpObjectById($seq);

    if ( !empty($otpObject) ) {
        $vars = array(
            'loginID' => 'neetguidance',
            'password' => '12345',
            'mobile' => $mobile,
            'text' => "OTP - {$otpObject['otp']}\r\n Login for Neet Guidance",
            'senderid' => 'NHIOTP',
            'route_id' => 3
        );

        $url = 'http://184.95.37.226/API/pushsms.aspx?' . http_build_query($vars);

    }

    // all fields are valid.
    $page->json('success', array(
        'seq' => $seq,
		'otp' => $otpObject['otp']
    ));
}

function _login( $page )
{
    $seq = trim(filter_input(INPUT_POST,'seq'));
    $otp = trim(filter_input(INPUT_POST,'otp'));

    if ( empty($seq) || empty($otp) ) $page->json("invalidParameters");

    $otpObj = OtpManager::getInstance()->getAnOtpObjectById($seq);
    if ( empty($otpObj) || $otpObj['otp'] != $otp ||
        $otpObj['objectType'] != OtpManager::OBJECT_TYPE_LOGIN ) $page->json('error', "OTP is not valid.");

    UserManager::getInstance()
        ->loginUser(
            $otpObj['object']['userId'],
            $otpObj['object']['type']
        );

    // all fields are valid.
    $page->json('success', array(
        'redirectUrl' => SITE_URL . DS . 'home'
    ));
}