<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 3/24/19
 * Time: 11:03 AM
 */

$this->onlyLoggedInAllowed( UserManager::USER_TYPE_SUBSCRIBER, PackageManager::FEATURE_EMAIL_NOTI );

$neetUpdate = filter_input(INPUT_POST, 'neetSms');
$promoStatus = filter_input(INPUT_POST, 'promoSms');
$neetUpdate = $neetUpdate != 1 ? 0 : 1;
$promoStatus = $promoStatus != 1 ? 0 : 1;

/** @var User $user */
$user = $this->getUser();

if ( EmailSmsManager::getInstance()->changeEmailStatus($user->getId(), array(
    'neet_update' => $neetUpdate,
    'promo_update' => $promoStatus
)) ) {
    $this->json('success');
}