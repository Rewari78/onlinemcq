<?php
$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );

$stateId = filter_input(INPUT_POST, 'stateId', FILTER_SANITIZE_NUMBER_INT);

if ( empty($stateId) ||
    empty(StateManager::getInstance()->getStateById($stateId)) )
    $this->json('error', "invalidStateId");

// Now its time to get the cities.
$cities         = StateManager::getInstance()->getCityListByStateId($stateId);
$universities   = CollegeManager::getInstance()->getUniversitiesByStateId($stateId);

$collegeTypes   = CollegeManager::getInstance()->getAvlCollegeTypes();
$recognition    = CollegeManager::getInstance()->getAllRecognitions();

$this->json('success', array(
    'cities' => $cities,
    'universities' => $universities,
    'collegeType' => $collegeTypes,
    'recognition' => $recognition
));