<?php
$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );

$cType = filter_input(INPUT_POST, 'college-type');
$cTypeId = filter_input(INPUT_POST, 'college-type-id');

if ( empty($cType) || empty($cTypeId) )
    $this->json('error', 'Please fill the form properly.');

CollegeManager::getInstance()->updateCollegeType($cTypeId, $cType);

$this->json('success' );
