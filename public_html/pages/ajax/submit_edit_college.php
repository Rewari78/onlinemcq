<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 2/11/18
 * Time: 2:16 PM
 */
$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );

$collegeId = filter_input(INPUT_POST, 'collegeId', FILTER_SANITIZE_NUMBER_INT);
$stateId = filter_input(INPUT_POST, 'select-state', FILTER_SANITIZE_NUMBER_INT);
$colName = filter_input(INPUT_POST, 'college_name');
$established = filter_input(INPUT_POST, 'established');
$selectAffTo = filter_input(INPUT_POST, 'select-affiliated-to', FILTER_SANITIZE_NUMBER_INT);
$colType = filter_input(INPUT_POST, 'select-college-type', FILTER_SANITIZE_NUMBER_INT);
$recognition = filter_input(INPUT_POST, 'select-recognition', FILTER_SANITIZE_NUMBER_INT);
$govFee = filter_input(INPUT_POST, 'government_fee');
$nriFee = filter_input(INPUT_POST, 'NRI_fee');
$privateFee = filter_input(INPUT_POST, 'private_fee');
$otherFee = filter_input(INPUT_POST, 'other_fee');

$cutGen = filter_input(INPUT_POST, 'cut_gen');
$cutGenPh = filter_input(INPUT_POST, 'cut_gen_ph');
$cutObc = filter_input(INPUT_POST, 'cut_obc');
$cutObcPh = filter_input(INPUT_POST, 'cut_obc_ph');
$cutSc = filter_input(INPUT_POST, 'cut_sc');
$cutScPh = filter_input(INPUT_POST, 'cut_sc_ph');
$cutSt = filter_input(INPUT_POST, 'cut_st');
$cutStPh = filter_input(INPUT_POST, 'cut_st_ph');

$selectCity = filter_input(INPUT_POST, 'select-city', FILTER_SANITIZE_NUMBER_INT);
$websiteUrl = filter_input(INPUT_POST, 'website_url');
$totalSeats = filter_input(INPUT_POST, 'total-seats', FILTER_SANITIZE_NUMBER_INT);

$govFee = !empty($govFee) ? $govFee : 0;
$nriFee = !empty($nriFee) ? $nriFee : 0;
$privateFee = !empty($privateFee) ? $privateFee : 0;
$otherFee = !empty($otherFee) ? $otherFee : 0;
$cutGen = !empty($cutGen) ? $cutGen : 0;
$cutGenPh = !empty($cutGenPh) ? $cutGenPh : 0;
$cutObc = !empty($cutObc) ? $cutObc : 0;
$cutObcPh = !empty($cutObcPh) ? $cutObcPh : 0;
$cutSc = !empty($cutSc) ? $cutSc : 0;
$cutScPh = !empty($cutScPh) ? $cutScPh : 0;
$cutSt = !empty($cutSt) ? $cutSt : 0;
$cutStPh = !empty($cutStPh) ? $cutStPh : 0;
$totalSeats = !empty($totalSeats) ? $totalSeats : 0;

if (
    empty($collegeId) ||
    empty( CollegeManager::getInstance()->getCollegeById($collegeId) )
) $this->json('error', "Oops! college not found, please reopen this page.");

if ( empty($stateId) ||
    empty($colName) ||
    empty($selectAffTo) ||
    empty($colType) ||
    empty($recognition) ||

    empty($selectCity)


) $this->json('error', "Fill all the fields.");

// now validate if the state is valid.
if ( empty(StateManager::getInstance()->getStateById($stateId)) ) $this->json('error', "Please select valid state");
if ( empty(CollegeManager::getInstance()->getUniversityById($selectAffTo)) ) $this->json('error', 'Please select a valid university.');
if ( empty(CollegeManager::getInstance()->getCollegeTypeById($colType)) ) $this->json('error', "Please select a valid type");
if ( empty(CollegeManager::getInstance()->getRecognitionsByIdList($recognition)) ) $this->json('error', "Please select a valid recognition.");
if ( !is_numeric($established) ) $this->json('error', "Please type a valid established year for  the college.");
if ( !is_numeric($govFee) ) $this->json('error', "Please type a valid govt fee.");
if ( !is_numeric($nriFee) ) $this->json('error', "Please type a valid nri fee.");
if ( !is_numeric($privateFee) ) $this->json('error', "Please type a valid private fee.");
if ( !is_numeric($otherFee) ) $this->json('error', "Please type a valid private fee.");

if ( !is_numeric($cutGen)   )     $this->json('error', 'Please type a valid last year cut off');
if ( !is_numeric($cutGenPh) )   $this->json('error', 'Please type a valid last year cut off');
if ( !is_numeric($cutObc)   )     $this->json('error', 'Please type a valid last year cut off');
if ( !is_numeric($cutObcPh) )   $this->json('error', 'Please type a valid last year cut off');
if ( !is_numeric($cutSc)    )      $this->json('error', 'Please type a valid last year cut off');
if ( !is_numeric($cutScPh)  )    $this->json('error', 'Please type a valid last year cut off');
if ( !is_numeric($cutSt)    )      $this->json('error', 'Please type a valid last year cut off');
if ( !is_numeric($cutStPh)  )    $this->json('error', 'Please type a valid last year cut off');

if ( empty(StateManager::getInstance()->getCityById($selectCity)) ) $this->json('error', "Please select a valid city.");
if ( !is_scalar($totalSeats) ) $this->json('error', 'Please put valid total seats.');

CollegeManager::getInstance()->updateCollege(
    $collegeId,
    $colName, $established, $stateId, $selectAffTo, $colType,
    $recognition, $govFee, $nriFee, $privateFee, $otherFee,
    $cutGen, $cutGenPh, $cutObc, $cutObcPh, $cutSc, $cutScPh,
    $cutSt, $cutStPh, $selectCity, $websiteUrl, $totalSeats
);

$this->json('success');