<?php

function isPostValid($key)
{
    return isset($_POST[$key]) && !empty($_POST[$key]);
}

$error = [];

if ( isPostValid('testId') ) {
	$testId = filter_input(INPUT_POST, 'testId', FILTER_SANITIZE_NUMBER_INT);
} else {
	$error['testId'] = 'Invalid Test ID.';
}

if ( isPostValid('test-subject') ) {
	$subject = filter_input(INPUT_POST, 'test-subject', FILTER_SANITIZE_STRING);
} else {
	$error['test-subject'] = 'Test subject cannot be empty.';
}

if ( isPostValid('test-class') ) {
	$class = filter_input(INPUT_POST, 'test-class', FILTER_SANITIZE_STRING);
} else {
	$error['test-class'] = 'Please select proper Class';
}

if ( isPostValid('test-divs') ) {
	$divisions = '[' . implode(',', $_POST['test-divs']) . ']';
} else {
	$error['test-divs'] = 'Please select proper Division';
}

if ( isPostValid('test-desc') ) {
	$description = filter_input(INPUT_POST, 'test-desc', FILTER_SANITIZE_STRING);
} else {
	$error['test-desc'] = 'Test Description cannot be empty.';
}

if ( isPostValid('test-start') ) {
	$start = filter_input(INPUT_POST, 'test-start', FILTER_SANITIZE_STRING);
	if ( strtotime( $start ) <= time() ) {
		$error['test-start'] = 'Date and Time to conduct test is already passed. Edit and pick a future date and time to publish test paper.';
	}
} else {
	$error['test-start'] = 'Date and Time cannot be empty';
}

if ( isPostValid('test-time') ) {
	$time = filter_input(INPUT_POST, 'test-time', FILTER_SANITIZE_NUMBER_INT);
} else {
	$error['test-time'] = 'Invalid test time duration.';
}

if ( empty( $error ) ) {
	$testId = TestManager::getInstance()->updateTest($testId, $subject, $class, $divisions, $description, $start, $time, 0);
	echo json_encode(array('success' => true, 'testId' => $testId));
} else {
	echo json_encode(array('success' => false, 'error' => $error));
}
