<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 3/11/18
 * Time: 6:30 PM
 */

$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );

$stateId        = filter_input(INPUT_POST, 'select-state', FILTER_SANITIZE_NUMBER_INT);
$collegeTypeId  = filter_input(INPUT_POST, 'select-college-type', FILTER_SANITIZE_NUMBER_INT);
$studyTypeId    = filter_input(INPUT_POST, 'select-study-type', FILTER_SANITIZE_NUMBER_INT);
$courseId       = filter_input(INPUT_POST, 'select-course-type', FILTER_SANITIZE_NUMBER_INT);
$branchId       = filter_input(INPUT_POST, 'select-branch-type', FILTER_SANITIZE_NUMBER_INT);
$title          = trim(filter_input(INPUT_POST, 'notifi-title'));
$startTime      = trim(filter_input(INPUT_POST, 'custom-date-time-pick'));
$endTime        = trim(filter_input(INPUT_POST, 'custom-date-time-pick1'));
$textArea       = trim(filter_input(INPUT_POST, 'ckeditor1'));
$link           = trim(filter_input(INPUT_POST, 'link'));
$customText     = trim(filter_input(INPUT_POST, 'custom-text'));
$smgSenderId    = filter_input(INPUT_POST, 'sms-sender-id', FILTER_SANITIZE_NUMBER_INT);
$smsText        = trim(filter_input(INPUT_POST, 'sms-text'));
$emailSub       = trim(filter_input(INPUT_POST, 'email-subject'));
$emailFrom      = trim(filter_input(INPUT_POST, 'email-from'));
$emailText      = trim(filter_input(INPUT_POST, 'email-text'));

// Now we have to validate the fields.
if (
    empty($stateId) ||
    empty($collegeTypeId) ||
    empty($studyTypeId) ||
    empty($courseId) ||
    empty($branchId) ||

    empty($title) ||
    empty($startTime) ||
    empty($endTime) ||
    empty($textArea) ||
    empty($link) ||
    empty($customText)
//
//    empty($smgSenderId) ||
//    empty($smsText) ||
//    empty($emailSub) ||
//    empty($emailFrom) ||
//    empty($emailText)
) $this->json('error', "Please fill all the fields");


if ( empty(StateManager::getInstance()->getStateById($stateId)) ) $this->json('error', "Please select a valid state");
if ( empty(CollegeManager::getInstance()->getCollegeTypeById($collegeTypeId)) ) $this->json('error', "Please select a valid college type.");
if ( empty(CollegeManager::getInstance()->getStudyTypeById($studyTypeId)) ) $this->json('error', "Please select a valid study type");
if ( empty(CollegeManager::getInstance()->getCourseTypeById($courseId)) ) $this->json('error', "Please select a valid course type.");
if ( empty(CollegeManager::getInstance()->getBranchTypeById($branchId)) ) $this->json('error', "Please select a valid branch type.");

// Now validate the timestamp
$startStamp = strtotime($startTime);
$endStamp   = strtotime($endTime);

if ( empty($startTime) || empty($endTime) ) $this->json('error', "Start and end time is not valid.");

// Now it is time to update the data.
Announcements::getInstance()->addStateCounterAnnouncement(
    $stateId, $collegeTypeId, $studyTypeId, $courseId, $branchId,
    $title, $startStamp, $endStamp, $textArea, $link, $customText
);

// Now check of email sending.

$_SESSION['state_counter_announcement_msg'] = "New State counter announcement is created";

$this->json('success');