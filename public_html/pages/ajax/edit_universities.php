<?php
$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );

$id = filter_input(INPUT_POST, 'university-id');
$stateId = filter_input(INPUT_POST, 'select-state');
$universityTypeId = filter_input(INPUT_POST, 'university-type');
$name = filter_input(INPUT_POST, 'university-name');

if ( empty($id) || empty($stateId) ||
    empty($universityTypeId) || empty($name) )
    $this->json('error', 'Please, fill the form properly.');

if ( empty(StateManager::getInstance()->getStateById($stateId)) ||
    empty(CollegeManager::getInstance()->getUniversityTypeById($universityTypeId))

) $this->json('error', "Please enter valid data");

CollegeManager::getInstance()->updateUniversity(
    $id, $stateId,
    $universityTypeId,
    $name
);

$this->json('success');