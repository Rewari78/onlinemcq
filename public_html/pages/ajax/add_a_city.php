<?php

$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );

$stateId = filter_input(INPUT_POST, 'stateId', FILTER_SANITIZE_NUMBER_INT);
$cName = trim(filter_input(INPUT_POST, 'city'));
$cPin = filter_input(INPUT_POST, 'city-pincode', FILTER_SANITIZE_NUMBER_INT);

if ( empty($stateId) || empty($cName) || empty($cPin) )
{
    $this->json('error', "Please fill the form correctly.");
}

// Now check if the state is a valid state.
$state = StateManager::getInstance()->getStateById($stateId);

if ( empty($state) ) $this->json('error', "State id is not valid.");

// Now it is F**king time to update the city.
if ( !StateManager::getInstance()
    ->addACity($stateId, $cName, $cPin) )
{
    $this->json('error', "Internal error while trying to create.");
}

$this->json('success');