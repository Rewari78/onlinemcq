<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 16/10/18
 * Time: 8:43 AM
 */

$dS = filter_input(INPUT_POST, 'dS', FILTER_SANITIZE_NUMBER_INT);
$s12 = filter_input(INPUT_POST, 's12', FILTER_SANITIZE_NUMBER_INT);
$pcb = filter_input(INPUT_POST, 'pcb', FILTER_SANITIZE_NUMBER_INT);
$course = filter_input(INPUT_POST, 'course', FILTER_SANITIZE_NUMBER_INT);
$dob = filter_input(INPUT_POST, 'dob');


if ( empty($dS) || empty($s12) || empty($course) || empty($dob) )
    $this->json('invalidParameters', "Please fill all the fields.");

// Now state is selected validate the state if the is actually a state.
$state = StateManager::getInstance()->getStateById($dS);
if ( empty($state) ) $this->json('invalidState', 'Please select a valid state.');

switch ( $s12 )
{
    case UserManager::STUDY_12_PASSOUT:
        if (  empty($pcb) || $pcb > 100 ) {
            $this->json('invalidPCB', "Your pcb percent is not valid. Eg. 70%.");
        }
        break;
    case UserManager::STUDY_12_APPEARED:
        $pcb = 0;
        break;
    default:
        $this->json('invalidStudy', "Please select you are appeared/passout.");
}

// Now validate the course.
if ( $course != 1 && $course != 2 ) $this->json('invalidCourse', "Please select your course properly.");

$userId = UserManager::getInstance()->getUserId();

// Now all fields are validated
// It is time to save.
if ( !UserDatabase::getInstance()
    ->saveExtraInfo( $userId, $dS, $s12,
        $pcb, $course, $dob ) ) $this->json('internalServerError', "Internal Server Error.");

$this->json('success', array(
    'redirectUrl' => SITE_URL . DS . 'home'
));

