<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 12/3/19
 * Time: 3:19 PM
 */
$seq = filter_input(INPUT_POST, 'seq', FILTER_SANITIZE_NUMBER_INT);
if ( empty($seq) ) $seq = 0;

$otpObject = OtpManager::getInstance()->getAnOtpObjectById($seq);

if ( !empty($otpObject) ) {

    $userInfo = UserDatabase::getInstance()->getUserInfoByIdList($otpObject['object']['userId']);

    if ( empty($userInfo) ) exit;

    $userInfo = $userInfo[$otpObject['object']['userId']];

    $vars = array(
        'loginID' => 'neetguidance',
        'password' => '12345',
        'mobile' => $userInfo['phone'],
        'text' => "OTP - {$otpObject['otp']}\r\n Login for Neet Guidance",
        'senderid' => 'NHIOTP',
        'route_id' => 3
    );

    $url = 'http://184.95.37.226/API/pushsms.aspx?' . http_build_query($vars);
    @file_get_contents($url);

}