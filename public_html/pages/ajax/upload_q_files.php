<?php

/**
 * Created by PhpStorm.
 * User: akash
 * Date: 7/5/19
 * Time: 1:02 PM
 */

$this->onlyLoggedInAllowed(UserManager::USER_TYPE_TEACHER);

if (!isset($_FILES['upload'])) return;

$file = $_FILES['upload'];

if ( $file['size'] > 5242880 ) {
    header("content-type: application/json");
    echo json_encode(
        array(
            "uploaded" => 2,
            "error" => array(
                "message" => "File upload size maxed."
            )
        )
    );
    exit;
}

$fileTypes = array(
    'jpg',
    'jpeg',
    'gif',
    'png',
    'webp',
    'pdf'
);

$ext = explode('.', $file['name']);
$ext = count($ext) > 1 ? strtolower($ext[count($ext) - 1]) : 'unknown';

if (file_exists($file['tmp_name']) && in_array($ext, $fileTypes) )
{
    $name = time() . '.' . $ext;
    $uploadPath = FLD_IMAGES . DS  . $name;
    if (move_uploaded_file($file['tmp_name'], $uploadPath)) {
        header("content-type: application/json");
        echo json_encode(
            array(
                "uploaded" => 1,
                'name' => $file['name'],
                "filename" => $name,
                'type' => $ext,
                "url" => "/images/" . $name
            )
        );
    } else {
        header("content-type: application/json");
        echo json_encode(
            array(
                "uploaded" => 0,
                "error" => array(
                    "message" => "Internal server error."
                )
            )
        );
    }
} else {
    header("content-type: application/json");
    echo json_encode(
        array(
            "uploaded" => 0,
            "error" => array(
                "message" => "Please upload only image files and pdf files."
            )
        )
    );
}
