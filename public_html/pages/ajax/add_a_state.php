<?php
$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );

$sName = filter_input(INPUT_POST, 'name');
$ssName = filter_input(INPUT_POST, 'state-short-name');

if ( empty($sName) || empty($ssName)  )
{
    $this->json('error', "Please fill the form correctly.");
}

if ( !StateManager::getInstance()->addState($sName, $ssName) )
    $this->json("error", "internalError");

$this->json('success');
