<?php
if ( UserManager::getInstance()->isLoggedIn() ) $this->json('error', "userLoggedIn");

$validate = trim(filter_input(INPUT_POST,'validate'));

if ( $validate === '2' )
{
    _validate($this);

} else if ( $validate === '1' )
{

    _validateOtp($this);

} else
{
    _register($this);
}

function _validate( $page )
{
    $firstName = trim(filter_input(INPUT_POST, 'firstName'));
    $lastName = trim(filter_input(INPUT_POST, 'lastName'));
    $email = trim(filter_input(INPUT_POST, 'email'));
    $pass1 = trim(filter_input(INPUT_POST, 'password1'));
    $pass2 = trim(filter_input(INPUT_POST, 'password2'));
    $mobile = trim(filter_input(INPUT_POST, 'mobile'));
    $packageId = trim(filter_input(INPUT_POST, 'packageId'));

    if ( empty($firstName) || empty($lastName) || empty($email)
        || empty($pass1) || empty($pass2) ||
        empty($mobile) || empty($packageId)
    ) $page->json('invalidParameters');

    // First check if the package is valid.
    if ( empty(PackageManager::getInstance()->getAPackageById($packageId)) )
        $page->json('error', "Invalid package selected, please select a proper package.");

    if ( !Validator::isValidEmail($email) )
        $page->json('error', "You should give a valid email. Eg. <strong>you@example.com</strong>");

    // Now check if the email is available or not.
    $userInfo = UserDatabase::getInstance()->getUserInfoByEmail($email);

    if ( !empty($userInfo) ) $page->json('error', "Email address is already taken.");

    // Now check password.
    if ( $pass1 !== $pass2 ) $page->json('error', "Two password fields does not match.");

    if ( !Validator::isValidPhone($mobile) )
        $page->json('error', "Mobile number is not valid. Eg. 9807654321");

    $userInfo = UserDatabase::getInstance()->getUserInfoByPhone($mobile);

    if ( !empty($userInfo) ) $page->json('error', "Provided phone number is already used.");

    // Now it is time to generate OTP.
    $seq = OtpManager::getInstance()
        ->createAnOtpObject(OtpManager::OBJECT_TYPE_REGISTER, array(
            'firstName' => $firstName,
            'lastName' => $lastName,
            'email' => $email,
            'password1' => $pass1,
            'password2' => $pass2,
            'mobile' => $mobile,
            'packageId' => $packageId
        ));

    $otpObject = OtpManager::getInstance()->getAnOtpObjectById($seq);


    if ( !empty($otpObject) ) {
        $vars = array(
            'loginID' => 'neetguidance',
            'password' => '12345',
            'mobile' => $mobile,
            'text' => "OTP - {$otpObject['otp']}\r\n Congratulation On register for Neet Guidance",
            'senderid' => 'NHIOTP',
            'route_id' => 3
        );

        //file_get_contents('http://184.95.37.226/API/pushsms.aspx?' . http_build_query($vars));
    }


    // all fields are valid.
    $page->json('success', array(
        'seq' => $seq,
		'otp' => $otpObject['otp']
    ));
}

function _validateOtp( $page )
{
    $seq = trim(filter_input(INPUT_POST,'seq'));
    $otp = trim(filter_input(INPUT_POST,'otp'));

    if ( empty($seq) || empty($otp) ) $page->json("invalidParameters");

    $otpObj = OtpManager::getInstance()->getAnOtpObjectById($seq);
    if ( empty($otpObj) || $otpObj['otp'] != $otp ||
        $otpObj['objectType'] != OtpManager::OBJECT_TYPE_REGISTER ) $page->json('error', "OTP is not valid.");

    // else otp is valid and ask for transaction id.
    $packageId = $otpObj['object']['packageId'];
    $package = PackageManager::getInstance()->getAPackageById($packageId);
    if ( empty($package) ) $page->json('error', "Package mismatch.");

    $randNumber = Util::genRandomNumber(60);
    $amount = $package['packageInfo']['price'] - $package['packageInfo']['offPrice'];
    $pname = $package['packageInfo']['packageName'];
    $fname = $otpObj['object']['firstName'];
    $email = $otpObj['object']['email'];
    $phone = $otpObj['object']['mobile'];

    // before doing everything save tran id.
    if ( 0 === $id = TranManager::getInstance()
        ->createTran(
            $randNumber,
            TranManager::TRAN_TYPE_REGISTER,
            $amount,
            "Registering Package - " . $pname,
            array(
                'firstName' => $fname,
                'lastName' => $otpObj['object']['lastName'],
                'email' => $email,
                'password1' => $otpObj['object']['password1'],
                'password2' => $otpObj['object']['password2'],
                'mobile' => $phone,
                'packageId' => $packageId
            )
        ) ) $page->json('error', "Can not create a valid transaction.");

    $page->json('success', array(
        'hash' => $hash=hash('sha512', PAYU_MKEY.'|'.$id.'|'.$amount.'|'.$pname.'|'.$fname.'|'.$email.'|||||'.PAYU_UDF5.'||||||'.PAYU_SALT),
        'key' => PAYU_MKEY,
        'txnid' => $id,
        'amount' => $amount,
        'firstname' => $fname,
        'email' => $email,
        'phone' => $phone,
        'productinfo' => $pname,
        'udf5' => PAYU_UDF5,
        'surl' => SITE_URL . DS . 'register?packageId=' . $packageId . '&tranId=' . $id . '&hash=' . $hash . '&tranHash=' . $randNumber,
        'furl' => SITE_URL . DS . 'register?packageId=' . $packageId . '&tranFail=1',
        'tranHash' => $randNumber
    ));

}

function _register( $page )
{
    $tranId = trim(filter_input(INPUT_POST,'tranId'));

    if ( empty($tranId) ) $page->json("invalidParameters");

    $tranObj = TranManager::getInstance()->getAnTranById($tranId);
    if ( empty($tranObj) ||
        $tranObj['type'] != TranManager::TRAN_TYPE_REGISTER ) $page->json('error', "OTP is not valid.");

    // ELse otp is valid.
    if ( !UserDatabase::getInstance()
        ->createUser($tranObj['object']['firstName'],
            $tranObj['object']['lastName'], $tranObj['object']['email'],
            $tranObj['object']['mobile'], Util::passwordEncrypt($tranObj['object']['password1']),
            UserManager::USER_TYPE_SUBSCRIBER) )
    {
        $page->json("internalErrorOccurred");
    }

    // else login
    // get userInfo by email
    $userInfo = UserDatabase::getInstance()->getUserInfoByEmail($tranObj['object']['email']);

    UserPackageController::getInstance()
        ->givePackageToAUser(
            $userInfo['id'],UserPackageController::DEFAULT_PACKAGE_ID);

    UserManager::getInstance()->loginUser($userInfo['id'], $userInfo['userType']);

    // all fields are valid.
    $page->json('success', array(
        'redirectUrl' => SITE_URL . DS . 'home'
    ));
}

