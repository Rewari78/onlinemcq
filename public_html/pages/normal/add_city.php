<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 29/10/18
 * Time: 4:56 PM
 */

$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );

$stateList = StateManager::getInstance()->getAllStates();

// now get the stateId.
$stateId = filter_input(INPUT_GET, 'si', FILTER_SANITIZE_NUMBER_INT);
$stateId = empty($stateId) ? 0 : $stateId;

// get the stateInfo by state id.
if ( $this->isPost() )
{
    $state = StateManager::getInstance()->getStateById($stateId);
    if ( empty($state) ) $msg = "State is not valid.";

    $city = filter_input(INPUT_POST, 'city');
//    $pinCode = filter_input(INPUT_POST, 'city-pincode');

    if ( empty($city) ) {
        $msg = "Form fields can not be empty.";
    } else {
        // We need to submit the study types.
        StateManager::getInstance()->addACity($state['id'], $city);

        $msg = "New study type is added.";
    }
}

$cities = StateManager::getInstance()
    ->getCityListByStateId($stateId);

$showCities = false;

$this->_addHeader();
@include $this->getPath() . DS . 'navs.top.php';
?>
<div class="row">
        <div class="col-sm-4 col-xxxl-4">
            <div class="element-wrapper">
                <h6 class="element-header">
                    Select State
                </h6>
                <div class="element-box">
                    <form>
                        <div class="form-group">
                            <label for="select-states">Select State</label>
                            <select class="form-control" id="select-states">
                                <option value="0">
                                    Please Select
                                </option>

                                <?php foreach ( $stateList as $state ): ?>
                                    <?php $showCities = $showCities !== true ? $state['id'] == $stateId : $showCities; ?>
                                    <option value="<?php echo $state['id']; ?>" <?php echo $state['id'] == $stateId ? 'selected' : ''; ?> >
                                        <?php echo $state['name']; ?>
                                    </option>
                                <?php endforeach; ?>

                            </select>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-sm-4 col-xxxl-4">
            <?php if ( $showCities === true ): ?>
                <div class="element-wrapper">
                <div class="element-actions">
                    <button class="btn btn-primary custom-button" data-target="#add-city-modal" data-toggle="modal" type="button">Add City</button>
                </div>
                <h6 class="element-header">
                    City
                </h6>

                    <?php if ( !empty($msg) ) : ?>
                        <div class="alert alert-success alert-dismissible " role="alert" area-label="Close">
                            <?php echo $msg; ?>
                            <button aria-label="close" class="close" data-dismiss="alert" type="button">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    <?php endif; ?>

                    <?php if ( !empty($cities) ): ?>
                    <div class="element-box">
                    <div class="table-responsive">
                        <table class="table table-lightborder">
                            <thead>
                            <tr>
                                <th>
                                    SR.No
                                </th>
                                <th class="text-left">
                                    City Name
                                </th>
                                <th>
                                    Actions
                                </th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php foreach ( $cities as $city ): ?>
                                <tr>
                                    <td class="nowrap">
                                        <?php echo $city['id']; ?>
                                    </td>
                                    <td class="text-left">
                                        <?php echo Util::htmlEncode($city['name']); ?>
                                    </td>
                                    <td class="row-actions">

                                        <a href="#" data-target="#edit-city-modal-<?php  echo $city['id']; ?>" data-toggle="modal">
                                            <i class="os-icon os-icon-ui-49"></i>
                                        </a>

                                        <div aria-hidden="true" aria-labelledby="mySmallModalLabel" class="modal fade bd-example-modal-sm" id="edit-city-modal-<?php echo $city['id']; ?>" role="dialog" tabindex="-1">

                                            <div class="modal-dialog modal-sm">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">
                                                            Edit City
                                                        </h5>
                                                        <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                                                    </div>
                                                    <form method="post" action="#" onsubmit="submitForm(this); return false;" >
                                                        <div class="modal-body">

                                                            <div class="form-group" style="text-align: left;">
                                                                <label for="city-<?php echo $city['id']; ?>">City Name</label>
                                                                <input class="form-control"
                                                                       placeholder="Type"
                                                                       type="text"
                                                                       id="city-<?php echo $city['id']; ?>"
                                                                       name="city"
                                                                       value="<?php echo Util::htmlEncode($city['name']); ?>" >
                                                            </div>

                                                            <input type="hidden" name="city-id" value="<?php echo $city['id']; ?>" />

                                                            <div class="alert alert-warning alert-dismissible d-none" role="alert"></div>

                                                        </div>

                                                        <div class="modal-footer">
                                                            <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
                                                            <button class="btn btn-primary" type="submit">Save changes</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>

                                        </div>

                                    </td>
                                </tr>
                            <?php endforeach; ?>

                            </tbody>
                        </table>
                    </div>
                </div>
                    <?php endif; ?>
            </div>
            <?php endif; ?>
        </div>

        <div class="col-sm-4 col-xxxl-4">
        </div>
    </div>

<div aria-hidden="true" aria-labelledby="mySmallModalLabel" class="modal fade bd-example-modal-sm" id="add-city-modal" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        Add New City
                    </h5>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                </div>
                <form method="post" action="#">
                    <div class="modal-body">

                        <div class="form-group">
                            <label for="city">City Name</label>
                            <input class="form-control" placeholder="Type" type="text" id="city" name="city">
                        </div>

                        <input type="hidden" name="city-id" value="<?php echo $stateId; ?>" />

                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" data-d17ismiss="modal" type="button"> Close</button>
                        <button class="btn btn-primary" type="submit">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

<script type="text/javascript">

    function _buildQuery( obj ) {

        var prop, params = [];
        for( prop in obj )
        {
            if ( obj.hasOwnProperty(prop) )
                params.push( prop + '=' + obj[prop] );
        }

        return params.join('&');
    }


    function submitForm( form )
    {
        var $form = $(form);
        var $alert = $form.find('.alert');
        var _data = $(form).serialize();

        hideError($alert);

        $.post(
            homeUrl + '/ajax/edit-city',
            $form.serialize(),
            function (data) {
                if ( data.info !== 'success' )
                {
                    showError($alert, data.message);
                    return;
                }

                window.location.reload();

            },
            'json'
        );
    }

    function showError( box, msg )
    {

        var $alertBox = $(box);
        $alertBox.html(msg);
        $alertBox.removeClass('d-none');
    }

    function hideError( box )
    {
        var $alertBox = $(box);
        $alertBox.addClass('d-none');
    }

    (function ( _scope ) {

        $(document).on('change', '#select-states', function ( event ) {

            var value = $(this).val().trim();
            _scope.location.href = '/add-city?' + _buildQuery({ si: value });

        });

    })(window);
</script>
<?php
@include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();

