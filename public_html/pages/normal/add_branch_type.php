<?php

$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );


$studyTypeId    = filter_input(INPUT_GET, 'sti', FILTER_SANITIZE_NUMBER_INT);
$courseTypeId   = filter_input(INPUT_GET, 'coti', FILTER_SANITIZE_NUMBER_INT);

$studyTypeId    = !empty($studyTypeId)      ? $studyTypeId      : 0;
$courseTypeId   = !empty($courseTypeId)     ? $courseTypeId     : 0;

$studyTypes     = CollegeManager::getInstance()->getAvlStudyType();
$courseTypes    = CollegeManager::getInstance()->getAvlCourseType($studyTypeId);

if ( $this->isPost() )
{
    $branchType = filter_input(INPUT_POST, 'branch-type');
    $courseId = filter_input(INPUT_POST, 'course-type-id');

    if ( empty($branchType) || empty($courseId) ) {
        $msg = "Branch type can not be empty.";
    } else if ( empty(CollegeManager::getInstance()->getCourseTypeById($courseId)) ){
        $msg = "Course Id is not valid";
    }  else {
        // We need to submit the study types.
        CollegeManager::getInstance()->addBranchType($courseId, $branchType);
        $msg = "New Branch type is added.";
    }

}

$showCoursesType = false;
$showBranchType = false;

$branches = CollegeManager::getInstance()->getAvlBranchType($courseTypeId);

$this->_addHeader();
@include $this->getPath() . DS . 'navs.top.php';
?>

<div class="row">
        <div class="col-sm-4 col-xxxl-4">
            <div class="element-wrapper">

                <?php if ( !empty($msg) ) : ?>
                    <div class="alert alert-success alert-dismissible " role="alert" area-label="Close">
                        <?php echo $msg; ?>
                        <button aria-label="close" class="close" data-dismiss="alert" type="button">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                <?php endif; ?>

                <h6 class="element-header">
                    Select
                </h6>
                <div class="element-box">
                    <form>

                        <div class="form-group">
                            <label for="select-study-type">Study Type</label>
                            <select class="form-control" required="required" id="select-study-type" name="select-study-type">
                                <option value="0">Please Select</option>

                                <?php foreach ( $studyTypes as $studyType ):  ?>
                                    <?php $showCoursesType = $showCoursesType !== true ? $studyType['id'] == $studyTypeId : $showCoursesType; ?>
                                    <option
                                        value="<?php echo $studyType['id']; ?>"
                                        <?php echo $studyType['id'] == $studyTypeId ? 'selected' : ''; ?> >
                                        <?php echo Util::htmlEncode($studyType['studyType']); ?>
                                    </option>
                                <?php endforeach; ?>

                            </select>
                        </div>

                        <?php if( $showCoursesType == true ): ?>
                            <div class="form-group">
                            <label for="select-course-type">Course Type</label>
                                <select class="form-control" required="required" id="select-course-type" name="select-course-type">
                                        <option value="0">Please Select</option>
                                    <?php foreach ( $courseTypes as $courseType ): ?>
                                        <?php $showBranchType = $showBranchType !== true ? $courseType['id'] == $courseTypeId : $showBranchType; ?>
                                        <option value="<?php echo $courseType['id']; ?>" <?php echo $courseType['id'] == $courseTypeId ? 'selected' : '' ?> >
                                            <?php echo Util::htmlEncode($courseType['courseType']); ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        <?php endif; ?>

                    </form>
                </div>
            </div>
        </div>

        <div class="col-sm-4 col-xxxl-4">
            <?php if ( $showBranchType == true ): ?>
                <div class="element-wrapper">

                <?php if ( !empty($msg) ) : ?>
                    <div class="alert alert-success alert-dismissible " role="alert" area-label="Close">
                        <?php echo $msg; ?>
                        <button aria-label="close" class="close" data-dismiss="alert" type="button">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                <?php endif; ?>

                <div class="element-actions">
                    <button class="btn btn-primary custom-button" data-target="#add-branch-type" data-toggle="modal" type="button">Add Branch Type</button>
                </div>
                <h6 class="element-header">
                    Branch Types
                </h6>
                <?php if ( !empty($branches) ): ?>
                    <div class="element-box">

                            <div class="table-responsive">
                            <table class="table table-lightborder">
                                <thead>
                                    <tr>
                                    <th>
                                        SR.No
                                    </th>
                                    <th class="text-left">
                                        Branch Type
                                    </th>
                                    <th>
                                        Actions
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ( $branches as $branch ): ?>
                                    <tr>

                                        <td class="nowrap">
                                            <?php echo $branch['id']; ?>
                                        </td>
                                        <td class="text-left">
                                            <?php echo Util::htmlEncode($branch['branchType']); ?>
                                        </td>
                                        <td class="row-actions">

                                            <a href="#" data-target="#edit-branch-modal-<?php echo $branch['id']; ?>" data-toggle="modal">
                                                <i class="os-icon os-icon-ui-49"></i>
                                            </a>

                                            <div aria-hidden="true" aria-labelledby="mySmallModalLabel" class="modal fade bd-example-modal-sm" id="edit-branch-modal-<?php echo $branch['id']; ?>" role="dialog" tabindex="-1">

                                                <div class="modal-dialog modal-sm">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">
                                                                Edit Type
                                                            </h5>
                                                            <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                                                        </div>
                                                        <form method="post" action="#" onsubmit="submitForm(this); return false;" >
                                                            <div class="modal-body">

                                                                <div class="form-group" style="text-align: left;">
                                                                    <label for="branch-type-<?php echo $branch['id']; ?>">Branch Type</label>
                                                                    <input class="form-control" placeholder="Type" type="text" id="branch-type-<?php echo $branch['id']; ?>" name="branch-type" value="<?php echo Util::htmlEncode($branch['branchType']); ?>">
                                                                </div>

                                                                <input type="hidden" name="branch-type-id" value="<?php echo $branch['id']; ?>" />

                                                                <div class="alert alert-warning alert-dismissible d-none" role="alert">
                                                                </div>

                                                            </div>
                                                            <div class="modal-footer">
                                                                <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
                                                                <button class="btn btn-primary" type="submit">Save changes</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>

                                            </div>

                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
            <?php endif; ?>
        </div>

        <div class="col-sm-4 col-xxxl-4">
        </div>
    </div>

    <div aria-hidden="true" aria-labelledby="mySmallModalLabel" class="modal fade bd-example-modal-sm" id="add-branch-type" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        Add New Type
                    </h5>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                </div>
                <form method="post" action="/add-branch-type">
                    <div class="modal-body">

                        <div class="form-group">
                            <label for="study-type">Study Type</label>
                            <input class="form-control" placeholder="Type" type="text" id="branch-type" name="branch-type">
                        </div>

                        <input type="hidden" value="<?php echo $courseTypeId; ?>" name="course-type-id" />

                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" data-dismiss="modal" type="button"> Close</button>
                        <button class="btn btn-primary" type="submit">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

<script type="text/javascript">
    function _buildQuery( obj ) {

        var prop, params = [];
        for( prop in obj )
        {
            if ( obj.hasOwnProperty(prop) )
                params.push( prop + '=' + obj[prop] );
        }

        return params.join('&');
    }

    (function ( _scope ) {

       $(document).on('change', '#select-study-type', function ( event ) {

            // var cti = $('#select-college-type').val().trim();
            var sti = $('#select-study-type').val().trim();

            _scope.location.href = '/add-branch-type?' + _buildQuery({
                // cti: cti,
                sti: sti
            });

        });

        $(document).on('change', '#select-course-type', function ( event ) {

            var sti = $('#select-study-type').val().trim();
            var coti = $('#select-course-type').val().trim();

            _scope.location.href = '/add-branch-type?' + _buildQuery({
                // cti: cti,
                sti: sti,
                coti: coti
            });

        });


    })(window);

    function submitForm( form )
    {
        var $form = $(form);
        var $alert = $form.find('.alert');
        var _data = $(form).serialize();

        $.post(
            homeUrl + '/ajax/edit-branch-type',
            $form.serialize(),
            function (data) {
                if ( data.info !== 'success' )
                {
                    showError($alert, data.message);
                    return;
                }

                window.location.reload();

            },
            'json'
        );
    }

    function showError( box, msg )
    {
        var $alertBox = $(box);
        $alertBox.html(msg);
        $alertBox.removeClass('d-none');
    }

    function hideError( box )
    {
        var $alertBox = $(box);
        $alertBox.addClass('d-none');
    }
</script>

<?php
@include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();