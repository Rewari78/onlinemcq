<?php

$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );

// now check if this is post.
$msg = null;
if ( $this->isPost() )
{
    $studyTP = filter_input(INPUT_POST, 'study-type');
    if ( empty($studyTP) ) {
        $msg = "Study type can not be empty.";
    } else {
        // We need to submit the study types.
        CollegeManager::getInstance()->addStudyType( $studyTP );

        $msg = "New study type is added.";
    }
}

$studies = CollegeManager::getInstance()
    ->getAvlStudyType();

$this->_addHeader();
@include $this->getPath() . DS . 'navs.top.php';
?>
<div class="row">
        <div class="col-sm-8 col-xxxl-4">
            <div class="element-wrapper">

                    <?php if ( !empty($msg) ) : ?>
                        <div class="alert alert-success alert-dismissible " role="alert" area-label="Close">
                            <?php echo $msg; ?>
                            <button aria-label="close" class="close" data-dismiss="alert" type="button">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    <?php endif; ?>

                    <div class="element-actions">
                        <button class="btn btn-primary custom-button" data-target="#add-type-modal" data-toggle="modal" type="button">Add Study Type</button>
                    </div>
                    <h6 class="element-header">
                        Study Types
                    </h6>

                    <?php if ( !empty($studies) ): ?>
                    <div class="element-box">
                        <div class="table-responsive">
                            <table class="table table-lightborder">
                                <thead>
                                <tr>
                                    <th>
                                        SR.No
                                    </th>
                                    <th class="text-left">
                                        Study Type
                                    </th>
                                    <th>
                                        Actions
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ( $studies as $study ): ?>
                                    <tr>
                                        <td class="nowrap">
                                            <?php echo $study['id']; ?>
                                        </td>
                                        <td class="text-left">
                                            <?php echo Util::htmlEncode($study['studyType']); ?>
                                        </td>
                                        <td class="row-actions">
                                            <a href="#" data-target="#edit-study-modal-<?php echo $study['id']; ?>" data-toggle="modal">
                                                <i class="os-icon os-icon-ui-49"></i>
                                            </a>

                                            <div aria-hidden="true" aria-labelledby="mySmallModalLabel" class="modal fade bd-example-modal-sm" id="edit-study-modal-<?php echo $study['id']; ?>" role="dialog" tabindex="-1">
                                                <div class="modal-dialog modal-sm">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">
                                                                Edit Type
                                                            </h5>
                                                            <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                                                        </div>
                                                        <form method="post" action="#" onsubmit="submitForm(this); return false;" >
                                                            <div class="modal-body">

                                                                <div class="form-group" style="text-align: left;">
                                                                    <label for="study-type">Study Type</label>
                                                                    <input class="form-control" placeholder="Type" type="text" id="study-type" name="study-type" value="<?php echo Util::htmlEncode($study['studyType']); ?>">
                                                                </div>

                                                                <input type="hidden" name="study-type-id" value="<?php echo $study['id']; ?>" />

                                                                <div class="alert alert-warning alert-dismissible d-none" role="alert">
                                                                </div>

                                                            </div>
                                                            <div class="modal-footer">
                                                                <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
                                                                <button class="btn btn-primary" type="submit">Save changes</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php endif; ?>

                </div>
        </div>

        <div class="col-sm-4 col-xxxl-4">
        </div>

</div>

<div aria-hidden="true" aria-labelledby="mySmallModalLabel" class="modal fade bd-example-modal-sm" id="add-type-modal" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        Add New Type
                    </h5>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                </div>
                <form method="post" action="#">
                    <div class="modal-body">

                        <div class="form-group">
                            <label for="study-type">Study Type</label>
                            <input class="form-control" placeholder="Type" type="text" id="study-type" name="study-type">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" data-dismiss="modal" type="button"> Close</button>
                        <button class="btn btn-primary" type="submit">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

<script type="text/javascript">
    (function (_scope) {
        var $select = $('#select-college-id');

        $(document).on('change', '#select-college-id', function (event) {
            var value = $(this).val().trim();
            window.location.href = '/add-study-type?cti=' + value;
        });
    })(window);
</script>

<script type="text/javascript">
    function submitForm( form )
    {
        var $form = $(form);
        var $alert = $form.find('.alert');
        var _data = $(form).serialize();

        hideError($alert);

        $.post(
            homeUrl + '/ajax/edit-study-type',
            $form.serialize(),
            function (data) {
                if ( data.info !== 'success' )
                {
                    showError($alert, data.message);
                    return;
                }

                window.location.reload();

            },
            'json'
        );
    }

    function showError( box, msg )
    {

        var $alertBox = $(box);
        $alertBox.html(msg);
        $alertBox.removeClass('d-none');
    }

    function hideError( box )
    {
        var $alertBox = $(box);
        $alertBox.addClass('d-none');
    }
</script>
<?php
@include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();
;