
<?php

$testId = filter_input(INPUT_GET, 'test-id', FILTER_SANITIZE_NUMBER_INT);
$testInfo = TestManager::getInstance()->getTestInfo($testId);
$export = filter_input(INPUT_GET, 'export', FILTER_SANITIZE_NUMBER_INT);
$studentTests = TestManager::getInstance()->getStudentTestsByTestId($testId);

if ($export == 9999 && $testInfo >0 )
{
    try
    {

    echo 'Helllo....';
    $studentExcelTest = TestManager::getInstance()->getStudentTestsExcel($testId);
    $subj =str_replace($testInfo['description'],",", "");
    $subj =str_replace($subj,".", "");
    echo $testInfo['description'];

    //$filename = htmlentities($testInfo['subject']) . "-" . str_replace(' ', '_', $testInfo['description']). ".csv";
   // echo 'Message: ' .$filename ;
     }
    catch(Exception $e) {
    //    echo 'Message: ' .$e->getMessage();
    echo 'errot...';
      }
}
if ($export == 1 && $testInfo >0 )
{
    try
    {

    
    $studentExcelTest = TestManager::getInstance()->getStudentTestsExcel($testId);
    
/*   Export to excel  */
        //$filename = htmlentities($testInfo['subject']) . "-" . str_replace(' ', '_', $testInfo['description']). ".csv";
        $filename = "export.csv";
        header('Content-Encoding: UTF-8');
        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename='.$filename);

        $fp = fopen('php://output', 'w');
        $header = array("First Name", "Last Name","Class","Division","Roll No","Student Id","Questions Attempted","Score","Max Score");
        /*while ($row = mysqli_fetch_row($result)) {
            $header[] = $row[0];
        }*/	
        
         fputcsv($fp, $header);

       // rewind($fp);
       


        //$query = "SELECT * FROM toy";
        //$result = mysqli_query($conn, $query);
        //while($row =$studentTests) {
        foreach ($studentExcelTest as $test)
        {
            if ($test['questionsAttempted'] == 0)
                continue;
                
                //substr($variable, 0, strpos($variable, "for")); 
               //echo htmlentities($test['score']);
                $row = array();
                 
                array_push($row, htmlentities($test['firstName']));
                array_push($row, htmlentities($test['lastName']));
                array_push($row, htmlentities($test['className']));
                array_push($row, htmlentities($test['divName']));
                array_push($row, strval($test['rollNo']));
                array_push($row, strval($test['studentId']));
                array_push($row, htmlentities($test['questionsAttempted']));
                array_push($row,   substr(htmlentities($test['score']), 0, strpos(htmlentities($test['score']), "/") )) ;
                array_push($row,   substr(htmlentities($test['score']), strpos(htmlentities($test['score']), "/") )) ;
 
            //$row = array($test['rollNo'],$test['firstName'])
            fputcsv($fp, $row);
        } 

         fclose($fp);
       
exit;

    }
    catch(Exception $e) {
        echo 'Message: ' .$e->getMessage();
      }
}
/*  Export to excel   */
$this->_addHeader();
?>


<div class="container mt-5">

    <h2 class="text-left mb-4">Student Tests</h2>
    <strong>Subject: </strong><?php echo htmlentities($testInfo['subject']); ?><br />
    <strong>Description: </strong><?php echo htmlentities($testInfo['description']); ?><br /><br />
    <div class="row">
    <div class="col-md-12">
    <h5>
    <a  href="<?php echo SITE_URL . '/tests?export=1&test-id=' . $testId; ?>">  Export to Excel</a>
    </h5>
    
    </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table id="example" class="table table-striped table-bordered" style="width:100%">           
                <thead>
                    <tr>
                        <th>Roll No</th>
                        <th>Student Id</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Class</th>
                        <th>Division</th>
                        <th>Questions Attempted</th>
                        <th>Score</th>
                        <th>Details</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (count($studentTests) >= 1) { ?>

                        <?php foreach ($studentTests as $test) { ?>
                            <?php if ( $test['questionsAttempted'] == 0 ) continue; ?>
                            <tr>
                                <td><?php echo htmlentities($test['rollNo']); ?></td>
                                <td><?php echo htmlentities($test['studentId']); ?></td>                                
                                <td><?php echo htmlentities($test['firstName']); ?></td>
                                <td><?php echo htmlentities($test['lastName']); ?></td>
                                <td><?php echo htmlentities($test['className']); ?></td>
                                <td><?php echo htmlentities($test['divName']); ?></td>
                                <td><?php echo htmlentities($test['questionsAttempted']); ?></td>
                                <td><?php echo htmlentities($test['score']); ?></td>
                                <td><a target="_blank" href="<?php echo SITE_URL . '/result?result-id=' . $test['id']; ?>">Details</a></td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th>Roll No</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Questions Attempted</th>
                        <th>Score</th>
                        <th>Details</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#example').DataTable({
            "language": {
                "emptyTable": "No student test found."
            }
        });
    });

</script>