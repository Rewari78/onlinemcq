<?php

$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );

$stateId = isset($this->_data[0]) ? $this->_data[0] : 0;
$stateId = filter_var(filter_var($stateId, FILTER_SANITIZE_NUMBER_INT), FILTER_VALIDATE_INT);

if ( empty($stateId) ) $this->redirect('manage-locations');

$stateInfo  = StateManager::getInstance()->getStateById($stateId);

if ( empty($stateInfo) ) $this->redirect('manage-locations');

if ( $this->isPost() )
{
    $stateName = filter_input(INPUT_POST, 'state-name');
    $stateSName = filter_input(INPUT_POST, 'state-short-name');

    if ( empty( $stateName ) || empty($stateSName) )
    {

        $msg = "Please, fill the form properly.";
    }else {

        // We are going to submit.
        StateManager::getInstance()->updateState($stateInfo['id'], $stateName, $stateSName);
        $msg = "State info is successfully edited";
    }

}

$this->_addHeader();
@include $this->getPath() . DS . 'navs.top.php';
?>
<div class="row">
    <div class="col-sm-8 col-xxxl-8">
        <div class="element-wrapper">
            <h6 class="element-header">
                Edit State
            </h6>
            <div class="element-box">
                <form method="post" action="#">
                    <div class="form-group">
                        <label for="state-name">State Name</label>
                        <input class="form-control" placeholder="Type State Name" type="text" id="state-name" name="state-name" value="<?php echo Util::htmlEncode($stateInfo['name']); ?>">
                    </div>

                    <div class="form-group">
                        <label for="state-short-name">State Short Name</label>
                        <input class="form-control" placeholder="Type State Short Name" type="text" id="state-short-name" name="state-short-name" value="<?php echo Util::htmlEncode($stateInfo['shortName']) ?>">
                    </div>

                    <button class="btn btn-primary" type="submit">Update</button>
                </form>
            </div>
        </div>
    </div>


</div>


    <div aria-hidden="true" aria-labelledby="mySmallModalLabel" class="modal fade bd-example-modal-sm" id="add-type-modal-add-city" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        Add New City
                    </h5>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                </div>
                <form method="post" action="/ajax/add-a-city" id="add-a-city-form">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="city"> City</label>
                            <input class="form-control" placeholder="Type City Name" type="text" id="city" name="city">
                        </div>
                        <div class="form-group">
                            <label for="city-pincode"> City Pincode</label>
                            <input class="form-control" placeholder="Type City Pincode" type="number" id="city-pincode" name="city-pincode">
                        </div>
                        <input type="hidden" value="<?php echo $stateId; ?>" name="stateId">

                        <div class="alert alert-warning alert-dismissible d-none" role="alert" id="add-city-alert-box">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" data-dismiss="modal" type="button"> Close</button>
                        <button class="btn btn-primary" type="submit">Add City</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        (function (_scope) {
            var $form = $('#add-a-city-form');
            var $addCityModal = $('#add-type-modal-add-city');

            $addCityModal.on('show.bs.modal', function () {
                hideError();
                $form[0].reset();
            });

            $form.on('submit', function (e) {

                e.preventDefault();
                e.stopPropagation();

                hideError();

                $.post(
                    homeUrl + '/ajax/add-a-city',
                    $(this).serialize(),
                    function(data){

                        if (  data.info !== 'success' )
                        {
                            showError(data.message);
                            return;
                        }

                        window.location.reload();
                    },
                    'json'
                );

            });

            function showError( msg )
            {
                var alertBox = document.getElementById('add-city-alert-box');
                alertBox.innerHTML = msg;
                alertBox.className = alertBox.className.replace(/\bd-none\b/g, "");
            }

            function hideError()
            {
                var alert = document.getElementById('add-city-alert-box');
                alert.className += ' d-none ';
            }
        })(window);
    </script>

    <script type="text/javascript">
        (function (_scope) {

        })(window);
    </script>

<?php
@include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();