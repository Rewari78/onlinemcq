<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 18/12/18
 * Time: 8:59 PM
 */

$this->onlyLoggedInAllowed(UserManager::USER_TYPE_SUBSCRIBER, PackageManager::FEATURE_STUDY_MATERIAL);

/** @var User $user */
$user = $this->getUser();

$page = isset($this->_data[0]) ? $this->_data[0] : null;

$resourcePaths = array();
foreach ( $this->_data as $key => $item )
{

    if ( is_numeric($key) && $key != 0 ) $resourcePaths[] = $item;
}

$title = "Study";

switch ( $page )
{
    case StudyPages::PAGE_PHYSICS:
        $title = "Physics (Study Material)";
    case StudyPages::PAGE_BIO:
        $title = "Biology (Study Material)";
    case StudyPages::PAGE_CHEM:
        $title = "Chemistry (Study Material)";
        break;
    default:
        throw new Error404;
}


$studyPages = new StudyPages($page, SITE_URL . DS . 'study');
$getContent = $studyPages->getPageContent($resourcePaths);


$this->_addHeader();
@include $this->getPath() . DS . 'navs.top.php';
?>


<a href="#" class="go-back-link"><i class="fa fa-arrow-left"></i></a>
<div class="row">
    <div class="col-sm-12 col-xxxl-12">
        <div class="element-wrapper">
            <h6 class="custm-ele-hdr">
                <?php echo $title; ?>
            </h6>
            <div class="custm-ele-hdr-strk">
            </div>
            <div class="element-content">
                <p>Morbi mollis tellus ac sapien. Pellentesque auctor neque nec urna. Curabitur a felis in nunc fringilla tristique. Praesent vestibulum dapibus nibh. Nulla sit amet est. Morbi mollis tellus ac sapien. Pellentesque auctor neque nec urna. Curabitur a felis in nunc fringilla tristique. Praesent vestibulum dapibus nibh. Nulla sit amet est.</p>
            </div>
        </div>
    </div>
</div>

<?php echo $getContent; ?>

<?php
@include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();
