<?php

/**
 * Created by PhpStorm.
 * User: akash
 * Date: 18/12/18
 * Time: 8:59 PM
 */

$this->onlyLoggedInAllowed(UserManager::USER_TYPE_TEACHER);
ini_set('display_errors', 'On');



$testId = filter_input(INPUT_GET, 'test-id', FILTER_SANITIZE_NUMBER_INT);
$questionId = filter_input(INPUT_GET, 'question-id', FILTER_SANITIZE_NUMBER_INT);
$delete =filter_input(INPUT_GET, 'delete', FILTER_SANITIZE_NUMBER_INT);

$testInfo = TestManager::getInstance()->getTestInfo($testId);

if (empty($testInfo)) throw new Error404;

$questionsExists = TestManager::getInstance()->questionExists($testId, $questionId);


$allQuestionInfo = TestManager::getInstance()->getAllQuestions($testInfo['id']);
// var_dump($allQuestionInfo);exit;
if ($questionsExists) {
    $questionInfo = TestManager::getInstance()->getQuestion($testInfo['id'], $questionId);
}

if ( $delete == "1" && $questionsExists ) {
    $currNo = $questionInfo['questionNo'];

    // First Delete question
    $db = Db::getInstance();
    $stmt = $db->prepare("DELETE FROM `" . DB_TABLE_TEST_QUESTIONS . "` WHERE `id` = :id");
    Db::bindValues(array(
        ':id' => $questionInfo['id']
    ), $stmt);
    $stmt->execute();

    foreach ( $allQuestionInfo as $question ) {
        if ( $question['questionNo'] > $currNo ) {
            $stmt = $db->prepare("UPDATE `" . DB_TABLE_TEST_QUESTIONS . "` SET `questionNo` = :nom WHERE `testId` = :id AND `questionNo` = :nomp");
            Db::bindValues(array(
                ':nomp' => $question['questionNo'],
                ':nom' => $question['questionNo'] - 1,
                ':id' => $testId
            ), $stmt);
            $stmt->execute();
        }
    }

    $this->redirect("edit-questions?test-id={$testId}&question-id={$currNo}");
}

if ($this->isPost()) {

    $photo = filter_input(INPUT_POST, 'photo') !== '' ? filter_input(INPUT_POST, 'photo') : null;
    $shouldRedirect = filter_input(INPUT_POST, 'redirect');
    $question = filter_input(INPUT_POST, 'ckeditor1');
    $ans1 = filter_input(INPUT_POST, 'ckeditor2');
    $ans2 = filter_input(INPUT_POST, 'ckeditor3');
    $ans3 = filter_input(INPUT_POST, 'ckeditor4');
    $ans4 = filter_input(INPUT_POST, 'ckeditor5');
    $ans5 = filter_input(INPUT_POST, 'ckeditor6');
    $exp = filter_input(INPUT_POST, 'ckeditor7');
    $cor = filter_input(INPUT_POST, 'corr', FILTER_SANITIZE_NUMBER_INT);
    $weight = filter_input(INPUT_POST, 'weight', FILTER_SANITIZE_NUMBER_INT);
    $cor = $cor >= 1 && $cor <= 5 ? $cor : 1;

    if (!empty($question)) {
        if (!empty($ans1) || !empty($ans2) || !empty($ans3) || !empty($ans4) || !empty($ans5) || !empty($exp)) {

            $nextQuestionId = $questionId + 1;
            if ($questionsExists) {
                TestManager::getInstance()->updateQuestion(
                    $questionInfo['id'],
                    $testId,
                    $questionId,
                    $question,
                    $ans1,
                    $ans2,
                    $ans3,
                    $ans4,
                    $ans5,
                    $cor,
                    $exp,
                    $photo,
                    $weight
                );

                $shouldRedirect == 1 ? $this->redirect("manage-test") : $this->redirect("edit-questions?test-id={$testId}&question-id={$nextQuestionId}");
            } else {
                TestManager::getInstance()->createQuestion(
                    $testId,
                    $questionId,
                    $question,
                    $ans1,
                    $ans2,
                    $ans3,
                    $ans4,
                    $ans5,
                    $cor,
                    $exp,
                    $photo,
                    $weight
                );

                $shouldRedirect == 1 ? $this->redirect("manage-test") : $this->redirect("edit-questions?test-id={$testId}&question-id={$nextQuestionId}");
            }

            $allQuestionInfo = TestManager::getInstance()->getAllQuestions($testInfo['id']);
        }
    }

    if ($shouldRedirect) {
        $this->redirect("manage-test");
    }
}


$question = $questionsExists ? $questionInfo['question'] : "";
$ans1 = $questionsExists ? $questionInfo['ans1'] : "";
$ans2 = $questionsExists ? $questionInfo['ans2'] : "";
$ans3 = $questionsExists ? $questionInfo['ans3'] : "";
$ans4 = $questionsExists ? $questionInfo['ans4'] : "";
$ans5 = $questionsExists ? $questionInfo['ans5'] : "";
$exp = $questionsExists ? $questionInfo['exp'] : "";
$cor = $questionsExists ? $questionInfo['correctAns'] : "";
$weight = $questionsExists ? $questionInfo['weight'] : "";

$maxQuestionId = $questionsExists ? count($allQuestionInfo) - 1 : 1;
$this->_addHeader();

?>
<div class="row">
    <div class="col-sm-12 col-xxxl-12">
        <div class="element-wrapper">
            <h3 class="text-center">Subject: <?php echo htmlentities($testInfo['subject']); ?></h3>
            <h3 class="text-center">Description: <?php echo htmlentities($testInfo['description']); ?></h3>
            <h4 class="text-center">Question Number: <?php echo $questionId; ?></h4>


            <form method="post" action="#" id="add-form">

                <div class="element-box">
                    <div class="row">
                        <div class="col-sm-12">
                            <legend><span>Write Question</span></legend>
                            <textarea cols="80" id="ckeditor1" name="ckeditor1" rows="10"><?php echo htmlentities($question); ?></textarea>
                        </div>
                    </div>
                </div>


                <div class="element-box" style="height: auto;">
                    <div class="row">
                        <div class="col-sm-4">
                            <legend><span>Question Photo</span></legend>
                            <input type="file" id="questionPhotoFile" class="btn btn-primary btn-block" accept="image/webp, image/gif, image/jpeg, image/png, application/pdf">
                            <p class="small">Max upload size: 5MB</p>
                            <?php if ($questionsExists && $questionInfo['photo'] !== null) { ?>
                                <div id="photoContainer" class="mt-2" style="max-width: 250px;">
                                    <?php
                                        $chunks = explode('|||', $questionInfo['photo']);
                                        if ( $chunks[2] == 'pdf' ) {
                                        ?>
                                        <a href="<?php echo SITE_URL . $chunks[0]; ?>" target="_blank"><p style="padding: 10px; background-color: #047bf8;color: white;"><strong><?php echo $chunks[1]; ?></strong></p></a>
                                        <?php

                                        } else {
                                        ?>
                                            <img style="height:100%;width:100%;margin-bottom:20px;" src="<?php echo SITE_URL . $chunks[0]; ?>">
                                        <?php
                                        }
                                    ?>
                                    <button class="btn btn-primary" type="button" onclick="removePhotoPDF()">Remove</button>
                                </div>
                            <?php } else { ?>
                                <div id="photoContainer" class="mt-2" style="max-width: 250px;"></div>
                            <?php } ?>
                        </div>
                        <div class="col-sm-4">
                            <legend><span>Question Marks</span></legend>
                            <div class="form-group">
                                <label class="sr-only">Marks</label>
                                <select class="form-control" name="weight">
                                    <option value="1" <?php echo $weight == "1" ? 'selected' : '' ?>>1</option>
                                    <option value="2"  <?php echo $weight == "2" ? 'selected' : '' ?>>2</option>
                                    <option value="3"  <?php echo $weight == "3" ? 'selected' : '' ?>>3</option>
                                    <option value="4"  <?php echo $weight == "4" ? 'selected' : '' ?>>4</option>
                                    <option value="5"  <?php echo $weight == "5" ? 'selected' : '' ?>>5</option>
                                    <option value="6"  <?php echo $weight == "6" ? 'selected' : '' ?>>6</option>
                                    <option value="7"  <?php echo $weight == "7" ? 'selected' : '' ?>>7</option>
                                    <option value="8"  <?php echo $weight == "8" ? 'selected' : '' ?>>8</option>
                                    <option value="9"  <?php echo $weight == "9" ? 'selected' : '' ?>>9</option>
                                    <option value="10" <?php echo $weight == "10" ? 'selected' : '' ?>>10</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="element-box">
                    <div class="row">
                        <div class="col-md-6">
                            <legend><span>Answer 1</span></legend>
                            <textarea cols="80" id="ckeditor2" name="ckeditor2" rows="5"><?php echo htmlentities($ans1); ?></textarea>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check pt-3">
                                <label class="form-check-label">
                                    <input class="form-check-input" data-with="ckeditor2"  style="margin-top: 1.5px;" type="radio" <?php echo $cor == 1 ? 'checked' : ''; ?> name="corr" value="1">Mark this answer as Correct</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="element-box">
                    <div class="row">
                        <div class="col-md-6">
                            <legend><span>Answer 2</span></legend>
                            <textarea cols="80" id="ckeditor3" name="ckeditor3" rows="5"><?php echo htmlentities($ans2); ?></textarea>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check pt-3">
                                <label class="form-check-label">
                                    <input class="form-check-input" data-with="ckeditor3" style="margin-top: 1.5px;" type="radio" <?php echo $cor == 2 ? 'checked' : ''; ?> name="corr" value="2">Mark this answer as Correct</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="element-box">
                    <div class="row">
                        <div class="col-md-6">
                            <legend><span>Answer 3</span></legend>
                            <textarea cols="80" id="ckeditor4" name="ckeditor4" rows="5"><?php echo htmlentities($ans3); ?></textarea>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check pt-3">
                                <label class="form-check-label">
                                    <input class="form-check-input" style="margin-top: 1.5px;" data-with="ckeditor4" type="radio" <?php echo $cor == 3 ? 'checked' : ''; ?> name="corr" value="3">Mark this answer as Correct</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="element-box">
                    <div class="row">
                        <div class="col-md-6">
                            <legend><span>Answer 4</span></legend>
                            <textarea cols="80" id="ckeditor5" name="ckeditor5" rows="5"><?php echo htmlentities($ans4); ?></textarea>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check pt-3">
                                <label class="form-check-label">
                                    <input class="form-check-input" style="margin-top: 1.5px;" data-with="ckeditor5" type="radio" <?php echo $cor == 4 ? 'checked' : ''; ?> name="corr" value="4">Mark this answer as Correct</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="element-box">
                    <div class="row">
                        <div class="col-md-6">
                            <legend><span>Answer 5</span></legend>
                            <textarea cols="80" id="ckeditor6" name="ckeditor6" rows="5"><?php echo htmlentities($ans5); ?></textarea>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check pt-3">
                                <label class="form-check-label">
                                    <input class="form-check-input" style="margin-top: 1.5px;" data-with="ckeditor6" type="radio" <?php echo $cor == 5 ? 'checked' : ''; ?> name="corr" value="5">Mark this answer as Correct</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="element-box">
                    <div class="row">
                        <div class="col-sm-6">
                            <legend><span>Correct Ans Explaination</span></legend>
                            <textarea cols="80" id="ckeditor7" name="ckeditor7" rows="5"><?php echo htmlentities($exp); ?></textarea>
                        </div>
                    </div>
                </div>

                <div class="element-box">
                    <div class="row">
                        <div class="col-sm-12">
                            <p>Go to
                                <select id="jtq">
                                    <option disabled selected>questions</option>
                                    <?php
                                    $qNum  = (count($allQuestionInfo) !== 0) ? count($allQuestionInfo) : 1;
                                    for ($i = 1; $i <= $qNum; $i++) {
                                    ?>
                                        <option value="<?php echo '/edit-questions?test-id=' . $testId . '&question-id=' . $i; ?>">question <?php echo $i; ?></option>
                                    <?php } ?>
                                </select>
                            </p>
                            <?php if ($questionId != 1) : ?>
                                <a class="btn btn-primary" href="/edit-questions?test-id=<?php echo $testId ?>&question-id=<?php echo $questionId - 1 ?>">Previous</a>
                            <?php endif; ?>
                            <button class="btn btn-primary" type="submit"> Save and Next</button>
                            <?php if ( $questionsExists ): ?>
                                <button class="btn btn-primary" type="button" onclick="deleteQuestion()">Delete</button>
                            <?php endif; ?>

                            <!-- <?php if ($questionId < $maxQuestionId) : ?>
                                <a class="btn btn-primary" href="/edit-questions?test-id=<?php echo $testId ?>&question-id=<?php echo $questionId + 1 ?>">Next</a>
                            <?php endif; ?> -->
                            <button id="finishButton" class="btn btn-primary">Save and Finish</button>

                            <input type="hidden" id="questionPhoto" name="photo" value="<?php echo $questionsExists && isset($questionInfo['photo']) ? $questionInfo['photo'] : ''; ?>">
                            <input type="hidden" id="redirectFlag" name="redirect" value="0">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">

	function removePhotoPDF() {
		$('#questionPhoto').val('');
		$("#photoContainer").empty();
    }

    function deleteQuestion() {
        var url = window.location.href;
        url = url.replace('#', '');
        url = url.replace('&delete=1', '');
        window.location.href = url + '&delete=1';
    }

    $(function() {

        $("#questionPhotoFile").on('change', function() {

            var fd = new FormData();
            fd.append('upload', $("#questionPhotoFile")[0].files[0]);
            var $elm = $(this);

            $.ajax({
                url: '/ajax/upload-q-file/',
                type: 'post',
                data: fd,
                contentType: false,
                processData: false,
                success: function(response) {
                    if (response.uploaded == 1) {
                        $("#photoContainer").html('');

                        if ( response.type === 'pdf' ) {
                            $("#photoContainer").append('<a href="' + response.url + '" target="_blank"><p style="padding: 10px; background-color: #047bf8;color: white;"><strong>' + response.name + '</strong></p></a>');
                        } else {
                            $("#photoContainer").append('<img style="height:100%;width:100%;margin-bottom:20px;" src="' + homeUrl + response.url + '" ><button class="btn btn-primary" type="button" onclick="removePhotoPDF()">Remove</button>');

                        }
                        $("#questionPhoto").val( response.url + "|||" + response.name + '|||' + response.type );
                    } else if ( response.uploaded == 2 ) {
                        alert('File size should be within 5MB.');
                        $elm.val('');
                    } else {
                        alert('Unable to upload selected file!');
                        $elm.val('');
                    }
                },
            });

        });

        $("#finishButton").on('click', function() {
            $("#redirectFlag").val('1');
        });

        $("#jtq").on('change', function() {
            var questionUrl = $.trim(this.value);
            window.location.href = questionUrl;
        });

        CKEDITOR.config.toolbar = 'Basic';
        CKEDITOR.config.toolbar_Basic = [
            ['Cut','Copy', 'Paste', 'PasteText', 'Undo', 'Redo', 'Bold','Italic','Underline'] 
        ];
    
        CKEDITOR.config.format_tags = 'p;h1;h2;h3;pre;img';
        CKEDITOR.config.extraPlugins = 'uploadimage,uploadwidget,filebrowser';
        CKEDITOR.config.filebrowserUploadUrl = '/ajax/upload-file/';
        CKEDITOR.replace('ckeditor1');
        CKEDITOR.replace('ckeditor2');
        CKEDITOR.replace('ckeditor3');
        CKEDITOR.replace('ckeditor4');
        CKEDITOR.replace('ckeditor5');
        CKEDITOR.replace('ckeditor6');
        CKEDITOR.replace('ckeditor7');

        $('#add-form').on('submit', function(e) {

            var $radios = $('input[type="radio"]');
            var ques = CKEDITOR.instances.ckeditor1.getData();
            var ans1 = CKEDITOR.instances.ckeditor2.getData();
            var ans2 = CKEDITOR.instances.ckeditor3.getData();
            var ans3 = CKEDITOR.instances.ckeditor4.getData();
            var ans4 = CKEDITOR.instances.ckeditor5.getData();
            var ans5 = CKEDITOR.instances.ckeditor5.getData();

            console.log(ques);

            if ( ques.trim() === '' ) {
                alert("Please write a question first.");
                e.preventDefault();
                return;
            }

            var totalAns = 0;
            if ( ans1.trim() !== '' )
            {
                totalAns += 1;
            }

            if ( ans2.trim() !== '' )
            {
                totalAns += 1;
            }

            if ( ans3.trim() !== '' )
            {
                totalAns += 1;
            }

            if ( ans4.trim() !== '' )
            {
                totalAns += 1;
            }

            if ( ans5.trim() !== '' )
            {
                totalAns += 1;
            }

            if ( totalAns >= 2 ) {
                var $r = $radios.filter(':checked');
                if ( $r.length <= 0 ) {
                    e.preventDefault();
                    alert("Please select one option as Correct Answer");
                } else {

                    // check if the radio is selected for given ans.
                    var data = CKEDITOR.instances[$r.data('with')].getData();
                    if ( data === '' ) {
                        alert("The answer you marked as correct is empty, please check.");
                        e.preventDefault();
                    }

                }
            } else {
                e.preventDefault();
                alert("You need write at least two answers before save.");
            }

        });

    });
</script>
<?php
// @include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();
