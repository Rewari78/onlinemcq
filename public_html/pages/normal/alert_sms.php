<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 18/12/18
 * Time: 8:59 PM
 */

$this->onlyLoggedInAllowed(UserManager::USER_TYPE_SUBSCRIBER, PackageManager::FEATURE_SMS_NOTI);


/** @var User $user */
$user = $this->getUser();

$smsStatus = EmailSmsManager::getInstance()->getSmsStatus($user->getId());
$neetStatus = 0;
$promoStatus = 0;
if ( !empty($smsStatus) )
{
    $values = json_decode($smsStatus[$user->getId()], true);

    $neetStatus = $values['neet_update'];
    $promoStatus = $values['promo_update'];
}

$this->_addHeader();
@include $this->getPath() . DS . 'navs.top.php';
?>

    <div class="content-box">
        <a href="#" class="go-back-link"><i class="fa fa-arrow-left"></i></a>

        <div class="row">
            <div class="col-sm-12 col-xxxl-12">
                <div class="element-wrapper">
                    <h6 class="custm-ele-hdr">
                        SMS Alerts
                    </h6>
                    <div class="custm-ele-hdr-strk">
                    </div>
                    <div class="element-content">
                        <p>Morbi mollis tellus ac sapien. Pellentesque auctor neque nec urna. Curabitur a felis in nunc fringilla tristique. Praesent vestibulum dapibus nibh. Nulla sit amet est. Morbi mollis tellus ac sapien. Pellentesque auctor neque nec urna. Curabitur a felis in nunc fringilla tristique. Praesent vestibulum dapibus nibh. Nulla sit amet est.</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row pt-3">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-4">
                        <h5 class="alert-head">Alerts Type</h5>
                        <div class="custm-ele-hdr-strk">
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                                <p class="alert-head-p">NEET Updates</p>
                            </div>
                            <div class="col-sm-4">
                                <label class="switch">
                                    <input type="checkbox" <?php echo $neetStatus == 1? 'checked' : '' ?> name="promo-input" value="1" id="neet-input">
                                    <span class="slider-cus round"></span>
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                                <p class="alert-head-p">Promotional Updates</p>
                            </div>
                            <div class="col-sm-4">
                                <label class="switch">
                                    <input type="checkbox" name="promo-input" value="1" <?php echo $promoStatus == 1? 'checked' : '' ?> id="promo-input">
                                    <span class="slider-cus round"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <h5 class="alert-head">Change Number</h5>
                        <div class="custm-ele-hdr-strk">
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <p class="alert-head-p">Current Number</p>
                            </div>
                            <div class="col-sm-9">
                                <div class="c-num-link">
                                    <p class="num-eml"><?php echo $user->getPhone(); ?></p>
<!--                                    <p class="go-to-profile-link"><a href="/account-mobile-settings">Change</a></p>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">

                    </div>
                </div>
            </div>
        </div>
    <script type="text/javascript">
        $('#promo-input, #neet-input').on('change', function () {
            var promoValue = $('#promo-input').is(':checked');
            var neetValue = $('#neet-input').is(':checked');
            neetValue = neetValue ? 1 : 0;
            promoValue = promoValue ? 1 : 0;

            $.ajax({
                url: homeUrl + '/ajax/sms-status-save',
                type: 'POST',
                data: {
                    promoSms: promoValue,
                    neetSms: neetValue
                },
                success: function (data) {
                    if ( data.info !== 'success' ) return;
                }
            });


        });
    </script>
<?php
@include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();

