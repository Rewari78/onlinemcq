<?php

/** @var User $user */
// $user = $this->getUser();
?>

<body class="menu-position-side menu-side-left full-screen with-content-panel">
    <div class="all-wrapper with-side-panel solid-bg-all">
        <div class="search-with-suggestions-w">
            <div class="search-with-suggestions-modal">
                <div class="element-search">
                    <input class="search-suggest-input" placeholder="Start typing to search..." type="text">
                    <div class="close-search-suggestions">
                        <i class="os-icon os-icon-x"></i>
                    </div>
                    </input>
                </div>
                <div class="search-suggestions-group">
                    <div class="ssg-header">
                        <div class="ssg-icon">
                            <div class="os-icon os-icon-box"></div>
                        </div>
                        <div class="ssg-name">
                            Projects
                        </div>
                        <div class="ssg-info">
                            24 Total
                        </div>
                    </div>
                    <div class="ssg-content">
                        <div class="ssg-items ssg-items-boxed">
                            <a class="ssg-item" href="users_profile_big.html">
                                <div class="item-media" style="background-image: url(/images/company6.png)"></div>
                                <div class="item-name">
                                    Integ<span>ration</span> with API
                                </div>
                            </a><a class="ssg-item" href="users_profile_big.html">
                                <div class="item-media" style="background-image: url(img/company7.png)"></div>
                                <div class="item-name">
                                    Deve<span>lopm</span>ent Project
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="search-suggestions-group">
                    <div class="ssg-header">
                        <div class="ssg-icon">
                            <div class="os-icon os-icon-users"></div>
                        </div>
                        <div class="ssg-name">
                            Customers
                        </div>
                        <div class="ssg-info">
                            12 Total
                        </div>
                    </div>
                    <div class="ssg-content">
                        <div class="ssg-items ssg-items-list">
                            <a class="ssg-item" href="users_profile_big.html">
                                <div class="item-media" style="background-image: url(images/avatar1.jpg)"></div>
                                <div class="item-name">
                                    John Ma<span>yer</span>s
                                </div>
                            </a><a class="ssg-item" href="users_profile_big.html">
                                <div class="item-media" style="background-image: url(/statics/images/avatar2.jpg)"></div>
                                <div class="item-name">
                                    Th<span>omas</span> Mullier
                                </div>
                            </a><a class="ssg-item" href="users_profile_big.html">
                                <div class="item-media" style="background-image: url(img/avatar3.jpg)"></div>
                                <div class="item-name">
                                    Kim C<span>olli</span>ns
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="search-suggestions-group">
                    <div class="ssg-header">
                        <div class="ssg-icon">
                            <div class="os-icon os-icon-folder"></div>
                        </div>
                        <div class="ssg-name">
                            Files
                        </div>
                        <div class="ssg-info">
                            17 Total
                        </div>
                    </div>
                    <div class="ssg-content">
                        <div class="ssg-items ssg-items-blocks">
                            <a class="ssg-item" href="#">
                                <div class="item-icon">
                                    <i class="os-icon os-icon-file-text"></i>
                                </div>
                                <div class="item-name">
                                    Work<span>Not</span>e.txt
                                </div>
                            </a><a class="ssg-item" href="#">
                                <div class="item-icon">
                                    <i class="os-icon os-icon-film"></i>
                                </div>
                                <div class="item-name">
                                    V<span>ideo</span>.avi
                                </div>
                            </a><a class="ssg-item" href="#">
                                <div class="item-icon">
                                    <i class="os-icon os-icon-database"></i>
                                </div>
                                <div class="item-name">
                                    User<span>Tabl</span>e.sql
                                </div>
                            </a><a class="ssg-item" href="#">
                                <div class="item-icon">
                                    <i class="os-icon os-icon-image"></i>
                                </div>
                                <div class="item-name">
                                    wed<span>din</span>g.jpg
                                </div>
                            </a>
                        </div>
                        <div class="ssg-nothing-found">
                            <div class="icon-w">
                                <i class="os-icon os-icon-eye-off"></i>
                            </div>
                            <span>No files were found. Try changing your query...</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="layout-w">
            <!--------------------
        START - Mobile Menu
        -------------------->
            <div class="menu-mobile menu-activated-on-click color-scheme-dark">
                <div class="mm-logo-buttons-w">
                    <a class="mm-logo" href="/"><img src="/statics/images/logo.png"></a>
                    <div class="mm-buttons">
                        <div class="content-panel-open">
                            <div class="os-icon os-icon-grid-circles"></div>
                        </div>
                        <div class="mobile-menu-trigger">
                            <div class="os-icon os-icon-hamburger-menu-1"></div>
                        </div>
                    </div>
                </div>
                <div class="menu-and-user">
                    <!--------------------
                START - Mobile Menu List
                -------------------->
                    <ul class="main-menu">

                        <li class="">
                            <a href="/">
                                <div class="icon-w">
                                    <i class="picons-thin-icon-thin-0045_home_house"></i>
                                </div>
                                <span>Home</span>
                            </a>
                        </li>


                        <li class="">
                            <a href="/mock-tests">
                                <div class="icon-w">
                                    <div class="picons-thin-icon-thin-0003_write_pencil_new_edit"></div>
                                </div>
                                <span>Give Tests</span>
                            </a>
                        </li>




                        <li class="sub-header">
                            <span>User</span>
                        </li>

                        <li class=" has-sub-menu">
                            <a href="/account-general-settings">
                                <div class="icon-w">
                                    <div class="picons-thin-icon-thin-0303_notification_badge"></div>
                                </div>
                                <span>Account Settings</span>
                            </a>
                        </li>

                        <li class=" has-sub-menu">
                            <a href="/logout">
                                <div class="icon-w">
                                    <div class="picons-thin-icon-thin-0303_notification_badge"></div>
                                </div>
                                <span>Log out</span>
                            </a>
                        </li>



                    </ul>
                    <!--------------------
                END - Mobile Menu List
                -------------------->
                </div>
            </div>
            <!--------------------
        END - Mobile Menu
        -------------------->
            <!--------------------
        START - Main Menu
        -------------------->
            <div class="menu-w color-scheme-light color-style-transparent menu-position-side menu-side-left menu-layout-compact sub-menu-style-over sub-menu-color-bright selected-menu-color-light menu-activated-on-hover menu-has-selected-link">
                <div class="logo-w" style="display: block !important;">
                    <a class="logo" href="/">
                        <div class="logo-element"><img src="/statics/images/logo.png"></div>
                    </a>
                </div>

                <h1 class="menu-page-header">
                    Page Header
                </h1>
                <ul class="main-menu">

                    <li class="">
                        <a href="/">
                            <div class="icon-w">
                                <i class="picons-thin-icon-thin-0045_home_house"></i>
                            </div>
                            <span>Home</span>
                        </a>
                    </li>


                    <li class="">
                        <a href="/mock-tests">
                            <div class="icon-w">
                                <div class="picons-thin-icon-thin-0003_write_pencil_new_edit"></div>
                            </div>
                            <span>Manage Tests</span>
                        </a>
                    </li>

                    <li class="">
                        <a href="/mock-tests">
                            <div class="icon-w">
                                <div class="picons-thin-icon-thin-0003_write_pencil_new_edit"></div>
                            </div>
                            <span>View Tests</span>
                        </a>
                    </li>

                    <li class="sub-header">
                        <span>User</span>
                    </li>

                    <li class=" has-sub-menu">
                        <a href="/account-general-settings">
                            <div class="icon-w">
                                <div class="picons-thin-icon-thin-0303_notification_badge"></div>
                            </div>
                            <span>Account Settings</span>
                        </a>

                    </li>

                    <li class=" has-sub-menu">
                        <a href="/logout">
                            <div class="icon-w">
                                <div class="picons-thin-icon-thin-0303_notification_badge"></div>
                            </div>
                            <span>Log out</span>
                        </a>
                    </li>

                    <!--------- SUB MENU FOR ADMINS/MODERATORS ------->
                    <?php if ($user->isAdmin()) : ?>
                        <li class="sub-header">
                            <span>Admin Roles</span>
                        </li>



                        <li class=" has-sub-menu">
                            <a href="/manage-test">
                                <div class="icon-w">
                                    <div class="picons-thin-icon-thin-0290_phone_telephone_contact"></div>
                                </div>
                                <span>Give Tests</span>
                            </a>

                        </li>

                        <li class=" has-sub-menu">
                            <a href="#">
                                <div class="icon-w">
                                    <div class="picons-thin-icon-thin-0290_phone_telephone_contact"></div>
                                </div>
                                <span>Manage Locations</span>
                            </a>
                            <div class="sub-menu-w">
                                <div class="sub-menu-header">
                                    Manage Locations
                                </div>
                                <div class="sub-menu-icon">
                                    <i class="os-icon os-icon-mail"></i>
                                </div>
                                <div class="sub-menu-i">
                                    <ul class="sub-menu">

                                        <li>
                                            <a href="/add-states">Add States</a>
                                        </li>
                                        <li>
                                            <a href="/add-city">Add City</a>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </li>



                        <li class=" has-sub-menu">
                            <a href="/manage-user">
                                <div class="icon-w">
                                    <div class="picons-thin-icon-thin-0290_phone_telephone_contact"></div>
                                </div>
                                <span>View Students</span>
                            </a>
                        </li>



                    <?php endif; ?>

                    <!---------END SUB MENU FOR ADMINS/MODERATORS ------->

                </ul>



            </div>
            <!--------------------
        END - Main Menu
        -------------------->
            <div class="content-w">
                <!--------------------
            START - Top Bar
            -------------------->
                <div class="top-bar color-scheme-transparent">
                    <!--------------------
                START - Top Menu Controls
                -------------------->
                    <div class="top-menu-controls">
                        <div class="element-search autosuggest-search-activator">
                            <input placeholder="Start typing to search..." type="text">
                        </div>
                        <!--------------------
                    START - Messages Link in secondary top menu
                    -------------------->
                        <div class="messages-notifications os-dropdown-trigger os-dropdown-position-left">
                            <i class="os-icon os-icon-mail-14"></i>
                            <div class="new-messages-count">
                                12
                            </div>
                            <div class="os-dropdown light message-list">
                                <ul>
                                    <li>
                                        <a href="#">
                                            <div class="user-avatar-w">
                                                <img alt="" src="img/avatar1.jpg">
                                            </div>
                                            <div class="message-content">
                                                <h6 class="message-from">
                                                    John Mayers
                                                </h6>
                                                <h6 class="message-title">
                                                    Account Update
                                                </h6>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <div class="user-avatar-w">
                                                <img alt="" src="img/avatar2.jpg">
                                            </div>
                                            <div class="message-content">
                                                <h6 class="message-from">
                                                    Phil Jones
                                                </h6>
                                                <h6 class="message-title">
                                                    Secutiry Updates
                                                </h6>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <div class="user-avatar-w">
                                                <img alt="" src="img/avatar3.jpg">
                                            </div>
                                            <div class="message-content">
                                                <h6 class="message-from">
                                                    Bekky Simpson
                                                </h6>
                                                <h6 class="message-title">
                                                    Vacation Rentals
                                                </h6>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <div class="user-avatar-w">
                                                <img alt="" src="img/avatar4.jpg">
                                            </div>
                                            <div class="message-content">
                                                <h6 class="message-from">
                                                    Alice Priskon
                                                </h6>
                                                <h6 class="message-title">
                                                    Payment Confirmation
                                                </h6>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!--------------------
                    END - Messages Link in secondary top menu
                    -------------------->
                        <!--------------------
              START - Settings Link in secondary top menu
              -------------------->
                        <div class="top-icon top-settings os-dropdown-trigger os-dropdown-position-left">
                            <i class="os-icon os-icon-ui-46"></i>
                            <div class="os-dropdown">
                                <div class="icon-w">
                                    <i class="os-icon os-icon-ui-46"></i>
                                </div>
                                <ul>
                                    <li>
                                        <a href="users_profile_small.html"><i class="os-icon os-icon-ui-49"></i><span>Profile Settings</span></a>
                                    </li>
                                    <li>
                                        <a href="users_profile_small.html"><i class="os-icon os-icon-grid-10"></i><span>Billing Info</span></a>
                                    </li>
                                    <li>
                                        <a href="users_profile_small.html"><i class="os-icon os-icon-ui-44"></i><span>My Invoices</span></a>
                                    </li>
                                    <li>
                                        <a href="users_profile_small.html"><i class="os-icon os-icon-ui-15"></i><span>Cancel Account</span></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!--------------------
                    END - Settings Link in secondary top menu
                    -------------------->
                        <!--------------------
              START - User avatar and menu in secondary top menu
              -------------------->
                        <div class="logged-user-w">
                            <div class="logged-user-i">
                                <div class="avatar-w">
                                    <img alt="" src="/statics/images/avatar1.jpg">
                                </div>
                                <div class="logged-user-menu color-style-bright">
                                    <div class="logged-user-avatar-info">
                                        <div class="avatar-w">
                                            <img alt="" src="/statics/images/avatar1.jpg">
                                        </div>
                                        <div class="logged-user-info-w">
                                            <div class="logged-user-name">
                                                Maria Gomez
                                            </div>
                                            <div class="logged-user-role">
                                                Administrator
                                            </div>
                                        </div>
                                    </div>
                                    <div class="bg-icon">
                                        <i class="os-icon os-icon-wallet-loaded"></i>
                                    </div>
                                    <ul>
                                        <li>
                                            <a href="/account-general-settings"><i class="picons-thin-icon-thin-0055_settings_tools_configuration_preferences"></i><span>Account Setings</span></a>
                                        </li>
                                        <li>
                                            <a href="/logout"><i class="os-icon os-icon-signs-11"></i><span>Logout</span></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!--------------------
                    END - User avatar and menu in secondary top menu
                    -------------------->
                    </div>
                    <!--------------------
                END - Top Menu Controls
                -------------------->
                </div>
                <!--------------------
            END - Top Bar
            -------------------->
                <!--------------------
          START - Breadcrumbs
          -------------------->
                <!--            <ul class="breadcrumb">-->
                <!--                <li class="breadcrumb-item">-->
                <!--                    <a href="index.html">Home</a>-->
                <!--                </li>-->
                <!--                <li class="breadcrumb-item">-->
                <!--                    <a href="index.html">Products</a>-->
                <!--                </li>-->
                <!--                <li class="breadcrumb-item">-->
                <!--                    <span>Laptop with retina screen</span>-->
                <!--                </li>-->
                <!--            </ul>-->
                <!--------------------
            END - Breadcrumbs
            -------------------->
                <div class="content-panel-toggler">
                    <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                </div>
                <div class="content-i">
                    <div class="content-box">