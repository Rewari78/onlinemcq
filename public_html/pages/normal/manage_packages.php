<?php

$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );

/** @var User $user */
$packages = PackageManager::getInstance()->getAvlPackageLists();
$disabledPackages = PackageManager::getInstance()->getDisabledPackageIds();

$this->_addHeader();
@include $this->getPath() . DS . 'navs.top.php';
?>
    <div class="row">
        <div class="col-sm-8 col-xxxl-8">
            <div class="element-wrapper">
                <div class="element-actions">
                    <button class="btn btn-primary custom-button" data-target="#add-package-modal" data-toggle="modal" type="button">Add Package</button>
                </div>
                <h6 class="element-header">
                    Packages
                </h6>
                <div class="element-box">
                    <div class="table-responsive">
                        <?php if ( count($packages) > 0 ): ?>
                            <table class="table table-lightborder">
                                <thead>
                                <tr>
                                    <th>
                                        Package Name
                                    </th>
                                    <th class="text-left">
                                        Package Price
                                    </th>
                                    <th>
                                        Discount Price
                                    </th>
                                    <th>
                                        Actions
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ( $packages as $package ): ?>

                                    <tr>
                                        <td class="nowrap">
                                            <?php echo Util::htmlEncode($package['packageInfo']['packageName']); ?>
                                            <?php echo $package['packageInfo']['id'] == 1 ? '<span class="badge badge-success">Default</span>' : '' ?>
                                        </td>
                                        <td class="text-left">
                                            <?php echo '&#8377; ' . $package['packageInfo']['offPrice']; ?>
                                        </td>
                                        <td class="text-left">
                                            <?php echo '&#8377; ' . $package['packageInfo']['price']; ?>
                                        </td>
                                        <td class="row-actions text-left">
                                            <?php if ( isset($disabledPackages[$package['packageInfo']['id']]) ) : ?>
                                                <a href="/deactivate-or-activate-package/activate/<?php echo $package['packageInfo']['id'] ?>" title="Activate Package">
                                                    <i class="dripicons-checkmark"></i>
                                                </a>
                                            <?php else : ?>
                                                <a class="danger" href="/deactivate-or-activate-package/deactivate/<?php echo $package['packageInfo']['id'] ?>" title="Diactivate Package">
                                                    <i class="dripicons-cross"></i>
                                                </a>
                                            <?php endif; ?>
                                            <a class="danger" href="<?php echo '/edit-package/' . $package['packageInfo']['id'] ; ?>">
                                                <i class="dripicons-scale"></i>
                                            </a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>

                            </table>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-4 col-xxxl-4">
        </div>
    </div>


    <div aria-hidden="true" aria-labelledby="mySmallModalLabel" class="modal fade bd-example-modal-sm" id="add-package-modal" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        Add New Package
                    </h5>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                </div>
                <form action="#" method="post" id="add-package-form">
                    <div class="modal-body">

                        <div class="form-group">
                            <label for="package-name">Package Name</label>
                            <input class="form-control" placeholder="Type Package Name" type="text" id="package-name" name="package-name">
                        </div>

                        <div class="form-group">
                            <label for="package-price">Package Price</label>
                            <input min="0" class="form-control" placeholder="Type Package Price" type="number" id="package-price" name="package-price">
                        </div>

                        <div class="form-group">
                            <label for="discount-price">Discount Price</label>
                            <input min="0" class="form-control" placeholder="Type Discount Price" type="number" id="discount-price" name="discount-price">
                        </div>

                        <div class="alert alert-warning alert-dismissible d-none" role="alert" id="add-p-alert-box">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" data-dismiss="modal" type="button"> Close</button><button class="btn btn-primary" name="add-package-submit-btn" type="submit">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


<script type="text/javascript">
    (function (_scope) {

        var $form = $('#add-package-form');
        $form.on('submit', function (e) {
            e.preventDefault();
            e.stopPropagation();

            hideError();

            $.post(
                homeUrl + '/ajax/add-a-package',
                $(this).serialize(),
                function(data){
                    if (  data.info !== 'success' )
                    {
                        showError(data.message);
                        return;
                    }

                    window.location.reload();
                },
                'json'
            );

        });

        function showError( msg )
        {
            var alertBox = document.getElementById('add-p-alert-box');
            alertBox.innerHTML = msg;
            alertBox.className = alertBox.className.replace(/\bd-none\b/g, "");
        }

        function hideError()
        {
            var alert = document.getElementById('add-p-alert-box');
            alert.className += ' d-none ';
        }

    })(window);
</script>
<?php
@include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();
