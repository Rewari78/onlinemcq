<?php
$this->onlyLoggedInAllowed();


$logId = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
if ( empty($logId) ) throw new Error404;

$log = UserLog::getInstance()->getUserLogById($logId);

if ( empty($log) ) throw new Error404;

$ans = json_decode($log['text'], true);
$testId = $ans['testId'];
$ans = $ans['ans'];

$i = 1;
echo '<h2 style="text-align: center;margin-bottom: 50px;">MyRanks Question Solutions</h2>';
foreach ( $ans as $key => $an ){

    echo '<div style="background-color: #cdd3d9; padding: 20px; margin-bottom: 20px;">';
    $question = TestManager::getInstance()->getQuestion($testId, $key);

    echo "<div><p>{$question['question']}</p></div>";
//    var_dump($question);

    if ( $an == 1 ) {
        echo "<div><strong>&#x2714; This answer is correct</strong></div>";
    } else {

        echo "<div><strong>&#x274C; This answer is not correct</strong></div>";
        echo "<div>{$question['exp']}</div>";
    }

    $i++;
    echo '</div>';
}

echo '<p><a href="/">Back To Home</a></p>';