<?php
if ( !UserManager::getInstance()->isLoggedIn() ) $this->redirect('login');

// else if authenticated but domicile state is not defined.
/** @var User $user */
$user = $this->getUser();
if ( !empty($user->getExtraInfo()) ) $this->redirect('home');

// process the form data.
if ( $this->isPost() ) {
    $dState = filter_input(INPUT_POST, 'domicile-state');
    $SStatus = filter_input(INPUT_POST, '12th-status');
    $pcb = filter_input(INPUT_POST, 'pcb-percent');
}

$this->_addHeader();

$states = StateManager::getInstance()->getAllStates();
?>
<body>
<div class="all-wrapper menu-side with-pattern">
    <div class="auth-box-w wider">
        <div class="logo-w">
            <a href="index.html"><img alt="" src="img/logo-big.png"></a>
        </div>
        <h4 class="auth-header">
            Complete your account
        </h4>
        <form action="#" method="POST" id="complete-form">
            <div class="form-group">
                <label for="domicile-state">Select Your Domicile State</label>
                <select class="form-control" required="required" id="domicile-state" name="domicile-state" placeholder="Select">
                    <option value="0">Please Select</option>
                    <?php
                        foreach ( $states as $state )
                        {
                            echo ("
                                <option value=\"{$state['id']}\">
                                    {$state['name']}
                                </option>
                            ");
                        }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="12th-status">12th Appeared / Passout</label>
                <select class="form-control" required="required" id="12th-status" name="12th-status" placeholder="Select">
                    <option value="1">
                        Appeared
                    </option>
                    <option value="2">
                        Passout
                    </option>
                </select>
            </div>
            <div class="form-group">
                <label for="pcb-percent">PCB %</label>
                <input disabled="disabled" name="pcb-percent" id="pcb-percent" class="form-control" placeholder="Enter your PCB %" type="text">
            </div>

            <div class="alert alert-warning alert-dismissible d-none" role="alert" id="alert-box">
            </div>

            <div class="buttons">
                <button type="submit" id="complete-submit-btn" name="complete-submit-btn" class="btn btn-primary">Continue</button>
            </div>
        </form>
    </div>
</div>
</body>



<script type="text/javascript">
    $(function () {

        $(document).on('change', '#12th-status', function () {
            var value = $(this).val();
            if (  parseInt(value, 10) === 2 ) {
                $('#pcb-percent')[0].disabled = false;
            }else {
                $('#pcb-percent').val('')[0].disabled = true;
            }
        });

        $('#complete-form').on('submit', function (e) {
            e.stopPropagation();
            e.preventDefault();

            var dS = $('#domicile-state').val().trim(),
                s12 = $('#12th-status').val().trim(),
                pcb = $('#pcb-percent').val().trim();

            var $btn = $('#complete-submit-btn');

            hideError();

            $.ajax({
                url: homeUrl + '/ajax/validate-complete-profile',
                dataType: 'JSON',
                type: "POST",
                data: {
                    dS: dS,
                    s12: s12,
                    pcb: pcb
                },
                beforeSend: function () {
                    $btn[0].disabled = true;
                },
                success: function ( ajaxD ) {
                    if ( ajaxD.info !== 'success' ) {
                        showError(ajaxD.message);
                        return false;
                    }

                    window.location.href = ajaxD.message.redirectUrl;

                },
                error: function () {

                },
                complete: function () {
                    $btn[0].disabled = false;
                }
            });

        });

        function showError( msg )
        {
            var alertBox = document.getElementById('alert-box');
            alertBox.innerHTML = msg;
            alertBox.className = alertBox.className.replace(/\bd-none\b/g, "");
        }

        function hideError()
        {
            var alert = document.getElementById('alert-box');
            alert.className += ' d-none ';
        }

    });
</script>

<?php
$this->_addFooter();