<?php

/**
 * Created by PhpStorm.
 * User: akash
 * Date: 18/12/18
 * Time: 8:59 PM
 */

$this->onlyLoggedInAllowed(UserManager::USER_TYPE_TEACHER);



$testId = filter_input(INPUT_GET, 'test-id', FILTER_SANITIZE_NUMBER_INT);
$questionId = filter_input(INPUT_GET, 'question-id', FILTER_SANITIZE_NUMBER_INT);


// $testId = empty($testId) ? 0 : $testId;

$testInfo = TestManager::getInstance()->getTestInfo($testId);

if (empty($testInfo)) throw new Error404;


if (TestManager::getInstance()->questionsExists($testId)) {
    echo 'Questions exists';
} else {
    echo 'Questions dont exists';
}

// $allQuestionInfo = TestManager::getInstance()->getAllQuestions($testInfo['id']);

// $questionId = empty($questionId) || count($allQuestionInfo) + 1 < $questionId ? 1 : $questionId;



if ($this->isPost()) {
    $question = filter_input(INPUT_POST, 'ckeditor1');
    $ans1 = filter_input(INPUT_POST, 'ckeditor2');
    $ans2 = filter_input(INPUT_POST, 'ckeditor3');
    $ans3 = filter_input(INPUT_POST, 'ckeditor4');
    $ans4 = filter_input(INPUT_POST, 'ckeditor5');
    $ans5 = filter_input(INPUT_POST, 'ckeditor6');
    $exp = filter_input(INPUT_POST, 'ckeditor7');
    $cor = filter_input(INPUT_POST, 'corr', FILTER_SANITIZE_NUMBER_INT);
    $cor = $cor >= 1 && $cor <= 5 ? $cor : 1;

    if (!empty($question)) {
        if (!empty($ans1) || !empty($ans2) || !empty($ans3) || !empty($ans4) || !empty($ans5) || !empty($exp)) {

            TestManager::getInstance()->updateQuestion(
                $questionId,
                $testId,
                $question,
                $ans1,
                $ans2,
                $ans3,
                $ans4,
                $ans5,
                $cor,
                $exp
            );

            $allQuestionInfo = TestManager::getInstance()->getAllQuestions($testInfo['id']);
        }
    }
}

// $questionInfo = TestManager::getInstance()->getQuestion($testInfo['id'], $questionId);
// $question = isset($questionInfo['question']) ? $questionInfo['question'] : "";
// $ans1 = isset($questionInfo['ans1']) ? $questionInfo['ans1'] : "";
// $ans2 = isset($questionInfo['ans2']) ? $questionInfo['ans2'] : "";
// $ans3 = isset($questionInfo['ans3']) ? $questionInfo['ans3'] : "";
// $ans4 = isset($questionInfo['ans4']) ? $questionInfo['ans4'] : "";
// $ans5 = isset($questionInfo['ans5']) ? $questionInfo['ans5'] : "";
// $exp = isset($questionInfo['exp']) ? $questionInfo['exp'] : "";
// $cor = isset($questionInfo['correctAns']) ? $questionInfo['correctAns'] : "";

// $maxQuestionId = count($allQuestionInfo) + 1;

$this->_addHeader();
// @include $this->getPath() . DS . 'navs.top.php';
?>
<div class="row">
    <div class="col-sm-12 col-xxxl-12">
        <div class="element-wrapper">

            <h6 class="element-header">
                Manage Paper Name
            </h6>
            <div class="element-box">
                <div class="row">
                    <div class="col-sm-20">
                        <p>Jump to question <input type="number" min="1" max="<?php echo $maxQuestionId; ?>" value="<?php echo $questionId; ?>" /> / <?php echo $maxQuestionId - 1; ?></p>
                    </div>
                </div>
            </div>

            <form method="post" action="#">

                <div class="element-box">
                    <div class="row">
                        <div class="col-sm-12">
                            <legend><span>Write Question</span></legend>
                            <textarea cols="80" id="ckeditor1" name="ckeditor1" rows="10"><?php echo htmlentities($question); ?></textarea>
                        </div>
                    </div>
                </div>

                <div class="element-box">
                    <div class="row">
                        <div class="col-sm-12">
                            <legend><span>Answer 1</span></legend>
                            <textarea cols="80" id="ckeditor2" name="ckeditor2" rows="10"><?php echo htmlentities($ans1); ?></textarea>
                            <div class="form-check pt-3">
                                <label class="form-check-label">
                                    <input class="form-check-input" style="margin-top: 1.5px;" type="radio" <?php echo $cor == 1 ? 'checked' : ''; ?> name="corr" value="1">Mark this answer as Correct</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="element-box">
                    <div class="row">
                        <div class="col-sm-12">
                            <legend><span>Answer 2</span></legend>
                            <textarea cols="80" id="ckeditor3" name="ckeditor3" rows="10"><?php echo htmlentities($ans2); ?></textarea>
                            <div class="form-check pt-3">
                                <label class="form-check-label">
                                    <input class="form-check-input" style="margin-top: 1.5px;" type="radio" <?php echo $cor == 2 ? 'checked' : ''; ?> name="corr" value="2">Mark this answer as Correct</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="element-box">
                    <div class="row">
                        <div class="col-sm-12">
                            <legend><span>Answer 3</span></legend>
                            <textarea cols="80" id="ckeditor4" name="ckeditor4" rows="10"><?php echo htmlentities($ans3); ?></textarea>
                            <div class="form-check pt-3">
                                <label class="form-check-label">
                                    <input class="form-check-input" style="margin-top: 1.5px;" type="radio" <?php echo $cor == 3 ? 'checked' : ''; ?> name="corr" value="3">Mark this answer as Correct</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="element-box">
                    <div class="row">
                        <div class="col-sm-12">
                            <legend><span>Answer 4</span></legend>
                            <textarea cols="80" id="ckeditor5" name="ckeditor5" rows="10"><?php echo htmlentities($ans4); ?></textarea>
                            <div class="form-check pt-3">
                                <label class="form-check-label">
                                    <input class="form-check-input" style="margin-top: 1.5px;" type="radio" <?php echo $cor == 4 ? 'checked' : ''; ?> name="corr" value="4">Mark this answer as Correct</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="element-box">
                    <div class="row">
                        <div class="col-sm-12">
                            <legend><span>Answer 5</span></legend>
                            <textarea cols="80" id="ckeditor6" name="ckeditor6" rows="10"><?php echo htmlentities($ans5); ?></textarea>
                            <div class="form-check pt-3">
                                <label class="form-check-label">
                                    <input class="form-check-input" style="margin-top: 1.5px;" type="radio" <?php echo $cor == 5 ? 'checked' : ''; ?> name="corr" value="5">Mark this answer as Correct</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="element-box">
                    <div class="row">
                        <div class="col-sm-12">
                            <legend><span>Correct Ans Explaination</span></legend>
                            <textarea cols="80" id="ckeditor7" name="ckeditor7" rows="10"><?php echo htmlentities($exp); ?></textarea>
                        </div>
                    </div>
                </div>

                <div class="element-box">
                    <div class="row">
                        <div class="col-sm-12">
                            <?php if ($questionId != 1) : ?>
                                <a class="btn btn-primary" href="/edit-questions?test-id=<?php echo $testId ?>&question-id=<?php echo $questionId - 1 ?>">Previous</a>
                            <?php endif; ?>
                            <button class="btn btn-primary" type="submit"> Save </button>
                            <?php if ($questionId < $maxQuestionId) : ?>
                                <a class="btn btn-primary" href="/edit-questions?test-id=<?php echo $testId ?>&question-id=<?php echo $questionId + 1 ?>">Next</a>
                            <?php endif; ?>
                            <a class="btn btn-primary" href="/manage-test">Finish</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        // Set the most common block elements.
        CKEDITOR.config.format_tags = 'p;h1;h2;h3;pre;img';
        CKEDITOR.config.extraPlugins = 'uploadimage,uploadwidget,filebrowser';
        CKEDITOR.config.filebrowserUploadUrl = '/ajax/upload-file/';
        CKEDITOR.replace('ckeditor2');
        CKEDITOR.replace('ckeditor3');
        CKEDITOR.replace('ckeditor4');
        CKEDITOR.replace('ckeditor5');
        CKEDITOR.replace('ckeditor6');
        CKEDITOR.replace('ckeditor7');
    });
</script>
<?php
@include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();
