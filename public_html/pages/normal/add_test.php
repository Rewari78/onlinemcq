<?php

/**
 * Created by PhpStorm.
 * User: akash
 * Date: 3/11/18
 * Time: 12:03 PM
 */
$this->onlyLoggedInAllowed(UserManager::USER_TYPE_TEACHER);

$subjects = array(
    'History',
    'Math',
    'Science',
    'Geography'
);

// set post fields
$post = json_encode(array(
    "adminLevel" => "3",
    "groupId" => "custom",
    "groupName" => "Custom Classes",
    "instDbValue" => $_SESSION['schoolId'],
    "pId" => $_SESSION['pid']
));

$ch = curl_init('http://pinnacleapp.in/MobileApp/rest/adminAlert/getAlertDetails');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
$json_string = curl_exec($ch);
curl_close($ch);


// $allPackages = PackageManager::getInstance()->getAvlPackageLists();

// $testId = filter_input(INPUT_GET, 'test-id', FILTER_SANITIZE_NUMBER_INT);
// $testId = empty($testId) ? null : $testId;
// $testInfo = TestManager::getInstance()->getTestInfo($testId);

// if (empty($testInfo) && is_numeric($testId)) throw new Error404;

// if ($this->isPost()) {


//     var_dump($_POST);
//     exit;

//     $testName = filter_input(INPUT_POST, 'test-name');
//     $subject = filter_input(INPUT_POST, 'subject', FILTER_SANITIZE_NUMBER_INT);
//     $selectPackage = 1;
//     $marks = filter_input(INPUT_POST, 'total-marks', FILTER_SANITIZE_NUMBER_INT);
//     $time = filter_input(INPUT_POST, 'total-time', FILTER_SANITIZE_NUMBER_INT);

//     $testName = empty($testName) ? "Unknown" : $testName;

//     // Now validate the subject.
//     switch ($subject) {
//         case TestManager::SUBJECT_CHEM:
//         case TestManager::SUBJECT_PHY:
//         case TestManager::SUBJECT_BIO:
//             break;
//         default:
//             $subject = TestManager::SUBJECT_BIO;
//     }

//     // Now select package
//     $selectPackage = empty(PackageManager::getInstance()->getAPackageById($selectPackage)) ? 1 : $selectPackage;
//     $marks = empty($marks) ? 100 : $marks;
//     $time = empty($time) ? 60 : $time;

//     if (!empty($testInfo)) {
//         TestManager::getInstance()->updateTest($testInfo['id'], $testName, $subject, $selectPackage, $marks, $time);
//     } else {
//         TestManager::getInstance()->createTest($testName, $subject, $selectPackage, $marks, $time);
//     }


//     $this->redirect('manage-test?added-new=1');
// }

// $testName = isset($testInfo['title']) ? $testInfo['title'] : "";
// $subject = isset($testInfo['subject']) ? $testInfo['subject'] : "";
// $packageId = isset($testInfo['packageId']) ? $testInfo['packageId'] : "";
// $totalMarks = isset($testInfo['totalMarks']) ? $testInfo['totalMarks'] : "100";
// $totalTime = isset($testInfo['totalTime']) ? $testInfo['totalTime'] : "";

$this->_addHeader();
?>

<!-- <?php if (!empty($msg)) : ?>
    <div class="alert alert-success alert-dismissible " role="alert" area-label="Close" class="close">
        <?php echo $msg; ?>
        <button aria-label="close" class="close" data-dismiss="alert" type="button"><span aria-hidden="true">&times;</span> </button>
    </div>
<?php endif; ?> -->


<div class="container d-flex h-100 flex-column">
    <div class="row h-100 justify-content-center align-items-center">
        <div class="col-md-4 m-auto menu_box">
            <form id="add_test_form" method="POST">
                <h3 class="text-center text-uppercase pt-2 pb-3">Add a new test</h3>
                <div class="alert alert-danger alert-dismissible" role="alert" area-label="Close" class="close">
                </div>
                <div class="form-group">
                    <label for="test-subject">Subject</label>
                    <input class="form-control" id="test-subject" name="test-subject" placeholder="Give Subject" required>
                </div>
                <div class="form-group">
                    <label for="test-name">Select Class</label>
                    <select class="form-control" id="test-class" name="test-class" required>
                        <option selected disabled value="">Select Class</option>
                    </select>
                </div>
                <div class="form-group text-center" id="divisions_list">
                    <label for="test-div">Select Division</label><br>
                </div>
                <div class="form-group">
                    <label for="test-desc">Test Description</label>
                    <input class="form-control" id="test-desc" name="test-desc" placeholder="Test Description" required>
                </div>
                <div class="form-group">
                    <label for="total-time">Start From</label>
                    <input type="datetime-local" class="form-control" id="total-start" name="test-start" min="<?php echo date('Y-m-d') . 'T' . date('h:i') ?>" autocomplete="off" required />
                </div>
                <div class="form-group">
                    <label for="total-time">Total Time (In minutes Eg. 90)</label>
                    <input type="number" class="form-control" id="total-time" name="test-time" autocomplete="off" required />
                </div>
                <div class="form-group">
                    <button id="form_submit_button" type="submit" class="btn btn-primary btn-block btn-lg">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>


<script type="text/javascript">
    var apiResponse = <?php echo $json_string; ?>;
    console.log(apiResponse);
    $(".alert-danger").hide();
    $(document).ready(function() {
        $.each(apiResponse.classList, function(key, value) {
            $("#test-class").append(
                $('<option></option>')
                .attr('value', value)
                .text(value)
            );
        });

        $("#test-class").on('change', function() {
            $(".form-check").remove();

            var divisions = [];
            for (var i = 1; i <= Object.keys(apiResponse.classDivList).length; i++) {
                if (apiResponse.classDivList[i].className === this.value) divisions.push(apiResponse.classDivList[i]);
            }

            if (divisions.length) {
                $.each(divisions, function(index) {
                    var divisionCheckbox = '<div class="form-check form-check-inline">' +
                        '<input class="form-check-input"  type="checkbox" id="inlineCheckbox' + index + '" name="test-divs[]" value=' + JSON.stringify(divisions[index]) + ' onclick="deRequireCb(\'form-check-input\')" required>' +
                        '<label class="form-check-label" for="inlineCheckbox' + index + '">' + divisions[index].divisionName + '</label>' +
                        '</div>';
                    $("#divisions_list").append(divisionCheckbox);
                    $("#divisions_list").show();
                });
            }


        });

        $("#add_test_form").submit(function(e) {
            e.preventDefault();


            $.ajax({
                cache: false,
                url: homeUrl + '/ajax/add-test-process',
                dataType: 'JSON',
                type: "POST",
                data: $("#add_test_form").serialize(),
                success: function(data) {
                    if (data.success) {

                        window.location = homeUrl + '/edit-questions?test-id=' + data.testId + '&question-id=1'

                    } else {
                        $(".alert-danger").html('Please fill the form correctly. <button aria-label="close" class="close" data-dismiss="alert" type="button"><span aria-hidden="true">&times;</span> </button>');
                        $(".alert-danger").show();
                    }

                },
            })


        });



    });


    function deRequireCb(elClass) {
            el=document.getElementsByClassName(elClass);

            var atLeastOneChecked=false;//at least one cb is checked
            for (i=0; i<el.length; i++) {
                if (el[i].checked === true) {
                    atLeastOneChecked=true;
                }
            }

            if (atLeastOneChecked === true) {
                for (i=0; i<el.length; i++) {
                    el[i].required = false;
                }
            } else {
                for (i=0; i<el.length; i++) {
                    el[i].required = true;
                }
            }
        }





    // $(function() {
    //     $('#custom-date-time-pick, #custom-date-time-pick1').datetimepicker({});
    // });
</script>
<?php
// @include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();
