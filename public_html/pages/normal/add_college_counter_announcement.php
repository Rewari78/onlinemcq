<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 3/11/18
 * Time: 12:03 PM
 */
$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );

$states = StateManager::getInstance()->getAllStates();
$collegeTypes = CollegeManager::getInstance()->getAvlCollegeTypes();

$this->_addHeader();
@include $this->getPath() . DS . 'navs.top.php';
?>
    <form method="post" action="#" onsubmit="submitForm(this); return false;">
        <div class="row">

            <div class="col-sm-8 col-xxxl-8">
                <div class="element-wrapper">
                    <h6 class="element-header">
                        Add College Counter Announcement
                    </h6>
                    <div class="element-box">

                        <legend>
                            <span>Dashboard Announcement</span>
                        </legend>

                        <div class="form-group">
                            <label for="select-states">Select State</label>

                            <select class="form-control" id="select-states" name="select-state">

                                <option value="0">Please Select</option>
                                <?php foreach ( $states as $state ): ?>
                                    <option value="<?php echo $state['id']; ?>"><?php echo $state['name']; ?></option>
                                <?php endforeach; ?>

                            </select>
                        </div>

                        <div class="form-group">
                            <label for="select-college-type">College Type</label>
                            <select class="form-control" id="select-college-type" name="select-college-type">
                                <option value="0">
                                    Please Select
                                </option>

                                <?php foreach ( $collegeTypes as $collegeType ): ?>
                                    <option value="<?php echo $collegeType['id']; ?>">
                                        <?php echo $collegeType['collegeType']; ?>
                                    </option>
                                <?php endforeach; ?>

                            </select>
                        </div>

                        <div class="form-group">
                            <label for="select-college">Select College</label>
                            <select class="form-control" id="select-college" name="select-college" disabled>
                                <option value="0">
                                    Please Select
                                </option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="select-study-type">Study Type</label>
                            <select class="form-control" id="select-study-type" name="select-study-type" disabled>
                                <option value="0">
                                    Please Select
                                </option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="">Course</label>
                            <select class="form-control" id="select-course-type" name="select-course-type" disabled>

                                <option value="0">
                                    Please Select
                                </option>

                            </select>
                        </div>

                        <div class="form-group">
                            <label for="">Choose Branch</label>
                            <select class="form-control" id="select-branch-type" name="select-branch-type" disabled>

                                <option value="0">
                                    Please Select
                                </option>

                            </select>
                        </div>

                        <div class="form-group">
                            <label for="notifi-title">Announcement Title</label>
                            <input class="form-control" placeholder="Title" type="text" id="notifi-title" name="notifi-title" autocomplete="off">
                        </div>

                        <div class="row">
                            <div class="col-sm-6 col-xxxl-6">
                                <div class="form-group">
                                    <label for="custom-date-time-pick">Start</label>
                                    <input type='text' class="form-control" id="custom-date-time-pick" name="custom-date-time-pick" autocomplete="off"/>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xxxl-6">
                                <div class="form-group">
                                    <label for="custom-date-time-pick1">End</label>
                                    <input type='text' class="form-control" id="custom-date-time-pick1" name="custom-date-time-pick1" autocomplete="off"/>
                                </div>
                            </div>
                        </div>

                        <label for="ckeditor1">Message</label>
                        <textarea cols="80" id="ckeditor1" name="ckeditor1" rows="10">
                        </textarea>

                        <div class="row pt-3">
                            <div class="col-sm-6 col-xxxl-6">
                                <div class="form-group">
                                    <label for="link">Link</label>
                                    <input class="form-control" placeholder="Link" type="text" id="link" name="link">
                                </div>
                            </div>
                            <div class="col-sm-6 col-xxxl-6">
                                <div class="form-group">
                                    <label for="custom-text">Custom Text</label>
                                    <input class="form-control" placeholder="Custom Text" type="text" id="custom-text" name="custom-text">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 col-xxxl-6">
                                <legend>
                                    <span>SMS Announcement</span>
                                </legend>
                                <div class="form-group">
                                    <label for="sms-sender-id"> Select Sender ID</label>
                                    <select class="form-control" id="sms-sender-id" name="sms-sender-id">
                                        <option value="0">
                                            Please Select
                                        </option>
                                        <option>
                                            ASDFGH
                                        </option>
                                        <option>
                                            ZXCVBN
                                        </option>
                                        <option>
                                            QWERTY
                                        </option>
                                    </select>
                                </div>
                                <div class="form-group" >
                                    <label for="sms-text">Example textarea</label>
                                    <textarea class="form-control" rows="3" id="sms-text" name="sms-text"></textarea>
                                </div>
                            </div>





                            <div class="col-sm-6 col-xxxl-6">
                                <legend><span>Email Announcement</span></legend>
                                <div class="form-group">
                                    <label for="email-subject">Subject</label>
                                    <input class="form-control" placeholder="Subject" type="text" id="email-subject" name="email-subject">
                                </div>
                                <div class="form-group">
                                    <label for="email-from">From</label>
                                    <input class="form-control" placeholder="From" type="text" id="email-from" name="email-from">
                                </div>
                                <div class="form-group">
                                    <label for="email-text">Example textarea</label>
                                    <textarea class="form-control" rows="3" id="email-text" name="email-text">
                                    </textarea>
                                </div>
                            </div>
                        </div>


                        <div class="alert alert-warning alert-dismissible d-none" role="alert">
                        </div>

                        <div class="form-buttons-w text-right">
                            <button class="btn btn-primary custom-button" data-target="#avl-var-modal" data-toggle="modal" type="button">Available Variable</button>
                            <button class="btn btn-primary" type="submit">Submit Form</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-4 col-xxxl-4">
            </div>

        </div>
    </form>

<script type="text/javascript">

    var $states = $('#select-states');

    $(document).on('change', '#select-college-type', function (e) {

        var value = $states.val().trim();

        if ( parseInt(value, 10) === 0 ) {
            $('#select-college')[0].disabled = true;
            $('#select-study-type')[0].disabled = true;
            $('#select-course-type')[0].disabled = true;
            $('#select-branch-type')[0].disabled = true;
            return;
        }

        $.post(
            homeUrl + '/ajax/college-counter-announcement',
            { rq: 'getCollege', stateId: value },
            function(data) {

                var html = '<option value="0">Please Select</option>';
                if ( data.info !== 'success' )
                {
                    $('#select-college')[0].disabled = true;
                    return;
                }

                var i = 0;
                for( ; i < data.message.length; i++ )
                {
                    html += '<option value="' + data.message[i].id + '">' + data.message[i].collegeName + '</option>';
                }

                $('#select-college').html(html)[0].disabled = false;

            },
            'json'
        );

    });

    $(document).on('change', '#select-college', function (e) {

        var value = $(this).val().trim();

        if ( parseInt(value, 10) === 0 ) {
            $('#select-study-type')[0].disabled = true;
            $('#select-course-type')[0].disabled = true;
            $('#select-branch-type')[0].disabled = true;
            return;
        }

        $.post(
            homeUrl + '/ajax/college-counter-announcement',
            { rq: 'getStudyType', colTypeId: value },
            function(data) {

                var html = '<option value="0">Please Select</option>';
                if ( data.info !== 'success' )
                {
                    $('#select-study-type').html(html)[0].disabled = true;
                    return;
                }

                var i = 0;
                for( ; i < data.message.length; i++ )
                {
                    html += '<option value="' + data.message[i].id + '">' + data.message[i].studyType + '</option>';
                }

                $('#select-study-type').html(html)[0].disabled = false;

            },
            'json'
        );

    });

    $(document).on('change', '#select-study-type', function (e) {

        var value = $(this).val().trim();

        if ( parseInt(value, 10) === 0 ) {
            $('#select-course-type')[0].disabled = true;
            $('#select-branch-type')[0].disabled = true;
            return;
        }

        $.post(
            homeUrl + '/ajax/college-counter-announcement',
            { rq: 'getCourseType', studyTypeId: value },
            function(data) {

                var html = '<option value="0">Please Select</option>';
                if ( data.info !== 'success' )
                {
                    $('#select-course-type').html(html)[0].disabled = true;
                    return;
                }

                var i = 0;
                for( ; i < data.message.length; i++ )
                {
                    html += '<option value="' + data.message[i].id + '">' + data.message[i].courseType + '</option>';
                }

                $('#select-course-type').html(html)[0].disabled = false;

            },
            'json'
        );

    });

    $(document).on('change', '#select-course-type', function (e) {

        var value = $(this).val().trim();

        if ( parseInt(value, 10) === 0 ) {
            $('#select-branch-type')[0].disabled = true;
            return;
        }

        $.post(
            homeUrl + '/ajax/college-counter-announcement',
            { rq: 'getBranchType', courseTypeId: value },
            function(data) {

                var html = '<option value="0">Please Select</option>';
                if ( data.info !== 'success' )
                {
                    $('#select-branch-type').html(html)[0].disabled = true;
                    return;
                }

                var i = 0;
                for( ; i < data.message.length; i++ )
                {
                    html += '<option value="' + data.message[i].id + '">' + data.message[i].branchType + '</option>';
                }

                $('#select-branch-type').html(html)[0].disabled = false;

            },
            'json'
        );

    });

    $(document).on('change', '#select-branch-type', function (e) {

        var value = $(this).val().trim();

    });

    function submitForm( form )
    {
        var $form = $(form);
        var $alert = $form.find('.alert');
        var _data = $(form).serialize();

        hideError($alert);

        $.post(
            homeUrl + '/ajax/submit-college-counter-announcement',
            $form.serialize(),
            function (data) {
                if ( data.info !== 'success' )
                {
                    showError($alert, data.message);
                    return;
                }

                window.location.href = '/college-counter-announcement';

            },
            'json'
        );
    }

    function showError( box, msg )
    {

        var $alertBox = $(box);
        $alertBox.html(msg);
        $alertBox.removeClass('d-none');
    }

    function hideError( box )
    {
        var $alertBox = $(box);
        $alertBox.addClass('d-none');
    }

    $(function () {
        $('#custom-date-time-pick, #custom-date-time-pick1').datetimepicker({});
    });
</script>
<?php
@include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();
