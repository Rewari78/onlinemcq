<?php
if ( !UserManager::getInstance()->isLoggedIn() ) $this->redirect('login');

// else if authenticated but domicile state is not defined.
/** @var User $user */
$user = $this->getUser();
if ( !empty($user->getExtraInfo()) ) $this->redirect('home');

// process the form data.
if ( $this->isPost() ) {
    $dState = filter_input(INPUT_POST, 'domicile-state');
    $SStatus = filter_input(INPUT_POST, '12th-status');
    $pcb = filter_input(INPUT_POST, 'pcb-percent');
}

$this->_addHeader();

$states = StateManager::getInstance()->getAllStates();
//$studyTypes = StateManager::getInstance()->
?>
    <body style="padding: 0px;">
    <div class="container-fluid cus-log-bg">
        <div class="row add-class">
            <div class="col-sm-3 col-xxxl-3">
            </div>
            <div class="col-sm-6 col-xxxl-6 lg-dv">
                <h6 class="custm-ele-hdr lg-pg">
                    Complete Your Profile
                </h6>
                <div class="custm-ele-hdr-strk lg-pg-strk">
                </div>
                <div class="alert alert-warning d-none" id="alert-box">
                </div>
                <div class="cus-log-sec">
                    <div class="cus-log-inp">
                        <form action="#" method="post" id="complete-form">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="domicile-state">Select Your Domicile State</label>
                                        <select class="form-control custom-select" required="required" id="domicile-state" name="domicile-state">
                                            <option value="0">Please Select</option>
                                            <?php
                                                foreach ( $states as $state )
                                                {
                                                    echo ("
                                                        <option value=\"{$state['id']}\">
                                                            {$state['name']}
                                                        </option>
                                                    ");
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6" style="display: none;">
                                    <div class="form-group">
                                        <label for="">Select Study Type</label>
                                        <select class="form-control custom-select">
                                            <option>
                                                Medical
                                            </option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="12th-status">12th Appeared / Passout</label>
                                        <select class="form-control custom-select" required="required" id="12th-status" name="12th-status" placeholder="Select">
                                            <option value="1">
                                                Appeared
                                            </option>
                                            <option value="2">
                                                Passout
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6" style="display: none;">
                                    <div class="form-group">
                                        <label for="">Select Course Type</label>
                                        <select class="form-control custom-select" id="course-type" name="course-type">
                                            <option value="1">
                                                MBBS
                                            </option>
                                            <option value="2">
                                                BDS
                                            </option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-sm-6" style="display: none;">
                                    <div class="">
                                        <label for="pcb-percent">PCB %</label>
                                        <input
                                            class=""
                                            placeholder="Enter your PCB %"
                                            type="hidden"
                                            id="pcb-percent"
                                            name="pcb-percent"
                                            value="56"
                                            disabled >
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for=""> Date of Birth</label>
                                        <div class="date-input">
                                            <input
                                                class="single-daterange form-control"
                                                placeholder="Date of birth"
                                                type="text"
                                                id="date-of-birth"
                                                name="date-of-birth"
                                                value="04/12/1978" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="cus-login-bt" id="complete-submit-btn">Continue <span>&#xE72A;</span></button>
                        </form>
                    </div>
                </div>
                <div class="col-sm-4 col-xxxl-4">
                </div>
            </div>
            <div class="col-sm-3 col-xxxl-3">
            </div>
        </div>
    </div>




    <script src="<?php echo SITE_URL ?>/statics/bower_components/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/js/jquery.datetimepicker.full.min.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/moment/moment.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/chart.js/dist/Chart.min.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/jquery-bar-rating/dist/jquery.barrating.min.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/ckeditor/ckeditor.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap-validator/dist/validator.min.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/ion.rangeSlider/js/ion.rangeSlider.min.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/dropzone/dist/dropzone.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/editable-table/mindmup-editabletable.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/tether/dist/js/tether.min.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/slick-carousel/slick/slick.min.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap/js/dist/util.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap/js/dist/alert.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap/js/dist/button.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap/js/dist/carousel.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap/js/dist/collapse.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap/js/dist/dropdown.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap/js/dist/modal.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap/js/dist/tab.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap/js/dist/tooltip.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap/js/dist/popover.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/js/demo_customizer.js?version=4.4.0"></script>
    <script src="<?php echo SITE_URL ?>/statics/js/main.js?version=4.4.0"></script>
    <script src="<?php echo SITE_URL ?>/statics/js/jquery-ui.min.js"></script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-XXXXXXX-9', 'auto');
        ga('send', 'pageview');
    </script>

    </body>


    <script type="text/javascript">
    $(function () {

        $(document).on('selectmenuchange', '#12th-status', function () {
            var value = $(this).val();
            if (  parseInt(value, 10) === 2 ) {
                $('#pcb-percent')[0].disabled = false;
            }else {
                $('#pcb-percent').val('')[0].disabled = true;
            }
        });

        $('#complete-form').on('submit', function (e) {
            e.stopPropagation();
            e.preventDefault();

            var dS = $('#domicile-state').val().trim(),
                s12 = $('#12th-status').val().trim(),
                pcb = $('#pcb-percent').val().trim(),
                course = $('#course-type').val().trim(),
                dateofbirth = $('#date-of-birth').val().trim();

            var $btn = $('#complete-submit-btn');

            hideError();

            $.ajax({
                url: homeUrl + '/ajax/validate-complete-profile',
                dataType: 'JSON',
                type: "POST",
                data: {
                    dS: dS,
                    s12: s12,
                    pcb: pcb,
                    course: course,
                    dob: dateofbirth
                },
                beforeSend: function () {
                    $btn[0].disabled = true;
                },
                success: function ( ajaxD ) {
                    if ( ajaxD.info !== 'success' ) {
                        showError(ajaxD.message);
                        return false;
                    }

                    window.location.href = ajaxD.message.redirectUrl;

                },
                error: function () {

                },
                complete: function () {
                    $btn[0].disabled = false;
                }
            });

        });

        function showError( msg )
        {
            var alertBox = document.getElementById('alert-box');
            alertBox.innerHTML = msg;
            alertBox.className = alertBox.className.replace(/\bd-none\b/g, "");
        }

        function hideError()
        {
            var alert = document.getElementById('alert-box');
            alert.className += ' d-none ';
        }

    });
</script>
    <script type="text/javascript">
        $('.custom-select').selectmenu();
    </script>
<?php
$this->_addFooter();