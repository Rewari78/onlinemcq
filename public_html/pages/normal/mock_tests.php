<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 18/12/18
 * Time: 8:59 PM
 */

$this->onlyLoggedInAllowed( UserManager::USER_TYPE_SUBSCRIBER , PackageManager::FEATURE_TEST_SERIES );

$allTests = TestManager::getInstance()->getAllTests();

$allPhyTests = array();
$allBioTests = array();
$allChemTests = array();

foreach ( $allTests as $allTest )
{

    switch ( $allTest['subject'] )
    {
        case TestManager::SUBJECT_PHY:
            $allPhyTests[] = $allTest;
            break;
        case TestManager::SUBJECT_BIO:
            $allBioTests[] = $allTest;
            break;
        case TestManager::SUBJECT_CHEM:
            $allChemTests[] = $allTest;
            break;
    }
}

$this->_addHeader();
@include $this->getPath() . DS . 'navs.top.php';
?>
    <div class="row">
        <div class="col-sm-12 col-xxxl-12">
            <div class="element-wrapper">
                <h6 class="custm-ele-hdr">
                    Online Exams
                </h6>
                <div class="custm-ele-hdr-strk">
                </div>
                <div class="element-content">
                    <p>Select the exam you want to give and give.</p>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-sm-12">
            <div class="row">

                <div class="col-sm-12">
                    <?php if ( empty($allBioTests) ): ?>
                        <p>
                            No test available yet for you.
                        </p>
                    <?php else: ?>
                        <form method="get" action="/take-test">
                            <div class="form-group">
                                <label for=""> Select Test</label>
                                <select class="form-control ext-cntr custom-select" name="test-id">
                                    <?php foreach ( $allBioTests as $allBioTest ): ?>
                                        <option value="<?php echo $allBioTest['id'] ?>"><?php echo $allBioTest['title']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <button class="btn btn-primary ex-take-test-bt" type="submit"> Take Test</button>
                        </form>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
<?php
@include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();

