<?php

$this->onlyLoggedInAllowed(UserManager::USER_TYPE_SUBSCRIBER);

/** @var User $user */
$user = $this->getUser();
$extraUserInfo = $user->getExtraInfo();

$package = $user->getPackageDetails();
$showOnlyGoverment = $package['id'] == 1;

$getStateId = filter_input(INPUT_GET, 'si', FILTER_SANITIZE_NUMBER_INT);
$recogId = filter_input(INPUT_GET, 'ri', FILTER_SANITIZE_NUMBER_INT);
$cityId = filter_input(INPUT_GET, 'ci', FILTER_SANITIZE_NUMBER_INT);
$colType = filter_input(INPUT_GET, 'ct', FILTER_SANITIZE_NUMBER_INT);
$courseType = filter_input(INPUT_GET, 'courseT', FILTER_SANITIZE_NUMBER_INT);
$orderId = filter_input(INPUT_GET, 'ot', FILTER_SANITIZE_NUMBER_INT);

// now get the selected state id.
$selectedStateId = $extraUserInfo['stateId'];
if ( !empty(StateManager::getInstance()->getStateById($getStateId)) ) {
    $selectedStateId = $getStateId;
}
$selectedCityId = 0;
if ( !empty(StateManager::getInstance()->getCityById($cityId)) )
{
    $selectedCityId = $cityId;
}
$selectedRecogId = 0;
if ( !empty(CollegeManager::getInstance()->getRecognitionsByIdList($recogId)) )
{
    $selectedRecogId = $recogId;
}
$selectedColType = null;
if ( !empty(CollegeManager::getInstance()->getCollegeTypeById($colType)) )
{
    $selectedColType = $colType;
}
$selectedCourseType = null;
if ( !empty(CollegeManager::getInstance()->getCourseTypeById($courseType)) )
{
    $selectedCourseType = $courseType;
}

if ( ( $package['id'] == 1  || $package['id'] == 2 ) && $selectedStateId != $extraUserInfo['stateId']  )
{
    throw new RenderUpgradePackage("College List Other States", "You are not allowed to see other states colleges to see that you have to upgrade to your next package.");
}
if ( ( $package['id'] == 1 ) )
{
    if (  is_numeric($selectedColType) && $selectedColType > 1  )
    {
        throw new RenderUpgradePackage("Other Types Of College", "You are not allowed to view other types of college as your current package is Free selected, please upgrade your package to view other colleges.");
    }else {
        $selectedColType = 1;
    }
}

$states = StateManager::getInstance()->getAllStates();
$cities = StateManager::getInstance()->getCityListByStateId($selectedStateId);
$universities = CollegeManager::getInstance()->getUniversitiesByStateId($getStateId);
$recognitions = CollegeManager::getInstance()->getAllRecognitions();
$allCollegeTypes = CollegeManager::getInstance()->getAvlCollegeTypes();
$courseTypes = CollegeManager::getInstance()->getAvlCourseType(2);


$sAnnouncements = Announcements::getInstance()->getActiveStateAnnouncementsByStateId($selectedStateId);
$sAnnouncements = array_slice($sAnnouncements, 0, 3);

switch ( $orderId )
{
    case CollegeManager::ORDER_BY_ALPHA:
    case CollegeManager::ORDER_BY_LTH:
    case CollegeManager::ORDER_BY_HTL:
        break;
    default:
        $orderId = 0;
}

$colleges = CollegeManager::getInstance()
    ->getCollege(
        $selectedStateId, $selectedCityId,
        $selectedRecogId, $selectedColType,
        $selectedCourseType, $orderId
    );

$recognitionIds = array();
$colStateIds = array();
$colCityIds = array();
$universityIds = array();
$collegeTypeIds = array();
foreach ( $colleges as $college )
{
    $recognitionIds[$college['recognitionId']]  = $college['recognitionId'];
    $colStateIds[$college['stateId']]           = $college['stateId'];
    $colCityIds[$college['cityId']]             = $college['cityId'];
    $universityIds[$college['affUniversityId']] = $college['affUniversityId'];
    $collegeTypeIds[$college['collegeTypeId']]  = $college['collegeTypeId'];
}

$recogListWithId            = CollegeManager::getInstance()->getRecognitionsByIdList($recognitionIds);
$colStateListWithId         = StateManager::getInstance()->getStateByIdList($colStateIds);
$colCityListWithId          = StateManager::getInstance()->getCityByIdList($colCityIds);
$universitiesWithId         = CollegeManager::getInstance()->getUniversitiesByIdList($universityIds);
$collegeTypeWithId          = CollegeManager::getInstance()->getCollegeTypeIdList($collegeTypeIds);
$allCollegeCityAbility      = CollegeManager::getInstance()->getCityWiseCollegeAbility($selectedStateId);

$this->_addHeader();
@include $this->getPath() . DS . 'navs.top.php';
?>

    <?php if ( $package['id'] == 1 ): ?>
        <p class="text-bright" style="padding: 20px;border: 1px solid #000;">You are using our Free package, That is why only Government colleges are shown from your Home State. Please click  <a href="/upgrade-packages">here</a> to upgrade to next package.</p>
    <?php endif; ?>

    <div class="row">
        <div class="col-sm-12">
            <div class="element-actions">
                <form class="form-inline justify-content-sm-end">
                    <select class="form-control form-control-sm ex-bdr-r" id="select-state">
                        <?php foreach ( $states as $state ): ?>
                            <option value="<?php echo $state['id']; ?>" <?php echo $state['id'] === $selectedStateId ? 'selected' : ''; ?>>
                                <?php echo $state['name']; ?>
                                <?php echo ( $package['id'] == 1  || $package['id'] == 2 ) && $state['id'] != $extraUserInfo['stateId'] ? ' (Upgrade) ' : ''; ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </form>
            </div>
        </div>
    </div>

    <div class="row">
        <?php foreach ( $sAnnouncements as $sAnnouncement ): ?>
            <div class="col-sm-4 col-xxxl-4 ex-pa">
                <div class="element-wrapper">
                    <h6 class="custm-ele-hdr">
                        <?php echo Util::htmlEncode($sAnnouncement['title']); ?>
                    </h6>
                    <div class="custm-ele-hdr-strk">
                    </div>
                    <div class="element-content">
                        <div>
                            <?php echo $sAnnouncement['message']; ?>
                        </div>
                        <p class="fst-rund-p"><?php echo $sAnnouncement['custom_text']; ?></p>
                        <div class="row">
                            <div class="col-sm-6 col-xxxl-6">
                                <p class="s-d-t">
                                    Start: <span><?php echo date('d-m-Y', $sAnnouncement['start']); ?></span> <span class="s-d-t-time"><?php echo date('g:i A', $sAnnouncement['start']); ?></span>
                                </p>
                                <p class="s-d-t">
                                    End: <span><?php echo date('d-m-Y', $sAnnouncement['end']); ?></span> <span class="s-d-t-time"><?php echo date('g:i A', $sAnnouncement['end']); ?></span>
                                </p>
                            </div>
                            <div class="col-sm-6 col-xxxl-6">
                                <div class="blank-d">
                                </div>
                                <?php if ( substr($sAnnouncement['link'], 0, 7) !== 'http://' ): ?>
                                    <a class="visit-website-link" href="http://<?php echo $sAnnouncement['link']; ?>">Visit Website ></a>
                                <?php else :?>
                                    <a class="visit-website-link" href="<?php echo $sAnnouncement['link']; ?>">Visit Website ></a>
                                <?php endif; ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <?php endforeach; ?>

    </div>

    <div class="row pt-3">
        <div class="col-sm-12">
            <div class="element-actions">
                <form class="form-inline justify-content-sm-end ">
<!--                    <select class="form-control form-control-sm ex-m">-->
<!--                        <option value="Pending">-->
<!--                            Affilliation-->
<!--                        </option>-->
<!--                        <option value="Active">-->
<!--                            Last Week-->
<!--                        </option>-->
<!--                        <option value="Cancelled">-->
<!--                            Last 30 Days-->
<!--                        </option>-->
<!--                    </select>-->

                    <select class="form-control form-control-sm ex-m" id="select-order-type">
                        <option value="0">
                            Order By
                        </option>
                        <option <?php echo $orderId == CollegeManager::ORDER_BY_ALPHA ? 'selected' : ''; ?> value="<?php echo CollegeManager::ORDER_BY_ALPHA; ?>">Order Alphabetically</option>
                        <option <?php echo $orderId == CollegeManager::ORDER_BY_LTH ? 'selected' : ''; ?> value="<?php echo  CollegeManager::ORDER_BY_LTH; ?>">Order By Price: LOW To HIGH</option>
                        <option <?php echo $orderId == CollegeManager::ORDER_BY_HTL ? 'selected' : ''; ?> value="<?php echo  CollegeManager::ORDER_BY_HTL; ?>">Order By Price: HIGH To LOW</option>
                    </select>

                    <select class="form-control form-control-sm ex-m" id="select-course-type">
                    <option value="0">
                        Course Type
                    </option>

                    <?php foreach ( $courseTypes as $courseType ): ?>
                        <option value="<?php echo $courseType['id']; ?>" <?php echo $courseType['id'] == $selectedCourseType ? 'selected' : ''; ?> >
                            <?php echo $courseType['courseType']; ?>
                        </option>
                    <?php endforeach; ?>

                    </select>


                    <select class="form-control form-control-sm ex-m" id="select-college-type">
                        <option value="0">
                            College Type
                        </option>

                        <?php foreach ( $allCollegeTypes as $allCollegeType ): ?>
                            <option value="<?php echo $allCollegeType['id']; ?>" <?php echo $selectedColType == $allCollegeType['id'] ? 'selected' : '' ?>>
                                <?php echo $allCollegeType['collegeType']; ?>
                                <?php echo $package['id'] == 1 && $allCollegeType['id'] != 1 ? " (Upgrade) " : "";  ?>
                            </option>
                        <?php endforeach; ?>

                    </select>

                    <select class="form-control form-control-sm ex-m" id="select-recognition">
                        <option value="0">
                            Recognization
                        </option>

                        <?php foreach ( $recognitions as $recognition ): ?>
                            <option value="<?php echo $recognition['id']; ?>" <?php echo $selectedRecogId == $recognition['id'] ? 'selected' : null; ?>>
                                <?php echo $recognition['text']; ?>
                            </option>
                        <?php endforeach; ?>

                    </select>

                    <select class="form-control form-control-sm ex-m" id="select-city">
                        <option value="0">
                            City
                        </option>
                        <?php foreach ( $cities as $city ): ?>
                        <?php if ( !isset($allCollegeCityAbility[$city['id']]) || !$allCollegeCityAbility[$city['id']] ) continue; ?>
                            <option value="<?php echo $city['id'] ?>" <?php echo $selectedCityId == $city['id'] ? 'selected' : null; ?>>
                                <?php echo $city['name']; ?>
                            </option>
                        <?php endforeach; ?>
                    </select>


                </form>
            </div>
            <div class="ex-strk"></div>
        </div>
    </div>

    <?php if ( empty($colleges) ): ?>
    <div class="row r-eo-bg">
        <div class="col-sm-12">
            <h4 class="text-center">Sorry, no college is found for your current filter selection.</h4>
        </div>
    </div>
    <?php endif; ?>
    <?php foreach ( $colleges as $college ): ?>
    <div class="row r-eo-bg">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-8">
                    <h4><?php echo $college['collegeName']; ?></h4>
                    <p class="typ">
                        Type: <span class="typ-span"><?php echo isset($collegeTypeWithId[$college['collegeTypeId']]) ? $collegeTypeWithId[$college['collegeTypeId']]['collegeType'] : 'Unknown';  ?></span>
                        Affiliation: <span class="typ-span"><?php echo isset($universitiesWithId[$college['affUniversityId']]) ? $universitiesWithId[$college['affUniversityId']]['name'] : 'Unknown';  ?></span>

                    </p>
                    <p class="typ">Established: <span class="typ-span"><?php echo $college['established']; ?></span>
                        Location: <span class="typ-span"><?php echo isset($colCityListWithId[$college['cityId']]) ? $colCityListWithId[$college['cityId']]['cityName'] : 'Unknown'; ?>, <?php echo isset($colStateListWithId[$college['stateId']]) ? $colStateListWithId[$college['stateId']]['name'] : 'Unknown'; ?></span></p>
                </div>
                <div class="col-sm-4">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="blank-d-ex">
                            </div>
                            <a class="visit-website-link-ex" data-target="#fees-structure-<?php echo $college['id']; ?>" data-toggle="modal">Fees Structure ></a>
                        </div>
                        <div class="col-sm-6">
                            <div class="blank-d-ex">
                            </div>
                            <a class="visit-website-link-ex" href="<?php echo 'http://localhost/apply-neet-ug?select-type=2&select-state=' . $college['stateId'] ?>">Apply Now ></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="blank-d-ex">
                            </div>
                            <a class="visit-website-link-ex" data-target="#seat-matrix-<?php echo $college['id']; ?>" data-toggle="modal">Seat Matrix ></a>
                        </div>
                        <div class="col-sm-6">
                            <div class="blank-d-ex">
                            </div>
                            <?php if ( substr($college['websiteUrl'], 0, 7) !== 'http://' && substr($college['websiteUrl'], 0, 7) !== 'https:/' ): ?>
                                <?php if ( !empty($college['websiteUrl']) ): ?>
                                    <a target="_blank" class="visit-website-link-ex" href="http://<?php echo $college['websiteUrl']; ?>">Visit Website ></a>
                                <?php endif; ?>
                            <?php else :?>
                                <a target="_blank" class="visit-website-link-ex" href="<?php echo $college['websiteUrl']; ?>">Visit Website ></a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div aria-hidden="true" aria-labelledby="mySmallModalLabel" class="modal fade bd-example-modal-sm" id="fees-structure-<?php echo $college['id'] ?>" role="dialog" tabindex="-1">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">
                            See Fees Structure
                        </h5>
                        <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="table-responsive">
                            <table class="table table-lightborder">
                                <thead>
                                <tr>
                                    <th>
                                        F. Type
                                    </th>
                                    <th class="text-left">
                                        Amount
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="nowrap">
                                        Government
                                    </td>
                                    <td class="text-left">
                                        <?php echo $college['govFee']; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="nowrap">
                                        Private
                                    </td>
                                    <td class="text-left">
                                        <?php echo $college['privateFee']; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="nowrap">
                                        NRI
                                    </td>
                                    <td class="text-left">
                                        <?php echo $college['nriFee']; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="nowrap">
                                        Other
                                    </td>
                                    <td class="text-left">
                                        <?php echo $college['otherFee']; ?>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" data-dismiss="modal" type="button"> Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div aria-hidden="true" aria-labelledby="mySmallModalLabel" class="modal fade bd-example-modal-sm" id="seat-matrix-<?php echo $college['id']; ?>" role="dialog" tabindex="-1">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">
                            Seat Matrix
                        </h5>
                        <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="table-responsive">
                            <table class="table table-lightborder">
                                <thead>
                                <tr>
                                    <th>
                                        Quota
                                    </th>
                                    <th class="text-left">
                                        Seats
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="nowrap">
                                        GN
                                    </td>
                                    <td class="text-left">
                                        <?php echo $college['cutGen'] ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="nowrap">
                                        GN-PH
                                    </td>
                                    <td class="text-left">
                                        <?php echo $college['cutGenPh'] ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="nowrap">
                                        OBC
                                    </td>
                                    <td class="text-left">
                                        <?php echo $college['cutObc'] ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="nowrap">
                                        OBC-PH
                                    </td>
                                    <td class="text-left">
                                        <?php echo $college['cutObcPh'] ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="nowrap">
                                        SC
                                    </td>
                                    <td class="text-left">
                                        <?php echo $college['cutSc'] ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="nowrap">
                                        SC-PH
                                    </td>
                                    <td class="text-left">
                                        <?php echo $college['cutScPh']; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="nowrap">
                                        ST
                                    </td>
                                    <td class="text-left">
                                        <?php echo $college['cutSt']; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="nowrap">
                                        ST-PH
                                    </td>
                                    <td class="text-left">
                                        <?php echo $college['cutStPh']; ?>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" data-dismiss="modal" type="button"> Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php endforeach; ?>










    <script type="text/javascript">
    function _buildQuery( obj ) {

        var prop, params = [];
        for( prop in obj )
        {
            if ( obj.hasOwnProperty(prop) )
                params.push( prop + '=' + obj[prop] );
        }

        return params.join('&');
    }

    (function( __scope ){
        var $stateElm = $('#select-state');

        $(document).on('change', '#select-state', function (e) {
            var value = $(this).val().trim();

            __scope.location.href = '/user-college-list?' +  _buildQuery({
                si: value
            });

        });

        $(document).on('change', '#select-city', function (e) {
            var recog = $('#select-recognition').val().trim();
            var state = $('#select-state').val().trim();
            var city = $(this).val().trim();
            var colType = $('#select-college-type').val().trim();
            var orderType = $('#select-order-type').val().trim();
            var courseType = $('#select-course-type').val().trim();

            __scope.location.href = '/user-college-list?' +  _buildQuery({
                ci: city,
                ri: recog,
                si: state,
                ct: colType,
                ot: orderType,
                courseT: courseType
            });
        });

        $(document).on('change', '#select-recognition', function (e) {

            var state = $('#select-state').val().trim();
            var recog = $(this).val().trim();
            var city = $('#select-city').val().trim();
            var colType = $('#select-college-type').val().trim();
            var orderType = $('#select-order-type').val().trim();
            var courseType = $('#select-course-type').val().trim();

            __scope.location.href = '/user-college-list?' +  _buildQuery({
                ci: city,
                ri: recog,
                si: state,
                ct: colType,
                ot: orderType,
                courseT: courseType
            });

        });

        $(document).on('change', '#select-college-type', function (e) {
            var recog = $('#select-recognition').val().trim();
            var state = $('#select-state').val().trim();
            var city = $('#select-city').val().trim();
            var colType = $(this).val().trim();
            var orderType = $('#select-order-type').val().trim();
            var courseType = $('#select-course-type').val().trim();

            __scope.location.href = '/user-college-list?' +  _buildQuery({
                ci: city,
                ri: recog,
                si: state,
                ct: colType,
                ot: orderType,
                courseT: courseType
            });
        });

        $(document).on('change', '#select-order-type', function (e) {
            var recog = $('#select-recognition').val().trim();
            var state = $('#select-state').val().trim();
            var city = $('#select-city').val().trim();
            var colType = $('#select-college-type').val().trim();
            var orderType = $(this).val().trim();
            var courseType = $('#select-course-type').val().trim();

            __scope.location.href = '/user-college-list?' +  _buildQuery({
                ci: city,
                ri: recog,
                si: state,
                ct: colType,
                ot: orderType,
                courseT: courseType
            });
        });

        $(document).on('change', '#select-course-type', function (e) {
            var recog = $('#select-recognition').val().trim();
            var state = $('#select-state').val().trim();
            var city = $('#select-city').val().trim();
            var colType = $('#select-college-type').val().trim();
            var orderType = $('#select-order-type').val().trim();
            var courseType = $(this).val().trim();

            __scope.location.href = '/user-college-list?' +  _buildQuery({
                ci: city,
                ri: recog,
                si: state,
                ct: colType,
                ot: orderType,
                courseT: courseType
            });
        });

    })(window)
</script>


<?php
@include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();

