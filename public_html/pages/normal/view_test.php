<?php

/**
 * Created by PhpStorm.
 * User: akash
 * Date: 18/12/18
 * Time: 8:59 PM
 */

$this->onlyLoggedInAllowed(UserManager::USER_TYPE_TEACHER);

$publishedTest = filter_input(INPUT_GET, 'published', FILTER_SANITIZE_NUMBER_INT);
$testId = filter_input(INPUT_GET, 'test-id', FILTER_SANITIZE_NUMBER_INT);
$deletedTest = filter_input(INPUT_GET, 'deleted', FILTER_SANITIZE_NUMBER_INT);
$msg = null;


if ($publishedTest == 1) {
    $msg = 'Test published successfully.';
}

if ( !empty($_POST) ) {
    $testId = $_POST['testId'];
    $value = $_POST['dr'];

    $SQL = "UPDATE `" . DB_TABLE_TESTS . "` SET `displayResult` = :v WHERE `id` = :id";
    $stmt = Db::getInstance()->prepare($SQL);
    Db::bindValues(array(
        ':v' => $value,
        ':id' => $testId
    ), $stmt);
    $stmt->execute();

    exit("Test result updated, you can close this window now.");
}

if ($deletedTest == 1 && !empty($testId)) {

    $atemCount = TestManager::getInstance()->getAttemptCount($testId);
    if ( $atemCount > 0 )
    {
        $msg = 'This test can not be deleted as students  has already attemptted this test.';
    } 
    else
    {
    TestManager::getInstance()->deletePublishTest($testId);
    $msg = 'Test deleted successfully.';    
    }
    
}



$allTests = TestManager::getInstance()->getAllTestBySchool( $_SESSION['schoolId'] );

$this->_addHeader();

?>



<div class="container mt-5">
    <?php if (!empty($msg)) : ?>
        <div class="alert alert-success alert-dismissible" role="alert" area-label="Close" class="close">
            <?php echo $msg; ?>
            <!-- <button aria-label="close" class="close" data-dismiss="alert" type="button"><span aria-hidden="true">&times;</span> </button> -->
        </div>
    <?php endif; ?>
    <h2 class="text-left mb-4">Published Tests -</h2>
    <div class="row">
        <div class="col-md-12">
            <table id="example" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>DATE</th>
                        <th>STD</th>
                        <th>DIV</th>
                        <th>SUBJECT</th>
                        <th>DESCRIPTION</th>
                        <th>NO of QUESTIONS</th>
                        <th>TIME</th>
                        <th>DISPLAY RESULT</th>
                        <th>ATTEMPTS</th>
                        <th>ACTIONS</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (count($allTests) >= 1) { ?>

                        <?php foreach ($allTests as $test) {

                            if ( $test['teacherId'] != $_SESSION['username'] ) continue;

                            $testQuestions = TestManager::getInstance()->getAllQuestions($test['id']);
                            if ($test['published'] == 1) {
                        ?>
                                <tr>
                                    <td><?php echo $test['id']; ?></td>
                                    <td><?php echo  date('d/m/Y g:i a', strtotime( $test['startAt'] ) ); ?></td>
                                    <td><?php echo $test['class']; ?></td>
                                    <td>
                                        <?php
                                        $divsArray = [];
                                        $divs = json_decode($test['divisions'], true);
                                        foreach ($divs as $div) {
                                            $divsArray[] = $div['divisionName'];
                                        }
                                        echo implode(',', $divsArray);
                                        ?>
                                    </td>
                                    <td><?php echo htmlentities($test['subject']); ?></td>
                                    <td><?php echo htmlentities($test['description']); ?></td>
                                    <td>
                                        <?php

                                        echo count($testQuestions);
                                        ?>
                                    </td>
                                    <td><?php echo $test['totalTime']; ?></td>
                                    <td>
                                        <a href="#!" onclick="onChange(this)">Change (<?php                                        
                                        if ($test['displayResult'] ==1)
                                        {
                                            echo "SHOW MARKS AND ANSWERS";
                                        }
                                        elseif ($test['displayResult'] ==2)
                                        {
                                            echo "SHOW ONLY MARK";
                                        }
                                        else
                                        {
                                            echo "DONT SHOW ANYTHING";
                                        }
                                         ?>)</a>
                                        <div class="change-in" style="display: none;">
                                            <form method="post" action="<?php echo SITE_URL . DS . 'view-test' ?>">
                                                <input type="radio" name="dr" value="1" required/> SHOW MARKS AND ANSWERS<br />
                                                <input type="radio" name="dr" value="2" required/> SHOW ONLY MARKS<br />
                                                <input type="radio" name="dr" value="3" required/> DONT SHOW ANYTHING<br />
                                                <input type="hidden" name="testId" value="<?php echo $test['id']; ?>" /><br />
                                                <input type="submit" name="submitdr" value="Submit" />
                                            </form>
                                        </div>
                                    </td>
                                    <td>
                                        <ul>
                                            <li><a href="<?php echo SITE_URL . '/tests?test-id=' . $test['id']; ?>">View</a></li>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul>
                                            <li><a href="<?php echo SITE_URL . '/take-test?test-id=' . $test['id']; ?>">Review</a></li>
                                        </ul>
                                    </td>
                                    <!-- <td>
                                        <ul>
                                            <li><a href="javascript:void(0)" class="deleteTest" data-redirect-to="<?php echo SITE_URL . '/view_test?test-id=' . $test['id'] . '&deleted=1'; ?>">Delete Test</a>  </li>
                                        </ul>
                                    </td> -->
                                </tr>
                            <?php } ?>
                        <?php } ?>

                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>DATE</th>
                        <th>STD</th>
                        <th>DIV</th>
                        <th>SUBJECT</th>
                        <th>DESCRIPTION</th>
                        <th>DISPLAY RESULT</th>
                        <th>ATTEMPTS</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>


<script>
    $(document).ready(function() {

        setTimeout(function() {
            $(".alert").fadeTo(500, 0).slideUp(500, function() {
                $(this).remove();
            });
        }, 3000);

        $('#example').DataTable({
            "language": {
                "emptyTable": "No published test found."
            },         
            "order": [[ 0, "desc" ]],
            buttons: [           
            'excelHtml5',
            'csvHtml5'            
        ]
        });
    });

    function onChange(elm) {
        var $elm = $(elm).next();
        var myWindow = window.open("", "", "width=400,height=400");
        myWindow.document.write($elm.html());
        myWindow.onbeforeunload = function() {
            setTimeout(() => {
                location.reload();    
            }, 200);
            
  }
    }
</script>

<?php

$this->_addFooter();

