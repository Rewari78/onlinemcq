<?php
$this->onlyLoggedInAllowed();


// First get the user details.
/** @var User $user */
$user = $this->getUser();
$userExtras = $user->getExtraInfo();
$states = StateManager::getInstance()->getAllStates();

$packageInfo = $user->getPackageDetails();

$cPass = filter_input(INPUT_POST, 'current-password', FILTER_SANITIZE_STRING);
$nPass = filter_input(INPUT_POST, 'new-password', FILTER_SANITIZE_STRING);
$rnPass = filter_input(INPUT_POST, 'retype-new-password', FILTER_SANITIZE_STRING);

$msg = null;
if ( $this->isPost() )
{
    if ( !Validator::isPasswordValid($cPass,$user->getPasswordHash() ) ) {
        $msg = "Your current password is invalid.";
    }

    if ( empty($nPass) ) {
        $msg = "Please give us a new password first.";
    }

    if ( $nPass != $rnPass ) {
        $msg = "Your new password do not match with retype new password field.";
    }


    if ( empty($msg) ) {
        UserDatabase::getInstance()
            ->updateUser(
                    $user->getId(),
                    $user->getFirstName(),
                    $user->getLastName(),
                    $user->getEmail(),
                    $user->getPhone(),
                    Util::passwordEncrypt($nPass),
                    $user->getType()
            );
        $msg = "Your account password is changed.";
    }
}


$this->_addHeader();
@include $this->getPath() . DS . 'navs.top.php';

?>

    <div class="row">
        <div class="col-sm-8 col-xxxl-8">
            <div class="element-wrapper">
                <h6 class="element-header">
                    Change Password
                </h6>

                <?php if ( !empty($msg) ) : ?>
                    <div class="alert alert-success alert-dismissible " role="alert" area-label="Close">
                        <?php echo $msg; ?>
                        <button aria-label="close" class="close" data-dismiss="alert" type="button">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                <?php endif; ?>

                <div class="element-box">
                    <ul class="nav nav-tabs custom-tab">
                        <li><a href="/account-general-settings">General Setings</a></li>
                        <li><a href="/account-email-settings">Change Email</a></li>
                        <li><a href="#">Change Password</a></li>
                    </ul>

                    <div class="fs">
                        <form action="" method="post">
                            <div class="form-group">
                                <label for="current-password">Current Password</label><input class="form-control" placeholder="Enter Your Current Password" type="password" id="current-password" name="current-password">
                            </div>
                            <div class="form-group">
                                <label for="new-password">New Password</label><input class="form-control" placeholder="Enter Your New Password" type="password" id="new-password" name="new-password">
                            </div>
                            <div class="form-group">
                                <label for="retype-new-password">Re-Type New Password</label><input class="form-control" placeholder="Enter Your Current Password" type="password" id="retype-new-password" name="retype-new-password">
                            </div>
                            <div class="buttons">
                                <button class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-4 col-xxxl-4">
        </div>
    </div>

<script type="text/javascript">
    $(document).on('change', '#study12', function () {
        var value = $(this).val();
        if (  parseInt(value, 10) === 2 ) {
            $('#pcbPercent')[0].disabled = false;
        }else {
            $('#pcbPercent').val('')[0].disabled = true;
        }
    });
</script>

<?php
@include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();