<?php

$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );

$msg = "";

if ( $this->isPost() )
{
    $collegeType = filter_input(INPUT_POST, 'college-type');

    if ( empty($collegeType) )
    {
        $msg = "College type was not valid.";
    }else {
        // We need to
        CollegeManager::getInstance()->addCollegeType($collegeType);
        $msg = "New College type is added.";
    }
}

$collegeTypes = CollegeManager::getInstance()->getAvlCollegeTypes();

$this->_addHeader();
@include $this->getPath() . DS . 'navs.top.php';
?>
    <div class="row">
        <div class="col-sm-8 col-xxxl-8">
            <div class="element-wrapper">
                <?php if ( !empty($msg) ) : ?>
                    <div class="alert alert-success alert-dismissible " role="alert" area-label="Close" class="close">
                        <?php echo $msg; ?>
                        <button aria-label="close" class="close" data-dismiss="alert" type="button"><span aria-hidden="true">&times;</span> </button>
                    </div>
                <?php endif; ?>


                <div class="element-actions">
                    <button class="btn btn-primary custom-button" data-target="#add-college-type" data-toggle="modal" type="button">Add Type</button>
                </div>
                <h6 class="element-header">
                    College Types
                </h6>
                <div class="element-box">
                    <div class="table-responsive">

                        <?php if ( empty($collegeTypes) ): ?>
                            <p>No College type found.</p>
                        <?php else:?>
                            <table class="table table-lightborder">
                                <thead>
                                <tr>
                                    <th>
                                        SR.No
                                    </th>
                                    <th class="text-left">
                                        College Type
                                    </th>
                                    <th>
                                        Actions
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ( $collegeTypes as $collegeType ): ?>
                                    <tr>
                                        <td class="nowrap">
                                            <?php echo $collegeType['id']; ?>
                                        </td>
                                        <td class="text-left">
                                            <?php echo Util::htmlEncode($collegeType['collegeType']); ?>
                                        </td>
                                        <td class="row-actions">
                                            <a href="#"  data-target="#edit-college-<?php echo $collegeType['id']; ?>" data-toggle="modal" >
                                                <i class="os-icon os-icon-ui-49"></i>
                                            </a>
                                            <div aria-hidden="true" aria-labelledby="mySmallModalLabel" class="modal fade bd-example-modal-sm" id="edit-college-<?php echo $collegeType['id']; ?>" role="dialog" tabindex="-1">
                                                <div class="modal-dialog modal-sm">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">
                                                                Edit College Type
                                                            </h5>
                                                            <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                                                                <span aria-hidden="true"> &times;</span>
                                                            </button>
                                                        </div>

                                                        <form action="#" method="post" onsubmit="submitForm(this); return false;">
                                                            <div class="modal-body">

                                                                <div class="form-group"  style="text-align: left;">
                                                                    <label for="college-type">College Type</label>
                                                                    <input class="form-control" placeholder="Type" type="text" id="college-type-<?php echo $collegeType['id']; ?>" name="college-type" value="<?php echo Util::htmlEncode($collegeType['collegeType']); ?>" />
                                                                </div>

                                                                <input type="hidden" id="college-type-id-<?php echo $collegeType['id']; ?>" name="college-type-id" value="<?php echo $collegeType['id']; ?>"/>

                                                                <div class="alert alert-warning alert-dismissible d-none" role="alert">
                                                                </div>


                                                            </div>
                                                            <div class="modal-footer">
                                                                <button class="btn btn-secondary" data-dismiss="modal" type="button"> Close</button>
                                                                <button class="btn btn-primary" type="submit">Submit</button>
                                                            </div>
                                                        </form>

                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        <?php endif; ?>


                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-4 col-xxxl-4">
        </div>

    </div>

    <div aria-hidden="true" aria-labelledby="mySmallModalLabel" class="modal fade bd-example-modal-sm" id="add-college-type" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        Add New Type
                    </h5>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                </div>
                <form action="/add-college-type" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="college-type"> College Type</label>
                            <input class="form-control" placeholder="Type" type="text" id="college-type" name="college-type" />

                            <div class="alert alert-warning alert-dismissible d-none" role="alert" id="add-p-alert-box">
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" data-dismiss="modal" type="button"> Close</button>
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function submitForm( form )
        {
            var $form = $(form);
            var $alert = $form.find('.alert');
            var _data = $(form).serialize();

            hideError($alert);

            $.post(
                homeUrl + '/ajax/edit-college-type',
                $form.serialize(),
                function (data) {
                    if ( data.info !== 'success' )
                    {
                        showError($alert, data.message);
                        return;
                    }

                    window.location.reload();

                },
                'json'
            );
        }

        function showError( box, msg )
        {

            var $alertBox = $(box);
            $alertBox.html(msg);
            $alertBox.removeClass('d-none');
        }

        function hideError( box )
        {
            var $alertBox = $(box);
            $alertBox.addClass('d-none');
        }
    </script>

<?php
@include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();

