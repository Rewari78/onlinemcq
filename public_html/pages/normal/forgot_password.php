<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 3/27/19
 * Time: 2:00 PM
 */
if ( UserManager::getInstance()->isLoggedIn() ) $this->redirect('home');

/** @var User $user */
$user = $this->getUser();

if ( $this->isPost() )
{
    $mobile = filter_input(INPUT_POST, 'mobile', FILTER_SANITIZE_NUMBER_INT);

    $msg = null;
    if ( empty($mobile) )
    {
        $msg = "Please enter you mobile number.";
    }

    if ( empty($msg) )
    {
        $nPass = "";

        UserDatabase::getInstance()
            ->updateUser(
                $user->getId(),
                $user->getFirstName(),
                $user->getLastName(),
                $user->getEmail(),
                $user->getPhone(),
                Util::passwordEncrypt($nPass),
                $user->getType()
            );

        $msg = "Your password has been changed and sent to you successfully.";
    }
}

$this->_addHeader();
?>
    <body style="padding: 0;">
    <div class="container-tfluid cus-log-bg">
        <div class="row add-class">
            <div class="col-sm-3 col-xxxl-3">
            </div>
            <div class="col-sm-6 col-xxxl-6 lg-dv">
                <h6 class="custm-ele-hdr lg-pg">
                    Forgot your Password?
                </h6>
                <div class="custm-ele-hdr-strk lg-pg-strk"></div>
                <div class="cus-log-sec">
                    <div class="cus-log-inp">
                        <form method="post" action="#">

                            <label for="mobile">Mobile Number</label>
                            <input type="tel" name="mobile" placeholder="Enter 10 digit mobile number" id="mobile">
                            <button type="submit" class="cus-login-bt">Send SMS<span>&#xE72A;</span></button>

                        </form>
                    </div>
                </div>
                <div class="col-sm-4 col-xxxl-4">
                </div>
            </div>
            <div class="col-sm-3 col-xxxl-3">
            </div>
        </div>
    </div>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/js/jquery.datetimepicker.full.min.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/moment/moment.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/chart.js/dist/Chart.min.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/jquery-bar-rating/dist/jquery.barrating.min.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/ckeditor/ckeditor.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap-validator/dist/validator.min.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/ion.rangeSlider/js/ion.rangeSlider.min.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/dropzone/dist/dropzone.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/editable-table/mindmup-editabletable.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/tether/dist/js/tether.min.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/slick-carousel/slick/slick.min.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap/js/dist/util.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap/js/dist/alert.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap/js/dist/button.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap/js/dist/carousel.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap/js/dist/collapse.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap/js/dist/dropdown.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap/js/dist/modal.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap/js/dist/tab.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap/js/dist/tooltip.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap/js/dist/popover.js"></script>
    <script src="<?php echo SITE_URL ?>/statics/js/demo_customizer.js?version=4.4.0"></script>
    <script src="<?php echo SITE_URL ?>/statics/js/main.js?version=4.4.0"></script>
    <script src="<?php echo SITE_URL ?>/statics/js/jquery-ui.min.js"></script>
    <script id="bolt" src="https://sboxcheckout-static.citruspay.com/bolt/run/bolt.min.js" bolt-color="222f3f" bolt-logo="http://ihs.co/statics/images/Neetguidance-logo.jpg"></script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-XXXXXXX-9', 'auto');
        ga('send', 'pageview');
    </script>
    </body>
<?php
$this->_addFooter();
