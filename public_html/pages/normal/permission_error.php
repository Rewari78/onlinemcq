<?php

$this->_addHeader();
// @include $this->getPath() . DS . 'navs.top.php';
?>
<div class="element-content">
    <div class="big-error-w">
        <h2 class="permission-denied">
            Permission Denied
        </h2>
        <p>
            you are not authorized to view this page
        </p>
    </div>
</div>
<?php
// @include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();
