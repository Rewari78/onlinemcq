<?php
$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );

if ( $this->isPost() )
{
    $quotaName = filter_input(INPUT_POST, 'quota-name');
    if ( empty($quotaName) )
    {
        $msg = "Please enter the quota name properly.";
    }else {
        // Submit the quota.
        Quotas::getInstance()->addQuota($quotaName);
        $msg = "New Quota is created successfully.";
    }
}

$quotaList = Quotas::getInstance()->getAllQuotas();

$this->_addHeader();
@include $this->getPath() . DS . 'navs.top.php';
?>
<div class="row">
    <div class="col-sm-8 col-xxxl-8">
        <div class="element-wrapper">

            <?php if ( !empty($msg) ) : ?>
                <div class="alert alert-success alert-dismissible " role="alert" area-label="Close">
                    <?php echo $msg; ?>
                    <button aria-label="close" class="close" data-dismiss="alert" type="button">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            <?php endif; ?>

            <div class="element-actions">
                <button class="btn btn-primary custom-button" data-target="#add-type-modal-quota" data-toggle="modal" type="button">Add Quota</button>
            </div>
            <h6 class="element-header">
                Quota
            </h6>
            <?php if ( !empty($quotaList) ): ?>
                <div class="element-box">
                <div class="table-responsive">
                    <table class="table table-lightborder">
                        <thead>
                        <tr>
                            <th>
                                SR.No
                            </th>
                            <th class="text-left">
                                Quota Name
                            </th>
                            <th>
                                Actions
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ( $quotaList as $quota ): ?>
                        <tr>
                            <td class="nowrap">
                                <?php echo $quota['id']; ?>
                            </td>
                            <td class="text-left">
                                <?php echo Util::htmlEncode($quota['quota']); ?>
                            </td>
                            <td class="row-actions text-left">

                                <a href="#" data-target="#edit-modal-quota-<?php echo $quota['id']; ?>" data-toggle="modal">
                                    <i class="os-icon os-icon-ui-49"></i>
                                </a>

                                <div aria-hidden="true" aria-labelledby="mySmallModalLabel" class="modal fade bd-example-modal-sm" id="edit-modal-quota-<?php echo $quota['id']; ?>" role="dialog" tabindex="-1">
                                    <div class="modal-dialog modal-sm">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">
                                                    Edit Quota
                                                </h5>
                                                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                                            </div>
                                            <form method="post" action="#" onsubmit="submitForm(this); return false;">

                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <label for="quota-name-<?php echo $quota['id']; ?>">Quota Name</label>
                                                        <input class="form-control" placeholder="Type" type="text" id="quota-name-<?php echo $quota['id']; ?>" name="quota-name">
                                                    </div>
                                                    
                                                    <input type="hidden" name="quota-id" value="<?php echo $quota['id']; ?>" />

                                                    <div class="alert alert-warning alert-dismissible d-none" role="alert">
                                                    </div>

                                                </div>

                                                <div class="modal-footer">
                                                    <button class="btn btn-secondary" data-dismiss="modal" type="button">
                                                        Close
                                                    </button>
                                                    <button class="btn btn-primary" type="submit">
                                                        Save changes
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                            </td>
                        </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </div>

    <div class="col-sm-4 col-xxxl-4">
    </div>
</div>

    <div aria-hidden="true" aria-labelledby="mySmallModalLabel" class="modal fade bd-example-modal-sm" id="add-type-modal-quota" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        Add Quota
                    </h5>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                </div>
                <form method="post" action="#">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="quota-name">Quota Name</label>
                            <input class="form-control" placeholder="Type" type="text" id="quota-name" name="quota-name">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" data-dismiss="modal" type="button"> Close</button>
                        <button class="btn btn-primary" type="submit">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

<script type="text/javascript">

    function submitForm( form )
    {

        var $form = $(form);
        var $alert = $form.find('.alert');
        var _data = $(form).serialize();

        hideError($alert);

        $.post(
            homeUrl + '/ajax/edit-quota',
            $form.serialize(),
            function (data) {
                if ( data.info !== 'success' )
                {
                    showError($alert, data.message);
                    return;
                }

                window.location.reload();

            },
            'json'
        );
    }

    function showError( box, msg )
    {

        var $alertBox = $(box);
        $alertBox.html(msg);
        $alertBox.removeClass('d-none');
    }

    function hideError( box )
    {
        var $alertBox = $(box);
        $alertBox.addClass('d-none');
    }

</script>
<?php
@include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();