<?php

$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );

// get the package id.
$packageId = $this->_data[0];
$packageId = filter_var(filter_var($packageId, FILTER_SANITIZE_NUMBER_INT), FILTER_VALIDATE_INT);

if ( empty($packageId) ) $this->redirect('manage-packages');

$packageDetails = PackageManager::getInstance()->getAPackageById($packageId);

if ( empty($packageDetails) ) $this->redirect('manage-packages');

if ( !$this->isPost() ) $this->redirect('edit-packages/' . $packageId );

$features = isset($_POST['features']) ? $_POST['features'] : array();;
$featureIds = array();
foreach ( $features as $id )
{
    $id = filter_var((int) $id, FILTER_SANITIZE_NUMBER_INT);
    //TODO: I also need to validate feature Id.
    $featureIds[$id] = null;
}

// now filter the variables.
// We know the fields that will contain variables.
// As that has been discussed on the talk.
$questionWLimit = filter_input(INPUT_POST, 'qa_write', FILTER_SANITIZE_NUMBER_INT);
$testLimit = filter_input(INPUT_POST, 'text_limit', FILTER_SANITIZE_NUMBER_INT);

if ( empty($questionWLimit) ) $questionWLimit = 0;
else $questionWLimit = abs($questionWLimit);

if ( empty($testLimit) ) $testLimit = 0;
else if ( $testLimit < 0 || $testLimit > PackageManager::MAX_TEST_LIMIT ) $testLimit = 0;
else $testLimit = abs($testLimit);

// Now we have to add the variables.
if ( array_key_exists(PackageManager::FEATURE_QNA_RW, $featureIds) ) $featureIds[PackageManager::FEATURE_QNA_RW] = array( 'num' => $questionWLimit );
if ( array_key_exists(PackageManager::FEATURE_TEST_SERIES, $featureIds) ) $featureIds[PackageManager::FEATURE_TEST_SERIES] = array( 'num' => $testLimit );

if ( !PackageManager::getInstance()->addFeaturesToPackage($packageId, $featureIds) )
    $this->redirect('edit-package/' . $packageId . '?pf_error=1');

$this->redirect('edit-package/' . $packageId . '?pf_success=1');
