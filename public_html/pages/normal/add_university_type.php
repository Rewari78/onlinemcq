<?php
$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );

$msg = "";

if ( $this->isPost() )
{
    $universityPT = filter_input(INPUT_POST, 'university-type');

//    var_dump($_POST);

    if ( empty($universityPT) )
    {
        $msg = "College type was not valid.";
    }else {
        // We need to
        CollegeManager::getInstance()->addUniversityType($universityPT);
        $msg = "New College type is added.";
    }
}

$universityTypes = CollegeManager::getInstance()->getAvlUniversityTypes();

$this->_addHeader();
@include $this->getPath() . DS . 'navs.top.php';
?>
    <div class="row">
        <div class="col-sm-8 col-xxxl-8">
            <div class="element-wrapper">

                <div class="element-actions">
                    <button class="btn btn-primary custom-button" data-target="#add-university-type-modal" data-toggle="modal" type="button">
                        Add Type
                    </button>
                </div>
                <h6 class="element-header">
                    University Types
                </h6>
                <?php if ( !empty($msg) ) : ?>
                    <div class="alert alert-success alert-dismissible " role="alert" area-label="Close">
                        <?php echo $msg; ?>
                        <button aria-label="close" class="close" data-dismiss="alert" type="button">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                <?php endif; ?>
                <?php if ( !empty($universityTypes) ): ?>
                    <div class="element-box">
                    <div class="table-responsive">
                        <table class="table table-lightborder">
                            <thead>
                            <tr>
                                <th>
                                    SR.No
                                </th>
                                <th class="text-left">
                                    University Type
                                </th>
                                <th class="text-center">
                                    Actions
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php foreach ( $universityTypes as $universityType ): ?>
                                    <tr>
                                        <td class="nowrap">
                                            <?php echo $universityType['id']; ?>
                                        </td>
                                        <td class="text-left">
                                            <?php echo $universityType['universityType']; ?>
                                        </td>
                                        <td class="row-actions">
                                            <a href="#" data-target="#edit-university-type-<?php echo $universityType['id']; ?>" data-toggle="modal">
                                                <i class="os-icon os-icon-ui-49"></i>
                                            </a>

                                            <div aria-hidden="true" aria-labelledby="mySmallModalLabel" class="modal fade bd-example-modal-sm" id="edit-university-type-<?php echo $universityType['id']; ?>" role="dialog" tabindex="-1">
                                                <div class="modal-dialog modal-sm">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">
                                                                Edit Type
                                                            </h5>
                                                            <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                                                        </div>
                                                        <form action="#" method="post" onsubmit="submitForm(this); return false;">
                                                            <div class="modal-body">
                                                                <div class="form-group">
                                                                    <label for="university-type-<?php echo $universityType['id'] ?>">
                                                                        University Type
                                                                    </label>
                                                                    <input class="form-control"
                                                                           placeholder="Type"
                                                                           type="text"
                                                                           id="university-type-<?php echo $universityType['id'] ?>"
                                                                           name="university-type"
                                                                           value="<?php echo $universityType['universityType']; ?>" />
                                                                </div>

                                                                <input type="hidden" name="university-type-id" value="<?php echo $universityType['id']; ?>" />
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
                                                                <button class="btn btn-primary" type="submit">Save changes</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>

        <div class="col-sm-4 col-xxxl-4">
        </div>
    </div>


    <div aria-hidden="true" aria-labelledby="mySmallModalLabel" class="modal fade bd-example-modal-sm" id="add-university-type-modal" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        Add New Type
                    </h5>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                </div>
                <form action="#" method="post">
                    <div class="modal-body">
                            <div class="form-group">
                                <label for="university-type">University Type</label>
                                <input class="form-control" placeholder="Type" type="text" id="university-type" name="university-type">
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
                        <button class="btn btn-primary" type="submit">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function submitForm( form )
        {
            var $form = $(form);
            var $alert = $form.find('.alert');
            var _data = $(form).serialize();

            hideError($alert);

            $.post(
                homeUrl + '/ajax/edit-university-type',
                $form.serialize(),
                function (data) {
                    if ( data.info !== 'success' )
                    {
                        showError($alert, data.message);
                        return;
                    }

                    window.location.reload();

                },
                'json'
            );
        }

        function showError( box, msg )
        {

            var $alertBox = $(box);
            $alertBox.html(msg);
            $alertBox.removeClass('d-none');
        }

        function hideError( box )
        {
            var $alertBox = $(box);
            $alertBox.addClass('d-none');
        }
    </script>
<?php
@include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();

