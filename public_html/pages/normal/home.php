<?php

// var_dump($_SESSION);
// exit;

$this->onlyLoggedInAllowed(UserManager::USER_TYPE_TEACHER);

$this->_addHeader();
?>


<div class="container d-flex h-100 flex-column">
    <div class="row h-100 justify-content-center align-items-center">
        <div class="col-md-4 m-auto menu_box">
            <ul>
                <li><a href="/manage-test">Manage Test</a></li>
                <li><a href="/view-test">View Test</a></li>
                <li><a href="/copy">Copy Test</a></li>
            </ul>
        </div>
    </div>
</div>


<?php
// @include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();
