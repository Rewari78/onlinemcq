<?php
// if ( UserManager::getInstance()->isLoggedIn() ) $this->redirect('home');
?>

<?php // $this->_addHeader(); 
?>

<!-- <body style="padding: 0px;">
    <div class="container-fluid cus-log-bg">
        <div class="row add-class">
            <div class="col-sm-6 col-xxxl-6 l-side-bg">
                <div class="cus-log-sec-logo">
                </div>
            </div>
            <div class="col-sm-6 col-xxxl-6 l-side-bg1">
                <div class="row">
                    <div class="col-sm-3 col-xxxl-3">
                    </div>
                    <div class="col-sm-6 col-xxxl-6">
                        <h6 class="custm-ele-hdr lg-pg">
                            Login Now
                        </h6>
                        <div class="custm-ele-hdr-strk lg-pg-strk">
                        </div>
                        <div class="alert alert-warning d-none" id="alert-box">
                        </div>

                        <div class="cus-log-sec">
                            <div class="cus-log-inp">
                                <form action="#" method="post">
                                    <label>Mobile Number</label>
                                    <input type="tel" name="" placeholder="Enter your Mobile Number" id="login-username">
                                    <label>Password</label>
                                    <input type="password" name="" placeholder="Enter your Password" id="login-password">
                                    <button type="button" class="cus-login-bt" id="login-btn">Login <span>&#xE72A;</span></button>

                                    <div class="d-none" id="auth-otp-border">
                                        <label>OTP <span style="float: right;cursor: pointer;" class="resend-otp"></span></label>
                                        <input type="number" name="" placeholder="Enter six digit recive OTP" id="login-otp">
                                        <button type="button" class="cus-login-bt" id="otp-submit">
                                            Login <span>&#xE72A;</span>
                                        </button>
                                    </div>

                                </form>
                            </div>
                        </div>

                        <p class="text-center" style="color: #fff;">
                            Don't have any account yet? <a href="/register">Register now</a>.
                        </p>
                    </div>
                    <div class="col-sm-3 col-xxxl-3">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var data = {
            seq: null,
            isValidated: false
        };
        var btn = document.getElementById('login-btn');
        btn.addEventListener('click', function(e) {

            // First validate the info.
            var iMobile = document.getElementById('login-username');
            var iPass = document.getElementById('login-password');

            var btn = this;

            var mobile = iMobile.value.trim(),
                pass = iPass.value.trim();

            if (mobile === '' || pass === '') {
                showError("Please provide all the fields.");
                return false;
            }

            hideError();

            // Now send the ajax.
            $.ajax({
                url: homeUrl + '/ajax/validate-login-fields',
                dataType: 'JSON',
                type: "POST",
                data: {
                    validate: 1,
                    mobile: mobile,
                    password: pass
                },
                beforeSend: function() {
                    btn.disabled = true;
                },
                success: function(ajaxD) {
                    if (ajaxD.info !== 'success') {
                        showError(ajaxD.message);
                        btn.disabled = false;
                        return false;
                    }

                    var authOtpTop = document.getElementById('auth-otp-border');
                    authOtpTop.className = authOtpTop.className.replace(/\bd-none\b/g, "");

                    // make inputs readonly.
                    iMobile.disabled = true;
                    iPass.disabled = true;

                    data.isValidated = true;
                    data.seq = ajaxD.message.seq;
                    data.otp = ajaxD.message.otp;

                    var iotp = document.getElementById('login-otp');
                    var otpBtn = document.getElementById('otp-submit');

                    iotp.value = data.otp;

                    $(otpBtn).trigger('click');

                    otp.validateOtpLink();
                    otp.seq = data.seq;
                    var f = function() {
                        otp.validateOtpLink();
                        setTimeout(f, 1000);
                    };
                    setTimeout(f, 1000);
                },
                error: function() {

                },
                complete: function() {

                }
            });


        }, false);

        var otpBtn = document.getElementById('otp-submit');
        otpBtn.addEventListener('click', function(e) {

            var iotp = document.getElementById('login-otp');
            var otpV = iotp.value.trim();

            var btn = this;

            if (otpV === '') {
                showError("Please enter OTP");
                return false;
            }

            // Now send the ajax again with otp.
            $.ajax({
                url: homeUrl + '/ajax/validate-login-fields',
                dataType: 'JSON',
                type: "POST",
                data: {
                    validate: 0,
                    seq: data.seq,
                    otp: otpV,
                },
                beforeSend: function() {
                    btn.disabled = true;
                },
                success: function(data) {
                    if (data.info !== 'success') {
                        showError(data.message);
                        return false;
                    }

                    window.location.href = data.message.redirectUrl;
                },
                error: function() {

                },
                complete: function() {
                    btn.disabled = false;
                }
            });

        }, false);

        function showError(msg) {
            var alertBox = document.getElementById('alert-box');
            alertBox.innerHTML = msg;
            alertBox.className = alertBox.className.replace(/\bd-none\b/g, "");
        }

        function hideError() {
            var alert = document.getElementById('alert-box');
            alert.className += ' d-none ';
        }

        var otp = {
            timeGap: 40,

            seq: 0,

            validateOtpLink: function() {
                var text = this.timeGap < 1 ? "Click to to resend otp now" : "Wait " + this.timeGap + " seconds to resend otp.";

                $('.resend-otp').html(text);
                $('.resend-otp').off();

                if (this.timeGap < 1) {
                    $('.resend-otp').on('click', function() {

                        $.ajax({
                            url: homeUrl + '/ajax/resend-otp',
                            type: "POST",
                            data: {
                                seq: 1,
                            },
                        });

                        otp.resetTimeGap();
                    });
                }

                this.timeGap -= 1;
            },

            resetTimeGap: function() {
                this.timeGap = 41;
                this.validateOtpLink();
            }
        }
    </script>
</body> -->
<?php // $this->_addFooter(); 
?>