<?php
$this->onlyLoggedInAllowed();


// First get the user details.
/** @var User $user */
$user = $this->getUser();
$userExtras = $user->getExtraInfo();
$states = StateManager::getInstance()->getAllStates();

$email = filter_input(INPUT_POST, 'new-email', FILTER_SANITIZE_STRING);

$msg = null;

if ( $this->isPost() ) {
    if ( !Validator::isValidEmail($email) ) {
        $msg = "Please provide a valid email.";
    }

    if ( empty($msg) ) {
        UserDatabase::getInstance()->updateUser($user->getId(), $user->getFirstName(), $user->getLastName(), $email, $user->getPhone(), $user->getPasswordHash(), $user->getType());
        $msg = "Your email details is successfully updated.";
    }
}


$this->_addHeader();
@include $this->getPath() . DS . 'navs.top.php';

?>
<div class="row">

    <div class="col-sm-8 col-xxxl-8">
        <div class="element-wrapper">
            <h6 class="element-header">
                Change Email
            </h6>


            <?php if ( !empty($msg) ) : ?>
                <div class="alert alert-success alert-dismissible " role="alert" area-label="Close">
                    <?php echo $msg; ?>
                    <button aria-label="close" class="close" data-dismiss="alert" type="button">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            <?php endif; ?>

            <div class="element-box">
                <ul class="nav nav-tabs custom-tab">
                    <li><a href="/account-general-settings">General Setings</a></li>
                    <li><a href="#">Change Email</a></li>
                    <li><a href="/account-password-settings">Change Password</a></li>
                </ul>

                <div class="fs">
                    <form action="" method="post">
                        <div class="form-group">
                            <label for="old-email">Your Old Email</label><input class="form-control" placeholder="<?php echo $user->getEmail(); ?>" type="text" id="old-email" name="old-email" disabled>
                        </div>
                        <div class="form-group">
                            <label for="new-email">Enter Your New Email</label><input class="form-control" placeholder="Type Your New Email Address" type="email" id="new-email" name="new-email">
                        </div>
<!--                        <div class="row">-->
<!--                            <div class="col-sm-6 col-xxxl-6">-->
<!--                                <div class="form-group">-->
<!--                                    <label for="verify-email">Verify Email</label><input class="form-control" placeholder="Input Your Verification Code" type="text" id="verify-email" name="verify-email">-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="col-sm-6 col-xxxl-6">-->
<!--                            </div>-->
<!--                        </div>-->
                        <div class="buttons">
                            <button class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-4 col-xxxl-4">
    </div>

</div>
<?php
@include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();