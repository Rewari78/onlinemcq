<?php

$this->onlyLoggedInAllowed(UserManager::USER_TYPE_TEACHER);
$testId = filter_input(INPUT_POST, 'test-id', FILTER_SANITIZE_NUMBER_INT);
$copy_error = false;

if ( !empty($testId) )
{

    $testInfo = TestManager::getInstance()->getTestInfo($testId);

    if ( !empty($testInfo) && $_SESSION['schoolId'] === $testInfo['school_id'] )
    {
        $t = TestManager::getInstance()->createTest(
            $_SESSION['schoolId'],
            $_SESSION['username'],
            $testInfo['subject'],
            $testInfo['class'],
            $testInfo['divisions'],
            $testInfo['description'],
            $testInfo['startAt'],
	        $_SESSION['name'],
            $testInfo['totalTime']
        );
        $allQuestions = TestManager::getInstance()->getAllQuestions($testId);


        foreach( $allQuestions as $questions )
        {
            $result = TestManager::getInstance()->updateQuestion(
                null,
                $t,
                $questions['questionNo'],
                $questions['question'],
                $questions['ans1'],
                $questions['ans2'],
                $questions['ans3'],
                $questions['ans4'],
                $questions['ans5'],
                $questions['correctAns'],
                $questions['exp'],
                $questions['photo'],
                $questions['weight']
            );
        }

        $this->redirect('manage-test');
    } else {
	    $copy_error = true;
    }
}

$this->_addHeader();
?>

<div class="container d-flex h-100 flex-column">
    <div class="row h-100 justify-content-center align-items-center">
        <div class="col-md-4 m-auto menu_box">
            <form id="add_test_form" method="POST">
                <h3 class="text-center text-uppercase pt-2 pb-3">Test Copier</h3><?php
                if ( $copy_error ) { ?>
                    <div class="alert alert-danger alert-dismissible" role="alert" area-label="Close" class="close">
                        Not a valid Test ID. Try a different one.
                        <button aria-label="close" class="close button-close" onclick="closeError()" data-dismiss="alert" type="button"><span aria-hidden="true">&times;</span></button>
                    </div><?php
                } ?>
                <div class="form-group">
                    <label for="test-desc">Test Id</label>
                    <input class="form-control" id="test-id" name="test-id" placeholder="Give id of the test you want to copy" required>
                </div>
                <div class="form-group">
                    <button id="form_submit_button" type="submit" class="btn btn-primary btn-block btn-lg">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
	function closeError(e) {
		$(".alert-danger").hide();
		$(".alert-danger").empty();
	}
</script>
<?php
$this->_addFooter();
