<?php

$this->onlyLoggedInAllowed( UserManager::USER_TYPE_SUBSCRIBER );

/** @var User $user */
$user = $this->getUser();


$msg = "";

if ( $this->isPost() )
{
    $qtype_id = filter_input(INPUT_POST, 'qtype_id');
    $q_title = filter_input(INPUT_POST, 'q_title');
    $q_des = filter_input(INPUT_POST, 'q_des');
    if ( empty($qtype_id) || empty($q_title) ||empty($q_des))
    {
        $msg = "Please try again.";
    }else {
        // We need to
        QNA::getInstance()->addQues($user->getId(),$qtype_id,$q_title, $q_des);
        $msg = "New question is added.";
    }
}

$packageDetails = $user->getPackageDetails();
$userQuestions = QNA::getInstance()->getQuestionsByUser($user->getId(), 0, true);

if ( $packageDetails['id'] == 1 )
{
    if ( $userQuestions >= 2 ) throw new RenderUpgradePackage(
            "Can not ask more than 2 questions",
            "Please upgrade this package to answer more than two questions."
    );
}else if ( $packageDetails['id'] == 2 )
{
    if ( $userQuestions >= 5 ) throw new RenderUpgradePackage(
        "Can not ask more than 5 questions",
        "Please upgrade this package to answer more than give questions."
    );
}

$this->_addHeader();
@include $this->getPath() . DS . 'navs.top.php';
?>





	<div class="container dash-container">
		<h6 class="custm-ele-hdr">
            Question-Answer Panel <?php echo !empty($msg) ? " - " . $msg : ""; ?>
        </h6>
        <div class="custm-ele-hdr-strk"></div>
        <div class="row dash-row-one">
            <div class="col-md-12">
                <div class="dash-neet-ug">
                     <div class="dnu-body">
                     	<form action="/ask_question" method="post">
                     	<select class="browser-default custom-select" id="qtype_id" name="qtype_id">
  <option selected>Select Your Study Material</option>
  <option value="1">NEET</option>
  <option value="2">AIIMS</option>
  <option value="3">JIPMER</option>
  <option value="4">GENERAL</option>
</select>
 <label></br>Ask A Question:</label>
    <input type="text" class="form-control" id="q_title" name="q_title"></br>
    <label for="q_des">Put Your Question Description</label></br>
    <textarea class="form-control" id="q_des" rows="10" name="q_des"></textarea></br>
    <button type="submit" class="btn btn-primary" style="width: 100px;">Ask</button>
</form>

                     </div>
                </div>
            </div>
        </div>

	</div>



<?php @include $this->getPath() . DS . 'navs.bottom.php'; ?>
<?php $this->_addFooter(); ?>