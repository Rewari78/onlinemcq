<?php
$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );

// get the package id.
$packageId = $this->_data[0];
$packageId = filter_var(filter_var($packageId, FILTER_SANITIZE_NUMBER_INT), FILTER_VALIDATE_INT);

if ( empty($packageId) ) $this->redirect('manage-packages');

$packageDetails = PackageManager::getInstance()->getAPackageById($packageId);

if ( empty($packageDetails) ) $this->redirect('manage-packages');

$packageName = filter_input(INPUT_POST, 'package-name');
$price = filter_input(INPUT_POST, 'package-price', FILTER_SANITIZE_NUMBER_INT);
$offPrice = filter_input(INPUT_POST, 'discount-price', FILTER_SANITIZE_NUMBER_INT);

// validate the package name and price.
if ( empty($packageName)  ) $this->redirect('edit-package/' . $packageId);

PackageManager::getInstance()
    ->updatePackage($packageId, $packageName, $price, $offPrice);


$this->redirect('edit-package/' . $packageId . '?up_success=1');
