<?php

/**
 * Created by PhpStorm.
 * User: akash
 * Date: 18/12/18
 * Time: 8:59 PM
 */

$this->onlyLoggedInAllowed(UserManager::USER_TYPE_TEACHER);

$testId = filter_input(INPUT_GET, 'test-id', FILTER_SANITIZE_NUMBER_INT);
$publishedTest = filter_input(INPUT_GET, 'published', FILTER_SANITIZE_NUMBER_INT);
$deletedTest = filter_input(INPUT_GET, 'deleted', FILTER_SANITIZE_NUMBER_INT);
$updatedTest = filter_input(INPUT_GET, 'updated', FILTER_SANITIZE_NUMBER_INT);

$msg = null;

if ($updatedTest == 1) {
    TestManager::getInstance()->deleteTest($testId);
    $msg = 'Test updated successfully.';
}

if ($deletedTest == 1 && !empty($testId)) {
    TestManager::getInstance()->deleteTest($testId);
    $msg = 'Test deleted successfully.';
}

if ($publishedTest == 1 && !empty($testId)) {
    $testInfo = TestManager::getInstance()->getTestInfo($testId);
    $testQuestions = TestManager::getInstance()->getAllQuestions($testInfo['id']);
    $testQuestionsCount = (string) count($testQuestions);

    $responses = [];

    foreach (json_decode($testInfo['divisions'], true) as $division) {
        $teacher = UserManager::getInstance();

        // set post fields
        $post = json_encode(array(
            "instDbValue" => $testInfo['school_id'],
            "teacherUserName" => $teacher->get('name'),
            "teacherId" => $teacher->get('userId'),
            "className" => $testInfo['class'],
            "divName" => $division['divisionName'],
            "testId" => $testInfo['id'],
            "testSubject" => rawurlencode( $testInfo['subject'] ),
            "testDescription" => rawurlencode( $testInfo['description'] ),
            'testStartTime' => date('d-m-Y, l g:i a', strtotime($testInfo['startAt'])),
            "testDuration" => "{$testInfo['totalTime']} Minutes",
            "noOfQuestion" => "{$testQuestionsCount}",
        ));

        //var_dump($post);
        //exit;

        $ch = curl_init('http://pinnacleapp.in/MobileApp/rest/adminAlert/submitOnlineTest');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        $json_string = curl_exec($ch);
        $responses[] = $json_string;
        curl_close($ch);
    }

    foreach ($responses as $response) {
            if (strpos($response, 'Failed') !== false) {
                die('Publish api not working properly, Please try again later. Tech message' . $response);
                }
        //if ($response == '{"1":{"success":"Test accepted successfully"}}' ) {
          //  die('Publish api not working properly, Please try again later. Tech message' . $response);
        //}
    }

    TestManager::getInstance()->updateTest($testId, $testInfo['subject'], $testInfo['class'], $testInfo['divisions'], $testInfo['description'], $testInfo['startAt'], $testInfo['totalTime'], true);

    header('Location: ' . SITE_URL . '/view-test?published=1');
}

$allTests = TestManager::getInstance()->getAllTestBySchool($_SESSION['schoolId']);

$this->_addHeader();
// @include $this->getPath() . DS . 'navs.top.php';
?>




<div class="container mt-3">
    <?php if (!empty($msg)) : ?>
        <div class="alert alert-success alert-dismissible" role="alert" area-label="Close" class="close">
            <?php echo $msg; ?>
            <!-- <button aria-label="close" class="close" data-dismiss="alert" type="button"><span aria-hidden="true">&times;</span> </button> -->
        </div>
    <?php endif; ?>
    <div class="text-center mb-3">
        <a class="btn btn-lg btn-primary text-uppercase" href="/add-test">Add a new test</a>
    </div>
    <h2 class="text-left mb-4">Log of Un Published Test -</h2>
    <div class="row">
        <div class="col-md-12">
            <table id="example" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>DATE</th>
                        <th>STD</th>
                        <th>DIV</th>
                        <th>SUBJECT</th>
                        <th>DESCRIPTION</th>
                        <th>NO of QUESTIONS</th>
                        <th>PUBLISHED BY</th>
                        <th>TIME</th>
                        <th>ACTIONS</th>
                    </tr>
                </thead>
                <tbody>

                    <?php if (count($allTests) >= 1) { ?>

                        <?php
                        foreach ($allTests as $test)
                        {
                            if ( $test['teacherId'] != $_SESSION['username'] ) continue;

                            if ($test['published'] == 0)
                            {
                                $testQuestions = TestManager::getInstance()->getAllQuestions($test['id']);
                                // $firstQuestionId = (!empty($testQuestions)) ? $testQuestions[0]['id'] : 1;
                        ?>
                                <tr>
                                    <td><?php echo $test['id']; ?></td>
                                    <td><?php echo  date('d/m/Y g:i a', strtotime( $test['startAt'] ) ); ?></td>
                                    <td><?php echo $test['class']; ?></td>
                                    <td>
                                        <?php
                                        $divsArray = [];
                                        $divs = json_decode($test['divisions'], true);
                                        foreach ($divs as $div) {
                                            $divsArray[] = $div['divisionName'];
                                        }
                                        echo implode(',', $divsArray);
                                        ?>
                                    </td>
                                    <td><?php echo htmlentities($test['subject']); ?></td>
                                    <td><?php echo htmlentities($test['description']); ?></td>
                                    <td>
                                        <?php

                                        echo count($testQuestions);
                                        ?>
                                    </td>
                                    <td><?php echo htmlentities($test['createdBy']); ?></td>
                                    <td><?php echo $test['totalTime']; ?></td>
                                    <td>
                                        <ul>
                                            <li><a href="<?php echo SITE_URL . '/edit-test?test-id=' . $test['id']; ?>">Edit Info</a></li>
                                            <li><a href="<?php echo SITE_URL . '/edit-questions?test-id=' . $test['id'] . '&question-id=1'; ?>">Edit Questions</a></li>
                                            <li><a href="<?php echo SITE_URL . '/take-test?test-id=' . $test['id']; ?>">Review</a></li>
                                            <li><a href="javascript:void(0)" class="publishTest" data-time="<?php echo strtotime( $test['startAt'] ); ?>" data-redirect-to="<?php echo SITE_URL . '/manage-test?test-id=' . $test['id'] . '&published=1'; ?>">Publish</a></li>
                                            <li><a href="javascript:void(0)" class="deleteTest" data-redirect-to="<?php echo SITE_URL . '/manage-test?test-id=' . $test['id'] . '&deleted=1'; ?>">Delete</a></li>
                                        </ul>
                                    </td>
                                </tr>
                            <?php } ?>
                        <?php } ?>

                    <?php } ?>


                </tbody>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>DATE</th>
                        <th>STD</th>
                        <th>DIV</th>
                        <th>SUBJECT</th>
                        <th>DESCRIPTION</th>
                        <th>NO of QUESTIONS</th>
                        <th>PUBLISHED BY</th>
                        <th>TIME</th>
                        <th>ACTIONS</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>


<script>
    $(function() {

		$(".publishTest").on('click', function() {

            if( $(this).data('time') * 1000 <= new Date().getTime() ) {
               alert( 'Date and Time to conduct test is already passed. Edit and pick a future date and time to publish test paper.' );
		    } else if (confirm('Are you sure you want to publish this test?')) {
				window.location.href = $(this).attr("data-redirect-to");
				var parentUl = $(this).parent().parent();
				parentUl.empty();
				parentUl.append('<li>Publishing Test...</li>');
			}
		});

        $(".deleteTest").on('click', function() {
            if (confirm('Are you sure you want to delete this test?')) {
                window.location.href = $(this).attr("data-redirect-to");
				var parentUl = $(this).parent().parent();
				parentUl.empty();
				parentUl.append('<li>Deleting Test...</li>');
            }
        });

        setTimeout(function() {
            $(".alert").fadeTo(500, 0).slideUp(500, function() {
                $(this).remove();
            });
        }, 3000);

        $('#example').DataTable({
            "language": {
                "emptyTable": "No unpublished test found."
            },
            "order": [[ 0, "desc" ]]
        });


    });
</script>

<?php

$this->_addFooter();
