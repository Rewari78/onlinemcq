<?php
$this->onlyLoggedInAllowed();


// First get the user details.
/** @var User $user */
$user = $this->getUser();
$userExtras = $user->getExtraInfo();
$states = StateManager::getInstance()->getAllStates();

$packageInfo = $user->getPackageDetails();

$firstName = filter_input(INPUT_POST, 'firstName', FILTER_SANITIZE_STRING);
$lastName = filter_input(INPUT_POST, 'lastName', FILTER_SANITIZE_STRING);
$s12 = filter_input(INPUT_POST, 's12', FILTER_SANITIZE_NUMBER_INT);
$pcb = filter_input(INPUT_POST, 'pcbPercent', FILTER_SANITIZE_NUMBER_INT);

$msg = null;
if ( $this->isPost() ) {
    switch ( $s12 )
    {
        case UserManager::STUDY_12_PASSOUT:
            if (  empty($pcb) || $pcb > 100 ) {
                $msg = "Your pcb percent is not valid. Eg. 70%.";
            }
            break;
        case UserManager::STUDY_12_APPEARED:
            $pcb = 0;
            break;
        default:
            $msg = "Please select you are appeared/passout.";
    }
}


if ( $this->isPost() && ( empty($firstName) || empty($lastName) ) ) {
    $msg = "First and and last name can not be empty.";
}

if ( empty($msg) && $this->isPost() ) {
    UserDatabase::getInstance()->updateUser($user->getId(), $firstName, $lastName, $user->getEmail(), $user->getPhone(), $user->getPasswordHash(), $user->getType());
    $extraInfo = $user->getExtraInfo();
    UserDatabase::getInstance()->saveExtraInfo($user->getId(), $extraInfo['stateId'], $s12, $pcb, $extraInfo['course'], $extraInfo['dob']);
    $msg = "Your account details is successfully updated.";
}

$user = new User($user->getId());
$userExtras = $user->getExtraInfo();


$this->_addHeader();
@include $this->getPath() . DS . 'navs.top.php';

?>

<div class="row">
        <div class="col-sm-8 col-xxxl-8">
            <div class="element-wrapper">
                <h6 class="element-header">
                    General Settings
                </h6>

                <?php if ( !empty($msg) ) : ?>
                    <div class="alert alert-success alert-dismissible " role="alert" area-label="Close">
                        <?php echo $msg; ?>
                        <button aria-label="close" class="close" data-dismiss="alert" type="button">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                <?php endif; ?>

                <div class="element-box">
                    <ul class="nav nav-tabs custom-tab">
                        <li><a href="#" class="active">General Setings</a></li>
                        <li><a href="/account-email-settings">Change Email</a></li>
                        <li><a href="/account-password-settings">Change Password</a></li>
                    </ul>

                    <div class="fs">
                        <form action="" method="post">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="firstName">First Name</label>
                                        <input class="form-control" id="firstName" name="firstName" placeholder="First Name" type="text" value="<?php echo Util::htmlEncode($user->getFirstName()) ?>">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="lastName">Last Name</label><input class="form-control" id="lastName" name="lastName" placeholder="Last Name" type="text" value="<?php echo Util::htmlEncode($user->getLastName()) ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="mt-3 mb-3">
                                <strong>Selected State</strong>
                                <?php
                                foreach ( $states as $state )
                                {
                                    if ( $state['id'] == $userExtras['stateId'] ) {
                                        echo ("{$state['name']}");
                                        break;
                                    }

                                }
                                ?>
                            </div>

                            <div class="form-group">
                                <label for="study12">12th Appeared / Passout</label>
                                <select class="form-control" required="required" id="study12" name="s12">
                                    <option value="1" <?php echo ( $userExtras['study12'] == 1 ? 'selected' : '' ) ?> >
                                        Appeared
                                    </option>
                                    <option value="2" <?php echo ( $userExtras['study12'] == 2 ? 'selected' : '' ) ?> >
                                        Passout
                                    </option>
                                </select>
                            </div>
                            <div class="form-group d-none">
                                <label for="pcbPercent">PCB %</label><input class="form-control" name="pcbPercent" id="pcbPercent" <?php if ( $userExtras['study12'] == 1 ? 'disabled' : '' ) ?>  placeholder="Enter your PCB %" type="text" value="<?php echo $userExtras['pcbPercent']; ?>%">
                            </div>
                            <div class="buttons">
                                <button class="btn btn-primary">Save</button>
                            </div>
                        </form>
                        <p class="mt-3 pt-2 mb-0 border-top d-none">Currently Active Package: <strong><?php echo $packageInfo['packageName']; ?></strong></p>

                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-4 col-xxxl-4">
        </div>
</div>

<script type="text/javascript">
    $(document).on('change', '#study12', function () {
        var value = $(this).val();
        if (  parseInt(value, 10) === 2 ) {
            $('#pcbPercent')[0].disabled = false;
        }else {
            $('#pcbPercent').val('')[0].disabled = true;
        }
    });
</script>

<?php
@include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();