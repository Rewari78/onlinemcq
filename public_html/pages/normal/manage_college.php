<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 15/12/18
 * Time: 12:52 PM
 */

$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );

$colleges = CollegeManager::getInstance()->getCollege();

$stateIds = array();
$cityIds = array();
$collegeTypeIds = array();
$universityIds = array();
$recogTypeIds = array();

foreach ( $colleges as $college )
{
    $stateIds[$college['stateId']] = $college['stateId'];
    $collegeTypeIds[$college['collegeTypeId']] = $college['collegeTypeId'];
    $cityIds[$college['cityId']] = $college['cityId'];
    $universityIds[$college['affUniversityId']] = $college['affUniversityId'];
    $recogTypeIds[$college['recognitionId']] = $college['recognitionId'];
}



$states = StateManager::getInstance()->getStateByIdList($stateIds);
$cities = StateManager::getInstance()->getCityByIdList($cityIds);
$collegeTypes = CollegeManager::getInstance()->getCollegeTypeIdList($collegeTypeIds);
$unversities = CollegeManager::getInstance()->getUniversitiesByIdList($universityIds);
$recogs = CollegeManager::getInstance()->getRecognitionsByIdList($recogTypeIds);


$msg = null;

$collegeUpdated = filter_input(INPUT_GET, 'college-updated');
$collegeAdded = filter_input(INPUT_GET, 'college-added');
if ( $collegeUpdated == '1' ) $msg = "College is successfully updated.";
if ( $collegeAdded == '1' ) $msg = "College is successfully added";



$this->_addHeader();
@include $this->getPath() . DS . 'navs.top.php';
?>
    <div class="row">
        <div class="col-sm-12 col-xxxl-12">
            <div class="element-wrapper">
                <?php if ( !empty($msg) ) : ?>
                    <div class="alert alert-success alert-dismissible " role="alert" area-label="Close" class="close">
                        <?php echo $msg; ?>
                        <button aria-label="close" class="close" data-dismiss="alert" type="button"><span aria-hidden="true">&times;</span> </button>
                    </div>
                <?php endif; ?>


                <div class="element-actions">
                    <a class="btn btn-primary custom-button" href="/add-college">Add College</a>
                </div>
                <h6 class="element-header">
                    College List
                </h6>
                <div class="element-box">
                    <div class="table-responsive">

                        <?php if ( empty($colleges) ): ?>
                            <p>No College found.</p>
                        <?php else:?>
                            <table class="table table-lightborder">

                                <tr>
                                    <td>
                                        Actions
                                    </td>
                                    <td>
                                        Id
                                    </td>
                                    <td class="text-left">
                                        College Name
                                    </td>
                                    <td>
                                        State
                                    </td>
                                    <td>
                                        City
                                    </td>
                                    <td>
                                        Est
                                    </td>
                                    <td>
                                        Affiliated
                                    </td>
                                    <td>
                                        Type
                                    </td>
                                    <td>
                                        Recog
                                    </td>
                                    <td>
                                        Gov Fee
                                    </td>
                                    <td>
                                        Nri Fee
                                    </td>
                                    <td>
                                        Private Fee
                                    </td>
                                    <td>
                                        Other Fee
                                    </td>
                                    <td>
                                        Cut Gen
                                    </td>
                                    <td>
                                        Cut Gen Ph
                                    </td>
                                    <td>
                                        Cut Obc
                                    </td>
                                    <td>
                                        Cut Obc Ph
                                    </td>
                                    <td>
                                        Cut Sc
                                    </td>
                                    <td>
                                        Cut Sc Ph
                                    </td>
                                    <td>
                                        Cut St
                                    </td>
                                    <td>
                                        Cut St Ph
                                    </td>
                                    <td>
                                        Website
                                    </td>
                                    <td>
                                       Total Seats
                                    </td>

                                </tr>

                                <?php foreach ( $colleges as $college ): ?>
                                    <tr>
                                        <td class="row-actions">
                                            <a href="/edit-college/<?php echo $college['id']; ?>" >
                                                <i class="os-icon os-icon-ui-49"></i>
                                            </a>

                                        </td>
                                        <td class="nowrap">
                                            <?php echo $college['id']; ?>
                                        </td>
                                        <td class="nowrap">
                                            <?php echo $college['collegeName'] ?>
                                        </td>
                                        <td class="nowrap">
                                            <?php echo !empty($states[$college['stateId']]) ? $states[$college['stateId']]['name'] : "Unknown"; ?>
                                        </td>
                                        <td class="nowrap">
                                            <?php echo !empty($cities[$college['cityId']]) ? $cities[$college['cityId']]['cityName'] : "Unknown"; ?>
                                        </td>
                                        <td class="nowrap">
                                            <?php echo $college['established']; ?>
                                        </td>
                                        <td class="nowrap">
                                            <?php echo !empty($unversities[$college['affUniversityId']]) ? $unversities[$college['affUniversityId']]['name'] : "Unknown"; ?>
                                        </td>
                                        <td class="nowrap">
                                            <?php echo !empty($collegeTypes[$college['collegeTypeId']]) ? $collegeTypes[$college['collegeTypeId']]['collegeType'] : "Unknown"; ?>
                                        </td>
                                        <td class="nowrap">
                                            <?php echo !empty($recogs[$college['recognitionId']]) ? $recogs[$college['recognitionId']]['text'] : "Unknown"; ?>
                                        </td>
                                        <td class="nowrap">
                                            <?php echo $college['govFee'] ?>
                                        </td>
                                        <td class="nowrap">
                                            <?php echo $college['nriFee'] ?>
                                        </td>
                                        <td class="nowrap">
                                            <?php echo $college['privateFee'] ?>
                                        </td>
                                        <td class="nowrap">
                                            <?php echo $college['otherFee'] ?>
                                        </td>
                                        <td class="nowrap">
                                            <?php echo $college['cutGen'] ?>
                                        </td>
                                        <td class="nowrap">
                                            <?php echo $college['cutGenPh'] ?>
                                        </td>
                                        <td class="nowrap">
                                            <?php echo $college['cutObc'] ?>
                                        </td>
                                        <td class="nowrap">
                                            <?php echo $college['cutObcPh'] ?>
                                        </td>
                                        <td class="nowrap">
                                            <?php echo $college['cutSc'] ?>
                                        </td>
                                        <td class="nowrap">
                                            <?php echo $college['cutScPh'] ?>
                                        </td>
                                        <td class="nowrap">
                                            <?php echo $college['cutSt'] ?>
                                        </td>
                                        <td class="nowrap">
                                            <?php echo $college['cutStPh'] ?>
                                        </td>
                                        <td class="nowrap">
                                            <?php echo $college['websiteUrl'] ?>
                                        </td>
                                        <td class="nowrap">
                                            <?php echo $college['totalSeats'] ?>
                                        </td>

                                    </tr>
                                <?php endforeach; ?>

                            </table>
                        <?php endif; ?>


                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-4 col-xxxl-4">
        </div>

    </div>

    <div aria-hidden="true" aria-labelledby="mySmallModalLabel" class="modal fade bd-example-modal-sm" id="add-college-type" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        Add New Type
                    </h5>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                </div>
                <form action="/add-college-type" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="college-type"> College Type</label>
                            <input class="form-control" placeholder="Type" type="text" id="college-type" name="college-type" />

                            <div class="alert alert-warning alert-dismissible d-none" role="alert" id="add-p-alert-box">
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" data-dismiss="modal" type="button"> Close</button>
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function submitForm( form )
        {
            var $form = $(form);
            var $alert = $form.find('.alert');
            var _data = $(form).serialize();

            hideError($alert);

            $.post(
                homeUrl + '/ajax/edit-college-type',
                $form.serialize(),
                function (data) {
                    if ( data.info !== 'success' )
                    {
                        showError($alert, data.message);
                        return;
                    }

                    window.location.reload();

                },
                'json'
            );
        }

        function showError( box, msg )
        {

            var $alertBox = $(box);
            $alertBox.html(msg);
            $alertBox.removeClass('d-none');
        }

        function hideError( box )
        {
            var $alertBox = $(box);
            $alertBox.addClass('d-none');
        }
    </script>

<?php
@include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();