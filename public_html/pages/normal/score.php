<?php

/**
 * Created by PhpStorm.
 * User: akash
 * Date: 13/4/19
 * Time: 3:22 AM
 */
if (!UserManager::getInstance()->isTeacher()) {
    $this->onlyLoggedInAllowed(UserManager::USER_TYPE_STUDENT);
}

$score = filter_input(INPUT_GET, 'score', FILTER_SANITIZE_NUMBER_INT);
$score = empty($score) ? 0 : $score;

$this->_addHeader();
// @include $this->getPath() . DS . 'navs.top.php';
?>
<div class="row">
    <div class="col-sm-12 col-xxxl-12">
        <div class="element-wrapper">

            <h1 class="custm-ele-hdr text-center">
                Your test is now finished, <br />
                You have scored <?php echo $score ?>.
            </h1>

        </div>
    </div>
</div>
<?php
// @include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();
