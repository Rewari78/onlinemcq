<?php

/** @var User $user */
$user = $this->getUser();
$package = $user->getPackageDetails();

$tranFailed = filter_input(INPUT_GET, 'tranFail');
if ( $tranFailed == 1 ) {
    $msg = "Sorry transaction is failed, Please retry.";
}

$this->_addHeader();
@include $this->getPath() . DS . 'navs.top.php';

?>
<div class="row">
    <div class="col-sm-12 col-xxxl-12">
        <div class="element-wrapper">

            <?php if ( isset($this->_data['others']) && is_array($this->_data['others']) ) : ?>
                <h6 class="custm-ele-hdr">
                    Please upgrade package to use feature - <?php echo $this->_data['others']['featureName']; ?>
                </h6>
                <div class="custm-ele-hdr-strk">
                </div>
                <div class="element-content">
                    <p><?php echo $this->_data['others']['desc']; ?></p>
                </div>
            <?php else: ?>
                <h6 class="custm-ele-hdr">
                    Upgrade Package
                </h6>
                <div class="custm-ele-hdr-strk"></div>
                <div class="element-content">
                    <p>orem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. </p>
                </div>
            <?php endif; ?>



            <?php if ( !empty($msg) ) : ?>
                <br />
                <br />
                <div class="alert alert-success alert-dismissible " role="alert" area-label="Close" class="close">
                    <?php echo $msg; ?>
                    <button aria-label="close" class="close" data-dismiss="alert" type="button"><span aria-hidden="true">&times;</span> </button>
                </div>
            <?php endif; ?>

        </div>
    </div>
</div>

<div class="pricing-item">
    <div class="pricing-header">
        <h3>
            <span class="left-heading">Free</span>

            <span class="right-heading">
                        <sup>₹</sup>0<span class="small-head"> /Yearly</span>
                       </span>
        </h3>
    </div>

    <div class="pricing-body">
        <p class="will-expand college-notification">
            <strong>College List <!-- <span>(Home State)</span> --></strong>
            <span class="pricing-expand"><span></span></span>
            <span class="pricing-collapse"><span></span></span>
        </p>
        <div class="notification-content">
            <div class="notification-content-body">
                <table>
                    <tbody><tr>
                        <th></th>
                        <th></th>
                        <th>Home State</th>
                        <th>All States</th>
                    </tr>
                    <tr>
                        <td>Government</td>
                        <td></td>
                        <td></td>
                        <td></td>

                    </tr>
                    <tr>
                        <td>Private</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Deemed</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>

                    </tr>

                    </tbody></table>
            </div>
        </div>
        <p class="no-check"><strong>College Predictor</strong><span></span></p>
        <p class="no-check"><strong>Mock Tests</strong><span></span></p>
        <p class="no-check"><strong>On Demand Study</strong><span></span></p>
        <p class="no-check"><strong>Video Lessons</strong><span></span></p>
        <p class="no-check"><strong>Study Materials</strong><span></span></p>
        <p class="no-check"><strong>Chat Support</strong><span></span></p>
        <p><strong>Q&amp;A <span>(Only 2)</span></strong><span></span></p>
        <p class="will-expand sms-notification no-check">
            <strong>SMS Alerts <!-- <span>(Home State)</span> --></strong>
            <span class="pricing-expand" style="display: block;"><span></span></span>
            <span class="pricing-collapse" style="display: none;"><span></span></span>
        </p>
        <div class="notification-content">
            <div class="notification-content-body">
                <table>
                    <tbody><tr>
                        <th></th>
                        <th>NEET UG</th>
                        <th>AIIMS</th>
                        <th>JIPMER</th>
                    </tr>
                    <tr>
                        <td>Application</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Registration</td>
                        <td></td>
                        <td></td>
                        <td></td>

                    </tr>
                    <tr>
                        <td>Counselling</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>

                    </tr>
                    <tr>
                        <td>Offline Reg.</td>
                        <td></td>
                        <td></td>
                        <td></td>

                    </tr>

                    </tbody></table>
            </div>
        </div>
        <p class="will-expand email-notification no-check">
            <strong>Email Alerts <!-- <span>(Home State)</span> --></strong>
            <span class="pricing-expand" style="display: block;"><span></span></span>
            <span class="pricing-collapse" style="display: none;"><span></span></span>
        </p>
        <div class="notification-content">
            <div class="notification-content-body">
                <table>
                    <tbody><tr>
                        <th></th>
                        <th>NEET UG</th>
                        <th>AIIMS</th>
                        <th>JIPMER</th>
                    </tr>
                    <tr>
                        <td>Application</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Registration</td>
                        <td></td>
                        <td></td>
                        <td></td>

                    </tr>
                    <tr>
                        <td>Counselling</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>

                    </tr>
                    <tr>
                        <td>Offline Reg.</td>
                        <td></td>
                        <td></td>
                        <td></td>

                    </tr>

                    </tbody></table>
            </div>
        </div>
        <p class="will-expand dash-notification">
            <strong>Dash Alerts <span>(Home State)</span></strong>
            <span class="pricing-expand"><span></span></span>
            <span class="pricing-collapse" style="display: none;"><span></span></span>
        </p>
        <div class="notification-content">
            <div class="notification-content-body">
                <table>
                    <tbody><tr>
                        <th></th>
                        <th>NEET UG</th>
                        <th>AIIMS</th>
                        <th>JIPMER</th>
                    </tr>
                    <tr>
                        <td>Application</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Registration</td>
                        <td></td>
                        <td></td>
                        <td></td>

                    </tr>
                    <tr>
                        <td>Counselling</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>

                    </tr>
                    <tr>
                        <td>Offline Reg.</td>
                        <td></td>
                        <td></td>
                        <td></td>

                    </tr>

                    </tbody></table>
            </div>
        </div>
    </div>

    <div class="pricing-footer">
        <span class="pricing-terms">* Terms & Conditions Apply.</span>
<!--        <a href="#" class="btn btn-primary btn-grey">Upgrade</a>-->
    </div>
</div>

<div class="pricing-item">
    <div class="pricing-header">
        <h3>
            <span class="left-heading">Silver</span>

            <span class="right-heading">
                          <sup>₹</sup>599<span class="small-head"> /Yearly</span>
                         </span>
        </h3>
    </div>

    <div class="pricing-body">
        <p class="will-expand college-notification">
            <strong>College List <!-- <span>(Home State)</span> --></strong>
            <span class="pricing-expand"><span></span></span>
            <span class="pricing-collapse"><span></span></span>
        </p>
        <div class="notification-content">
            <div class="notification-content-body">
                <table>
                    <tbody><tr>
                        <th></th>
                        <th></th>
                        <th>Home State</th>
                        <th>All States</th>
                    </tr>
                    <tr>
                        <td>Government</td>
                        <td></td>
                        <td></td>
                        <td></td>

                    </tr>
                    <tr>
                        <td>Private</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Deemed</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>

                    </tr>

                    </tbody></table>
            </div>
        </div>
        <p class="no-check"><strong>College Predictor</strong><span></span></p>
        <p><strong>Mock Tests <span>(1/Subject)</span></strong><span></span></p>
        <p class="no-check"><strong>On Demand Study</strong><span></span></p>
        <p class="no-check"><strong>Video Lessons</strong><span></span></p>
        <p class="no-check"><strong>Study Materials</strong><span></span></p>
        <p class="no-check"><strong>Chat Support</strong><span></span></p>
        <p><strong>Q&amp;A <span>(Only 5)</span></strong><span></span></p>
        <p class="will-expand sms-notification no-check">
            <strong>SMS Alerts <!-- <span>(Home State)</span> --></strong>
            <span class="pricing-expand" style="display: block;"><span></span></span>
            <span class="pricing-collapse" style="display: none;"><span></span></span>
        </p>
        <div class="notification-content">
            <div class="notification-content-body">
                <table>
                    <tbody><tr>
                        <th></th>
                        <th>NEET UG</th>
                        <th>AIIMS</th>
                        <th>JIPMER</th>
                    </tr>
                    <tr>
                        <td>Application</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Registration</td>
                        <td></td>
                        <td></td>
                        <td></td>

                    </tr>
                    <tr>
                        <td>Counselling</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>

                    </tr>
                    <tr>
                        <td>Offline Reg.</td>
                        <td></td>
                        <td></td>
                        <td></td>

                    </tr>

                    </tbody></table>
            </div>
        </div>
        <p class="will-expand  email-notification">
            <strong>Email Alerts <span>(Home State)</span></strong>
            <span class="pricing-expand"><span></span></span>
            <span class="pricing-collapse" style="display: none;"><span></span></span>
        </p>
        <div class="notification-content">
            <div class="notification-content-body">
                <table>
                    <tbody><tr>
                        <th></th>
                        <th>NEET UG</th>
                        <th>AIIMS</th>
                        <th>JIPMER</th>
                    </tr>
                    <tr>
                        <td>Application</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Registration</td>
                        <td></td>
                        <td></td>
                        <td></td>

                    </tr>
                    <tr>
                        <td>Counselling</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>

                    </tr>
                    <tr>
                        <td>Offline Reg.</td>
                        <td></td>
                        <td></td>
                        <td></td>

                    </tr>

                    </tbody></table>
            </div>
        </div>
        <p class="will-expand dash-notification">
            <strong>Dash Alerts <span>(Home State)</span></strong>
            <span class="pricing-expand"><span></span></span>
            <span class="pricing-collapse" style="display: none;"><span></span></span>
        </p>
        <div class="notification-content ">
            <div class="notification-content-body">
                <table>
                    <tbody><tr>
                        <th></th>
                        <th>NEET UG</th>
                        <th>AIIMS</th>
                        <th>JIPMER</th>
                    </tr>
                    <tr>
                        <td>Application</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Registration</td>
                        <td></td>
                        <td></td>
                        <td></td>

                    </tr>
                    <tr>
                        <td>Counselling</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>

                    </tr>
                    <tr>
                        <td>Offline Reg.</td>
                        <td></td>
                        <td></td>
                        <td></td>

                    </tr>

                    </tbody></table>
            </div>
        </div>
    </div>

    <div class="pricing-footer">
        <span class="pricing-terms">* Terms & Conditions Apply.</span>
        <?php if ( $package['price'] < 599 ): ?>
            <a href="#" class="btn btn-primary" data-package-id="2">Upgrade</a>
        <?php endif; ?>
    </div>
</div>

<div class="pricing-item">
    <div class="pricing-header">
        <h3>
            <span class="left-heading">Gold</span>

            <span class="right-heading">
                            <sup>₹</sup>1999<span class="small-head"> /Yearly</span>
                           </span>
        </h3>
    </div>

    <div class="pricing-body">
        <p class="will-expand college-notification">
            <strong>College List <!-- <span>(Premium Filters)</span> --></strong>
            <span class="pricing-expand"><span></span></span>
            <span class="pricing-collapse"><span></span></span>
        </p>
        <div class="notification-content">
            <div class="notification-content-body">
                <table>
                    <tbody><tr>
                        <th></th>
                        <th></th>
                        <th>Home State</th>
                        <th>All Statse</th>
                    </tr>
                    <tr>
                        <td>Government</td>
                        <td></td>
                        <td></td>
                        <td></td>

                    </tr>
                    <tr>
                        <td>Private</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Deemed</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>

                    </tr>

                    </tbody></table>
            </div>
        </div>
        <p><strong>College Predictor</strong><span></span></p>
        <p><strong>Mock Tests <span>(5/Subject)</span></strong><span></span></p>
        <p class="no-check"><strong>On Demand Study</strong><span></span></p>
        <p><strong>Video Lessons</strong><span></span></p>
        <p><strong>Study Materials</strong><span></span></p>
        <p><strong>Chat Support</strong><span></span></p>
        <p><strong>Q&amp;A <span>(Unlimited)</span></strong><span></span></p>
        <p class="will-expand sms-notification">
            <strong>SMS Alerts <span>(All State)</span></strong>
            <span class="pricing-expand"><span></span></span>
            <span class="pricing-collapse"><span></span></span>
        </p>
        <div class="notification-content">
            <div class="notification-content-body">
                <table>
                    <tbody><tr>
                        <th></th>
                        <th>NEET UG</th>
                        <th>AIIMS</th>
                        <th>JIPMER</th>
                    </tr>
                    <tr>
                        <td>Application</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Registration</td>
                        <td></td>
                        <td></td>
                        <td></td>

                    </tr>
                    <tr>
                        <td>Counselling</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>

                    </tr>
                    <tr>
                        <td>Offline Reg.</td>
                        <td></td>
                        <td></td>
                        <td></td>

                    </tr>

                    </tbody></table>
            </div>
        </div>
        <p class="will-expand email-notification">
            <strong>Email Alerts <span>(All State)</span></strong>
            <span class="pricing-expand"><span></span></span>
            <span class="pricing-collapse"><span></span></span>
        </p>
        <div class="notification-content">
            <div class="notification-content-body">
                <table>
                    <tbody><tr>
                        <th></th>
                        <th>NEET UG</th>
                        <th>AIIMS</th>
                        <th>JIPMER</th>
                    </tr>
                    <tr>
                        <td>Application</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Registration</td>
                        <td></td>
                        <td></td>
                        <td></td>

                    </tr>
                    <tr>
                        <td>Counselling</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>

                    </tr>
                    <tr>
                        <td>Offline Reg.</td>
                        <td></td>
                        <td></td>
                        <td></td>

                    </tr>

                    </tbody></table>
            </div>
        </div>
        <p class="will-expand dash-notification">
            <strong>Dash Alerts <span>(All State)</span></strong>
            <span class="pricing-expand"><span></span></span>
            <span class="pricing-collapse"><span></span></span>
        </p>
        <div class="notification-content">
            <div class="notification-content-body">
                <table>
                    <tbody><tr>
                        <th></th>
                        <th>NEET UG</th>
                        <th>AIIMS</th>
                        <th>JIPMER</th>
                    </tr>
                    <tr>
                        <td>Application</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Registration</td>
                        <td></td>
                        <td></td>
                        <td></td>

                    </tr>
                    <tr>
                        <td>Counselling</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>

                    </tr>
                    <tr>
                        <td>Offline Reg.</td>
                        <td></td>
                        <td></td>
                        <td></td>

                    </tr>

                    </tbody></table>
            </div>
        </div>
    </div>

    <div class="pricing-footer">
        <span class="pricing-terms">* Terms & Conditions Apply.</span>
        <?php if ( $package['price'] < 1999 ): ?>
            <a href="#" class="btn btn-primary" data-package-id="3">Upgrade</a>
        <?php endif; ?>
    </div>
</div>

<div class="pricing-item">
    <div class="pricing-header">
        <h3>
            <span class="left-heading">Platinum</span>

            <span class="right-heading">
                              <sup>₹</sup>2499<span class="small-head"> /Yearly</span>
                             </span>
        </h3>
    </div>

    <div class="pricing-body">
        <p class="will-expand college-notification">
            <strong>College List <!-- <span>(Premium Filters)</span> --></strong>
            <span class="pricing-expand"><span></span></span>
            <span class="pricing-collapse"><span></span></span>
        </p>
        <div class="notification-content">
            <div class="notification-content-body">
                <table>
                    <tbody><tr>
                        <th></th>
                        <th></th>
                        <th>Home State</th>
                        <th>All Statse</th>
                    </tr>
                    <tr>
                        <td>Government</td>
                        <td></td>
                        <td></td>
                        <td></td>

                    </tr>
                    <tr>
                        <td>Private</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Deemed</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>

                    </tr>

                    </tbody></table>
            </div>
        </div>
        <p><strong>College Predictor</strong><span></span></p>
        <p><strong>Mock Tests <span>(5/Subject)</span></strong><span></span></p>
        <p class="no-check"><strong>On Demand Study</strong><span></span></p>
        <p><strong>Video Lessons</strong><span></span></p>
        <p><strong>Study Materials</strong><span></span></p>
        <p><strong>Chat Support</strong><span></span></p>
        <p><strong>Q&amp;A <span>(Unlimited)</span></strong><span></span></p>
        <p class="will-expand sms-notification">
            <strong>SMS Alerts <span>(All State)</span></strong>
            <span class="pricing-expand"><span></span></span>
            <span class="pricing-collapse"><span></span></span>
        </p>
        <div class="notification-content">
            <div class="notification-content-body">
                <table>
                    <tbody><tr>
                        <th></th>
                        <th>NEET UG</th>
                        <th>AIIMS</th>
                        <th>JIPMER</th>
                    </tr>
                    <tr>
                        <td>Application</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Registration</td>
                        <td></td>
                        <td></td>
                        <td></td>

                    </tr>
                    <tr>
                        <td>Counselling</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>

                    </tr>
                    <tr>
                        <td>Offline Reg.</td>
                        <td></td>
                        <td></td>
                        <td></td>

                    </tr>

                    </tbody></table>
            </div>
        </div>
        <p class="will-expand email-notification">
            <strong>Email Alerts <span>(All State)</span></strong>
            <span class="pricing-expand"><span></span></span>
            <span class="pricing-collapse"><span></span></span>
        </p>
        <div class="notification-content">
            <div class="notification-content-body">
                <table>
                    <tbody><tr>
                        <th></th>
                        <th>NEET UG</th>
                        <th>AIIMS</th>
                        <th>JIPMER</th>
                    </tr>
                    <tr>
                        <td>Application</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Registration</td>
                        <td></td>
                        <td></td>
                        <td></td>

                    </tr>
                    <tr>
                        <td>Counselling</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>

                    </tr>
                    <tr>
                        <td>Offline Reg.</td>
                        <td></td>
                        <td></td>
                        <td></td>

                    </tr>

                    </tbody></table>
            </div>
        </div>
        <p class="will-expand dash-notification">
            <strong>Dash Alerts <span>(All State)</span></strong>
            <span class="pricing-expand"><span></span></span>
            <span class="pricing-collapse"><span></span></span>
        </p>
        <div class="notification-content">
            <div class="notification-content-body">
                <table>
                    <tbody><tr>
                        <th></th>
                        <th>NEET UG</th>
                        <th>AIIMS</th>
                        <th>JIPMER</th>
                    </tr>
                    <tr>
                        <td>Application</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Registration</td>
                        <td></td>
                        <td></td>
                        <td></td>

                    </tr>
                    <tr>
                        <td>Counselling</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>

                    </tr>
                    <tr>
                        <td>Offline Reg.</td>
                        <td></td>
                        <td></td>
                        <td></td>

                    </tr>

                    </tbody></table>
            </div>
        </div>
    </div>

    <div class="pricing-footer">
        <span class="pricing-terms">* Terms & Conditions Apply.</span>
        <?php if ( $package['price'] < 2499 ): ?>
            <a href="#" class="btn btn-primary" data-package-id="4">Upgrade</a>
        <?php endif; ?>
    </div>
</div>

<script type="text/javascript">
    $('.will-expand').on('click', function() {
        $(this).next('.notification-content').toggleClass('hide');

        if ( $(this).next('.notification-content').hasClass('hide') ) {
            $(this).find('.pricing-collapse').hide();
            $(this).find('.pricing-expand').show();
        } else {
            $(this).find('.pricing-collapse').show();
            $(this).find('.pricing-expand').hide();
        }


    });

    var isAjax = false;

    $('.pricing-item a.btn-primary').on('click', function (e) {
        e.stopPropagation();
        e.preventDefault();

        if ( isAjax ) return;

        var packageId = $(this).data('package-id');

        $.ajax({
            url: homeUrl + '/ajax/upgrade-ajax-package',
            dataType: 'JSON',
            type: "POST",
            data: {
                packageId: packageId
            },
            beforeSend: function () {
                isAjax = true;
            },
            success: function ( data ) {
                if ( data.info !== 'success' ) {
                    showError(data.message);
                    return false;
                }

                // We are going to check for the
                launchBOLT(
                    data.message.key,
                    data.message.txnid,
                    data.message.hash,
                    data.message.amount,
                    data.message.firstname,
                    data.message.email,
                    data.message.phone,
                    data.message.productinfo,
                    data.message.udf5,
                    data.message.surl,
                    data.message.furl
                )

                // window.location.href = data.message.redirectUrl;
            },
            error: function () {

            },
            complete: function () {
                isAjax = false;
            }
        });
    });

    function launchBOLT( key, txnid, hash, amount, firstName, email, phone, pInfo, udf5, surl, furl )
    {
        var reuest = {
            key: key,
            txnid: txnid,
            hash: hash,
            amount: amount,
            firstname: firstName,
            email: email,
            phone: phone,
            productinfo: pInfo,
            udf5: udf5,
            surl : surl,
            furl: furl,
            mode: 'dropout'
        };

        var handlers = {
            responseHandler: function (BOLT) {
                if (BOLT.response.txnStatus != 'CANCEL') {
                    // //Salt is passd here for demo purpose only. For practical use keep salt at server side only.
                    // var fr = '<form action=\"' + $('#surl').val() + '\" method=\"post\">' +
                    //     '<input type=\"hidden\" name=\"key\" value=\"' + BOLT.response.key + '\" />' +
                    //     '<input type=\"hidden\" name=\"salt\" value=\"' + $('#salt').val() + '\" />' +
                    //     '<input type=\"hidden\" name=\"txnid\" value=\"' + BOLT.response.txnid + '\" />' +
                    //     '<input type=\"hidden\" name=\"amount\" value=\"' + BOLT.response.amount + '\" />' +
                    //     '<input type=\"hidden\" name=\"productinfo\" value=\"' + BOLT.response.productinfo + '\" />' +
                    //     '<input type=\"hidden\" name=\"firstname\" value=\"' + BOLT.response.firstname + '\" />' +
                    //     '<input type=\"hidden\" name=\"email\" value=\"' + BOLT.response.email + '\" />' +
                    //     '<input type=\"hidden\" name=\"udf5\" value=\"' + BOLT.response.udf5 + '\" />' +
                    //     '<input type=\"hidden\" name=\"mihpayid\" value=\"' + BOLT.response.mihpayid + '\" />' +
                    //     '<input type=\"hidden\" name=\"status\" value=\"' + BOLT.response.status + '\" />' +
                    //     '<input type=\"hidden\" name=\"hash\" value=\"' + BOLT.response.hash + '\" />' +
                    //     '</form>';
                    // var form = jQuery(fr);
                    // jQuery('body').append(form);
                    // form.submit();
                } else {
                    window.location.href = furl;
                }
            },
            catchException: function (BOLT) {
                alert(BOLT.message);
            }
        };

        bolt.launch( reuest, handlers );

    }
</script>

<?php
@include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();
