<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 3/24/19
 * Time: 4:24 PM
 */

$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );

// Get all the users.

$userCount = UserDatabase::getInstance()->getUserCount();
$packageUsers = $userCount['packageUserCount'];
foreach ( $packageUsers as $key => $packageUser ) {
        $packageUsers[$key]['package'] = PackageManager::getInstance()->getAPackageById($packageUser['packageId']);
}

$userList = UserDatabase::getInstance()->getAllUsers();

$this->_addHeader();
@include $this->getPath() . DS . 'navs.top.php';
?>
    <div class="row">
        <div class="col-sm-12">
            <div class="element-wrapper">

                <h6 class="element-header">
                    All Users
                </h6>



                <div class="table-background-custom">
                    <!--------------------
                    START - Controls Above Table
                    -------------------->

                    <!--------------------
                    END - Controls Above Table
                    ------------------          --><!--------------------
                      START - Table with actions
                      ------------------  -->
                    <div class="table-responsive">
                        <table class="table table-bordered table-lg table-v2 table-striped">
                            <thead>
                            <tr>
                                <th>
                                    Name
                                </th>
                                <th>
                                    Mobile
                                </th>
                                <th>
                                    Email
                                </th>
                                <th>
                                    D.State
                                </th>
                                <th>
                                    Last Test Score
                                </th>
                                <th>
                                    12th
                                </th>

                                <th>
                                    Joined On
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ( $userList as $user ): ?>
                            <?php $extra = UserDatabase::getInstance()->getExtraInfo($user['id']); ?>
                            <?php $lastTest = UserLog::getInstance()->getUserLog($user['id'], UserLog::TYPE_TEST, 1); ?>
                            <?php $lastScore = !empty($lastTest) ? $lastTest[0]['text'] :  '{"score": 0, "testName": "None"}'; ?>
                            <?php $lastScore = json_decode($lastScore, true); ?>

                            <?php if ( empty($extra) ) continue; ?>
                            <?php $state = StateManager::getInstance()->getStateById($extra['stateId']); ?>
                                <tr>
                                    <td>
                                        <?php echo htmlentities($user['firstName']) . ' ' . htmlentities($user['lastName']); ?>
                                    </td>
                                    <td class="text-center">
                                        <?php echo htmlentities($user['phone']); ?>
                                    </td>
                                    <td class="text-center">
                                        <?php echo htmlentities($user['email']); ?>
                                    </td>
                                    <td class="text-center">
                                        <?php echo $state['name'] . ' (' . $state['shortName'] . ') '; ?>
                                    </td>
                                    <td>
                                        <?php echo $lastScore['testName'] . ' - ' .  $lastScore['score'];  ?>
                                    </td>
                                    <td class="row-actions">
                                        <?php echo $extra['study12'] == 1 ? "Appeared" : "Passout"; ?>
                                    </td>

                                    <td class="row-actions">
                                        <?php echo date('d/m/Y', $user['join']); ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>

                            </tbody>
                        </table>
                    </div>
                    <!--------------------
                    END - Table with actions
                    -------------------->
                    <!--------------------
                    START - Controls below table
                    ------------------  -->
<!--                    <div class="controls-below-table">-->
<!--                        <div class="table-records-info">-->
<!--                            Showing records 1 - 5-->
<!--                        </div>-->
<!--                        <div class="table-records-pages">-->
<!--                            <ul>-->
<!--                                <li>-->
<!--                                    <a href="#">Previous</a>-->
<!--                                </li>-->
<!--                                <li>-->
<!--                                    <a class="current" href="#">1</a>-->
<!--                                </li>-->
<!--                                <li>-->
<!--                                    <a href="#">2</a>-->
<!--                                </li>-->
<!--                                <li>-->
<!--                                    <a href="#">3</a>-->
<!--                                </li>-->
<!--                                <li>-->
<!--                                    <a href="#">4</a>-->
<!--                                </li>-->
<!--                                <li>-->
<!--                                    <a href="#">Next</a>-->
<!--                                </li>-->
<!--                            </ul>-->
<!--                        </div>-->
<!--                    </div>-->
                    <!--------------------
                    END - Controls below table
                    -------------------->
                </div>

            </div>
        </div>
    </div>
<?php
@include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();


