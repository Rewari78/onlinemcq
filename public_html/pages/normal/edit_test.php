<?php

/**
 * Created by PhpStorm.
 * User: akash
 * Date: 3/11/18
 * Time: 12:03 PM
 */
$this->onlyLoggedInAllowed(UserManager::USER_TYPE_TEACHER);
$testId = filter_input(INPUT_GET, 'test-id', FILTER_SANITIZE_NUMBER_INT);
$testInfo = TestManager::getInstance()->getTestInfo($testId);

$testDivisions = json_decode($testInfo['divisions'], true);

$subjects = array(
    'History',
    'Math',
    'Science',
    'Geography'
);

// set post fields
$post = json_encode(array(
    "adminLevel" => "3",
    "groupId" => "custom",
    "groupName" => "Custom Classes",
    "instDbValue" => $_SESSION['schoolId'],
    "pId" => $_SESSION['pid']
));

$ch = curl_init('http://pinnacleapp.in/MobileApp/rest/adminAlert/getAlertDetails');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
$json_string = curl_exec($ch);
curl_close($ch);



$this->_addHeader();
?>

<div class="container d-flex h-100 flex-column">
    <div class="row h-100 justify-content-center align-items-center">
        <div class="col-md-4 m-auto menu_box">
            <form id="add_test_form" method="POST">
                <h3 class="text-center text-uppercase pt-2 pb-3">Edit Test</h3>
                <div class="alert alert-danger alert-dismissible" role="alert" area-label="Close" class="close">
                </div>
                <div class="form-group">
                    <label for="test-subject">Subject</label>
                    <input class="form-control" id="test-subject" name="test-subject" placeholder="Give Subject" value="<?php echo $testInfo['subject']; ?>" required>
                </div>
                <div class="form-group">
                    <label for="test-name">Select Class</label>
                    <select class="form-control" id="test-class" name="test-class" required>
                        <option selected disabled value="">Select Class</option>
                    </select>
                </div>
                <div class="form-group text-center" id="divisions_list">
                    <label for="test-div">Select Division</label><br>
                    <?php foreach ($testDivisions as $division) { ?>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" checked id="inlineCheckbox<?php echo $division['classDivId']; ?>" name="test-divs[]" value='<?php echo json_encode($division); ?>' onclick="deRequireCb('form-check-input')" required>
                            <label class="form-check-label" for="inlineCheckbox<?php echo $division['classDivId']; ?>"><?php echo $division['divisionName'] ?></label>
                        </div>
                    <?php } ?>
                </div>
                <div class="form-group">
                    <label for="test-desc">Test Description</label>
                    <input class="form-control" id="test-desc" name="test-desc" value="<?php echo $testInfo['description']; ?>" placeholder="Test Description" required>
                </div>
                <div class="form-group">
                    <label for="total-time">Start From</label>
                    <input type="datetime-local" class="form-control" id="total-start" name="test-start" min="<?php echo date('Y-m-d') . 'T' . date('h:i') ?>" autocomplete="off" required value="<?php echo $testInfo['startAt'] ?>" />
                </div>
                <div class="form-group">
                    <label for="total-time">Total Time (In minutes Eg. 90)</label>
                    <input type="number" class="form-control" id="total-time" name="test-time" value="<?php echo $testInfo['totalTime']; ?>" autocomplete="off" required />
                </div>
                <div class="form-group">
                    <input type="hidden" value="<?php echo $testInfo['id']; ?>" name="testId">
                    <button id="form_submit_button" type="submit" class="btn btn-primary btn-block btn-lg">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>


<script type="text/javascript">
    var apiResponse = <?php echo $json_string; ?>;
    var test = <?php echo json_encode($testInfo); ?>;
    var testDivision = <?php echo json_encode($testDivisions); ?>;
    $(".alert-danger").hide();
    $(document).ready(function() {
        $.each(apiResponse.classList, function(key, value) {

            if (test.class == value) {
                $("#test-class").append(
                    $('<option></option>')
                    .attr('value', value)
                    .attr('selected', 'selected')
                    .text(value)
                );
                updateDivisions(value);
            } else {
                $("#test-class").append(
                    $('<option></option>')
                    .attr('value', value)
                    .text(value)
                );
            }
        });

        function updateDivisions(value)

        {


            var divisions = [];
            for (var i = 1; i <= Object.keys(apiResponse.classDivList).length; i++) {
                if (apiResponse.classDivList[i].className.trim() === value.trim()) divisions.push(apiResponse.classDivList[i]);
            }

            if (divisions.length) {
                $.each(divisions, function(index) {
                    let divName = divisions[index].divisionName;
                    let add = true;
                    if (testDivision && testDivision.length > 0) {
                        testDivision.forEach(element => {
                            if (element.divisionName == divName)
                                add = false;
                        });
                    }

                    if (add) {
                        var divisionCheckbox = '<div class="form-check form-check-inline">' +
                            '<input class="form-check-input" type="checkbox" id="inlineCheckbox' + index + '" name="test-divs[]" value=' + JSON.stringify(divisions[index]) + ' onclick="deRequireCb(\'form-check-input\')" required>' +
                            '<label class="form-check-label" for="inlineCheckbox' + index + '">' + divisions[index].divisionName + '</label>' +
                            '</div>';
                        $("#divisions_list").append(divisionCheckbox);
                        $("#divisions_list").show();
                    }
                });

            }
        }
        $("#test-class").on('change', function() {
            $(".form-check").remove();

            var divisions = [];
            for (var i = 1; i <= Object.keys(apiResponse.classDivList).length; i++) {
                if (apiResponse.classDivList[i].className.trim() === this.value.trim()) divisions.push(apiResponse.classDivList[i]);
            }

            if (divisions.length) {
                $.each(divisions, function(index) {
                    var divisionCheckbox = '<div class="form-check form-check-inline">' +
                        '<input class="form-check-input" type="checkbox" id="inlineCheckbox' + index + '" name="test-divs[]" value=' + JSON.stringify(divisions[index]) + ' onclick="deRequireCb(\'form-check-input\')" required>' +
                        '<label class="form-check-label" for="inlineCheckbox' + index + '">' + divisions[index].divisionName + '</label>' +
                        '</div>';
                    $("#divisions_list").append(divisionCheckbox);
                    $("#divisions_list").show();
                });
            }


        });

        $("#add_test_form").submit(function(e) {
            e.preventDefault();

            $.ajax({
                cache: false,
                url: homeUrl + '/ajax/update-test-process',
                dataType: 'JSON',
                type: "POST",
                data: $("#add_test_form").serialize(),
                success: function(data) {
                    if (data.success) {
                        window.location = homeUrl + '/manage-test?updated=1';
                    } else {
                        var errors = '';
                        $.each(data.error, function(index, value) {
                            errors += value + '<br />';
                        });
                        $(".alert-danger").empty();
                        $(".alert-danger").append(errors);
                        $(".alert-danger").append('<button aria-label="close" class="close button-close" onclick="closeError()" data-dismiss="alert" type="button"><span aria-hidden="true">&times;</span></button>');
                        $(".alert-danger").show();
                    }

                }
            })


        });




    });

    function closeError(e) {
        $(".alert-danger").hide();
        $(".alert-danger").empty();
    }

    function deRequireCb(elClass) {
        el = document.getElementsByClassName(elClass);

        var atLeastOneChecked = false; //at least one cb is checked
        for (i = 0; i < el.length; i++) {
            if (el[i].checked === true) {
                atLeastOneChecked = true;
            }
        }

        if (atLeastOneChecked === true) {
            for (i = 0; i < el.length; i++) {
                el[i].required = false;
            }
        } else {
            for (i = 0; i < el.length; i++) {
                el[i].required = true;
            }
        }
    }




    // $(function() {
    //     $('#custom-date-time-pick, #custom-date-time-pick1').datetimepicker({});
    // });
</script>
<?php
// @include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();
