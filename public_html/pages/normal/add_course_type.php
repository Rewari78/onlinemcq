<?php
$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );

$collegeTypeId  = filter_input(INPUT_GET, 'cti', FILTER_SANITIZE_NUMBER_INT);
$studyTypeId    = filter_input(INPUT_GET, 'sti', FILTER_SANITIZE_NUMBER_INT);

$collegeTypeId = !empty($collegeTypeId ) ? $collegeTypeId : 0;
$studyTypeId = !empty($studyTypeId) ? $studyTypeId : 0;

$collegeTypes   = CollegeManager::getInstance()->getAvlCollegeTypes(  );
$studyTypes     = CollegeManager::getInstance()->getAvlStudyType();

if ( $this->isPost() )
{

    $courseTP = filter_input(INPUT_POST, 'course-type');
    if ( empty($courseTP) ) {
        $msg = "Course type can not be empty.";
    } else {

        // We need to submit the study types.
        CollegeManager::getInstance()->addCourseType($studyTypeId, $courseTP);
        $msg = "New Course type is added.";
    }

}

$courseTypes = CollegeManager::getInstance()->getAvlCourseType($studyTypeId);

$showCoursesType = false;

$this->_addHeader();
@include $this->getPath() . DS . 'navs.top.php';
?>
<div class="row">

    <div class="col-sm-4 col-xxxl-4">
        <div class="element-wrapper">

            <?php if ( !empty($msg) ) : ?>
                <div class="alert alert-success alert-dismissible " role="alert" area-label="Close">
                    <?php echo $msg; ?>
                    <button aria-label="close" class="close" data-dismiss="alert" type="button">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            <?php endif; ?>

            <h6 class="element-header">
                Select
            </h6>
            <div class="element-box">
                <form method="post" action="#">

                        <div class="form-group">
                            <label for="select-study-type">Study Type</label>
                            <select class="form-control" required="required" id="select-study-type" name="select-study-type">
                                <option value="0">Please Select</option>

                                <?php foreach ( $studyTypes as $studyType ):  ?>
                                    <?php $showCoursesType = $showCoursesType !== true ? $studyType['id'] == $studyTypeId : $showCoursesType; ?>
                                        <option
                                            value="<?php echo $studyType['id']; ?>"
                                            <?php echo $studyType['id'] == $studyTypeId ? 'selected' : ''; ?> >
                                            <?php echo $studyType['studyType']; ?>
                                        </option>
                                <?php endforeach; ?>

                            </select>
                        </div>

                </form>
            </div>
        </div>
    </div>

    <div class="col-sm-4 col-xxxl-4">
        <?php if ( $showCoursesType ): ?>
            <div class="element-wrapper">
            <div class="element-actions">
                <button class="btn btn-primary custom-button" data-target="#add-course-type-modal" data-toggle="modal" type="button">Add Course Type</button>
            </div>
            <h6 class="element-header">
                Course Types
            </h6>
            <?php if ( !empty($courseTypes) ) : ?>
                <div class="element-box">
                    <div class="table-responsive">
                        <table class="table table-lightborder">
                            <thead>
                            <tr>
                                <th>
                                    SR.No
                                </th>
                                <th class="text-left">
                                    Course Type
                                </th>
                                <th>
                                    Actions
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ( $courseTypes as $courseType ): ?>
                                <tr>
                                    <td class="nowrap">
                                        <?php echo $courseType['id']; ?>
                                    </td>
                                    <td class="text-left">
                                        <?php echo Util::htmlEncode($courseType['courseType']); ?>
                                    </td>
                                    <td class="row-actions">

                                        <a href="#" data-target="#edit-course-modal-<?php echo $courseType['id']; ?>" data-toggle="modal">
                                            <i class="os-icon os-icon-ui-49"></i>
                                        </a>

                                        <div aria-hidden="true" aria-labelledby="mySmallModalLabel" class="modal fade bd-example-modal-sm" id="edit-course-modal-<?php echo $courseType['id']; ?>" role="dialog" tabindex="-1">
                                            <div class="modal-dialog modal-sm">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">
                                                            Edit Type
                                                        </h5>
                                                        <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                                                    </div>
                                                    <form method="post" action="#" onsubmit="submitForm(this); return false;" >
                                                        <div class="modal-body">

                                                            <div class="form-group" style="text-align: left;">
                                                                <label for="course-type-<?php echo $courseType['id']; ?>">Course Type</label>
                                                                <input class="form-control" placeholder="Type" type="text" id="course-type-<?php echo $courseType['id']; ?>" name="course-type" value="<?php echo Util::htmlEncode($courseType['courseType']); ?>">
                                                            </div>

                                                            <input type="hidden" name="course-type-id" value="<?php echo $courseType['id']; ?>" />
                                                            <input type="hidden" name="study-type-id" value="<?php echo $studyTypeId; ?>" />

                                                            <div class="alert alert-warning alert-dismissible d-none" role="alert">
                                                            </div>

                                                        </div>
                                                        <div class="modal-footer">
                                                            <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
                                                            <button class="btn btn-primary" type="submit">Save changes</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                    </td>
                                </tr>
                            <?php endforeach; ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <?php endif; ?>
    </div>

    <div class="col-sm-4 col-xxxl-4">
    </div>
</div>

<div aria-hidden="true" aria-labelledby="mySmallModalLabel" class="modal fade bd-example-modal-sm" id="add-course-type-modal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        Add New Type
                    </h5>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                </div>
                <form method="post" action="#">
                    <div class="modal-body">

                        <div class="form-group">
                            <label for="study-type">Course Type</label>
                            <input class="form-control" placeholder="Type" type="text" id="course-type" name="course-type">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
                        <button class="btn btn-primary" type="submit">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
</div>

<script type="text/javascript">
    function _buildQuery( obj ) {

        var prop, params = [];
        for( prop in obj )
        {
            if ( obj.hasOwnProperty(prop) )
                params.push( prop + '=' + obj[prop] );
        }

        return params.join('&');
    }

    (function ( _scope ) {

        // $(document).on('change', '#select-college-type', function ( event ) {
        //
        //     var value = $(this).val().trim();
        //     _scope.location.href = '/add-course-type?' + _buildQuery({ cti: value });
        //
        // });

        $(document).on('change', '#select-study-type', function ( event ) {


            var sti = $('#select-study-type').val().trim();

            _scope.location.href = '/add-course-type?' + _buildQuery({
                sti: sti
            });

        });
    })(window);

    function submitForm( form )
    {

        var $form = $(form);
        var $alert = $form.find('.alert');
        var _data = $(form).serialize();

        hideError($alert);

        $.post(
            homeUrl + '/ajax/edit-course-type',
            $form.serialize(),
            function (data) {
                if ( data.info !== 'success' )
                {
                    showError($alert, data.message);
                    return;
                }

                window.location.reload();

            },
            'json'
        );
    }

    function showError( box, msg )
    {

        var $alertBox = $(box);
        $alertBox.html(msg);
        $alertBox.removeClass('d-none');
    }

    function hideError( box )
    {
        var $alertBox = $(box);
        $alertBox.addClass('d-none');
    }
    

</script>
<?php
@include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();
