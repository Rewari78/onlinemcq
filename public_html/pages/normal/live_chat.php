<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 18/12/18
 * Time: 8:59 PM
 */

$this->onlyLoggedInAllowed(UserManager::USER_TYPE_SUBSCRIBER, PackageManager::FEATURE_LIVE_CHAT);


$this->_addHeader();
@include $this->getPath() . DS . 'navs.top.php';
?>
    <h1>Congratulation your chat is enabled.</h1>
<?php
@include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();