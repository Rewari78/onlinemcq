<?php
$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );

$states = StateManager::getInstance()->getAllStates();

if ( $this->isPost() )
{

    $statesId = filter_input(INPUT_POST, 'select-states');
    $collegeTypeId = filter_input(INPUT_POST, 'select-college-type');
    $studyTypeId = filter_input(INPUT_POST, 'select-study-type');
    $courseTypeId = filter_input(INPUT_POST, 'select-course-type');
    $quotaTypeId = filter_input(INPUT_POST, 'quota-type');
    $quotaPercent = filter_input(INPUT_POST, 'quota-percentage');

    // We a have to validate one by one.
    if ( empty($statesId) || empty($collegeTypeId) ||
        empty($studyTypeId) || empty($courseTypeId) ||
        empty($quotaTypeId) || empty($quotaPercent)
    ) {
        $msg = "Please select all fields.";
    } else if ( !is_numeric($quotaPercent) ||
        $quotaPercent < 0 ||
        $quotaPercent > 100 )
    {
        $msg = "Quota percentage is not valid.";

    } else if ( empty(StateManager::getInstance()->getStateById($statesId)) )
    {
        $msg = "State is not valid.";
    } else if ( empty(CollegeManager::getInstance()->getCollegeTypeById($collegeTypeId)) )
    {
        $msg = "College type is not valid.";
    } else if ( empty(CollegeManager::getInstance()->getStudyTypeById($studyTypeId)) )
    {
        $msg = "Study type is not valid.";
    } else if ( empty(CollegeManager::getInstance()->getCourseTypeById($courseTypeId)) )
    {
        $msg = "Course type is not valid.";
    } else if ( empty(Quotas::getInstance()->getQuotaById($quotaTypeId)) )
    {
        $msg = "Quota is not valid.";

    } else {
        // Now we can submit.
        Quotas::getInstance()->addQuotaPercentage($statesId, $courseTypeId, $studyTypeId,
            $courseTypeId, $quotaTypeId, $quotaPercent);
    }

    //TODO: You can also use this page to update.

}

$this->_addHeader();
@include $this->getPath() . DS . 'navs.top.php';
?>
<div class="row">
        <div class="col-sm-8 col-xxxl-8">
            <div class="element-wrapper">

                <?php if ( !empty($msg) ) : ?>
                    <div class="alert alert-success alert-dismissible " role="alert" area-label="Close">
                        <?php echo $msg; ?>
                        <button aria-label="close" class="close" data-dismiss="alert" type="button">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                <?php endif; ?>

                <h6 class="element-header">
                    Add Quotas Percentage
                </h6>
                <div class="element-box">
                    <form method="post" action="#">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="select-states">Select State</label>
                                    <select class="form-control" id="select-states" name="select-states">
                                        <option value="0">
                                            Please select
                                        </option>

                                        <?php foreach ( $states as $state ): ?>
                                            <option value="<?php echo $state['id']; ?>" >
                                                <?php echo $state['name']; ?>
                                            </option>
                                        <?php endforeach; ?>

                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="select-col-type">Select College Type</label>
                                    <select class="form-control" id="select-col-type" name="select-college-type" disabled>
                                        <option value="0">
                                            Please select
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="select-study-type">Study Type</label>
                                    <select class="form-control" required="required" id="select-study-type" name="select-study-type" disabled>
                                        <option>Please Select</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="select-course-type">Course Type</label>
                                    <select class="form-control" required="required" id="select-course-type" name="select-course-type" disabled>
                                        <option>Please Select</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="select-quota-type">Quota Type</label>
                            <select class="form-control" required="required" id="select-quota-type" name="quota-type" disabled>
                                <option>Please Select</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="select-quota-percentage">Percentage</label>
                            <input
                                min="0"
                                class="form-control"
                                placeholder="Type Percentage"
                                type="number"
                                id="select-quota-percentage"
                                name="quota-percentage"
                                disabled />
                        </div>

                        <button class="btn btn-primary" type="submit">Save changes</button>
                    </form>
                </div>
            </div>
        </div>



        <div class="col-sm-4 col-xxxl-4">

        </div>
    </div>

<script type="text/javascript">

    var $states = $('#select-states');

    $(document).on('change', '#select-states', function (e) {
        var value = $(this).val().trim();

        if ( parseInt(value, 10) === 0 ) {
            $('#select-col-type')[0].disabled = true;
            $('#select-study-type')[0].disabled = true;
            $('#select-course-type')[0].disabled = true;
            $('#select-quota-type')[0].disabled = true;
            $('#select-quota-percentage')[0].disabled = true;
            return;
        }

        $.post(
            homeUrl + '/ajax/quota-percentage',
            { rq: 'getCollegeTypes', stateId: value },
            function(data) {

                var html = '<option value="0">Please Select</option>';
                if ( data.info !== 'success' )
                {
                    $('#select-col-type').html(html)[0].disabled = true;
                    return;
                }

                var i = 0;
                for( ; i < data.message.length; i++ )
                {
                    html += '<option value="' + data.message[i].id + '">' + data.message[i].collegeType + '</option>';
                }

                $('#select-col-type').html(html)[0].disabled = false;

            },
            'json'
        );
    });

    $(document).on('change', '#select-col-type', function (e) {

        var value = $(this).val().trim();

        if ( parseInt(value, 10) === 0 ) {
            $('#select-study-type')[0].disabled = true;
            $('#select-course-type')[0].disabled = true;
            $('#select-quota-type')[0].disabled = true;
            $('#select-quota-percentage')[0].disabled = true;
            return;
        }

        $.post(
            homeUrl + '/ajax/quota-percentage',
            { rq: 'getStudyType', colTypeId: value },
            function(data) {

                var html = '<option value="0">Please Select</option>';
                if ( data.info !== 'success' )
                {
                    $('#select-study-type').html(html)[0].disabled = true;
                    return;
                }

                var i = 0;
                for( ; i < data.message.length; i++ )
                {
                    html += '<option value="' + data.message[i].id + '">' + data.message[i].studyType + '</option>';
                }

                $('#select-study-type').html(html)[0].disabled = false;

            },
            'json'
        );

    });

    $(document).on('change', '#select-study-type', function (e) {

        var value = $(this).val().trim();

        if ( parseInt(value, 10) === 0 ) {
            $('#select-course-type')[0].disabled = true;
            $('#select-quota-type')[0].disabled = true;
            $('#select-quota-percentage')[0].disabled = true;
            return;
        }

        $.post(
            homeUrl + '/ajax/quota-percentage',
            { rq: 'getCourseType', studyTypeId: value },
            function(data) {

                var html = '<option value="0">Please Select</option>';
                if ( data.info !== 'success' )
                {
                    $('#select-course-type').html(html)[0].disabled = true;
                    return;
                }

                var i = 0;
                for( ; i < data.message.length; i++ )
                {
                    html += '<option value="' + data.message[i].id + '">' + data.message[i].courseType + '</option>';
                }

                $('#select-course-type').html(html)[0].disabled = false;

            },
            'json'
        );

    });

    $(document).on('change', '#select-course-type', function (e) {

        var value = $(this).val().trim();

        if ( parseInt(value, 10) === 0 ) {
            $('#select-quota-type')[0].disabled = true;
            $('#select-quota-percentage')[0].disabled = true;
            return;
        }

        $.post(
            homeUrl + '/ajax/quota-percentage',
            { rq: 'getQuotaType', courseTypeId: value },
            function(data) {

                var html = '<option value="0">Please Select</option>';
                if ( data.info !== 'success' )
                {
                    $('#select-quota-type').html(html)[0].disabled = true;
                    return;
                }

                var i = 0;
                for( ; i < data.message.length; i++ )
                {
                    html += '<option value="' + data.message[i].id + '">' + data.message[i].quota + '</option>';
                }

                $('#select-quota-type').html(html)[0].disabled = false;

            },
            'json'
        );

    });

    $(document).on('change', '#select-quota-type', function (e) {

        var value = $(this).val().trim();

        if ( parseInt(value, 10) === 0 ) {
            $('#select-quota-percentage')[0].disabled = true;
            return;
        }

        $('#select-quota-percentage')[0].disabled = false;

    });
</script>
<?php
@include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();