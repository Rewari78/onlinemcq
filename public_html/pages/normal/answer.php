<?php

$this->onlyLoggedInAllowed( UserManager::USER_TYPE_SUBSCRIBER, PackageManager::FEATURE_QNA_R_ONLY );

$user = $this->getUser();

$questionId = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
if ( empty($questionId) ) throw new Error404;
$msg = "";

$getQuesById =QNA::getInstance()->getQuesById($questionId);
if ( empty($getQuesById) ) throw new Error404;

if ( $this->isPost() )
{
    // $id = filter_input(INPUT_POST, 'id');
    $q_ans = filter_input(INPUT_POST, 'q_ans');
    $q_ans = !empty($q_ans) ? $q_ans : '';
    // We need to
    QNA::getInstance()->ansUpdate($questionId, $q_ans);
    $getQuesById =QNA::getInstance()->getQuesById($questionId);
    $msg = "New answer is added.";
}

$this->_title = $getQuesById[0]['q_title'];

$this->_addHeader();
@include $this->getPath() . DS . 'navs.top.php';
?>
<div class="container dash-container">

                <h6 class="custm-ele-hdr">
                    <?php echo Util::htmlEncode($getQuesById[0]['q_title']); ?>
                </h6>
                <div class="custm-ele-hdr-strk"></div>

                <div class="row row-question">
                  <div class="col-md-1">
                    <div class="question-number">
                      <p>Question No.</p>
                      <h4><?php echo $questionId; ?></h4>
                    </div>
                  </div>

                  <div class="col-md-11">
                    <div class="question-name">
                        <?php foreach ( $getQuesById as $getQuesById ): ?>
                            <p class="question-definition">
                             <?php echo Util::htmlEncode($getQuesById['q_des']); ?>
                            </p>
                        <?php endforeach; ?>
                    </div>
                  </div>
                </div>

    <hr />


                <?php if ( $user->isAdmin() ) : ?>

                    <?php if (  !empty($getQuesById['q_ans']) ): ?>
                        <h6 class="custm-ele-hdr">
                            Update Previous Answer
                        </h6>

                        <div class="custm-ele-hdr-strk"></div>

                    <div class="col-12 textarea-add">
                        <form action="/answer?id=<?php echo $questionId;?>" method="post">
                            <div class="form-group">
                            <label for="textAreaAdd"></label>
                            <textarea class="form-control ckeditor" rows="4" id="textAreaAdd" name="q_ans"><?php echo Util::htmlEncode($getQuesById['q_ans']); ?></textarea>
                            </div>
                            <button type="submit" class="form-control mt-3 btn btn-primary" style="width: 100px;">Edit Answer</button>
                        </form>
                    </div>
                    <?php else: ?>

                        <h6 class="custm-ele-hdr">
                            Give an answer to this question
                        </h6>

                        <div class="custm-ele-hdr-strk"></div>

                        <div class="col-12 textarea-add">
                            <form action="/answer?id=<?php echo $questionId;?>" method="post">
                                <div class="form-group">
                                    <label for="textAreaAdd"></label>
                                    <textarea class="form-control ckeditor" rows="4" id="textAreaAdd" name="q_ans"></textarea>
                                </div>

                                <button type="submit" class=" form-control mt-3 btn btn-primary" style="width: 100px;">Add Answer</button>
                            </form>
                        </div>
                    <?php endif; ?>
                  <?php else: ?>


                    <?php if ( !empty($getQuesById['q_ans']) ): ?>

                        <h6 class="custm-ele-hdr">
                            Answer
                        </h6>

                        <div class="custm-ele-hdr-strk"></div>

                    <div class="row row-question">

                        <div class="col-md-12">
                            <div class="question-name">
                                <p class="question-definition">
                                    <?php echo $getQuesById['q_ans']; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <?php else: ?>
                        <h6 class="custm-ele-hdr text-center mt-5">
                            No answer yet given please wait.
                        </h6>
                    <?php endif; ?>
                  <?php endif; ?>

</div>
<?php
@include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();