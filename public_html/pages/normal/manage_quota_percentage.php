<?php
$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );

$quotaPercentageList = Quotas::getInstance()->getAllQuotaPercentage();

$filteredPercentageList = array();
foreach ( $quotaPercentageList as $quotaPercentage )
{
    $filteredPercentageList['stateIds']         [$quotaPercentage['stateId']]       = $quotaPercentage['stateId'];
    $filteredPercentageList['collegeTypeIds']   [$quotaPercentage['collegeTypeId']] = $quotaPercentage['collegeTypeId'];
    $filteredPercentageList['studyTypeIds']     [$quotaPercentage['studyTypeId']]   = $quotaPercentage['studyTypeId'];
    $filteredPercentageList['courseTypeIds']    [$quotaPercentage['courseTypeId']]  = $quotaPercentage['courseTypeId'];
    $filteredPercentageList['quotaTypeIds']     [$quotaPercentage['quotaTypeId']]   = $quotaPercentage['quotaTypeId'];
    $filteredPercentageList['percents']         [$quotaPercentage['percent']]       = $quotaPercentage['percent'];
}

// Now get all the states based on ids.
$statesInfo = StateManager::getInstance()->getStateByIdList($filteredPercentageList['stateIds']);
$collegeTypes = CollegeManager::getInstance()->getCollegeTypeIdList($filteredPercentageList['collegeTypeIds']);
$studyTypes = CollegeManager::getInstance()->getStudyTypeByIdList($filteredPercentageList['studyTypeIds']);
$courseTypes = CollegeManager::getInstance()->getCourseTypeByIdList($filteredPercentageList['courseTypeIds']);
$quotaTypes = Quotas::getInstance()->getQuotaByIdList($filteredPercentageList['quotaTypeIds']);

$this->_addHeader();
@include $this->getPath() . DS . 'navs.top.php';
?>
    <div class="row">
        <div class="col-sm-12 col-xxxl-12">
            <div class="element-wrapper">
                <div class="element-actions">
                    <a class="btn btn-primary custom-button" href="/add-quota-percentage">Add Quota</a>
                </div>
                <h6 class="element-header">
                    Quotas
                </h6>
                <div class="element-box">
                    <div class="table-responsive">
                        <table class="table table-lightborder">
                            <thead>
                            <tr>
                                <th>
                                    Quota type
                                </th>
                                <th class="text-left">
                                    State
                                </th>
                                <th class="text-left">
                                    College Type
                                </th>
                                <th class="text-left">
                                    Study Type
                                </th>
                                <th class="text-left">
                                    Course Type
                                </th>
                                <th class="text-left">
                                    Percentage
                                </th>
                                <th class="text-left">
                                    Action
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ( $quotaPercentageList as $quotaPercentage ): ?>
                                <tr>
                                    <td class="nowrap">
                                        <?php echo isset($quotaTypes[$quotaPercentage['quotaTypeId']]) ? $quotaTypes[$quotaPercentage['quotaTypeId']]['quota'] : ''; ?>
                                    </td>
                                    <td class="text-left">
                                        <?php echo isset($statesInfo[$quotaPercentage['stateId']]) ? $statesInfo[$quotaPercentage['stateId']]['name'] : ''; ?>
                                    </td>
                                    <td class="text-left">
                                        <?php echo isset($collegeTypes[$quotaPercentage['collegeTypeId']]) ? $collegeTypes[$quotaPercentage['collegeTypeId']]['collegeType'] : ''; ?>
                                    </td>
                                    <td class="text-left">
                                        <?php echo isset($studyTypes[$quotaPercentage['studyTypeId']]) ? $studyTypes[$quotaPercentage['studyTypeId']]['studyType'] : ''; ?>
                                    </td>
                                    <td class="text-left">
                                        <?php echo isset($courseTypes[$quotaPercentage['courseTypeId']]) ? $courseTypes[$quotaPercentage['courseTypeId']]['courseType'] : ''; ?>
                                    </td>
                                    <td class="text-left">
                                        <?php echo $quotaPercentage['percent'] . '%'; ?>
                                    </td>
                                    <td class="row-actions text-left">
                                        <a href="#" data-target="#edit-quota-percent-<?php echo $quotaPercentage['id']; ?>" data-toggle="modal">
                                            <i class="os-icon os-icon-ui-49"></i>
                                        </a>

                                        <div aria-hidden="true" aria-labelledby="mySmallModalLabel" class="modal fade bd-example-modal-sm" id="edit-quota-percent-<?php echo $quotaPercentage['id']; ?>" role="dialog" tabindex="-1">
                                            <div class="modal-dialog modal-sm">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">
                                                            Edit Percentage
                                                        </h5>
                                                        <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                                                    </div>
                                                    <form action="#" method="post" onsubmit="submitForm(this); return false;">
                                                        <div class="modal-body">
                                                            <div class="form-group">

                                                                <label for="quota-percent-field-<?php echo $quotaPercentage['id']; ?>">Percentage</label>
                                                                <input
                                                                    class="form-control"
                                                                    placeholder="Type"
                                                                    type="number"
                                                                    id="quota-percent-field-<?php echo $quotaPercentage['id']; ?>"
                                                                    name="quota-percentage"
                                                                    value="<?php echo $quotaPercentage['percent']; ?>"
                                                                    min="0" max="100" />


                                                            </div>


                                                            <div class="alert alert-warning alert-dismissible d-none" role="alert" id="add-p-alert-box">
                                                            </div>

                                                            <input type="hidden" name="id" value="<?php echo $quotaPercentage['id']; ?>" />

                                                        </div>
                                                        <div class="modal-footer">
                                                            <button class="btn btn-secondary" data-dismiss="modal" type="button"> Close</button>
                                                            <button class="btn btn-primary" type="submit">Submit</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                    </td>
                                </tr>
                            <?php endforeach; ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function submitForm( form )
        {
            var $form = $(form);
            var $alert = $form.find('.alert');
            var _data = $(form).serialize();

            hideError($alert);

            $.post(
                homeUrl + '/ajax/edit-quota-percent',
                $form.serialize(),
                function (data) {

                    if ( data.info !== 'success' )
                    {
                        showError($alert, data.message);
                        return;
                    }

                    window.location.reload();

                },
                'json'
            );
        }

        function showError( box, msg )
        {

            var $alertBox = $(box);
            $alertBox.html(msg);
            $alertBox.removeClass('d-none');
        }

        function hideError( box )
        {
            var $alertBox = $(box);
            $alertBox.addClass('d-none');
        }
    </script>
<?php
@include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();

