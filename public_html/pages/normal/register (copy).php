<?php
if ( UserManager::getInstance()->isLoggedIn() ) $this->redirect('home');
$this->_addHeader();

?>
<body class="auth-wrapper">
<div class="all-wrapper menu-side with-pattern">
    <div class="auth-box-w wider">
        <div class="logo-w">
            <a href="index.html"><img alt="" src="img/logo-big.png"></a>
        </div>
        <h4 class="auth-header">
            Create an account
        </h4>
        <form action="">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="firstName"> First Name</label><input class="form-control" name="firstName" id="firstName" placeholder="First Name" type="text">
                        <div class="pre-icon picons-thin-icon-thin-0711_young_boy_user_profile_avatar_man_male"></div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="lastName">Last Name</label><input class="form-control" name="lastName" id="lastName" placeholder="Last Name" type="text">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="email"> Email address</label><input class="form-control" name="email" id="email" placeholder="Enter email" type="email">
                <div class="pre-icon picons-thin-icon-thin-0321_email_mail_post_at"></div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="password1"> Password</label><input class="form-control" name="password1"  id="password1" placeholder="Password" type="password">
                        <div class="pre-icon os-icon os-icon-fingerprint"></div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="password2">Confirm Password</label><input class="form-control" name="password2" id="password2" placeholder="Password" type="password">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="mobile"> Mobile</label><input class="form-control" name="mobile" id="mobile" placeholder="Enter 10 Digit Mobile Number" type="text">
                <div class="pre-icon picons-thin-icon-thin-0344_iphone_mobile"></div>
            </div>
            <div class="pt-3 mt-4 border-top d-none" id="register-otp-top"></div>
            <div class="form-group d-none" id="register-otp-field">
                <label for=""> Verify OTP</label><input class="form-control" name="otp" id="otp" placeholder="Enter your OTP" type="text">
                <div class="pre-icon picons-thin-icon-thin-0156_checkbox_ok_successful"></div>
            </div>

            <div class="alert alert-warning alert-dismissible d-none" role="alert" id="alert-box">
            </div>

            <div class="buttons-w">
                <button class="btn btn-primary" type="button" id="login-btn">Generate OTP</button>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    var genOtpBtn = document.getElementById('login-btn');
    var data = {
        seq: null,
        isValidated: false
    };
    genOtpBtn.addEventListener('click', function(e){

        // First validate the info.
        var firstName = document.getElementById('firstName');
        var lastName = document.getElementById('lastName');
        var email = document.getElementById('email');
        var password1 = document.getElementById('password1');
        var password2 = document.getElementById('password2');
        var mobile = document.getElementById('mobile');
        var otp = document.getElementById('otp');

        var btn = this;

        if ( data.isValidated === false ) {

            var fnV = firstName.value.trim(),
                lnV = lastName.value.trim(),
                emailV = email.value.trim(),
                password1V = password1.value.trim(),
                password2V = password2.value.trim(),
                mobileV = mobile.value.trim();

            if ( fnV === '' || lnV === '' || emailV === '' ||
                password1V === '' || password2V === ''
                || mobileV === '' )
            {
                showError("Please provide all the fields.");
                return false;
            }

            hideError();

            // Now send the ajax.
            $.ajax({
                url: homeUrl + '/ajax/validate-register-fields',
                dataType: 'JSON',
                type: "POST",
                data: {
                    validate: 1,
                    firstName: fnV,
                    lastName: lnV,
                    email: emailV,
                    password1: password1V,
                    password2: password2V,
                    mobile: mobileV
                },
                beforeSend: function () {
                    btn.disabled = true;
                },
                success: function ( ajaxD ) {
                    if ( ajaxD.info !== 'success' ) {
                        showError(ajaxD.message);
                        return false;
                    }

                    firstName.disabled = true;
                    lastName.disabled = true;
                    email.disabled = true;
                    password1.disabled = true;
                    password2.disabled = true;
                    mobile.disabled = true;

                    btn.innerHTML = "Verify";

                    var regOtpTop = document.getElementById('register-otp-top');
                    var regOtpFields = document.getElementById('register-otp-field');

                    regOtpTop.className = regOtpTop.className.replace(/\bd-none\b/g, "");
                    regOtpFields.className = regOtpFields.className.replace(/\bd-none\b/g, "");

                    console.log(data);
                    data.isValidated = true;
                    data.seq = ajaxD.message.seq;

                },
                error: function () {

                },
                complete: function () {
                    btn.disabled = false;
                }
            });

        } else {

            var otpV = otp.value.trim();

            if ( otpV === '' ) {
                showError("Please enter OTP");
                return false;
            }

            // Now send the ajax again with otp.
            $.ajax({
                url: homeUrl + '/ajax/validate-register-fields',
                dataType: 'JSON',
                type: "POST",
                data: {
                    validate: 0,
                    seq: data.seq,
                    otp: otpV,
                },
                beforeSend: function () {
                    btn.disabled = true;
                },
                success: function ( data ) {
                    if ( data.info !== 'success' ) {
                        showError(data.message);
                        return false;
                    }

                    window.location.href = data.message.redirectUrl;
                },
                error: function () {

                },
                complete: function () {
                    btn.disabled = false;
                }
            });
        }


    }, false);

    function showError( msg )
    {
        var alertBox = document.getElementById('alert-box');
        alertBox.innerHTML = msg;
        alertBox.className = alertBox.className.replace(/\bd-none\b/g, "");
    }

    function hideError()
    {
        var alert = document.getElementById('alert-box');
        alert.className += ' d-none ';
    }

</script>
</body>
<?php $this->_addFooter() ?>
