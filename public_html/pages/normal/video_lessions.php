<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 18/12/18
 * Time: 8:59 PM
 */

$this->onlyLoggedInAllowed( UserManager::USER_TYPE_SUBSCRIBER, PackageManager::FEATURE_VIDEO_LESSIONS );

/** @var User $user */
$user = $this->getUser();

$page = isset($this->_data[0]) ? $this->_data[0] : null;

$resourcePaths = array();
foreach ( $this->_data as $key => $item )
{
    if ( is_numeric($key) ) $resourcePaths[] = $item;
}

$title = "Video Lessons";

$studyPages = new VideoLists(SITE_URL . DS . 'video-lessions');
$getContent = $studyPages->getPageContent($resourcePaths);
//var_dump($getContent);exit;


$this->_addHeader();
@include $this->getPath() . DS . 'navs.top.php';
?>


    <a href="#" class="go-back-link"><i class="fa fa-arrow-left"></i></a>
    <div class="row">
        <div class="col-sm-12 col-xxxl-12">
            <div class="element-wrapper">
                <h6 class="custm-ele-hdr">
                    <?php echo $title; ?>
                </h6>
                <div class="custm-ele-hdr-strk">
                </div>
                <div class="element-content">
                    <p>Morbi mollis tellus ac sapien. Pellentesque auctor neque nec urna. Curabitur a felis in nunc fringilla tristique. Praesent vestibulum dapibus nibh. Nulla sit amet est. Morbi mollis tellus ac sapien. Pellentesque auctor neque nec urna. Curabitur a felis in nunc fringilla tristique. Praesent vestibulum dapibus nibh. Nulla sit amet est.</p>
                </div>
            </div>
        </div>
    </div>

<script>
    if (document.layers) {
        //Capture the MouseDown event.
        document.captureEvents(Event.MOUSEDOWN);

        //Disable the OnMouseDown event handler.
        document.onmousedown = function () {
            return false;
        };
    }
    else {
        //Disable the OnMouseUp event handler.
        document.onmouseup = function (e) {
            if (e != null && e.type == "mouseup") {
                //Check the Mouse Button which is clicked.
                if (e.which == 2 || e.which == 3) {
                    //If the Button is middle or right then disable.
                    return false;
                }
            }
        };
    }

    //Disable the Context Menu event.
    document.oncontextmenu = function () {
        return false;
    };
</script>

<?php echo $getContent; ?>
<?php
@include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();

