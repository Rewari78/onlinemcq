<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 1/11/18
 * Time: 5:16 PM
 */

$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );
$collegeId = !empty($this->_data[0]) ? $this->_data[0] : null;

if ( empty($collegeId) ) $this->redirect('manage-college');
$college = CollegeManager::getInstance()->getCollegeById($collegeId);

if ( empty($college) ) $this->redirect('manage-college');

$universityList     = CollegeManager::getInstance()->getAvlUniversities();
$cityList           = StateManager::getInstance()->getCityListByStateId($college['stateId']);
$stateList          = StateManager::getInstance()->getAllStates();
$recognizedList     = CollegeManager::getInstance()->getAllRecognitions();
$collegeTypes       = CollegeManager::getInstance()->getAvlCollegeTypes();

$this->_addHeader();
@include $this->getPath() . DS . 'navs.top.php';
?>
    <div class="row">
        <div class="col-sm-12">
            <div class="element-wrapper">
                <div class="element-box-tp">
                    <?php if ( !empty($_SESSION['submit_col_msg']) ) : ?>
                        <div class="alert alert-success alert-dismissible " role="alert" area-label="Close" class="close">
                            <?php echo $_SESSION['submit_col_msg']; ?>
                            <button aria-label="close" class="close" data-dismiss="alert" type="button"><span aria-hidden="true">&times;</span> </button>
                        </div>
                    <?php unset($_SESSION['submit_col_msg']); ?>
                    <?php endif; ?>


                    <form method="post" action="#" onsubmit="submitForm(this); return false;">
                    <div class="row">

                        <div class="col-sm-6">
                            <div class="element-wrapper">
                                <div class="element-box">


                                        <h5 class="form-header">
                                            Add New College
                                        </h5>

                                        <div class="form-desc">
                                            All fields are required
                                        </div>

                                        <div class="form-group">
                                            <label for="select-state">Select State</label>
                                            <select class="form-control" id="select-state" name="select-state">
                                                <option value="0">Please Select</option>

                                                <?php foreach ( $stateList as $state ): ?>
                                                    <option value="<?php echo $state['id']; ?>" <?php echo $state['id'] === $college['stateId'] ? 'selected' : null ?>>
                                                        <?php echo $state['name']; ?>
                                                    </option>
                                                <?php endforeach; ?>

                                            </select>
                                        </div>

                                        <div class="row">

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="college_name">College Name</label>
                                                    <input class="form-control" value="<?php echo Util::htmlEncode($college['collegeName']); ?>" data-minlength="6" placeholder="Enter College Name" type="text" id="college_name" name="college_name" />
                                                    <div class="help-block form-text text-muted form-control-feedback">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="established">Established</label>
                                                    <input class="form-control" value="<?php echo Util::htmlEncode($college['established']); ?>" data-minlength="6" placeholder="Year of Establishment" type="text" id="established" name="established" />
                                                    <div class="help-block form-text text-muted form-control-feedback">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="select-affiliated-to">Affiliated To</label>
                                            <select class="form-control" id="select-affiliated-to" name="select-affiliated-to">
                                                <option value="0">Please Select</option>
                                                <?php foreach ( $universityList as $university ): ?>
                                                    <option value="<?php echo  $university['id'] ?>" <?php echo $university['id'] === $college['affUniversityId'] ? 'selected' : '';  ?>>
                                                        <?php echo $university['name'] ?>
                                                    </option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="select-college-type">College Type</label>
                                            <select class="form-control"  id="select-college-type" name="select-college-type" placeholder="Select">
                                                <option value="0">Please Select</option>

                                                <?php foreach ( $collegeTypes as $collegeType ): ?>
                                                    <option value="<?php echo $collegeType['id']; ?>" <?php echo $collegeType['id'] === $college['collegeTypeId'] ? 'selected' : '';  ?>>
                                                        <?php echo $collegeType['collegeType']; ?>
                                                    </option>
                                                <?php endforeach; ?>

                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="select-recognition">Recognition</label>
                                            <select class="form-control" id="select-recognition" name="select-recognition">
                                                <option value="0">Please Select</option>

                                                <?php foreach ( $recognizedList as $recognized ): ?>
                                                    <option value="<?php echo $recognized['id'] ?>" <?php echo $recognized['id'] === $college['recognitionId'] ? 'selected' : '';  ?>>
                                                        <?php echo $recognized['text']; ?>
                                                    </option>
                                                <?php endforeach; ?>

                                            </select>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="government_fee">Government Fee</label>
                                                    <input class="form-control" value="<?php echo Util::htmlEncode($college['govFee']); ?>" data-minlength="6" placeholder="Enter Amount" type="text" id="government_fee" name="government_fee">
                                                    <div class="help-block form-text text-muted form-control-feedback">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="NRI_fee">NRI Fee</label>
                                                    <input class="form-control" value="<?php echo Util::htmlEncode($college['nriFee']); ?>" data-minlength="6" placeholder="Enter Amount" type="text" id="NRI_fee" name="NRI_fee">
                                                    <div class="help-block form-text text-muted form-control-feedback">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="private_fee">Private Fee</label>
                                                    <input class="form-control" value="<?php echo Util::htmlEncode($college['privateFee']); ?>" data-minlength="6" placeholder="Enter Amount" type="text" id="private_fee" name="private_fee">
                                                    <div class="help-block form-text text-muted form-control-feedback">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="other_fee">Other Fee</label>
                                                    <input class="form-control" value="<?php echo Util::htmlEncode($college['otherFee']); ?>" data-minlength="6" placeholder="Enter Amount" type="text" id="other_fee" name="other_fee">
                                                    <div class="help-block form-text text-muted form-control-feedback">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="cut_gen">LY Cut Off (General)</label>
                                                    <input class="form-control" value="<?php echo Util::htmlEncode($college['cutGen']); ?>" data-minlength="6" placeholder="Enter Cut Off" type="text" id="cut_gen" name="cut_gen">
                                                    <div class="help-block form-text text-muted form-control-feedback">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="cut_gen_ph">LY Cut Off (General PH)</label>
                                                    <input class="form-control" value="<?php echo Util::htmlEncode($college['cutGenPh']); ?>" data-minlength="6" placeholder="Enter Cut Off" type="text" id="cut_gen_ph" name="cut_gen_ph">
                                                    <div class="help-block form-text text-muted form-control-feedback">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="cut_obc">LY Cut Off (OBC)</label>
                                                    <input class="form-control" value="<?php echo Util::htmlEncode($college['cutObc']); ?>" data-minlength="6" placeholder="Enter Cut Off" type="text" id="cut_obc" name="cut_obc">
                                                    <div class="help-block form-text text-muted form-control-feedback">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="cut_obc_ph">LY Cut Off (OBC PH)</label>
                                                    <input class="form-control" value="<?php echo Util::htmlEncode($college['cutObcPh']); ?>" data-minlength="6" placeholder="Enter Cut Off" type="text" id="cut_obc_ph" name="cut_obc_ph">
                                                    <div class="help-block form-text text-muted form-control-feedback">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="cut_sc">LY Cut Off (SC)</label>
                                                    <input class="form-control" value="<?php echo Util::htmlEncode($college['cutSc']); ?>" data-minlength="6" placeholder="Enter Cut Off" type="text" id="cut_sc" name="cut_sc">
                                                    <div class="help-block form-text text-muted form-control-feedback">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="cut_sc_ph">LY Cut Off (SC PH)</label>
                                                    <input class="form-control" value="<?php echo Util::htmlEncode($college['cutScPh']); ?>" data-minlength="6" placeholder="Enter Cut Off" type="text" id="cut_sc_ph" name="cut_sc_ph" >
                                                    <div class="help-block form-text text-muted form-control-feedback">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="cut_st">LY Cut Off (ST)</label>
                                                    <input class="form-control" value="<?php echo Util::htmlEncode($college['cutSt']); ?>" data-minlength="6" placeholder="Enter Cut Off" type="text" id="cut_st" name="cut_st">
                                                    <div class="help-block form-text text-muted form-control-feedback">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="cut_st_ph">LY Cut Off (ST PH)</label>
                                                    <input class="form-control" value="<?php echo Util::htmlEncode($college['cutStPh']); ?>" data-minlength="6" placeholder="Enter Cut Off" type="text" id="cut_st_ph" name="cut_st_ph">
                                                    <div class="help-block form-text text-muted form-control-feedback">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="select-city">Select City</label>
                                            <select class="form-control" id="select-city" name="select-city">
                                                <option value="0">Please Select</option>
                                                <?php foreach ( $cityList as $city ): ?>
                                                    <option value="<?php echo $city['id'] ?>" <?php echo $city['id'] === $college['cityId'] ? 'selected' : '';  ?>><?php echo Util::htmlEncode($city['name']) ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="website_url">Website URL</label>
                                            <input class="form-control" value="<?php echo Util::htmlEncode($college['websiteUrl']); ?>" placeholder="Enter Website URL" type="text" id="website_url" name="website_url">
                                            <div class="help-block form-text text-muted form-control-feedback">
                                            </div>
                                        </div>

                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="element-wrapper">
                                <div class="element-box">
                                        <h5 class="form-header">
                                            Seats Matrix
                                        </h5>
                                        <div class="form-desc">
                                            All fields are required
                                        </div>
                                        <legend><span>Total Intake (Seats)</span></legend>
                                        <div class="form-group">
                                            <label for="total-seats">Total Seats</label>
                                            <input class="form-control" value="<?php echo Util::htmlEncode($college['totalSeats']); ?>" placeholder="Total Number of Seats" type="text" id="total-seats" name="total-seats">
                                            <div class="help-block form-text text-muted form-control-feedback">
                                            </div>
                                        </div>


                                    <div class="alert alert-warning alert-dismissible d-none" role="alert">
                                    </div>


                                    <div class="form-buttons-w">
                                            <button class="btn btn-primary" type="submit"> Submit</button>
                                        </div>

                                </div>
                            </div>
                        </div>

                    </div>
                        <input value="<?php echo $college['id']; ?>" name="collegeId" type="hidden" />
                    </form>


                </div>
            </div>
        </div>
    </div>

<script type="text/javascript">
    (function(_scope){

        var $collegeName = $('#college_name');
        var $established = $('#established');
        var $affTo = $('#select-affiliated-to');
        var $collegeType = $('#select-college-type');
        var $recognition = $('#select-recognition');
        var $govFee = $('#government_fee');
        var $nriFee = $('#NRI_fee');
        var $privateFee = $('#private_fee');
        var $otherFee = $('#other_fee');
        var $lastYearCut = $('#last_year_cut_off');
        var $cutGen = $('#cut_gen');
        var $cutGenPh = $('#cut_gen_ph');
        var $cutObc = $('#cut_obc');
        var $cutObcPh = $('#cut_obc_ph');
        var $cutSc = $('#cut_sc');
        var $cutScPh = $('#cut_sc_ph');
        var $cutSt = $('#cut_st');
        var $cutStPh = $('#cut_st_ph');
        var $city = $('#select-city');
        var $webUrl = $('#website_url');
        var $totalSeats = $('#total-seats');

        $(document).on('change', '#select-state', function () {
            var $stateSelector = $(this);
            var stateId = $(this).val().trim();

            if ( stateId <= 0 ) _disableEveryThing();
            
            $.post(
                homeUrl + '/ajax/get-fields-value-for-college',
                { stateId: stateId },
                function( data ) {

                    if ( data.info !== 'success' ) {
                        _disableEveryThing();
                        return;
                    }

                    var cities = data.message.cities;
                    var universities = data.message.universities;
                    var collegeType = data.message.collegeType;
                    var recognition = data.message.recognition;

                    _addCities($city, cities);
                    _addUniversities($affTo, universities);
                    _addCollegeType($collegeType, collegeType);
                    _addRecognition($recognition, recognition);

                    _enableEveryThing();


                },
                'json'
            );
            
        });

        function _disableEveryThing()
        {
            _enableOrDisabled([
                $collegeName, $established, $affTo,
                $collegeType, $recognition, $govFee, $nriFee,
                $privateFee, $otherFee, $lastYearCut,
                $city,
                $webUrl,
                $totalSeats,
                $cutGen, $cutGenPh,
                $cutObc, $cutObcPh,
                $cutSc, $cutScPh,
                $cutSt, $cutStPh
            ], true);
        }

        function _enableEveryThing()
        {
            _enableOrDisabled([
                $collegeName, $established, $affTo,
                $collegeType, $recognition, $govFee, $nriFee,
                $privateFee, $otherFee, $lastYearCut,
                $city,
                $webUrl,
                $totalSeats,
                $cutGen, $cutGenPh,
                $cutObc, $cutObcPh,
                $cutSc, $cutScPh,
                $cutSt, $cutStPh
            ], false);
        }

        function _addCities( select, data )
        {
            var i = 0;
            var html = '<option value="0">Please Select</option>';
            for(; i < data.length; i++)
            {
                html += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
            }

            $(select).html(html);
        }

        function _addUniversities( select, data ) {
            var i = 0;
            var html = '<option value="0">Please Select</option>';
            for(; i < data.length; i++)
            {
                html += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
            }

            $(select).html(html);
        }
        
        function _addCollegeType(select, data) {
            var i = 0;
            var html = '<option value="0">Please Select</option>';
            for(; i < data.length; i++ )
            {
                html += '<option value="' + data[i].id + '">' + data[i].collegeType + '</option>';
            }

            $(select).html(html);
        }

        function _addRecognition( select, data )
        {
            var i = 0;
            var html = '<option value="0">Please Select</option>';
            for( ; i < data.length; i++ )
            {
                html += '<option value="' + data[i].id + '">' + data[i].text + '</option>';
            }

            $(select).html(html);
        }

        function _enableOrDisabled( arr, disabled ) {

            var i = 0;
            for( ; i < arr.length; i++ )
            {
                $(arr[i])[0].disabled = disabled;
            }

        }


    })(window);

    function submitForm( form )
    {
        var $form = $(form);
        var $alert = $form.find('.alert');
        var _data = $(form).serialize();

        hideError($alert);

        $.post(
            homeUrl + '/ajax/submit-edit-college',
            $form.serialize(),
            function (data) {
                if ( data.info !== 'success' )
                {
                    showError($alert, data.message);
                    return;
                }

                window.location.href = '/manage-college?college-updated=1';

            },
            'json'
        );
    }

    function showError( box, msg )
    {

        var $alertBox = $(box);
        $alertBox.html(msg);
        $alertBox.removeClass('d-none');
    }

    function hideError( box )
    {
        var $alertBox = $(box);
        $alertBox.addClass('d-none');
    }

</script>
<?php
@include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();

