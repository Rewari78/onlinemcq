<?php

$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );

// Get the package id.
$packageId = $this->_data[0];
$packageId = filter_var(filter_var($packageId, FILTER_SANITIZE_NUMBER_INT), FILTER_VALIDATE_INT);

if ( empty($packageId) ) $this->redirect('manage-packages');

// Now get the package Details.
$packageDetails = PackageManager::getInstance()->getAPackageById($packageId);

if ( empty($packageDetails) ) $this->redirect('manage-packages');

$states = StateManager::getInstance()->getAllStates();
$features = PackageManager::getInstance()->getAllFeatures();

// Enabled features for this package.
$enabledFeatures = array();

foreach ( $packageDetails['features'] as $feature )
{
    $enabledFeatures[$feature['featureId']] = array(
        'id' => $feature['featureId'],
        'variables' => $feature['variables']
    );
}

$msg = null;
//Message
if ( isset($_GET['pf_success']) )
{
    $msg = "New Feature selection is now updated.";
} else if ( isset($_GET['up_success']) )
{
    $msg = "Package details has been updated successfully.";
} else if ( isset($_GET['ps_success']) )
{
    $msg = "Non Domicile States has been changed for package: <b>{$packageDetails['packageInfo']['packageName']}</b>";
}

$packageStateIds = PackageManager::getInstance()->getStateIdsForPackages($packageId);
$packageStateIds = !empty($packageStateIds) ? $packageStateIds[key($packageStateIds)] : array();

$this->_addHeader();
@include $this->getPath() . DS . 'navs.top.php';
?>
    <div class="row">
    <div class="col-sm-8 col-xxxl-8">
        <div class="element-wrapper">

            <?php if ( !empty($msg) ) : ?>
            <div class="alert alert-success alert-dismissible " role="alert" area-label="Close" class="close">
                <?php echo $msg; ?>
                <button aria-label="close" class="close" data-dismiss="alert" type="button"><span aria-hidden="true">&times;</span> </button>
            </div>
            <?php endif; ?>

            <!-- <div class="element-actions">
              <button class="btn btn-primary custom-button" data-target="#add-type-modal" data-toggle="modal" type="button">Add Package</button>
            </div> -->
            <h6 class="element-header">
                Edit Packages
            </h6>
            <div class="element-box">
                <form method="post" action="/update-package-info/<?php echo $packageId ?>">

                    <div class="form-group">
                        <label for="package-name">Package Name</label>
                        <input class="form-control" placeholder="Type Package Name" type="text" id="package-name" name="package-name" value="<?php echo Util::htmlEncode($packageDetails['packageInfo']['packageName']) ?>">
                    </div>

                    <div class="row">

                        <div class="col-sm-6 col-xxxl-6">
                            <div class="form-group">
                                <label for="package-price">Package Price</label>
                                <input class="form-control" min="0" placeholder="Type Package Price" type="number" id="package-price" name="package-price" value="<?php echo $packageDetails['packageInfo']['price'] ?>">
                            </div>
                        </div>

                        <div class="col-sm-6 col-xxxl-6">
                            <div class="form-group">
                                <label for="discount-price">Discount Price</label>
                                <input class="form-control" min="0" placeholder="Type Discount Price" type="number" id="discount-price" name="discount-price" value="<?php echo $packageDetails['packageInfo']['offPrice'] ?>">
                            </div>
                        </div>

                    </div>
                    <button class="btn btn-primary" type="submit">Update</button>
                </form>
            </div>
        </div>

        <div class="element-wrapper">
            <h6 class="element-header">
                Packages Details
            </h6>
            <div class="element-box">
                <form method="post" action="/submit-package-features/<?php echo $packageId; ?>">
                    <div class="table-responsive">
                        <table class="table table-lightborder">
                            <thead>
                            <tr>
                                <th>
                                    Package Details
                                </th>
                                <th class="text-left">
                                    Enable / Disable
                                </th>
                                <th>
                                    Varibales
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ( $features as $feature ) : ?>
                                <tr>
                                    <td class="nowrap">
                                        <?php  echo $feature['featureName'] ?>
                                    </td>
                                    <td class="text-left">
                                        <input type="checkbox" name="features[]" value="<?php  echo $feature['id'] ?>" <?php echo isset($enabledFeatures[$feature['id']]) ? 'checked' : '' ?> />
                                    </td>
                                    <td class="row-actions text-left">
                                        <?php if ( $feature['id'] == PackageManager::FEATURE_QNA_RW ): ?>
                                            <label for="qa_write"></label>
                                            <input class="form-control"
                                                   placeholder="Write Limit"
                                                   min="0"
                                                   type="number"
                                                   id="qa_write"
                                                   name="qa_write"
                                                   value="<?php echo !empty($enabledFeatures[$feature['id']]['variables']) ? $enabledFeatures[$feature['id']]['variables']['num'] : '0' ?>" />
                                        <?php endif; ?>
                                        <?php if ( $feature['id'] == PackageManager::FEATURE_TEST_SERIES ): ?>
                                            <label for="text_limit"></label>
                                            <input class="form-control"
                                                   min="0"
                                                   max="5"
                                                   placeholder="Test Limit"
                                                   type="number"
                                                   id="text_limit"
                                                   name="text_limit"
                                                   value="<?php echo !empty($enabledFeatures[$feature['id']]['variables']) ? $enabledFeatures[$feature['id']]['variables']['num'] : '0' ?>" />
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="col-sm-4 col-xxxl-4">
        <div class="element-wrapper">
            <div class="element-actions">
            </div>
            <h6 class="element-header">
                Non Domicile State
            </h6>
            <div class="element-box">
                <form method="post" action="/submit-package-states/<?php echo $packageId ?>">
                    <div class="form-group">
                        <label for="add-state">Add State</label>
                        <select class="form-control select2" multiple="true" id="add-state" name="add-state[]">
                            <?php foreach ( $states as $state ) : ?>
                                <option value="<?php echo $state['id']; ?>" <?php echo isset($packageStateIds[$state['id']]) ? 'selected' : ''; ?> >
                                    <?php echo $state['name'] . ' (' . $state['shortName'] . ')'; ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="form-buttons-w">
                        <button class="btn btn-primary" type="submit">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php
@include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();
