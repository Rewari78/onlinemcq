<?php
if ( !UserManager::getInstance()->isLoggedIn() ) $this->redirect('login');

// else we need to logout.
UserManager::getInstance()->logout();

$this->redirect('login');