<?php
$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );
$qtype_id = filter_input(INPUT_GET, 'ot', FILTER_SANITIZE_NUMBER_INT);

//$getQues = QNA::getInstance()->getQues();

switch ( $qtype_id )
{
    case QNA::NEET:
    case QNA::AIIMS:
    case QNA::JPMER:
    case QNA::GENERAL:
        break;
    default:
        $qtype_id = 0;
}
$getQues = QNA::getInstance()->getQuesType($qtype_id);

$this->_addHeader();
@include $this->getPath() . DS . 'navs.top.php';
?>
<div class="container dash-container">

                <h6 class="custm-ele-hdr">
                  Question & Answers
                </h6>
                <div class="custm-ele-hdr-strk"></div>
                <div class="row row-question-filter">
                  <div class="col-md-9"></div>
                  <div class="col-md-3">
                  <form class="form-inline justify-content-sm-end ">
                    <label for="questionSort">Search</label>
                    <select class="form-control form-control-sm ex-m" id="select-order-type">
                      <option value="0">
                            Filter By
                        </option>
                        <option <?php echo $qtype_id == QNA::NEET ? 'selected' : ''; ?> value="<?php echo QNA::NEET; ?>">NEET</option>
                        <option <?php echo $qtype_id == QNA::AIIMS ? 'selected' : ''; ?> value="<?php echo  QNA::AIIMS; ?>">AIIMS</option>
                        <option <?php echo $qtype_id == QNA::JPMER ? 'selected' : ''; ?> value="<?php echo  QNA::JPMER; ?>">JPMER</option>
                        <option <?php echo $qtype_id == QNA::GENERAL ? 'selected' : ''; ?> value="<?php echo  QNA::GENERAL; ?>">GENERAL</option>
                    </select>
                  </form>
                </div>
                <?php foreach ( $getQues as $getQues ): ?>

                <div class="row row-question">
                  <div class="col-md-1">
                    
                    <div class="question-number">
                      <p>Question No.</p>
                      <h4><?php echo ($getQues['id']); ?></h4>
                    </div>
                  </div>

                  <div class="col-md-11">
                    <div class="question-name">
                    <h4><a href=""><?php echo ($getQues['q_title']); ?></a></h4>
                    
                    <p class="question-definition">
                      <?php echo ($getQues['q_des']); ?>
                    </p>
                    <p class="tags">
                       <button class="btn btn-primary" style="background-color: white"><a href=""><?php
                    switch ($getQues['qtype_id']) {
                      case QNA::NEET:
                        echo 'Neet';
                        break;

                        case QNA::AIIMS:
                        echo 'Aiims';
                        break;

                      case QNA::JPMER:
                        echo 'Jpmer';
                        break;

                        case QNA::GENERAL:
                        echo 'General';
                        break;
                    }?></a></button>
                    </p>
                    </div>
                  </div>
                </div>

                <div class="row row-answer">
                  <div class="col-md-2">
                    <div class="question-number">
                      <p>Answer</p>
                    </div>
                  </div>

                  <div class="col-md-10">
                    <div class="question-name">
                    
                    
                    <p class="question-definition">
                     <?php echo ($getQues['q_des']); ?>
                    </p>
                    </div>
                  </div>
                </div>

                
                
                <?php endforeach; ?>



               </div>


               <script type="text/javascript">
      function _buildQuery( obj ) {

            var prop, params = [];
        for( prop in obj )
        {
            if ( obj.hasOwnProperty(prop) )
                params.push( prop + '=' + obj[prop] );
        }

        return params.join('&');
    }

    (function( __scope ){
 

          $(document).on('change', '#select-order-type', function (e) {
            var orderType = $(this).val().trim();

            __scope.location.href = '/question-defination?' +  _buildQuery({
               
                ot: orderType,
            });
        });

       

    })(window)
</script>


<?php 
@include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();