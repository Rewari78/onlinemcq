<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 18/2/19
 * Time: 11:25 PM
 */
$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );

// Delete if selected.
$deleteId = filter_input(INPUT_GET, 'delete-id', FILTER_SANITIZE_NUMBER_INT);
if ( !empty($deleteId) )
{
    if ( ApplyNow::getInstance()->deleteApplyNeet($deleteId) )
    {
        $msg = "Selected apply now is not deleted";
    }
}


$list = ApplyNow::getInstance()->getApplyNeet();

$stateIds = array();
$collegeIds = array();

$currentTime = time();

foreach ( $list as $item )
{
    $stateIds[$item['stateId']] = $item['stateId'];
    $collegeIds[$item['collegeId']] = $item['collegeId'];
}

$stateList = StateManager::getInstance()->getStateByIdList($stateIds);
$collegeList = CollegeManager::getInstance()->getCollegeByIdList($collegeIds);


$this->_addHeader();
@include $this->getPath() . DS . 'navs.top.php';
?>
    <div class="row">
        <div class="col-sm-8 col-xxxl-8">
            <div class="element-wrapper">
                <?php if ( !empty($msg) ) : ?>
                    <div class="alert alert-success alert-dismissible " role="alert" area-label="Close" class="close">
                        <?php echo $msg; ?>
                        <button aria-label="close" class="close" data-dismiss="alert" type="button"><span aria-hidden="true">&times;</span> </button>
                    </div>
                <?php endif; ?>


                <div class="element-actions">
                    <a class="btn btn-primary custom-button" href="add-apply-neet">Add Apply Neet</a>
                </div>
                <h6 class="element-header">
                    Manage Apply Neet
                </h6>
                <div class="element-box">
                    <div class="table-responsive">

                        <?php if ( empty($list) ): ?>
                            <p>No Apply type found.</p>
                        <?php else:?>
                            <table class="table table-lightborder">
                                <thead>
                                <tr>
                                    <th>
                                        SR.No
                                    </th>
                                    <th class="text-left">
                                        State
                                    </th>
                                    <th>
                                        College
                                    </th>
                                    <th>
                                        isButton1Active
                                    </th>
                                    <th>
                                        isButton2Active
                                    </th>
                                    <th>
                                        isButton3Active
                                    </th>
                                    <th>
                                        Actions
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ( $list as $item ):?>
                                <tr>
                                    <td>
                                        <?php echo $item['id']; ?>
                                    </td>
                                    <td>
                                        <?php if ( $item['stateId'] == 100 ) : ?>
                                            All India
                                        <?php else: ?>
                                            <?php echo isset($stateList[$item['stateId']]) ? $stateList[$item['stateId']]['name'] : 'Unknown';  ?>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php echo isset($collegeList[$item['collegeId']]) ? $collegeList[$item['collegeId']]['collegeName'] : 'No College'; ?>
                                    </td>
                                    <td>
                                        <?php echo $currentTime >= $item['button1StartTime'] && $currentTime <= $item['button1EndTime'] ? 'Yes' : 'No'; ?>
                                    </td>
                                    <td>
                                        <?php echo $currentTime >= $item['button2StartTime'] && $currentTime <= $item['button2EndTime'] ? 'Yes' : 'No'; ?>
                                    </td>
                                    <td>
                                        <?php echo $currentTime >= $item['button3StartTime'] && $currentTime <= $item['button3EndTime'] ? 'Yes' : 'No'; ?>
                                    </td>
                                    <td>
                                        <a href="/manage-apply-neet?delete-id=<?php echo $item['id']; ?>" >
                                            Delete
                                        </a>
<!--                                        <a href="/manage-apply-neet?delete-id=--><?php //echo $item['id']; ?><!--" >-->
<!--                                            Edit-->
<!--                                        </a>-->
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        <?php endif; ?>


                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-4 col-xxxl-4">
        </div>

    </div>

    <div aria-hidden="true" aria-labelledby="mySmallModalLabel" class="modal fade bd-example-modal-sm" id="add-college-type" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        Add New Type
                    </h5>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                </div>
                <form action="/add-college-type" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="college-type"> College Type</label>
                            <input class="form-control" placeholder="Type" type="text" id="college-type" name="college-type" />

                            <div class="alert alert-warning alert-dismissible d-none" role="alert" id="add-p-alert-box">
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" data-dismiss="modal" type="button"> Close</button>
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function submitForm( form )
        {
            var $form = $(form);
            var $alert = $form.find('.alert');
            var _data = $(form).serialize();

            hideError($alert);

            $.post(
                homeUrl + '/ajax/edit-college-type',
                $form.serialize(),
                function (data) {
                    if ( data.info !== 'success' )
                    {
                        showError($alert, data.message);
                        return;
                    }

                    window.location.reload();

                },
                'json'
            );
        }

        function showError( box, msg )
        {

            var $alertBox = $(box);
            $alertBox.html(msg);
            $alertBox.removeClass('d-none');
        }

        function hideError( box )
        {
            var $alertBox = $(box);
            $alertBox.addClass('d-none');
        }
    </script>
<?php
@include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();