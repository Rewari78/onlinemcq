<?php
$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );

$msg = "";

if ( $this->isPost() )
{
    $stateId = filter_input(INPUT_POST, 'select-state', FILTER_SANITIZE_NUMBER_INT);
    $universityTypeId = filter_input(INPUT_POST, 'university-type', FILTER_SANITIZE_NUMBER_INT);
    $universityName = filter_input(INPUT_POST, 'university-name');

    if ( empty($stateId) || empty($universityTypeId) || empty($universityName) )
    {
        $msg = "Please fill all the fields properly.";
    }else if( empty(StateManager::getInstance()->getStateById($stateId)) )
    {
        $msg = "State is not valid.";
    } else if ( empty(CollegeManager::getInstance()->getUniversityTypeById($universityTypeId)) )
    {
        $msg = "University is not valid.";
    }else {
        CollegeManager::getInstance()
            ->addUniversity($stateId, $universityTypeId, $universityName);

        $msg = "New university is added.";
    }
}

$KVStates = array();
$stateOptions = '';
$states = StateManager::getInstance()->getAllStates();
foreach ( $states as $state ) {

    $id = $state['id'];
    unset($state['id']);
    $KVStates[$id] = $state;

    $stateOptions .= '<option value="' . $id . '">' . $state['name'] . '</option>';
}
$KVUniversityTypes = array();
$universityTypes = CollegeManager::getInstance()->getAvlUniversityTypes();
$universityTypesOptions = '';
foreach ( $universityTypes as $universityType ) {
    $id = $universityType['id'];
    unset($universityType['id']);
    $KVUniversityTypes[$id] = $universityType;
    $universityTypesOptions .= '<option value="' . $id . '">' . $universityType['universityType'] . '</option>';
}

$universities = CollegeManager::getInstance()
    ->getAvlUniversities();

$this->_addHeader();
@include $this->getPath() . DS . 'navs.top.php';
?>
    <div class="row">
        <div class="col-sm-12 col-xxxl-12">
            <div class="element-wrapper">
                <div class="element-actions">
                    <button class="btn btn-primary custom-button" data-target="#add-university-modal" data-toggle="modal" type="button">Add University</button>
                </div>
                <h6 class="element-header">
                    Add New University
                </h6>

                <?php if ( !empty($msg) ) : ?>
                    <div class="alert alert-success alert-dismissible " role="alert" area-label="Close">
                        <?php echo $msg; ?>
                        <button aria-label="close" class="close" data-dismiss="alert" type="button">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                <?php endif; ?>

                <?php if ( !empty($universities) ): ?>
                    <div class="element-box">
                    <div class="table-responsive">
                        <table class="table table-lightborder">
                            <thead>
                            <tr>
                                <th>
                                    SR.No
                                </th>
                                <th class="text-left">
                                    State
                                </th>
                                <th class="text-left">
                                    University Type
                                </th>
                                <th class="text-left">
                                    University Name
                                </th>
                                <th class="text-center">
                                    Actions
                                </th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php foreach ( $universities as $university ): ?>
                                <tr>
                                    <td class="nowrap">
                                        <?php echo $university['id']; ?>
                                    </td>
                                    <td class="text-left">
                                        <?php echo isset($KVStates[$university['stateId']]) ? $KVStates[$university['stateId']]['name'] : 'Unknown'; ?>
                                    </td>
                                    <td class="text-left">
                                        <?php echo isset($KVUniversityTypes[$university['universityTypeId']]) ? $KVUniversityTypes[$university['universityTypeId']]['universityType'] : 'Unknown'; ?>
                                    </td>
                                    <td class="text-left">
                                        <?php echo $university['name']; ?>
                                    </td>
                                    <td class="row-actions">
                                        <a href="#" data-target="#edit-university-modal-<?php echo $university['id']; ?>" data-toggle="modal"><i class="os-icon os-icon-ui-49"></i></a>

                                        <div aria-hidden="true" aria-labelledby="mySmallModalLabel" class="modal fade bd-example-modal-sm" id="edit-university-modal-<?php echo $university['id']; ?>" role="dialog" tabindex="-1">
                                            <div class="modal-dialog modal-sm">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">
                                                            Edit University
                                                        </h5>
                                                        <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                                                    </div>
                                                    <form method="post" action="#" onsubmit="submitForm(this); return false;">
                                                        <div class="modal-body">
                                                            <div class="form-group">
                                                                <label for="select-state">Select State</label>
                                                                <select class="form-control" required="required" id="select-state-<?php echo $university['id']; ?>" name="select-state" placeholder="Select">
                                                                    <option value="0">Please Select</option>
                                                                    <?php echo $stateOptions; ?>
                                                                </select>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="university-type">University Type</label>
                                                                <select class="form-control" required="required" id="university-type-<?php echo $university['id']; ?>" name="university-type" placeholder="Select">
                                                                    <option value="0">Please Select</option>
                                                                    <?php echo $universityTypesOptions; ?>
                                                                </select>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="university-name"> University Name</label>
                                                                <input class="form-control" placeholder="Type" type="text" id="university-name-<?php echo $university['id']; ?>" name="university-name">
                                                            </div>

                                                            <input type="hidden" value="<?php echo $university['id']; ?>" name="university-id" />
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
                                                            <button class="btn btn-primary" type="submit">Save changes</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>

                            </tbody>
                        </table>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>

        <div class="col-sm-4 col-xxxl-4">
        </div>
    </div>

    <div aria-hidden="true" aria-labelledby="mySmallModalLabel" class="modal fade bd-example-modal-sm" id="add-university-modal" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        Add New University
                    </h5>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                </div>
                <form method="post" action="#" >
                    <div class="modal-body">

                        <div class="form-group">
                            <label for="select-state">Select State</label>
                            <select class="form-control" required="required" id="select-state" name="select-state">
                                <option value="0">Please Select</option>
                                <?php echo $stateOptions; ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="select-city">Select City</label>
                            <select class="form-control" required="required" id="select-city" name="select-city" disabled>
                                <option value="0">Please Select</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="university-type">University Type</label>
                            <select class="form-control" required="required" id="university-type" name="university-type">
                                <option value="0">Please Select</option>
                                <?php echo $universityTypesOptions; ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="university-name"> University Name</label>
                            <input class="form-control" placeholder="Type" type="text" id="university-name" name="university-name">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
                        <button class="btn btn-primary" type="submit">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).on('change', '#select-state', function ( event ) {

            // We need to fetch the cities.
            $.post(
                homeUrl + '/ajax/get-cities-by-state',
                $form.serialize(),
                function (data) {

                    // There is some data that is
                    // going to be in the cities.
                    if ( data.info !== 'success' )
                    {
                        return;
                    }

                    window.location.reload();

                },
                'json'
            );
        });

        function submitForm( form )
        {
            var $form = $(form);
            var $alert = $form.find('.alert');
            var _data = $(form).serialize();

            hideError($alert);

            $.post(
                homeUrl + '/ajax/edit-universities',
                $form.serialize(),
                function (data) {
                    if ( data.info !== 'success' )
                    {
                        showError($alert, data.message);
                        return;
                    }

                    window.location.reload();

                },
                'json'
            );
        }

        function showError( box, msg )
        {

            var $alertBox = $(box);
            $alertBox.html(msg);
            $alertBox.removeClass('d-none');
        }

        function hideError( box )
        {
            var $alertBox = $(box);
            $alertBox.addClass('d-none');
        }
    </script>
<?php
@include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();
