<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 3/28/19
 * Time: 1:29 PM
 */
$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );
$this->_addHeader();
@include $this->getPath() . DS . 'navs.top.php';
?>

<?php
@include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();
