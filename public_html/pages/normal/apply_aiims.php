<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 18/2/19
 * Time: 11:18 PM
 */

$this->onlyLoggedInAllowed();

/** @var User $user */
$user = $this->getUser();
$extraInfo = $user->getExtraInfo();

$lists = call_user_func_array(array(ApplyNow::getInstance(), 'getApplyAiims'), []);

$currentTime = time();


$allStateList = StateManager::getInstance()->getAllStates();


function remaining_time( $toTime )
{

    $now = new DateTime();
    $future_date = new DateTime('@' . $toTime);

    $interval = $future_date->diff($now);

    echo $interval->format("%a days, %h hours, %i minutes");
}

$this->_addHeader();
@include $this->getPath() . DS . 'navs.top.php';
?>

<?php if ( empty($lists) ) : ?>
    <div class="row r-eo-bg">
        <div class="col-sm-12">
            <h4 class="text-center">Sorry looks like nothing is going on here right now.</h4>
        </div>
    </div>
<?php endif; ?>

<?php foreach ( $lists as $item ): ?>
    <div class="row r-eo-bg">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-6">
                    <h4 class="cus-cls-fr-txt">AIIMS APPLY NOTIFICATION<span class="cus-cls-fr-txt-span"><a href="#">Check Eligibility</a></span></h4>
                    <p class="typ-text"><?php echo htmlentities($item['desc']); ?></p>
                </div>
                <div class="col-sm-1">
                </div>

                <?php
                $isBtn1Active = $currentTime >= $item['button1StartTime'] && $currentTime <= $item['button1EndTime'];
                $isBtn2Active = $currentTime >= $item['button2StartTime'] && $currentTime <= $item['button2EndTime'];
                $isBtn3Active = $currentTime >= $item['button3StartTime'] && $currentTime <= $item['button3EndTime'];
                ?>
                <div class="col-sm-5">
                    <div class="row">
                        <?php if ( $isBtn1Active ): ?>

                            <div class="col-sm-4">
                                <p class="dy-lft-txt"><?php remaining_time($item['button1EndTime']); ?></p>
                                <a class="cus-new-l" href="#"><?php echo $item['button1Label']; ?> <span>&#xE76C;</span></a>
                            </div>
                        <?php endif; ?>

                        <?php if ( $isBtn2Active ): ?>

                            <div class="col-sm-4">
                                <p class="dy-lft-txt"><?php remaining_time($item['button2EndTime']); ?></p>
                                <a class="cus-new-l" href="#"><?php echo $item['button2Label']; ?> <span>&#xE76C;</span></a>
                            </div>
                        <?php endif; ?>

                        <?php if ( $isBtn3Active ): ?>

                            <div class="col-sm-4">
                                <p class="dy-lft-txt"><?php remaining_time($item['button3EndTime']); ?></p>
                                <a class="cus-new-l" href="#"><?php echo $item['button3Label']; ?> <span>&#xE76C;</span></a>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endforeach; ?>

    <script type="text/javascript">
        $(document).on('change', '#select-type', function () {
            window.location.href = '/apply-neet-ug?select-type=' + $(this).val();
        });

        $(document).on('change', '#select-state', function () {
            window.location.href = '/apply-neet-ug?select-type=2&select-state=' + $(this).val();
        });

    </script>

<?php
@include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();
