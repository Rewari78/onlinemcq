<?php

/**
 * Created by PhpStorm.
 * User: akash
 * Date: 13/4/19
 * Time: 1:28 AM
 */


if (!UserManager::getInstance()->isTeacher()) {
    $this->onlyLoggedInAllowed(UserManager::USER_TYPE_STUDENT);
}





$testId = filter_input(INPUT_GET, 'test-id', FILTER_SANITIZE_NUMBER_INT);
$testId = empty($testId) ? 0 : $testId;

if ($testId == 99999 )
{
    ?>
        <div class="big-error-w">
        <h2 class="permission-denied">
            Test yet not started. Please close this page and click on View Test link again
        </h2>
    </div>
    <?php
    exit();
}
$user = UserManager::getInstance();
if ($user->isLoggedIn() && $user->isStudent()) {
    $studentsTest = TestManager::getInstance()->getTestResult($testId, $user->getUserId());
    if (!empty($studentsTest)) {
        header('Location: ' . SITE_URL . '/result?result-id=' . $studentsTest['id']);
    }
}

$testInfo = TestManager::getInstance()->getTestInfo($testId);

if (empty($testInfo)) throw new Error404;


$allQuestions = TestManager::getInstance()->getAllQuestions($testInfo['id']);
$totalQuestions = count($allQuestions);

$this->_addHeader();

$curr = time();
$startT = strtotime($testInfo['startAt']);

if ( $startT > $curr && $_SESSION['userType'] === 'student'  ) {
    ?>
    <div class="big-error-w">
        <h2 class="permission-denied">
            Test yet not started
        </h2>
        <p>
            Test will start at <?php echo date("F j, Y, g:i a",strtotime($testInfo['startAt']));  ?>
                <script>

                setTimeout(() => {
                    location.href="/take-test?test-id=99999";    
                }, 7000);
                
                //window.location.href += "&noreload=yes";
                </script>
        </p>
    </div>
    <?php
    exit();
} else {

// var_dump($startT, $curr, $startT > $curr);

$remainingTime = $_SESSION['userType'] == 'student' ? ( $testInfo['totalTime'] * 60 ) - ($curr - $startT) : $testInfo['totalTime'] * 60;
// var_dump($_SESSION);exit;
?>
<div class="container">

<style>
b,
strong {
  font-weight: bolder;
}

</style>
    <div class="row">
        <div class="col-sm-12 col-xxxl-12">
            <div class="element-wrapper">

                <h1 class="custm-ele-hdr">
                    You are now giving <?php echo $testInfo['subject'] ?> - <?php echo $testInfo['description']; ?>
                </h1>
                <?php if (  1 == 2  && isset( $_SESSION['first_name'] ) ) { ?>
                    <strong>Name: </strong><?php echo $_SESSION['first_name'] . ' ' . $_SESSION['last_name']; ?><br />
                <?php } ?>
	            <?php if ( isset( $_SESSION['className'] ) ) { ?>
                <strong>Class: </strong><?php echo $_SESSION['className']; ?><br />
	            <?php } ?>
	            <?php if ( isset( $_SESSION['divName'] ) ) { ?>
                <strong>Division: </strong><?php echo $_SESSION['divName']; ?><br /><br />
	            <?php } ?>
                <div class="custm-ele-hdr-strk">
                </div>

                <div class="element-content">
                    <!-- <p class="blinkingWarning">*****Please do not <strong style="font-weight: bold;">Close, Refresh or leave</strong> this page. Doing so will end the test.*****</p> -->
     
                    <p class="time">Time Remaining <span class="utime"><?php echo round($remainingTime / 60) ?>:00</span></p>
                    <?php $i = 0; ?>
                    <?php foreach ($allQuestions as $allQuestion) : ?>
                        <?php $i++; ?>
                        <div class="question-wrapper <?php echo $i == 1 ? "active" : "" ?>" id="question-wrapper-<?php echo $i; ?>">
                            <p class="weight">Marks: <?php echo $allQuestion['weight']; ?></p>
                            <p class="weight">Question No: <?php echo $i .'/' . $totalQuestions; ?></p>
                            <h6 class="element-header" style="margin-bottom: 10px;margin-top: 50px;"><?php echo "Question Number: " .  $i; ?></h6>
                            <div><?php echo $allQuestion['question']; ?></div>

                            <?php if ( $allQuestion['photo'] !== null ) { ?>
                                <div style="max-width: 250px;max-height: 250px;">
                                <?php
                                    $chunks = explode('|||', $allQuestion['photo']);
                                    if ( $chunks[2] == 'pdf' ) {
                                    ?>
                                    <a href="<?php echo SITE_URL . $chunks[0]; ?>" target="_blank"><p style="padding: 10px; background-color: #047bf8;color: white;"><strong><?php echo $chunks[1]; ?></strong></p></a>
                                    <?php

                                    } else {
                                    ?>
                                        <img style="height:100%;width:100%;" src="<?php echo SITE_URL . $chunks[0]; ?>">
                                    <?php
                                    }
                                ?>

                                </div>
                            <?php } ?>

                            <h6 class="element-header" style="margin-bottom: 30px; margin-top: 50px;">Select answers from below.</h6>
                            <form class="question-forms" method="get" action="#" id="question-form-<?php echo $allQuestion['id'] ?>" >

                                <?php if ( trim(strip_tags($allQuestion['ans1'])) != '' ): ?>
                                <div class="form-check pt-3" style="border: 1px solid #d8cece59; padding: 10px 50px; margin-bottom: 20px;">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="ans<?php echo $allQuestion['id']; ?>" value="1">
                                        <?php echo $allQuestion['ans1'] ?>
                                    </label>
                                </div>
                                <?php endif; ?>

                                <?php if ( trim(strip_tags($allQuestion['ans2'])) != '' ): ?>
                                <div class="form-check pt-3" style="border: 1px solid #d8cece59; padding: 10px 50px; margin-bottom: 20px;">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="ans<?php echo $allQuestion['id']; ?>" value="2">
                                        <?php echo $allQuestion['ans2'] ?>
                                    </label>
                                </div>
                                <?php endif; ?>

                                <?php if ( trim(strip_tags($allQuestion['ans3'])) != '' ): ?>
                                <div class="form-check pt-3" style="border: 1px solid #d8cece59; padding: 10px 50px; margin-bottom: 20px;">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="ans<?php echo $allQuestion['id']; ?>" value="3">
                                        <?php echo $allQuestion['ans3'] ?>
                                    </label>
                                </div>
                                <?php endif; ?>

                                <?php if ( trim(strip_tags($allQuestion['ans4'])) != '' ): ?>
                                <div class="form-check pt-3" style="border: 1px solid #d8cece59; padding: 10px 50px; margin-bottom: 20px;">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="ans<?php echo $allQuestion['id']; ?>" value="4">
                                        <?php echo $allQuestion['ans4'] ?>
                                    </label>
                                </div>
                                <?php endif; ?>

                                <?php if ( trim(strip_tags($allQuestion['ans5'])) != '' ): ?>
                                <div class="form-check pt-3" style="border: 1px solid #d8cece59; padding: 10px 50px; margin-bottom: 20px;">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="ans<?php echo $allQuestion['id']; ?>" value="5">
                                        <?php echo $allQuestion['ans5'] ?>
                                    </label>
                                </div>
                                <?php endif; ?>

                                <!--                                <input type="hidden" value="--><?php //echo $allQuestion['id'];
                                                                                                    ?>
                                <!--" name="questionId">-->

                                <?php if ($i != 1) : ?>
                                    <!-- <button type="button" class="btn btn-primary" onclick="onClickPrev(this);">Previous</button> -->
                                <?php endif; ?>
                                <?php if ($i != $totalQuestions) : ?>
                                    <button type="button" class="btn btn-primary" onclick="onClickNext(this, <?php echo $i ?>);">Next</button>
                                <?php else: ?>
                                <button type="button" class="btn btn-primary finishedeee" onclick="onClickFinish(this);">Finish</button>
                                <?php endif; ?>
                            </form>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var isTeacher = <?php echo $user->isTeacher() ? 'true' : 'false'; ?>;

    function onClickPrev(elm) {
        var $elm = $(elm);
        var $parent = $elm.parent('form').parent('.question-wrapper');
        $parent.removeClass('active').prev().addClass('active');

        <?php if ( $_SESSION['userType'] == 'student' ): ?>
        saveData();
        <?php endif; ?>

        $(window).scrollTop(100);
    }

    function onClickNext(elm, i) {

        var $elm = $(elm);
        var $form = $elm.parent('form');

        $input = $form.find('input[type=radio]:checked');
        if ( $input.length < 1 ) {
            alert("You need to give an answer before moving into next question.");
            return;
        }

        var $parent = $elm.parent('form').parent('.question-wrapper');
        $parent.removeClass('active').next().addClass('active');

        <?php if ( $_SESSION['userType'] == 'student' ): ?>
        saveData(i);
        <?php endif; ?>

        $(window).scrollTop(100);
    }

    function onClickFinish(elm) {
        var $elm = $(elm);
		$elm.prop('disabled',true);

        // var $form = $elm.parent('form');

        // $input = $form.find('input[type=radio]:checked');
        // if ( $input.length < 1 ) {
        //     if ( confirm("Are you sure want to finish this test?") ) {

        //     }

        //     return;
        // }



var ip ='<?php echo $_SERVER['REMOTE_ADDR']?>';
var userBrowser ='<?php echo $_SERVER['HTTP_USER_AGENT']?>';
var $forms = $('form.question-forms');
var array = $forms.serialize();

//+ '&ipaddress' + ip & '&userBrowser' = userBrowser
        $.ajax({
            url: '/ajax/submit-test-form',
            method: 'POST',
            type: 'POST',
            dataType: 'JSON',
            data: array + '&testId=<?php echo $testId ?>' + '&ipaddress=' + ip + "&userBrowser=" + userBrowser  ,
            success: function(data) {
                if (isTeacher) {
                    window.location.href = '/result';
                } else {
                    window.location.href = '/result?result-id=' + data.message;
                }

                <?php if ( $_SESSION['userType'] == 'student' ): ?>

                var key = <?php echo '\'' . $_SESSION['userId'] . '-' . $_SESSION['roll_no'] . '-' . $_SESSION['test_id'] . '\''; ?>;

                localStorage.removeItem(key);
                <?php endif; ?>

            }
        })
        // console.log($forms);
    }

    document.addEventListener("contextmenu", function(e) {
        e.preventDefault();
    }, false);

    // $(window).on('blur', function() {
    //     var a = $('.finishedeee');
    //     $(a.get(0)).trigger('click');
    // });

    $(function() {
        var time = <?php echo $remainingTime; ?>;

        function timeUpdate() {
            time -= 1;
            var sec = Math.round(time % 60);
            var munite = Math.floor(time / 60);

            if (time <= 0) {
                var a = $('.finishedeee');
                $(a.get(0)).trigger('click');
            } else {
                $('.utime').text(munite + ":" + sec + "")
            }

            setTimeout(function() {
                timeUpdate();
            }, 1000);

        }

        setTimeout(function() {
            timeUpdate();
        }, 1000);
    });

    <?php if ( $_SESSION['userType'] == 'student' ): ?>

    var key = <?php echo '\'' . $_SESSION['userId'] . '-' . $_SESSION['roll_no'] . '-' . $_SESSION['test_id'] . '\''; ?>;

    function saveData( lastId ) {
        var $forms = $('form');
        var arr = [];
        $forms.each(function(i, v) {
            var id = v.id;
            var input = $(v).find('input[name=ans' + id.replace('question-form-', '') + ']:checked').val();

            if ( input <= 0 ) return;

            var val = input || 0;
            var obj = {};
            obj[id] = val;
            arr.push(obj);
        });

        localStorage.setItem(key + '_id', lastId);

        localStorage.setItem(key, JSON.stringify(arr));

    };


    function setData() {
        var str = localStorage.getItem(key);
        var arr = str && JSON.parse(str);

        if ( !arr )  return;

        var i = 0;
        for(i; i < arr.length; i++ ) {
            for ( var props in arr[i] ) {
                var form = $('form#' + props);
                var input = form.find('input[value=' + arr[i][props] + ']');
                if ( input.length > 0 )  input[0].checked = true;
            }
        }
    }

    setData();

    var id = localStorage.getItem(key + '_id');

    $('.question-wrapper').removeClass('active');
    $('#question-wrapper-' + (++id)).addClass('active');


    <?php endif; ?>

</script>
<?php
}
?>

<?php
$this->_addFooter();
