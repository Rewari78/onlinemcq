<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 3/11/18
 * Time: 12:03 PM
 */
$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );

$states = StateManager::getInstance()->getAllStates();
$collegeTypes = CollegeManager::getInstance()->getAvlCollegeTypes();

$msg = null;

if ( $this->isPost() )
{
    $stateId = filter_input(INPUT_POST, 'select-state', FILTER_SANITIZE_NUMBER_INT);
    $collegeId = filter_input(INPUT_POST, 'select-college', FILTER_SANITIZE_NUMBER_INT);
    $collegeTypeId = filter_input(INPUT_POST, 'select-college-type', FILTER_SANITIZE_NUMBER_INT);
    $desc = filter_input(INPUT_POST, 'desc');
    $b1L = filter_input(INPUT_POST, 'button-1-name');
    $b1Start = filter_input(INPUT_POST, 'button-1-start', FILTER_SANITIZE_NUMBER_INT);
    $b1End = filter_input(INPUT_POST, 'button-1-end', FILTER_SANITIZE_NUMBER_INT);
    $b1Url = filter_input(INPUT_POST, 'button-1-url');
    $b2L = filter_input(INPUT_POST, 'button-2-name');
    $b2Start = filter_input(INPUT_POST, 'button-2-start', FILTER_SANITIZE_NUMBER_INT);
    $b2End = filter_input(INPUT_POST, 'button-2-end', FILTER_SANITIZE_NUMBER_INT);
    $b2Url = filter_input(INPUT_POST, 'button-2-url');
    $b3L = filter_input(INPUT_POST, 'button-3-name');
    $b3Start =  filter_input(INPUT_POST, 'button-3-start', FILTER_SANITIZE_NUMBER_INT);
    $b3End = filter_input(INPUT_POST, 'button-3-end', FILTER_SANITIZE_NUMBER_INT);
    $b3Url = filter_input(INPUT_POST, 'button-3-url');

    $collegeTypeId = !empty($collegeType) ? $collegeTypeId : 0;
    $b1Start = !empty(strtotime($b1Start)) ? strtotime($b1Start) : 0;
    $b1End = !empty(strtotime($b1End)) ? strtotime($b1End) : 0;
    $b2Start = !empty(strtotime($b2Start)) ? strtotime($b3Start) : 0;
    $b2End = !empty(strtotime($b2End)) ? strtotime($b3End) : 0;
    $b3Start = !empty(strtotime($b3Start)) ? strtotime($b1Start) : 0;
    $b3End = !empty(strtotime($b3End)) ? strtotime($b1End) : 0;


    // First check if the state Id is valid.
    if ( empty($stateId) || ( ( $stateId != 100 ) && empty(StateManager::getInstance()->getStateById($stateId)) ) )
    {
        $msg = "Please select a valid state.";
    }
    
    if ( !empty($collegeTypeId) && empty(CollegeManager::getInstance()->getCollegeTypeById($collegeTypeId)) )
    {
        $msg = "Please select a valid college type.";
    }

    if ( empty($collegeId) || empty(CollegeManager::getInstance()->getCollegeById($collegeId)) )
    {
        $collegeId = 0;
    }

    if ( empty($msg) ) {
        if ( ApplyNow::getInstance()->insertApplyNeet(
            $stateId, $collegeId,  $collegeTypeId, $desc,
            $b1L, $b1Start, $b1End, $b1Url,
            $b2L, $b2Start, $b2End, $b2Url,
            $b3L, $b3Start, $b3End, $b3Url
        ) )
        {
            $this->redirect('manage-apply-neet?new-added=1');
        }
    }
}

$this->_addHeader();
@include $this->getPath() . DS . 'navs.top.php';
?>
    <form method="post" action="#">
        <div class="row">

            <div class="col-sm-12 col-xxxl-12">
                <div class="element-wrapper">
                    <h6 class="element-header">
                        Add Apply Neet
                    </h6>
                    <div class="element-box">

                        <legend>
                            <span> Add Apply Neet</span>
                        </legend>

                        <div class="form-group">
                            <label for="select-states">Select State</label>

                            <select class="form-control" id="select-states" name="select-state">

                                <option value="0">Please Select</option>
                                <option value="100">All India</option>
                                <?php foreach ( $states as $state ): ?>
                                    <option value="<?php echo $state['id']; ?>"><?php echo $state['name']; ?></option>
                                <?php endforeach; ?>

                            </select>
                        </div>

                        <div class="form-group">
                            <label for="select-college-type">College Type</label>
                            <select class="form-control" id="select-college-type" name="select-college-type">
                                <option value="0">
                                    Please Select
                                </option>

                                <?php foreach ( $collegeTypes as $collegeType ): ?>
                                    <option value="<?php echo $collegeType['id']; ?>">
                                        <?php echo $collegeType['collegeType']; ?>
                                    </option>
                                <?php endforeach; ?>

                            </select>
                        </div>

                        <div class="form-group">
                            <label for="select-college">Select College</label>
                            <select class="form-control" id="select-college" name="select-college" disabled>
                                <option value="0">
                                    Please Select
                                </option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="ckeditor1">Description</label>
                            <textarea cols="80" rows="10" class="form-control" name="desc"></textarea>
                        </div>

                        <div class="row">
                            <div class="col-sm-3 col-xxxl-3">
                                <div class="form-group">
                                    <label for="button-1-name"">Button#1 name</label>
                                    <input type='text' class="form-control" id="button-1-name"" name="button-1-name" autocomplete="off"/>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xxxl-3">
                                <div class="form-group">
                                    <label for="button-1-url">Button#1 URL</label>
                                    <input type='text' class="form-control" id="button-1-url" name="button-1-url" autocomplete="off"/>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xxxl-3">
                                <div class="form-group">
                                    <label for="button-1-start">Button#1 Start</label>
                                    <input type='text' class="form-control" id="custom-date-time-pick1"  name="button-1-start" autocomplete="off"/>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xxxl-3">
                                <div class="form-group">
                                    <label for="button-1-end">Button#1 End</label>
                                    <input type='text' class="form-control" id="custom-date-time-pick1"  name="button-1-end" autocomplete="off"/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-3 col-xxxl-3">
                                <div class="form-group">
                                    <label for="button-2-name">Button#2 name</label>
                                    <input type='text' class="form-control" id="button-2-name" name="button-2-name" autocomplete="off"/>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xxxl-3">
                                <div class="form-group">
                                    <label for="button-2-url">Button#2 URL</label>
                                    <input type='text' class="form-control" id="button-2-url" name="button-2-url" autocomplete="off"/>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xxxl-3">
                                <div class="form-group">
                                    <label for="button-2-start">Button#2 Start</label>
                                    <input type='text' class="form-control" id="custom-date-time-pick1"  name="button-2-start" autocomplete="off"/>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xxxl-3">
                                <div class="form-group">
                                    <label for="button-2-end">Button#2 End</label>
                                    <input type='text' class="form-control" id="custom-date-time-pick1"  name="button-2-end" autocomplete="off"/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-3 col-xxxl-3">
                                <div class="form-group">
                                    <label for="button-3-name">Button#3 name</label>
                                    <input type='text' class="form-control" id="button-3-name" name="button-3-name" autocomplete="off"/>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xxxl-3">
                                <div class="form-group">
                                    <label for="button-3-url">Button#3 URL</label>
                                    <input type='text' class="form-control" id="button-3-url" name="button-3-url" autocomplete="off"/>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xxxl-3">
                                <div class="form-group">
                                    <label for="custom-date-time-pick">Button#3 Start</label>
                                    <input type='text' class="form-control" id="custom-date-time-pick" name="button-3-start" autocomplete="off"/>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xxxl-3">
                                <div class="form-group">
                                    <label for="custom-date-time-pick1">Button#3 End</label>
                                    <input type='text' class="form-control" id="custom-date-time-pick1" name="button-3-end" autocomplete="off"/>
                                </div>
                            </div>
                        </div>


                        <div class="alert alert-warning alert-dismissible d-none" role="alert">
                        </div>

                        <div class="form-buttons-w text-right">
                            <button class="btn btn-primary custom-button" data-target="#avl-var-modal" data-toggle="modal" type="button">Available Variable</button>
                            <button class="btn btn-primary" type="submit">Submit Form</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-4 col-xxxl-4">
            </div>

        </div>
    </form>

    <script type="text/javascript">

        var $states = $('#select-states');

        $(document).on('change', '#select-college-type', function (e) {

            var value = $states.val().trim();

            if ( parseInt(value, 10) === 0 ) {
                $('#select-college')[0].disabled = true;
                $('#select-study-type')[0].disabled = true;
                $('#select-course-type')[0].disabled = true;
                $('#select-branch-type')[0].disabled = true;
                return;
            }

            $.post(
                homeUrl + '/ajax/college-counter-announcement',
                { rq: 'getCollege', stateId: value },
                function(data) {

                    var html = '<option value="0">Please Select</option>';
                    if ( data.info !== 'success' )
                    {
                        $('#select-college')[0].disabled = true;
                        return;
                    }

                    var i = 0;
                    for( ; i < data.message.length; i++ )
                    {
                        html += '<option value="' + data.message[i].id + '">' + data.message[i].collegeName + '</option>';
                    }

                    $('#select-college').html(html)[0].disabled = false;

                },
                'json'
            );

        });

        $(document).on('change', '#select-college', function (e) {

            var value = $(this).val().trim();

            if ( parseInt(value, 10) === 0 ) {
                $('#select-study-type')[0].disabled = true;
                $('#select-course-type')[0].disabled = true;
                $('#select-branch-type')[0].disabled = true;
                return;
            }

            $.post(
                homeUrl + '/ajax/college-counter-announcement',
                { rq: 'getStudyType', colTypeId: value },
                function(data) {

                    var html = '<option value="0">Please Select</option>';
                    if ( data.info !== 'success' )
                    {
                        $('#select-study-type').html(html)[0].disabled = true;
                        return;
                    }

                    var i = 0;
                    for( ; i < data.message.length; i++ )
                    {
                        html += '<option value="' + data.message[i].id + '">' + data.message[i].studyType + '</option>';
                    }

                    $('#select-study-type').html(html)[0].disabled = false;

                },
                'json'
            );

        });

        $(document).on('change', '#select-study-type', function (e) {

            var value = $(this).val().trim();

            if ( parseInt(value, 10) === 0 ) {
                $('#select-course-type')[0].disabled = true;
                $('#select-branch-type')[0].disabled = true;
                return;
            }

            $.post(
                homeUrl + '/ajax/college-counter-announcement',
                { rq: 'getCourseType', studyTypeId: value },
                function(data) {

                    var html = '<option value="0">Please Select</option>';
                    if ( data.info !== 'success' )
                    {
                        $('#select-course-type').html(html)[0].disabled = true;
                        return;
                    }

                    var i = 0;
                    for( ; i < data.message.length; i++ )
                    {
                        html += '<option value="' + data.message[i].id + '">' + data.message[i].courseType + '</option>';
                    }

                    $('#select-course-type').html(html)[0].disabled = false;

                },
                'json'
            );

        });

        $(document).on('change', '#select-course-type', function (e) {

            var value = $(this).val().trim();

            if ( parseInt(value, 10) === 0 ) {
                $('#select-branch-type')[0].disabled = true;
                return;
            }

            $.post(
                homeUrl + '/ajax/college-counter-announcement',
                { rq: 'getBranchType', courseTypeId: value },
                function(data) {

                    var html = '<option value="0">Please Select</option>';
                    if ( data.info !== 'success' )
                    {
                        $('#select-branch-type').html(html)[0].disabled = true;
                        return;
                    }

                    var i = 0;
                    for( ; i < data.message.length; i++ )
                    {
                        html += '<option value="' + data.message[i].id + '">' + data.message[i].branchType + '</option>';
                    }

                    $('#select-branch-type').html(html)[0].disabled = false;

                },
                'json'
            );

        });

        $(document).on('change', '#select-branch-type', function (e) {

            var value = $(this).val().trim();

        });

        function submitForm( form )
        {
            var $form = $(form);
            var $alert = $form.find('.alert');
            var _data = $(form).serialize();

            hideError($alert);

            $.post(
                homeUrl + '/ajax/submit-college-counter-announcement',
                $form.serialize(),
                function (data) {
                    if ( data.info !== 'success' )
                    {
                        showError($alert, data.message);
                        return;
                    }

                    window.location.href = '/college-counter-announcement';

                },
                'json'
            );
        }

        function showError( box, msg )
        {

            var $alertBox = $(box);
            $alertBox.html(msg);
            $alertBox.removeClass('d-none');
        }

        function hideError( box )
        {
            var $alertBox = $(box);
            $alertBox.addClass('d-none');
        }

        $(function () {
            $('#custom-date-time-pick, #custom-date-time-pick1').datetimepicker({});
        });
    </script>
<?php
@include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();
