<?php

/**
 * Created by PhpStorm.
 * User: akash
 * Date: 4/11/18
 * Time: 3:25 PM
 */
$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );

if ( isset($_GET['activate_id']) )
{
    $annIdG = filter_input(INPUT_GET, 'activate_id', FILTER_SANITIZE_NUMBER_INT);
    if ( !empty($annIdG) ) {
        Announcements::getInstance()->activateOrDeactivateAIOCounter($annIdG, true);
        $msg = "Announcement is activated successfully.";
    }
} else if ( isset($_GET['deactivate_id']) )
{
    $annIdG = filter_input(INPUT_GET, 'deactivate_id', FILTER_SANITIZE_NUMBER_INT);
    if ( !empty($annIdG) ) {
        Announcements::getInstance()->activateOrDeactivateAIOCounter($annIdG, false);
        $msg = "Announcement is de-activated successfully.";
    }
} else if ( isset($_GET['delete_id']) )
{
    $annIdG = filter_input(INPUT_GET, 'delete_id', FILTER_SANITIZE_NUMBER_INT);
    if ( !empty($annIdG) ) {
        Announcements::getInstance()->deleteAIOCounterAnnouncement($annIdG);
        $msg = "Announcement is deleted successfully.";
    }
}

if ( !empty($_SESSION['aio_counter_announcement_msg']) )
{
    $msg = $_SESSION['aio_counter_announcement_msg'];
    unset($_SESSION['aio_counter_announcement_msg']);
}

$announcements = Announcements::getInstance()->getAllAIOAnnouncements();

$idLists = array();
foreach ( $announcements as $announcement )
{
    $idLists['stateId'][$announcement['stateId']] = $announcement['stateId'];
    $idLists['collegeTypeId'][$announcement['collegeTypeId']] = $announcement['collegeTypeId'];
    $idLists['studyTypeId'][$announcement['studyTypeId']] = $announcement['studyTypeId'];
    $idLists['courseId'][$announcement['courseId']] = $announcement['courseId'];
    $idLists['branchId'][$announcement['branchId']] = $announcement['branchId'];
}

$stateList = array();
$collegeTypeList = array();
$studyTypeList = array();
$courseTypeList = array();
$branchTypeList = array();

if ( !empty($idLists) )
{
    $stateList          = StateManager::getInstance()->getStateByIdList($idLists['stateId']);
    $collegeTypeList    = CollegeManager::getInstance()->getCollegeTypeIdList($idLists['collegeTypeId']);
    $studyTypeList      = CollegeManager::getInstance()->getStudyTypeByIdList($idLists['studyTypeId']);
    $courseTypeList     = CollegeManager::getInstance()->getCourseTypeByIdList($idLists['courseId']);
    $branchTypeList     = CollegeManager::getInstance()->getBranchTypeByIdList($idLists['branchId']);

}

$this->_addHeader();
@include $this->getPath() . DS . 'navs.top.php';
?>
    <form>
        <div class="row">
            <div class="col-sm-12 col-xxxl-12">
                <div class="element-wrapper">

                    <?php if ( !empty($msg) ) : ?>
                        <div class="alert alert-success alert-dismissible " role="alert" area-label="Close" class="close">
                            <?php echo $msg; ?>
                            <button aria-label="close" class="close" data-dismiss="alert" type="button"><span aria-hidden="true">&times;</span> </button>
                        </div>
                    <?php endif; ?>

                    <div class="element-actions">
                        <a class="btn btn-primary custom-button" href="/add-aio-counter-announcement">Add All India Quota Counter Announcement</a>
                    </div>
                    <h6 class="element-header">
                        All India Quota Counter Announcement
                    </h6>
                    <?php if ( !empty($announcements) ): ?>
                        <div class="element-box">
                            <div class="table-responsive">
                                <table class="table table-lightborder">
                                    <thead>
                                    <tr>
                                        <th>
                                            A. Title
                                        </th>
                                        <th class="text-left">
                                            Start
                                        </th>
                                        <th>
                                            End
                                        </th>
                                        <th>
                                            State
                                        </th>
                                        <th>
                                            College
                                        </th>
                                        <th>
                                            Study Type
                                        </th>
                                        <th>
                                            Course
                                        </th>
                                        <th>
                                            Branch
                                        </th>
                                        <th>
                                            Action
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ( $announcements as $announcement ): ?>
                                            <tr>
                                                <td class="nowrap">
                                                    <?php echo Util::htmlEncode($announcement['title']); ?>
                                                </td>
                                                <td class="text-left">
                                                    <?php echo date('d-M-Y H:i', $announcement['start']); ?>
                                                </td>
                                                <td class="text-left">
                                                    <?php echo date('d-M-Y H:i', $announcement['end']); ?>
                                                </td>
                                                <td class="text-left">
                                                    <?php echo isset($stateList[$announcement['stateId']]) ? $stateList[$announcement['stateId']]['shortName'] : 'Unknown'; ?>
                                                </td>
                                                <td class="text-left">
                                                    <?php echo isset($collegeTypeList[$announcement['collegeTypeId']]) ? $collegeTypeList[$announcement['collegeTypeId']]['collegeType'] : 'Unknown'; ?>
                                                </td>
                                                <td class="text-left">
                                                    <?php echo isset($studyTypeList[$announcement['studyTypeId']]) ? $studyTypeList[$announcement['studyTypeId']]['studyType'] : 'Unknown'; ?>
                                                </td>
                                                <td class="text-left">
                                                    <?php echo isset($courseTypeList[$announcement['courseId']]) ? $courseTypeList[$announcement['courseId']]['courseType'] : 'Unknown'; ?>
                                                </td>
                                                <td class="text-left">
                                                    <?php echo isset($branchTypeList[$announcement['branchId']]) ? $branchTypeList[$announcement['branchId']]['branchType'] : 'Unknown'; ?>
                                                </td>
                                                <td class="row-actions text-left">
                                                    <?php if ( $announcement['isActive'] == 1 ): ?>
                                                        <a href="/aio-counter-announcement?deactivate_id=<?php echo $announcement['id']; ?>"><i class="dripicons-cross"></i></a>
                                                    <?php else: ?>
                                                        <a href="/aio-counter-announcement?activate_id=<?php echo $announcement['id']; ?>"><i class="dripicons-checkmark"></i></a>
                                                    <?php endif; ?>
                                                    <a href="/aio-counter-announcement?delete_id=<?php echo $announcement['id']; ?>"><i class="os-icon os-icon-ui-15"></i></a>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </form>
<?php
@include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();
