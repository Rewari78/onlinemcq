<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 11/11/18
 * Time: 12:53 PM
 */
$this->onlyLoggedInAllowed(UserManager::USER_TYPE_SUBSCRIBER, PackageManager::FEATURE_COLLEGE_PREDICTOR);

$colleges = array();
$recognitionIds = array();
$colStateIds = array();
$colCityIds = array();
$universityIds = array();
$collegeTypeIds = array();

$stateId = 0;
$catId = 0;
$score = 0;
if ( !empty($_GET) )
{
    $stateId = filter_input(INPUT_GET, 'select-state', FILTER_SANITIZE_NUMBER_INT);
    $catId = filter_input(INPUT_GET, 'select-category', FILTER_SANITIZE_NUMBER_INT);
    $score = filter_input(INPUT_GET, 'score', FILTER_SANITIZE_NUMBER_INT);

    $score = empty($score) ? 0 : $score;

    $msg = "";
    if ( empty($stateId) || empty($catId) || empty($score) )
    {
        $msg = "Please fill all the fields properly.";
    } else if ( empty(StateManager::getInstance()->getStateByIdList($stateId)) )
    {
        $msg = "Oops! selected state is not valid.";
    } else if ( !_isValidCatId($catId) )
    {
        $msg = "Please select a valid category.";
    } else {
        // Submit for filter.
        $colleges = CollegeManager::getInstance()->predictCollege($stateId, $catId, $score);

        foreach ( $colleges as $college )
        {
            $recognitionIds[$college['recognitionId']]  = $college['recognitionId'];
            $colStateIds[$college['stateId']]           = $college['stateId'];
            $colCityIds[$college['cityId']]             = $college['cityId'];
            $universityIds[$college['affUniversityId']] = $college['affUniversityId'];
            $collegeTypeIds[$college['collegeTypeId']]  = $college['collegeTypeId'];
        }

        $recogListWithId            = CollegeManager::getInstance()->getRecognitionsByIdList($recognitionIds);
        $colStateListWithId         = StateManager::getInstance()->getStateByIdList($colStateIds);
        $colCityListWithId          = StateManager::getInstance()->getCityByIdList($colCityIds);
        $universitiesWithId         = CollegeManager::getInstance()->getUniversitiesByIdList($universityIds);
        $collegeTypeWithId          = CollegeManager::getInstance()->getCollegeTypeIdList($collegeTypeIds);

    }
}

function _isValidCatId( $catId )
{
    $result = false;
    switch ( $catId )
    {
        case CollegeManager::CUTOFF_OBC:
        case CollegeManager::CUTOFF_OBC_PH:
        case CollegeManager::CUTOFF_GEN:
        case CollegeManager::CUTOFF_GEN_PH:
        case CollegeManager::CUTOFF_SC:
        case CollegeManager::CUTOFF_SC_PH:
        case CollegeManager::CUTOFF_ST:
        case CollegeManager::CUTOFF_ST_PH:
            $result = true;
            break;
    }

    return $result;
}

$states = StateManager::getInstance()->getAllStates();

$this->_addHeader();
@include $this->getPath() . DS . 'navs.top.php';
?>
    <a href="#" class="go-back-link"><i class="fa fa-arrow-left"></i></a>
    <div class="row">
        <div class="col-sm-12 col-xxxl-12">
            <div class="element-wrapper">
                <h6 class="custm-ele-hdr">
                    College Predictor
                </h6>
                <div class="custm-ele-hdr-strk">
                </div>
                <div class="element-content">
                    <p>
                        Morbi mollis tellus ac sapien.
                        Pellentesque auctor neque nec urna. Curabitur a felis in nunc fringilla tristique.
                        Praesent vestibulum dapibus nibh. Nulla sit amet est.
                        Morbi mollis tellus ac sapien.
                        Pellentesque auctor neque nec urna.
                        Curabitur a felis in nunc fringilla tristique.
                        Praesent vestibulum dapibus nibh.
                        Nulla sit amet est.
                    </p>
                </div>
            </div>
        </div>
    </div>

    <form>
        <div class="row pt-3">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="select-state">Select State</label>
                        <select class="form-control ext-cntr" name="select-state" id="select-state">

                            <?php foreach ( $states as $state ): ?>
                                <option value="<?php echo $state['id'] ?>" <?php if ( $stateId == $state['id'] ) echo 'selected' ?>>
                                    <?php echo $state['name']; ?>
                                </option>
                            <?php endforeach; ?>

                        </select>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="select-category">Select Category</label>
                        <select class="form-control ext-cntr" id="select-category" name="select-category">
                            <option value="<?php echo CollegeManager::CUTOFF_GEN; ?>" <?php if ( $catId == CollegeManager::CUTOFF_GEN ) echo 'selected' ?>>
                                General
                            </option>
                            <option value="<?php echo CollegeManager::CUTOFF_GEN_PH; ?>" <?php if ( $catId == CollegeManager::CUTOFF_GEN_PH ) echo 'selected' ?>>
                                General PH
                            </option>
                            <option value="<?php echo CollegeManager::CUTOFF_OBC; ?>" <?php if ( $catId == CollegeManager::CUTOFF_OBC ) echo 'selected' ?>>
                                OBC
                            </option>
                            <option value="<?php echo CollegeManager::CUTOFF_OBC_PH; ?>" <?php if ( $catId == CollegeManager::CUTOFF_OBC_PH ) echo 'selected' ?>>
                                OBC PH
                            </option>
                            <option value="<?php echo CollegeManager::CUTOFF_SC; ?>" <?php if ( $catId == CollegeManager::CUTOFF_SC ) echo 'selected' ?>>
                                SC
                            </option>
                            <option value="<?php echo CollegeManager::CUTOFF_SC_PH; ?>" <?php if ( $catId == CollegeManager::CUTOFF_SC_PH ) echo 'selected' ?>>
                                SC PH
                            </option>
                            <option value="<?php echo CollegeManager::CUTOFF_ST; ?>" <?php if ( $catId == CollegeManager::CUTOFF_ST ) echo 'selected' ?>>
                                ST
                            </option>
                            <option value="<?php echo CollegeManager::CUTOFF_ST_PH; ?>" <?php if ( $catId == CollegeManager::CUTOFF_ST_PH ) echo 'selected' ?>>
                                ST PH
                            </option>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="score">Your NEET Score</label>
                        <input class="form-control ext-cntr" placeholder="Expected Score" type="number" min="0" id="score" name="score" value="<?php echo $score; ?>">
                    </div>
                    <div class="form-buttons text-right">
                        <button class="btn btn-primary ex-sqn" type="submit">Predict Now</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
    </form>



<?php foreach ( $colleges as $college ): ?>
    <div class="row r-eo-bg">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-8">
                    <h4><?php echo $college['collegeName']; ?></h4>
                    <p class="typ">
                        Type: <span class="typ-span"><?php echo isset($collegeTypeWithId[$college['collegeTypeId']]) ? $collegeTypeWithId[$college['collegeTypeId']]['collegeType'] : 'Unknown';  ?></span>
                        Affiliation: <span class="typ-span"><?php echo isset($universitiesWithId[$college['affUniversityId']]) ? $universitiesWithId[$college['affUniversityId']]['name'] : 'Unknown';  ?></span>

                    </p>
                    <p class="typ">Established: <span class="typ-span"><?php echo $college['established']; ?></span>
                        Location: <span class="typ-span"><?php echo isset($colCityListWithId[$college['cityId']]) ? $colCityListWithId[$college['cityId']]['cityName'] : 'Unknown'; ?>, <?php echo isset($colStateListWithId[$college['stateId']]) ? $colStateListWithId[$college['stateId']]['name'] : 'Unknown'; ?></span></p>
                </div>
                <div class="col-sm-4">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="blank-d-ex">
                            </div>
                            <a class="visit-website-link-ex" data-target="#fees-structure" data-toggle="modal">Fees Structure ></a>
                        </div>
                        <div class="col-sm-6">
                            <div class="blank-d-ex">
                            </div>
                            <a class="visit-website-link-ex" href="#">Notifications ></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="blank-d-ex">
                            </div>
                            <a class="visit-website-link-ex" data-target="#seat-matrix" data-toggle="modal">Seat Matrix ></a>
                        </div>
                        <div class="col-sm-6">
                            <div class="blank-d-ex">
                            </div>
                            <?php if ( substr($college['websiteUrl'], 0, 7) !== 'http://' ): ?>
                                <?php if ( !empty($college['websiteUrl']) ): ?>
                                    <a class="visit-website-link-ex" href="http://<?php echo $college['websiteUrl']; ?>">Visit Website ></a>
                                <?php endif; ?>
                            <?php else :?>
                                <a class="visit-website-link-ex" href="<?php echo $college['websiteUrl']; ?>">Visit Website ></a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endforeach; ?>



<?php
@include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();