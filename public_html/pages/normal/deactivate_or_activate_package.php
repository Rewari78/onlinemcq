<?php
$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );

$packageId = $this->_data[1];
$packageId = filter_var(filter_var($packageId, FILTER_SANITIZE_NUMBER_INT), FILTER_VALIDATE_INT);

if ( empty($packageId) ) $this->redirect('manage-packages');

$packageDetails = PackageManager::getInstance()->getAPackageById($packageId);

if ( empty($packageDetails) ) $this->redirect('manage-packages');


$doDeactivate = isset($this->_data[0]) && $this->_data[0] === 'deactivate';

if ( $doDeactivate ) {
    PackageManager::getInstance()->disableAPackage($packageId);
} else {
    PackageManager::getInstance()->enableAPackage($packageId);
}

$this->redirect('manage-packages');
