<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 7/5/19
 * Time: 1:43 PM
 */


$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );
$this->_addHeader();

?>
    <body class="menu-position-side menu-side-left full-screen with-content-panel">

    </body>
<script>
    function getUrlParam( paramName ) {
        var reParam = new RegExp( '(?:[\?&]|&)' + paramName + '=([^&]+)', 'i' );
        var match = window.location.search.match( reParam );

        return ( match && match.length > 1 ) ? match[1] : null;
    }

    function returnFileUrl() {

        var funcNum = getUrlParam( 'CKEditorFuncNum' );
        var fileUrl = '/path/to/file.txt';
        window.opener.CKEDITOR.tools.callFunction( funcNum, fileUrl );
        window.close();
    }
</script>
<?php
$this->_addFooter();


