<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 3/11/18
 * Time: 12:03 PM
 */
$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );

$msg = null;

$newAdded = filter_input(INPUT_GET, 'new-added', FILTER_SANITIZE_NUMBER_INT);
if ( $newAdded == 1 )
{
    $msg = "Apply Aiims is now  updated";
}



if ( $this->isPost() )
{
    $desc = filter_input(INPUT_POST, 'desc');
    $b1L = filter_input(INPUT_POST, 'button-1-name');
    $b1Start = filter_input(INPUT_POST, 'button-1-start', FILTER_SANITIZE_NUMBER_INT);
    $b1End = filter_input(INPUT_POST, 'button-1-end', FILTER_SANITIZE_NUMBER_INT);
    $b1Url = filter_input(INPUT_POST, 'button-1-url');
    $b2L = filter_input(INPUT_POST, 'button-2-name');
    $b2Start = filter_input(INPUT_POST, 'button-2-start', FILTER_SANITIZE_NUMBER_INT);
    $b2End = filter_input(INPUT_POST, 'button-2-end', FILTER_SANITIZE_NUMBER_INT);
    $b2Url = filter_input(INPUT_POST, 'button-2-url');
    $b3L = filter_input(INPUT_POST, 'button-3-name');
    $b3Start = filter_input(INPUT_POST, 'button-3-start', FILTER_SANITIZE_NUMBER_INT);
    $b3End = filter_input(INPUT_POST, 'button-3-end', FILTER_SANITIZE_NUMBER_INT);
    $b3Url = filter_input(INPUT_POST, 'button-3-url');

    $b1Start = !empty(strtotime($b1Start)) ? strtotime($b1Start) : 0;
    $b1End = !empty(strtotime($b1End)) ? strtotime($b1End) : 0;
    $b2Start = !empty(strtotime($b2Start)) ? strtotime($b3Start) : 0;
    $b2End = !empty(strtotime($b2End)) ? strtotime($b3End) : 0;
    $b3Start = !empty(strtotime($b3Start)) ? strtotime($b1Start) : 0;
    $b3End = !empty(strtotime($b3End)) ? strtotime($b1End) : 0;

    if ( empty($msg) ) {
        if ( ApplyNow::getInstance()->insertApplyAIIMS( $desc,
            $b1L, $b1Start, $b1End, $b1Url,
            $b2L, $b2Start, $b2End, $b2Url,
            $b3L, $b3Start, $b3End, $b3Url
        ) )
        {
            $this->redirect('manage-apply-aiims?new-added=1');
        }
    }
}

$this->_addHeader();
@include $this->getPath() . DS . 'navs.top.php';
?>

<?php if ( !empty($msg) ) : ?>
    <div class="alert alert-success alert-dismissible " role="alert" area-label="Close" class="close">
        <?php echo $msg; ?>
        <button aria-label="close" class="close" data-dismiss="alert" type="button"><span aria-hidden="true">&times;</span> </button>
    </div>
<?php endif; ?>
    <form method="post" action="#">
        <div class="row">

            <div class="col-sm-12 col-xxxl-12">
                <div class="element-wrapper">
                    <h6 class="element-header">
                        Add Apply AIIMS
                    </h6>
                    <div class="element-box">

                        <legend>
                            <span> Add Apply AIIMS</span>
                        </legend>

                        <div class="form-group">
                            <label for="ckeditor1">Description</label>
                            <textarea cols="80" rows="10" class="form-control" name="desc"></textarea>
                        </div>

                        <div class="row">
                            <div class="col-sm-3 col-xxxl-3">
                                <div class="form-group">
                                    <label for="button-1-name"">Button#1 name</label>
                                    <input type='text' class="form-control" id="button-1-name"" name="button-1-name" autocomplete="off"/>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xxxl-3">
                                <div class="form-group">
                                    <label for="button-1-url">Button#1 URL</label>
                                    <input type='text' class="form-control" id="button-1-url" name="button-1-url" autocomplete="off"/>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xxxl-3">
                                <div class="form-group">
                                    <label for="button-1-start">Button#1 Start</label>
                                    <input type='text' class="form-control" id="custom-date-time-pick1"  name="button-1-start" autocomplete="off"/>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xxxl-3">
                                <div class="form-group">
                                    <label for="button-1-end">Button#1 End</label>
                                    <input type='text' class="form-control" id="custom-date-time-pick1"  name="button-1-end" autocomplete="off"/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-3 col-xxxl-3">
                                <div class="form-group">
                                    <label for="button-2-name">Button#2 name</label>
                                    <input type='text' class="form-control" id="button-2-name" name="button-2-name" autocomplete="off"/>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xxxl-3">
                                <div class="form-group">
                                    <label for="button-2-url">Button#2 URL</label>
                                    <input type='text' class="form-control" id="button-2-url" name="button-2-url" autocomplete="off"/>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xxxl-3">
                                <div class="form-group">
                                    <label for="button-2-start">Button#2 Start</label>
                                    <input type='text' class="form-control" id="custom-date-time-pick1"  name="button-2-start" autocomplete="off"/>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xxxl-3">
                                <div class="form-group">
                                    <label for="button-2-end">Button#2 End</label>
                                    <input type='text' class="form-control" id="custom-date-time-pick1"  name="button-2-end" autocomplete="off"/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-3 col-xxxl-3">
                                <div class="form-group">
                                    <label for="button-3-name">Button#3 name</label>
                                    <input type='text' class="form-control" id="button-3-name" name="button-3-name" autocomplete="off"/>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xxxl-3">
                                <div class="form-group">
                                    <label for="button-3-url">Button#3 URL</label>
                                    <input type='text' class="form-control" id="button-3-url" name="button-3-url" autocomplete="off"/>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xxxl-3">
                                <div class="form-group">
                                    <label for="custom-date-time-pick">Button#3 Start</label>
                                    <input type='text' class="form-control" id="custom-date-time-pick" name="button-3-start" autocomplete="off"/>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xxxl-3">
                                <div class="form-group">
                                    <label for="custom-date-time-pick1">Button#3 End</label>
                                    <input type='text' class="form-control" id="custom-date-time-pick1" name="button-3-end" autocomplete="off"/>
                                </div>
                            </div>
                        </div>


                        <div class="alert alert-warning alert-dismissible d-none" role="alert">
                        </div>

                        <div class="form-buttons-w text-right">
                            <button class="btn btn-primary custom-button" data-target="#avl-var-modal" data-toggle="modal" type="button">Available Variable</button>
                            <button class="btn btn-primary" type="submit">Submit Form</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-4 col-xxxl-4">
            </div>

        </div>
    </form>

    <script type="text/javascript">

        $(function () {
            $('#custom-date-time-pick, #custom-date-time-pick1').datetimepicker({});
        });
    </script>
<?php
@include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();
