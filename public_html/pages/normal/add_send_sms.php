<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 3/26/19
 * Time: 4:39 PM
 */
$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );


if ( $this->isPost() )
{
    $packageId = filter_input(INPUT_POST, 'select-package', FILTER_SANITIZE_NUMBER_INT);
    $senderId = filter_input(INPUT_POST, 'select-sender-id');
    $message = filter_input(INPUT_POST, 'message');
    $schedule = filter_input(INPUT_POST, 'schedule');
    $selectDate = filter_input(INPUT_POST, 'date-time');
    $t = strtotime($selectDate);
    $selectDate = empty($t) ? time() : $t;

    EmailSmsManager::getInstance()
        ->insertQueue(EmailSmsManager::QUEUE_TYPE_SMS, $selectDate, array(
            'senderId' => $senderId,
            'msg' => $message,
        ), array(
            'type' => 'package',
            'value' => $packageId
        ));


    $msg = "New Sms has been scheduled.";

}


$packages = PackageManager::getInstance()->getAvlPackageLists();

$this->_addHeader();
@include $this->getPath() . DS . 'navs.top.php';
?>
    <div class="row">
        <div class="col-sm-12">
            <div class="element-wrapper">
                <div class="element-box-tp">

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="element-wrapper">
                                <h6 class="element-header">
                                    Admin Send SMS
                                </h6>
                                <div class="element-box">
                                    <form id="formValidate" method="POST" action="#">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="select-state">Select Package</label>
                                                    <select class="form-control" required="required" id="select-package" name="select-package">

                                                        <option>Please Select</option>
                                                        <?php foreach( $packages as $package ): ?>
                                                            <option value="<?php echo $package['packageInfo']['id']; ?>">
                                                                <?php echo $package['packageInfo']['packageName']; ?>
                                                            </option>
                                                        <?php endforeach; ?>

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="select-state">Select Sender ID</label>
                                                <select class="form-control" required="required" id="select-sender-id" name="select-sender-id">
                                                    <option>Please Select</option>
													<option value="NHIOTP">NHIOTP</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="message">Message</label>
                                            <textarea class="form-control" rows="4" id="message" name="message"></textarea>
                                            <p class="mt-0 pt-2 mb-0 text-right" id="word-limit">0 / 160</p>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-check pt-3">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" style="margin-top: 1.5px;" value="1" type="checkbox" id="schedule" name="schedule">Schedule</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="dashboard-notifi-title">Select Date and Time</label>
                                                    <input type='text' class="form-control" id="custom-date-time-pick1" name="date-time" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-buttons-w">
                                            <button class="btn btn-primary" type="submit"> Send</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
    $('#message').on('keyup', function ( e ) {

        if ( $(this).val().trim().length <= 160 ) {
            $('#word-limit').text($(this).val().trim().length + ' / 160');
            return;
        }

        e.preventDefault();
        e.stopPropagation();
    });
    $(function () {
        $('#custom-date-time-pick, #custom-date-time-pick1').datetimepicker({});
    });
</script>
<?php
@include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();
