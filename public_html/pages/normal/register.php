<?php
if ( UserManager::getInstance()->isLoggedIn() ) $this->redirect('home');

$packages = PackageManager::getInstance()->getAvlPackageLists();
$availableIds = array();

foreach ( $packages as $package ) {
    $availableIds[$package['packageInfo']['id']] = $package['packageInfo']['id'];
}

$packageId = filter_input(INPUT_GET, 'packageId');
$packageId = !in_array($packageId, $availableIds) ? UserPackageController::DEFAULT_PACKAGE_ID : $packageId;
$package = PackageManager::getInstance()->getAPackageById($packageId);

$tranFailed = filter_input(INPUT_GET, 'tranFail');
if ( $tranFailed == 1 ) {
    $msg = "Sorry transaction is failed, Please retry.";
}

// check if the transaction is valid.
$tranId     = filter_input(INPUT_GET, 'tranId');
$hash       = filter_input(INPUT_GET, 'hash');
$tranHash   = filter_input(INPUT_GET, 'tranHash');

if ( !empty( $trans = TranManager::getInstance()->getAnTranById($tranId) ) )
{
    if ( $tranHash == $trans['hash'] )
    {
        TranManager::getInstance()->markATranAsSuccess($tranHash);

        // ELse otp is valid.
        UserDatabase::getInstance()
            ->createUser($trans['object']['firstName'],
                $trans['object']['lastName'], $trans['object']['email'],
                $trans['object']['mobile'], Util::passwordEncrypt($trans['object']['password1']),
                UserManager::USER_TYPE_SUBSCRIBER);

        // else login
        // get userInfo by email
        $userInfo = UserDatabase::getInstance()->getUserInfoByEmail($trans['object']['email']);

        UserPackageController::getInstance()
            ->givePackageToAUser(
                $userInfo['id'], $trans['object']['packageId']);

        UserManager::getInstance()->loginUser($userInfo['id'], $userInfo['userType']);

        $this->redirect('home');

    }else {
        $msg = "Transaction is failed.";
    }
}
$this->_addHeader();
?>
<body style="padding: 0;">
<div class="container-fluid cus-log-bg">
    <div class="row add-class">
        <div class="col-sm-3 col-xxxl-3">
        </div>
        <div class="col-sm-6 col-xxxl-6 lg-dv">
            <h6 class="custm-ele-hdr lg-pg">
                Register Now for
                <?php echo $package['packageInfo']['packageName']; ?>
                <?php if ( $package['packageInfo']['price'] > 0 ):  ?>
                - Rs. <?php echo $package['packageInfo']['price']; ?>
                <?php endif; ?>
                <?php if ( $package['packageInfo']['offPrice'] > 0 ):  ?>
                    / <?php echo ceil($package['packageInfo']['offPrice'] / $package['packageInfo']['price'] * 100) ?>% Off
                <?php endif; ?>
            </h6>
            <div class="custm-ele-hdr-strk lg-pg-strk">
            </div>
            <?php if ( !empty($msg) ): ?>
                <div class="alert alert-warning" id="alert-box">
                    <?php echo $msg; ?>
                </div>
            <?php else: ?>
                <div class="alert alert-warning d-none" id="alert-box">
                </div>
            <?php endif; ?>
            <div class="cus-log-sec">
                <div class="cus-log-inp">
                    <form>


                        <div class="row">
                            <div class="col-sm-6">
                                <label for="first-name">First Name</label>
                                <input type="text" name="first-name" placeholder="First Name" id="first-name">
                            </div>
                            <div class="col-sm-6">
                                <label for="last-name">Last Name</label>
                                <input type="text" name="last-name" placeholder="Last Name" id="last-name">
                            </div>
                        </div>
                        <label for="email">Email</label>
                        <input type="email" name="email" placeholder="Enter Email" id="email">
                        <div class="row">
                            <div class="col-sm-6">
                                <label for="password">Password</label>
                                <input type="password" name="password" placeholder="Enter Password" id="password">
                            </div>
                            <div class="col-sm-6">
                                <label for="confirm_password">Confirm Password</label>
                                <input type="password" name="confirm_password" placeholder="Enter Confirm Password" id="confirm_password">
                            </div>
                        </div>
                        <label for="mobile">Mobile</label>
                        <input type="tel" name="mobile" placeholder="Enter 10 digit mobile number" id="mobile">

                        <div class="row">
                            <div class="col-sm-6">
                                <p class="chkbx-txt">By registering to our website you agree to our term and conditions.</p>
                            </div>
                            <div class="col-sm-6">
                                <button type="button" class="cus-login-bt" id="register-btn">Register <span>&#xE72A;</span></button>
                            </div>
                        </div>

                        <div class="d-none" id="register-otp-top" style="display: none !important;">
                            <div class="row ">
                                <div class="col-sm-6"></div>
                                <div class="col-sm-6">
                                    <label for="otp">Enter OTP <span style="float: right;cursor: pointer" class="resend-otp"></span></label>
                                    <input type="otp" name="otp" placeholder="Enter 6 digit recive OTP" id="otp">
                                </div>
                            </div>
                            <button type="button" class="cus-login-bt" id="otp-btn">Submit & Register <span>&#xE72A;</span></button>
                        </div>

                        <p class="text-center" style="color: #fff;">
                            Already have an account? <a href="/">Login Now</a>
                        </p>

                    </form>
                </div>
            </div>
            <div class="col-sm-4 col-xxxl-4">
            </div>
        </div>
        <div class="col-sm-3 col-xxxl-3">
        </div>
    </div>
</div>



<script src="<?php echo SITE_URL ?>/statics/bower_components/popper.js/dist/umd/popper.min.js"></script>
<script src="<?php echo SITE_URL ?>/statics/js/jquery.datetimepicker.full.min.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/moment/moment.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/chart.js/dist/Chart.min.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/select2/dist/js/select2.full.min.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/jquery-bar-rating/dist/jquery.barrating.min.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/ckeditor/ckeditor.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap-validator/dist/validator.min.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/ion.rangeSlider/js/ion.rangeSlider.min.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/dropzone/dist/dropzone.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/editable-table/mindmup-editabletable.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/tether/dist/js/tether.min.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/slick-carousel/slick/slick.min.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap/js/dist/util.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap/js/dist/alert.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap/js/dist/button.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap/js/dist/carousel.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap/js/dist/collapse.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap/js/dist/dropdown.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap/js/dist/modal.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap/js/dist/tab.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap/js/dist/tooltip.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap/js/dist/popover.js"></script>
<script src="<?php echo SITE_URL ?>/statics/js/demo_customizer.js?version=4.4.0"></script>
<script src="<?php echo SITE_URL ?>/statics/js/main.js?version=4.4.0"></script>
<script src="<?php echo SITE_URL ?>/statics/js/jquery-ui.min.js"></script>
<script id="bolt" src="https://sboxcheckout-static.citruspay.com/bolt/run/bolt.min.js" bolt-color="222f3f" bolt-logo="http://ihs.co/statics/images/Neetguidance-logo.jpg"></script>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-XXXXXXX-9', 'auto');
    ga('send', 'pageview');
</script>

<script type="text/javascript">
    var genOtpBtn = document.getElementById('register-btn');
    var data = {
        seq: null,
        isValidated: false
    };
    var packageId = <?php echo $packageId; ?>;
    genOtpBtn.addEventListener('click', function(e){

        // First validate the info.
        var firstName = document.getElementById('first-name');
        var lastName = document.getElementById('last-name');
        var email = document.getElementById('email');
        var password1 = document.getElementById('password');
        var password2 = document.getElementById('confirm_password');
        var mobile = document.getElementById('mobile');

        var btn = this;

        var fnV = firstName.value.trim(),
            lnV = lastName.value.trim(),
            emailV = email.value.trim(),
            password1V = password1.value.trim(),
            password2V = password2.value.trim(),
            mobileV = mobile.value.trim();

        if ( fnV === '' || lnV === '' || emailV === '' ||
            password1V === '' || password2V === ''
            || mobileV === '' )
        {
            showError("Please provide all the fields.");
            return false;
        }

        hideError();

        // Now send the ajax.
        $.ajax({
            url: homeUrl + '/ajax/validate-register-fields',
            dataType: 'JSON',
            type: "POST",
            data: {
                validate: 2,
                firstName: fnV,
                lastName: lnV,
                email: emailV,
                password1: password1V,
                password2: password2V,
                mobile: mobileV,
                packageId: packageId
            },
            beforeSend: function () {
                btn.disabled = true;
            },
            success: function ( ajaxD ) {
                if ( ajaxD.info !== 'success' ) {
                    showError(ajaxD.message);
                    btn.disabled = false;
                    return false;
                }

                firstName.disabled = true;
                lastName.disabled = true;
                email.disabled = true;
                password1.disabled = true;
                password2.disabled = true;
                mobile.disabled = true;

                var regOtpTop = document.getElementById('register-otp-top');
                regOtpTop.className = regOtpTop.className.replace(/\bd-none\b/g, "");

                data.isValidated = true;
                data.seq = ajaxD.message.seq;
				
				// showError("YOur OTP is " + ajaxD.message.otp);

                otp.seq = data.seq;
                document.getElementById('otp').value = ajaxD.message.otp;
                var otpBtn = document.getElementById('otp-btn');
                $(otpBtn).trigger('click');
                var f = function(){
                    otp.validateOtpLink();
                    setTimeout(f, 1000);
                };
                setTimeout(f, 1000);

            },
            error: function () {

            },
            complete: function () {

            }
        });

    }, false);

    var otpBtn = document.getElementById('otp-btn');
    otpBtn.addEventListener('click', function (e) {

        var otp = document.getElementById('otp');
        var otpV = otp.value.trim();
        var btn = this;

        if ( otpV === '' ) {
            showError("Please enter OTP");
            return false;
        }

        // Now send the ajax again with otp.
        $.ajax({
            url: homeUrl + '/ajax/validate-register-fields',
            dataType: 'JSON',
            type: "POST",
            data: {
                validate: 1,
                seq: data.seq,
                otp: otpV,
            },
            beforeSend: function () {
                btn.disabled = true;
            },
            success: function ( data ) {
                if ( data.info !== 'success' ) {
                    showError(data.message);
                    return false;
                }

                // We are going to check for the
                launchBOLT(
                    data.message.key,
                    data.message.txnid,
                    data.message.hash,
                    data.message.amount,
                    data.message.firstname,
                    data.message.email,
                    data.message.phone,
                    data.message.productinfo,
                    data.message.udf5,
                    data.message.surl,
                    data.message.furl
                )

                // window.location.href = data.message.redirectUrl;
            },
            error: function () {

            },
            complete: function () {
                btn.disabled = false;
            }
        });

    }, false);

    function showError( msg )
    {
        var alertBox = document.getElementById('alert-box');
        alertBox.innerHTML = msg;
        alertBox.className = alertBox.className.replace(/\bd-none\b/g, "");
    }

    function hideError()
    {
        var alert = document.getElementById('alert-box');
        alert.className += ' d-none ';
    }

    function launchBOLT( key, txnid, hash, amount, firstName, email, phone, pInfo, udf5, surl, furl )
    {
        var reuest = {
            key: key,
            txnid: txnid,
            hash: hash,
            amount: amount,
            firstname: firstName,
            email: email,
            phone: phone,
            productinfo: pInfo,
            udf5: udf5,
            surl : surl,
            furl: furl,
            mode: 'dropout'
        };

        var handlers = {
            responseHandler: function (BOLT) {
                if (BOLT.response.txnStatus != 'CANCEL') {
                    // //Salt is passd here for demo purpose only. For practical use keep salt at server side only.
                    // var fr = '<form action=\"' + $('#surl').val() + '\" method=\"post\">' +
                    //     '<input type=\"hidden\" name=\"key\" value=\"' + BOLT.response.key + '\" />' +
                    //     '<input type=\"hidden\" name=\"salt\" value=\"' + $('#salt').val() + '\" />' +
                    //     '<input type=\"hidden\" name=\"txnid\" value=\"' + BOLT.response.txnid + '\" />' +
                    //     '<input type=\"hidden\" name=\"amount\" value=\"' + BOLT.response.amount + '\" />' +
                    //     '<input type=\"hidden\" name=\"productinfo\" value=\"' + BOLT.response.productinfo + '\" />' +
                    //     '<input type=\"hidden\" name=\"firstname\" value=\"' + BOLT.response.firstname + '\" />' +
                    //     '<input type=\"hidden\" name=\"email\" value=\"' + BOLT.response.email + '\" />' +
                    //     '<input type=\"hidden\" name=\"udf5\" value=\"' + BOLT.response.udf5 + '\" />' +
                    //     '<input type=\"hidden\" name=\"mihpayid\" value=\"' + BOLT.response.mihpayid + '\" />' +
                    //     '<input type=\"hidden\" name=\"status\" value=\"' + BOLT.response.status + '\" />' +
                    //     '<input type=\"hidden\" name=\"hash\" value=\"' + BOLT.response.hash + '\" />' +
                    //     '</form>';
                    // var form = jQuery(fr);
                    // jQuery('body').append(form);
                    // form.submit();
                } else {
                    window.location.href = furl;
                }
            },
            catchException: function (BOLT) {
                alert(BOLT.message);
            }
        };
		
		// Comment out this lines to disable user
		window.location.href = surl;
		return;

        if ( packageId !== 1 ) {
            bolt.launch( reuest, handlers );
        }else {
            window.location.href = surl;
        }

    }

    var otp = {
        timeGap: 40,

        seq: 0,

        validateOtpLink: function () {
            var text = this.timeGap < 1 ? "Click to to resend otp now" : "Wait " + this.timeGap + " seconds to resend otp.";

            $('.resend-otp').html(text);
            $('.resend-otp').off();

            if ( this.timeGap < 1 ) {
                $('.resend-otp').on('click', function() {

                    $.ajax({
                        url: homeUrl + '/ajax/resend-otp',
                        type: "POST",
                        data: {
                            seq: 1,
                        },
                    });

                    otp.resetTimeGap();
                });
            }

            this.timeGap -= 1;
        },

        resetTimeGap: function()
        {
            this.timeGap = 41;
            this.validateOtpLink();
        }
    }

</script>
<script type="text/javascript">
    $('.custom-select').selectmenu();
</script>
</body>
<?php $this->_addFooter() ?>
