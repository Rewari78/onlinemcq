<?php


if (!UserManager::getInstance()->isTeacher()) {
    $this->onlyLoggedInAllowed(UserManager::USER_TYPE_STUDENT);
}

$teachersResult = false;

if (isset($_GET['result-id']) && !isset($_SESSION['result'])) {

    $resultId = filter_input(INPUT_GET, 'result-id', FILTER_SANITIZE_NUMBER_INT);
    $resultInfo = TestManager::getInstance()->getTestResultById($resultId);
    $testInfo = TestManager::getInstance()->getTestInfo($resultInfo['testId']);
    $dataArray = json_decode('[' . $resultInfo['data'] . ']', true);
} else if (isset($_SESSION['result']) && !isset($_GET['result-id'])) {
    $resultInfo = [];
    $teachersResult = true;
    $result = $_SESSION['result'];
    $resultInfo['score'] = $result['score'];
    $dataArray = json_decode('[' . $result['data'] . ']', true);
    $testInfo = TestManager::getInstance()->getTestInfo($dataArray[0]['testId']);
} else {
    $this->redirect("manage-test");
}


$questions = TestManager::getInstance()->getAllQuestions($resultInfo['testId']);

$test_data = json_decode($resultInfo['data']);
$total_questions = count( (array)$test_data->questions );
$attempted = $resultInfo['questionsAttempted'];
$right = 0;
foreach ( $test_data->questions as $question_data ) {
    if ( $question_data->answered ) {
	    $right++;
    }
}
$wrong = $attempted - $right;

$this->_addHeader();
?>


<div class="container">
    <div class="row">
        <div class="col-md-8 m-auto">
            <?php
            $isTeacher = strtolower($_SESSION['userType']) === 'teacher'; ?>
            <?php if ( $isTeacher || ($testInfo['displayResult'] === '1' || $testInfo['displayResult'] === '2')  ) { ?>
            <h3 class="text-center">Test Subject: <?php echo htmlentities($testInfo['subject']); ?></h3>
            <h3 class="text-center">Test Description: <?php echo htmlentities($testInfo['description']); ?></h3>
            <h3 class="text-center">Score: <?php echo htmlentities($resultInfo['score']); ?></h3>
            <h5 class="text-center">Total questions: <?php echo $total_questions; ?></h5>
            <h5 class="text-center">Unattempted questions: <?php echo $total_questions - $attempted; ?></h5>
            <h5 class="text-center">No. of right questions: <?php echo $right; ?></h5>
            <h5 class="text-center">No. of wrong questions: <?php echo $wrong; ?></h5>
            <div class="text-center">
                <br />
                <strong>Name: </strong><?php echo $resultInfo['firstName'] . ' ' . $resultInfo['lastName']; ?><br />
                <strong>Class: </strong><?php echo $resultInfo['className']; ?><br />
                <strong>Division: </strong><?php echo $resultInfo['divName']; ?><br />
            </div>

            <?php if ( $isTeacher || $testInfo['displayResult'] === '1' ) { ?>
            <ul class="mt-5 list-unstyled text-center">
                <?php
                $counter = 1;
                foreach ($dataArray[0]['questions'] as $result) {
                ?>
                    <li class="mb-5">
                        <h5>Question <?php echo $counter; ?></h5>
                        <h5>Question Details: <?php echo $result['question']; ?></h5>
                        <h5 class="<?php echo ($result['answered']) ? 'text-success' : 'text-danger'; ?>">Result: <?php echo ($result['answered']) ? 'Right' : 'Wrong'; ?>
                            <h5 class="text-warning">Correct Answer : <?php echo  str_replace( '</p>','', str_replace ('<p>','', $questions[$counter-1]['ans'.$questions[$counter-1]['correctAns']])) ; ?></h5>
                            <?php if (!$result['answered'] && !empty($result['exp']) ) { ?>
                                <h5 class="<?php echo ($result['answered']) ? '' : 'text-info' ?>">Answer Explanation : <?php echo str_replace( '</p>','', str_replace ('<p>','',$result['exp'])); ?></h5>                                
                            <?php } ?>
                    </li>
                <?php
                    $counter++;
                }
                ?>
            </ul>
            <?php } ?>
            <?php } else {
                if ( $resultInfo['questionsAttempted'] == 0 ) { ?>
                    <div class="big-error-w">
                        <h2 class="permission-denied">
                            You have not given the test.
                        </h2>
                    </div><?php
                } else { ?>
                    <div class="big-error-w">
                        <h2 class="permission-denied">
                            Thank you for submitting the test.
                        </h2>
                    </div>
                    <?php
                }
            } ?>

            <?php if ($teachersResult) { ?>
                <div class="text-center mb-5">
                    <a class="btn btn-primary" href="<?php echo SITE_URL . '/manage-test' ?>">Go Back</a>
                </div>
            <?php } ?>

        </div>
    </div>
</div>



<?php

$this->_addFooter();

if (isset($teachersResult)) {
    unset($_SESSION['result']);
}

?>
