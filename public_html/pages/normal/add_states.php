<?php

$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );

$states = StateManager::getInstance()->getAllStates();

$this->_addHeader();
@include $this->getPath() . DS . 'navs.top.php';
?>
<div class="row">
    <div class="col-sm-8 col-xxxl-8">
        <div class="element-wrapper">
            <div class="element-actions">
                <button class="btn btn-primary custom-button" data-target="#add-type-modal-state" data-toggle="modal" type="button">Add State</button>
            </div>
            <h6 class="element-header">
                States
            </h6>
            <div class="element-box">
                <div class="table-responsive">
                    <table class="table table-lightborder">
                        <thead>
                        <tr>
                            <th>
                                Name
                            </th>
                            <th class="text-left">
                                S Name
                            </th>
                            <th>
                                Actions
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ( $states as $state ): ?>
                            <tr>
                                <td class="nowrap">
                                    <?php echo $state['name'] ?>
                                </td>
                                <td class="text-left">
                                    <?php echo $state['shortName'] ?>
                                </td>
                                <td class="row-actions text-left">
                                    <a href="/edit-state/<?php echo $state['id']; ?>"><i class="os-icon os-icon-ui-49"></i></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-4 col-xxxl-4">
    </div>
</div>

<div aria-hidden="true" aria-labelledby="mySmallModalLabel" class="modal fade bd-example-modal-sm" id="add-type-modal-state" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Add New State
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
            </div>
            <form method="post" action="#" id="add-state-form">
                <div class="modal-body">
                        <div class="form-group">
                            <label for="state"> Type State</label>
                            <input class="form-control" placeholder="State Name" type="text" id="state" name="name">
                        </div>
                        <div class="form-group">
                            <label for="state-short-name"> Type State Short Name</label>
                            <input class="form-control" placeholder="Short Name" type="text" id="state-short-name" name="state-short-name">
                        </div>

                    <div class="alert alert-warning alert-dismissible d-none" role="alert" id="add-s-alert-box">
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" data-dismiss="modal" type="button"> Close</button>
                    <button class="btn btn-primary" type="submit"> Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    (function (_scope) {
        var $form = $('#add-state-form');
        $form.on('submit', function (e) {
            e.preventDefault();
            e.stopPropagation();

            hideError();

            $.post(
                homeUrl + '/ajax/add-a-state',
                $(this).serialize(),
                function(data){

                    if (  data.info !== 'success' )
                    {
                        showError(data.message);
                        return;
                    }

                    window.location.reload();
                },
                'json'
            );

        });

        function showError( msg )
        {
            var alertBox = document.getElementById('add-s-alert-box');
            alertBox.innerHTML = msg;
            alertBox.className = alertBox.className.replace(/\bd-none\b/g, "");
        }

        function hideError()
        {
            var alert = document.getElementById('add-s-alert-box');
            alert.className += ' d-none ';
        }
    })(window);
</script>
<?php
@include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();