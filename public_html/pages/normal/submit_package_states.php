<?php
$this->onlyLoggedInAllowed( UserManager::USER_TYPE_ADMIN );

// get the package id.
$packageId = $this->_data[0];
$packageId = filter_var(filter_var($packageId, FILTER_SANITIZE_NUMBER_INT), FILTER_VALIDATE_INT);

if ( empty($packageId) ) $this->redirect('manage-packages');

$packageDetails = PackageManager::getInstance()->getAPackageById($packageId);

if ( empty($packageDetails) ) $this->redirect('manage-packages');


$states = isset($_POST['add-state']) ? $_POST['add-state'] : array();
$stateIds = array();
foreach ( $states as $id )
{
    $id = filter_var((int) $id, FILTER_SANITIZE_NUMBER_INT);

    // now check if the state is valid.
    $state = StateManager::getInstance()->getStateById($id);
    if ( !empty($state) ) $stateIds[$id] = $id;
}


PackageManager::getInstance()->addStatesToPackage($packageId, $stateIds);

$this->redirect('edit-package/' . $packageId . '?ps_success=1');

var_dump($stateIds);