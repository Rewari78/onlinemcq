<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 18/2/19
 * Time: 11:18 PM
 */

$this->onlyLoggedInAllowed();

/** @var User $user */
$user = $this->getUser();
$extraInfo = $user->getExtraInfo();

$package = $user->getPackageDetails();

$selectType = filter_input(INPUT_GET, 'select-type', FILTER_SANITIZE_NUMBER_INT);
$selectType = empty($selectType) || $selectType != 1 && $selectType != 2 ? 1 : $selectType;

$stateId = filter_input(INPUT_GET, 'select-state', FILTER_SANITIZE_NUMBER_INT);

// Check if the state is a valid id.
$stateId = !empty($stateId) && StateManager::getInstance()->getStateById($stateId) ? $stateId : $extraInfo['stateId'];

//var_dump($extraInfo);
$getParams = $selectType == 2 ?
                ( $stateId > 0 ? array($stateId) : array($extraInfo['stateId']) ) :
                array();

if ( ( $package['id'] == 1 || $package['id'] == 2 ) && $stateId != $extraInfo['stateId'] )
{
    throw new RenderUpgradePackage("Apply Neet Other States", "You are not allowed to see other states notifications to see that you have to upgrade to your next package.");
}
$lists = call_user_func_array(array(ApplyNow::getInstance(), 'getApplyNeet'), $getParams);

$stateIds = array();
$collegeIds = array();

$currentTime = time();

foreach ( $lists as $item )
{
    $stateIds[$item['stateId']] = $item['stateId'];
    $collegeIds[$item['collegeId']] = $item['collegeId'];
}

$stateList = StateManager::getInstance()->getStateByIdList($stateIds);
$collegeList = CollegeManager::getInstance()->getCollegeByIdList($collegeIds);

$allStateList = StateManager::getInstance()->getAllStates();


function remaining_time( $toTime )
{

    $now = new DateTime();
    $future_date = new DateTime('@' . $toTime);


    $interval = $future_date->diff($now);

    echo $interval->format("%a days, %h hours, %i minutes");
}

$this->_addHeader();
@include $this->getPath() . DS . 'navs.top.php';
?>
    <div class="row pt-3">
        <div class="col-sm-12">
            <div class="element-actions">
                <form class="form-inline">
                    <select class="form-control form-control-sm ex-m ex-width custm-select" id="select-type">
                        <option value="1">
                            All India
                        </option>
                        <option value="2" <?php echo $selectType == 2 ? 'selected' : '' ?>>
                            State Wise
                        </option>
                    </select>

                    <?php if ( $selectType == 2 ): ?>
                        <select class="form-control form-control-sm ex-m ex-width custm-select" id="select-state">
                            <option value="Pending">
                                Select State
                            </option>
                            <?php foreach ( $allStateList as $item ):?>
                            <option value="<?php echo $item['id']; ?>" <?php echo $stateId == $item['id'] ? 'selected' : ''; ?> >
                                <?php echo $item['name']; ?>
                            </option>
                            <?php endforeach; ?>
                        </select>
                    <?php endif; ?>

                </form>
            </div>
            <div class="ex-strk">
            </div>
        </div>
    </div>

    <?php if ( empty($lists) ) : ?>
        <div class="row r-eo-bg">
            <div class="col-sm-12">
                <h4 class="text-center">Sorry looks like nothing is going on here right now.</h4>
            </div>
        </div>
    <?php endif; ?>
    
    <?php foreach ( $lists as $item ): ?>
    <?php
        if ( $selectType == 1 ) {
            if (  $item['stateId'] != 100 ) {
                continue;
            }
        }
    ?>
        <div class="row r-eo-bg">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-6">
                        <h4 class="cus-cls-fr-txt">
                        <?php
                            if ( $item['stateId'] == 100 ) {
                                echo "All India";
                            } else {
                                echo isset($stateList[$item['stateId']]) ? $stateList[$item['stateId']]['name'] : 'Unknown';
                            }
                        ?>
                        </h4>
                        <p class="typ-text"><?php echo htmlentities($item['desc']); ?></p>
                    </div>
                    <div class="col-sm-1">
                    </div>

                    <?php

                    ?>

                    <?php
                        $isBtn1Active = $currentTime >= $item['button1StartTime'] && $currentTime <= $item['button1EndTime'];
                        $isBtn2Active = $currentTime >= $item['button2StartTime'] && $currentTime <= $item['button2EndTime'];
                        $isBtn3Active = $currentTime >= $item['button3StartTime'] && $currentTime <= $item['button3EndTime'];
                    ?>
                    <div class="col-sm-5">
                        <div class="row">
                            <?php if ( $isBtn1Active ): ?>

                                <div class="col-sm-4">
                                    <p class="dy-lft-txt"><?php remaining_time($item['button1EndTime']); ?></p>
                                    <a class="cus-new-l" target="_blank" href="<?php echo $item['button1url']; ?>"><?php echo $item['button1Label']; ?> <span>&#xE76C;</span></a>
                                </div>
                            <?php endif; ?>

                            <?php if ( $isBtn2Active ): ?>

                                <div class="col-sm-4">
                                    <p class="dy-lft-txt"><?php remaining_time($item['button2EndTime']); ?></p>
                                    <a class="cus-new-l" target="_blank" href="<?php echo $item['button2url']; ?>"><?php echo $item['button2Label']; ?> <span>&#xE76C;</span></a>
                                </div>
                            <?php endif; ?>

                            <?php if ( $isBtn3Active ): ?>

                                <div class="col-sm-4">
                                    <p class="dy-lft-txt"><?php remaining_time($item['button3EndTime']); ?></p>
                                    <a class="cus-new-l" target="_blank" href="<?php echo $item['button3url']; ?>"><?php echo $item['button3Label']; ?> <span>&#xE76C;</span></a>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>

<script type="text/javascript">
    $(document).on('change', '#select-type', function () {
        window.location.href = '/apply-neet-ug?select-type=' + $(this).val();
    });

    $(document).on('change', '#select-state', function () {
        window.location.href = '/apply-neet-ug?select-type=2&select-state=' + $(this).val();
    });

</script>

<?php
@include $this->getPath() . DS . 'navs.bottom.php';
$this->_addFooter();
