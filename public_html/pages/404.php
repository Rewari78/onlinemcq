<!DOCTYPE html>
<html lang="en">

<head>
    <title>404 page Not Found</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="" />
    <link href="<?php echo SITE_URL ?>/statics/css/404.css?version=4.4.0" rel="stylesheet">
</head>

<body>

    <div class="container-w3layouts  text-center">
        <div class="agileits-logo">
            <!-- PUT YOUR LOGO HERE -->
        </div>
        <h2 class="txt-wthree">error 404</h2>
        <p>Looks like the page you are trying to visit does not exist
            <br> Please check the URL and Try again.</p>
        <div class="home">
            <a href="/home">BACK TO HOME</a>
        </div>
    </div>
    <div class="w3_agile-footer">
        <p> &copy; 2020 Sleet. All Rights Reserved | Design by
            <a href="https://twiplo.com">Twiplo - Web Services</a>
        </p>
    </div>
</body>

</html>