<?php
$url = $_SERVER['REQUEST_URI'];
if (UserManager::getInstance()->isTeacher() && trim($url, '/') !== 'home') { ?>

    <a href="<?php echo SITE_URL . '/home'; ?>">
        <div class="floatingActionButton">
            <i class="os-icon os-icon-home"></i>
        </div>
    </a>

<?php } ?>




<!-- <script src="<?php echo SITE_URL ?>/statics/bower_components/popper.js/dist/umd/popper.min.js"></script>
<script src="<?php echo SITE_URL ?>/statics/js/jquery.datetimepicker.full.min.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/moment/moment.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/chart.js/dist/Chart.min.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/select2/dist/js/select2.full.min.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/jquery-bar-rating/dist/jquery.barrating.min.js"></script> -->
<script src="<?php echo SITE_URL ?>/statics/bower_components/ckeditor/ckeditor.js"></script>
<!-- <script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap-validator/dist/validator.min.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/ion.rangeSlider/js/ion.rangeSlider.min.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/dropzone/dist/dropzone.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/editable-table/mindmup-editabletable.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/tether/dist/js/tether.min.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/slick-carousel/slick/slick.min.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap/js/dist/util.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap/js/dist/alert.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap/js/dist/button.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap/js/dist/carousel.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap/js/dist/collapse.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap/js/dist/dropdown.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap/js/dist/modal.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap/js/dist/tab.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap/js/dist/tooltip.js"></script>
<script src="<?php echo SITE_URL ?>/statics/bower_components/bootstrap/js/dist/popover.js"></script>
<script src="<?php echo SITE_URL ?>/statics/js/demo_customizer.js?version=4.4.0"></script>
<script src="<?php echo SITE_URL ?>/statics/js/main.js?version=4.4.0"></script>
<script src="<?php echo SITE_URL ?>/statics/js/jquery-ui.min.js"></script> -->

</body>

</html>