<div class="header_lower">
     <ul class="main_nav clearfix">
        <li class="nav_item"><a href="<?php echo SITE_URL; ?>">Home</a></li>
        <li class="nav_item"><a href="<?php echo SITE_URL; ?>/photography">Photography</a></li>
        <li class="nav_item"><a href="<?php echo SITE_URL; ?>/videos">Videography</a></li>
        <li class="nav_item"><a href="<?php echo SITE_URL . DS . 'products'; ?>">Products</a></li>
        <li class="nav_item"><a href="<?php echo SITE_URL . DS . 'contact'; ?>">Contact</a></li>
    </ul>
</div>