<!DOCTYPE html>
<html>

<head>
    <title><?php echo $this->_title; ?></title>
    <meta charset="utf-8">
    <meta content="ie=edge" http-equiv="x-ua-compatible">
    <meta content="template language" name="keywords">
    <meta content="Tamerlan Soziev" name="author">
    <meta content="Admin dashboard html template" name="description">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <link href="favicon.png" rel="shortcut icon">
    <link href="apple-touch-icon.png" rel="apple-touch-icon">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
    <!-- <link href="<?php echo SITE_URL ?>/statics/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
    <link href="<?php echo SITE_URL ?>/statics/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="<?php echo SITE_URL ?>/statics/bower_components/dropzone/dist/dropzone.css" rel="stylesheet"> -->

    <!-- <link href="<?php echo SITE_URL ?>/statics/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
    <link href="<?php echo SITE_URL ?>/statics/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
    <link href="<?php echo SITE_URL ?>/statics/bower_components/slick-carousel/slick/slick.css" rel="stylesheet"> -->
    <link href="<?php echo SITE_URL ?>/statics/css/jquery-ui.min.css" rel="stylesheet">
    <link href="<?php echo SITE_URL ?>/statics/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="<?php echo SITE_URL ?>/statics/icon_fonts_assets/picons-thin/style.css" rel="stylesheet">
    <!-- <link href="<?php echo SITE_URL ?>/statics/icon_fonts_assets/dripicons/webfont.css" rel="stylesheet"> -->
    <link href="<?php echo SITE_URL ?>/statics/css/main.css?version=4.4.0" rel="stylesheet">
    <link href="<?php echo SITE_URL ?>/statics/css/page_based/<?php echo $this->_staticsName ?>.css" rel="stylesheet">
    <!-- <link href="https://vjs.zencdn.net/7.5.4/video-js.css" rel="stylesheet"> -->
    <!--- Extra links -->
    <link href="<?php echo SITE_URL ?>/statics/css/jquery.datetimepicker.css" rel="stylesheet">

    <script type="text/javascript" src="<?php echo SITE_URL; ?>/statics/bower_components/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo SITE_URL ?>/statics/js/dataTables.bootstrap4.min.js"></script>

    <script type="text/javascript">
        var homeUrl = '<?php echo SITE_URL; ?>';
    </script>
</head>
<body>
    <?php if ( isset($_SESSION['userType']) && $_SESSION['userType'] === 'teacher' ): ?>
        <div class="container text-center">
            <a href="/" class="btn btn-primary btn-lg mt-5">Home</a>
        </div>
    <?php endif; ?>