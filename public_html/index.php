<?php
ini_set('display_errors', 'Off');
ini_set('session.gc_maxlifetime', '7200');

date_default_timezone_set('Asia/Kolkata');


define('DS', DIRECTORY_SEPARATOR);
define('FLD_ROOT', dirname(__FILE__));
define('FLD_INC', FLD_ROOT  . DS .  'inc');

require FLD_INC . DS . 'init.php';
