<?php

define('FLD_EXTRAS', FLD_ROOT . DS . 'extras');
define('FLD_CLASSES', FLD_ROOT . DS . 'classes');
define('FLD_LIBS', FLD_ROOT . DS . 'libs');
define('FLD_IMAGES', FLD_ROOT . DS . 'images');
define('FLD_PAGES', FLD_ROOT . DS . 'pages');

define('FLD_STUDY_DIR', FLD_PAGES . DS . 'normal' . DS . 'study');
define('FLD_VIDEO_DIR', FLD_PAGES . DS . 'normal' . DS . 'videos');

/*
 * Define the tables.
 */
define('DB_TABLE_CONFIG', 'config');
define('DB_TABLE_FEATURES', 'features');
define('DB_TABLE_PACKAGE_FEATURE_REL', 'package_feature_relations');
define('DB_TABLE_PACKAGES', 'packages');
define('DB_TABLE_PACKAGE_STATE_RELATIONS', 'package_state_relations');
define('DB_TABLE_PACKAGE_USER_REL', 'package_user_relations');
define('DB_TABLE_PACKAGES_DISABLED', 'packages_disabled');
define('DB_TABLE_USERS', 'users');
define('DB_TABLE_USER_EXTRA_INFO', 'user_extra_info');
define('DB_TABLE_OTP_OBJECTS', 'otp_objects');
define('DB_TABLE_TRANS', 'transactions');
define('DB_TABLE_STATES', 'states');
define('DB_TABLE_CITIES', 'cities');
define('DB_TABLE_COLLEGE_TYPES', 'college_types');
define('DB_TABLE_COLLEGES', 'colleges');
define('DB_TABLE_STUDY_TYPES', 'study_types');
define('DB_TABLE_COURSE_TYPES', 'course_types');
define('DB_TABLE_BRANCH_TYPES', 'branch_types');
define('DB_TABLE_UNIVERSITY_TYPES', 'university_types');
define('DB_TABLE_UNIVERSITIES', 'universities');
define('DB_TABLE_RECOGNITION', 'recognition');
define('DB_TABLE_QUOTAS', 'quotas');
define('DB_TABLE_QUOTA_PERCENTAGE', 'quota_percentage');

define('DB_TABLE_ANNOUNCEMENT_COUNTER_STATES', 'announcement_counter_states');
define('DB_TABLE_ANNOUNCEMENT_COUNTER_COLLEGES', 'announcement_counter_colleges');
define('DB_TABLE_ANNOUNCEMENT_COUNTER_AIO', 'announcement_counter_aio');

define('DB_TABLE_APPLY_NOW_NEET', 'apply_now_neet');
define('DB_TABLE_APPLY_NOW_AIIMS', 'apply_now_aiims');
define('DB_TABLE_APPLY_NOW_JIPMER', 'apply_now_jipmer');

define('DB_TABLE_SMS_STATUS', 'sms_status');
define('DB_TABLE_EMAIL_STATUS', 'email_status');
define('DB_TABLE_EMAIL_SMS_QUEUE', 'email_sms_queue');
define('DB_TABLE_QUESTION_ANSWER', 'question_answer');
define('DB_TABLE_TESTS', 'tests');
define('DB_TABLE_TEST_QUESTIONS', 'test_questions');
define('DB_TABLE_USER_LOG', 'user_log');
define('DB_TABLE_STUDENT_TEST_RESULTS', 'student_test_results');
