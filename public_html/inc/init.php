<?php

include FLD_INC . DS . 'autoloader.php';
include FLD_INC . DS . 'config.php';
include FLD_INC . DS  . 'define.php';

// Include the exceptions
// as we  need them before routs.
include FLD_EXTRAS . DS . 'exceptions.php';

// Now add the routes.
include FLD_INC . DS . 'routes.php';

include FLD_EXTRAS . DS . 'page.php';

Autoloader::init();

// now its time to start the sessions
session_start();

// session_destroy();
// exit;

if (isset($_GET['school_id']) && isset($_GET['pid']) && isset($_GET['id']) && isset($_GET['username']) && isset($_GET['name']) && isset($_GET['password'])) {


    $available_session_keys = array_keys($_SESSION);
    foreach ($available_session_keys as $key) {
        unset($_SESSION[$key]);
    }



    UserManager::getInstance()->loginUser($_GET['id'], UserManager::USER_TYPE_TEACHER, array(
        'schoolId' => $_GET['school_id'],
        'name' => $_GET['name'],
        'username' => $_GET['username'],
        'pid' => $_GET['pid'],
        'password' => $_GET['password']
    ));


    // redirect to home page
    header('Location: ' . SITE_URL . '/home');
}


if (isset($_GET['test_id']) &&
 isset($_GET['user_id']) &&
  isset($_GET['className']) &&
   isset($_GET['divName']) &&
    isset($_GET['roll_no']) &&
     isset($_GET['first_name']) &&
      isset($_GET['last_name'])
      )
{

    $available_session_keys = array_keys($_SESSION);
    foreach ($available_session_keys as $key) {
        unset($_SESSION[$key]);
    }

    UserManager::getInstance()->loginUser($_GET['user_id'], UserManager::USER_TYPE_STUDENT, array(
        'roll_no' => $_GET['roll_no'],
        'className' => $_GET['className'],
        'divName' => $_GET['divName'],
        'first_name' => $_GET['first_name'],
        'last_name' => $_GET['last_name'],
        'test_id' => $_GET['test_id'],
    ));
    $testInfo = TestManager::getInstance()->getTestInfo($_GET['test_id']);
    header('Location: ' . SITE_URL . '/take-test?test-id=' . $testInfo['id']);
}




Requests::init();
