<?php
include FLD_EXTRAS . DS . 'requests.php';

Requests::addAjaxRoute('update_application', 'update-application');
Requests::addAjaxRoute('approve_application', 'approve-application');
Requests::addAjaxRoute('validate_register_fields', 'validate-register-fields');
Requests::addAjaxRoute('validate_login_fields', 'validate-login-fields');
Requests::addAjaxRoute('validate_complete_profile', 'validate-complete-profile');
Requests::addAjaxRoute('add_a_package', 'add-a-package');
Requests::addAjaxRoute('add_a_state', 'add-a-state');
Requests::addAjaxRoute('add_a_city', 'add-a-city');
Requests::addAjaxRoute('edit_city', 'edit-city');
Requests::addAjaxRoute('edit_college_type', 'edit-college-type');
Requests::addAjaxRoute('edit_study_type', 'edit-study-type');
Requests::addAjaxRoute('edit_course_type', 'edit-course-type');
Requests::addAjaxRoute('edit_branch_type', 'edit-branch-type');
Requests::addAjaxRoute('edit_quota', 'edit-quota');
Requests::addAjaxRoute('edit_quota_percent', 'edit-quota-percent');
Requests::addAjaxRoute('quota_percentage', 'quota-percentage');
Requests::addAjaxRoute('edit_university_type', 'edit-university-type');
Requests::addAjaxRoute('edit_universities', 'edit-universities');
Requests::addAjaxRoute('get_fields_value_for_college', 'get-fields-value-for-college');
Requests::addAjaxRoute('submit_college', 'submit-college');
Requests::addAjaxRoute('submit_edit_college', 'submit-edit-college');
Requests::addAjaxRoute('state_counter_announcement', 'state-counter-announcement');
Requests::addAjaxRoute('submit_state_counter_announcement', 'submit-state-counter-announcement');
Requests::addAjaxRoute('college_counter_announcement', 'college-counter-announcement');
Requests::addAjaxRoute('submit_college_counter_announcement', 'submit-college-counter-announcement');
Requests::addAjaxRoute('aio_counter_announcement', 'aio-counter-announcement');
Requests::addAjaxRoute('submit_aio_counter_announcement', 'submit-aio-counter-announcement');
Requests::addAjaxRoute('upgrade_ajax_package', 'upgrade-ajax-package');
Requests::addAjaxRoute('resend_otp', 'resend-otp');
Requests::addAjaxRoute('sms_status_save', 'sms-status-save');
Requests::addAjaxRoute('email_status_save', 'email-status-save');
Requests::addAjaxRoute('submit_test_form', 'submit-test-form');
Requests::addAjaxRoute('upload_file', 'upload-file');
Requests::addAjaxRoute('upload_q_files', 'upload-q-file');
Requests::addAjaxRoute('add_test_process', 'add-test-process');
Requests::addAjaxRoute('update_test_process', 'update-test-process');

Requests::addRoute('login', 'login', 'Login to Myranks Online Test');
Requests::addRoute('register', 'register', 'Create an account');
Requests::addRoute('logout', 'logout', 'Logout');
Requests::addRoute('complete_registration', 'complete-registration', 'Complete your registration');
Requests::addRoute('home', 'home', 'Home');
Requests::addRoute('user_college_list', 'user-college-list', "Add All India Quota Counter Announcement");
Requests::addRoute('college_predictor', 'college-predictor', "College Predictor");
Requests::addRoute('question_and_answers', 'question-and-answers', "Question And Answers");
Requests::addRoute('mock_tests', 'mock-tests', "Mock Tests");
Requests::addRoute('video_lessions', 'video-lessions', "Video Lessions");
Requests::addRoute('on_demand_study', 'on-demand-study', "On demand Study");
Requests::addRoute('alert_email', 'alert-email', "Alert Email");
Requests::addRoute('alert_sms', 'alert-sms', "Alert SMS");
Requests::addRoute('live_chat', 'live-chat', "Live Chat");

Requests::addRoute('study', 'study', "Study");

Requests::addRoute('permission_error', 'permission-error', 'Permission error');
Requests::addRoute('account_general_settings', 'account-general-settings', 'Account general settings');
Requests::addRoute('account_email_settings', 'account-email-settings', "Account Email Settings");
Requests::addRoute('account_password_settings', 'account-password-settings', "Account Password Settings");
Requests::addRoute('edit_package', 'edit-package', "Edit Package");
Requests::addRoute('deactivate_or_activate_package', 'deactivate-or-activate-package', "Edit Package");
Requests::addRoute('add_states', 'add-states', "Add States");
Requests::addRoute('add_city', 'add-city', "Add City");
Requests::addRoute('edit_state', 'edit-state', "Edit State");
Requests::addRoute('add_college_type', 'add-college-type', "Add College Type");
Requests::addRoute('add_study_type', 'add-study-type', "Add Study Type");
Requests::addRoute('add_course_type', 'add-course-type', "Add Course Type");
Requests::addRoute('add_branch_type', 'add-branch-type', "Add Branch Type");
Requests::addRoute('manage_college', 'manage-college', "Manage College");
Requests::addRoute('add_college', 'add-college', "Add College");
Requests::addRoute('edit_college', 'edit-college', "Edit College");
Requests::addRoute('add_quota', 'add-quotas', "Add Quotas");
Requests::addRoute('add_quota_percentage', 'add-quota-percentage', "Add Quotas Percentage");
Requests::addRoute('add_university_type', 'add-university-type', "Add University Type");
Requests::addRoute('add_universities', 'add-universities', "Add Universities");
Requests::addRoute('state_counter_announcement', 'state-counter-announcement', "State Counter Announcements");
Requests::addRoute('add_state_counter_announcement', 'add-state-counter-announcement', "Add State Counter Announcement");
Requests::addRoute('college_counter_announcement', 'college-counter-announcement', "College Counter Announcement");
Requests::addRoute('add_college_counter_announcement', 'add-college-counter-announcement', "Add College Counter Announcement");
Requests::addRoute('aio_counter_announcement', 'aio-counter-announcement', "Add All India Quota Counter Announcement");
Requests::addRoute('add_aio_counter_announcement', 'add-aio-counter-announcement', "Add All India Quota Counter Announcement");
Requests::addRoute('apply_neet', 'apply-neet-ug', "Apply NEET UG");
Requests::addRoute('apply_jipmar', 'apply-jipmar', "Apply Jipmar");
Requests::addRoute('apply_aiims', 'apply-aiims', "Apply AIIMS");
Requests::addRoute('manage_apply_neet', 'manage-apply-neet', "Manage Apply Neet");
Requests::addRoute('manage_apply_jipmar', 'manage-apply-jipmar', "Manage Apply Jipmar");
Requests::addRoute('manage_apply_aiims', 'manage-apply-aiims', "Manage Apply AIIMS");
Requests::addRoute('add_apply_neet', 'add-apply-neet', "Manage apply neet");
Requests::addRoute('upgrade_package', 'upgrade-package', "Upgrade Package");
Requests::addRoute('manage_user', 'manage-user', "Manage User");
Requests::addRoute('add_send_sms', 'send-sms', "Send SMS");
Requests::addRoute('add_send_email', 'send-email', "Send SMS");
Requests::addRoute('forgot_password', 'forgot-password', "Forget Your Password?");



// FOLLOWING ROUTES ARE FOR DATA SUBMISSIONS ONLY.
Requests::addRoute('submit_package_features', 'submit-package-features', "Submit Package Features");
Requests::addRoute('update_package_info', 'update-package-info', "Update Package Info");
Requests::addRoute('submit_package_states', 'submit-package-states', "Submit Package States");

// Admin routes
Requests::addRoute('manage_packages', 'manage-packages', 'Manage Packages');
Requests::addRoute('ask_question', 'ask_question', 'QnA Panel');
Requests::addRoute('answer', 'answer', 'Answer');
Requests::addRoute('question_and_answers', 'question-and-answer', 'Queston Answer');
Requests::addRoute('question-defination', 'question-defination', 'Queston Defination');


Requests::addRoute('manage_test', 'manage-test', 'Manage Test');
Requests::addRoute('view_test', 'view-test', 'View Test');
Requests::addRoute('tests', 'tests', "All Student Tests");
Requests::addRoute('add_test', 'add-test', 'Add Test');
Requests::addRoute('edit_test', 'edit-test', 'Edit Test');

Requests::addRoute('edit_questions', 'edit-questions', "Edit Questions");
Requests::addRoute('take_test', 'take-test', "Take Test");
Requests::addRoute('test_log', 'test-log', "Test Log");
Requests::addRoute('score', 'score', "Your Test Score");
Requests::addRoute('result', 'result', "Your Test Result");
Requests::addRoute('copy_test', 'copy', "Copy test");
Requests::addRoute('file_browser', 'file-browser', "File Browser");
