<?php

class Autoloader {

    private static $classInstance;

    public static function init()
    {
        if ( self::$classInstance == NULL )
        {
            self::$classInstance = new self;
        }

    }

    private function __construct()
    {
        spl_autoload_register(array($this, 'loadClass'));
    }

    public function loadClass( $className )
    {
        $className = preg_replace('/^AB\\\/', '',$className);

        @include FLD_CLASSES . DS . ltrim(strtolower(preg_replace('/([A-Z])/', '_$1', $className)), '_') . '.php';

    }

}
