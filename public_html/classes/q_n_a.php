<?php

class QNA
{
	const NEET = 1;
	const AIIMS = 2;
	const JPMER = 3;
	const GENERAL = 4;
    
    private $_question_answerTable;
    private $_db;

    private static $_classInstance;

    public static function getInstance()
    {
        if ( self::$_classInstance == null )
        {
            self::$_classInstance = new self;
        }

        return self::$_classInstance;
    }

	 private function __construct()
    {
        $this->_question_answerTable = DB_TABLE_QUESTION_ANSWER;

        $this->_db = Db::getInstance();
    }

	public function addQues(  $userId, $qType, $questionTitle, $questionDesc )
    {
        $SQL = "INSERT INTO `{$this->_question_answerTable}` 
                ( `qtype_id`, `userid`, `q_title`, `q_des`, `q_ans`, `q_timestamp`, `a_timestamp`)
                VALUES ( :qtype_id, :uId, :q_title, :q_des, :q_ans, :q_t, :at )";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':qtype_id' => $qType,
            ':uId' => $userId,
            ':q_title' => $questionTitle, 
            ':q_des' => $questionDesc,
            ':q_ans' => '',
            ':q_t' => time(),
            ':at' => 0
        ), $stmt);
        $stmt->execute();

        return (bool) $stmt->rowCount();
    }

    public function getQues()
    {
        $SQL = "SELECT * 
                FROM `{$this->_question_answerTable}` ";

        $stmt = $this->_db->prepare($SQL);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    public function ansUpdate($qid, $q_ans)
    {
        $SQL = "UPDATE `{$this->_question_answerTable}` 
                SET `q_ans` = :q_ans, `a_timestamp` = :at
                WHERE `id` = :id";

       $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':q_ans' => $q_ans,
            ':at' => time(),
            ':id' => $qid
        ), $stmt);
        $stmt->execute();

        return (bool) $stmt->rowCount();
    }

    public function getQuesById( $qid )
    {
        $SQL = "SELECT * FROM `{$this->_question_answerTable}` 
                WHERE `id` = :id";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':id' => $qid
        ), $stmt);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function getQuesType($qtype_id = null, $showEmptyAns = false )
    {
         $SQL = "SELECT * FROM `{$this->_question_answerTable}`";
         $dbValues = array();
         $tails = array();
        switch ( $qtype_id )
        {
            case self::NEET:            
            case self::AIIMS:                
            case self::JPMER:
            case self::GENERAL:
                $tails[] = " `qtype_id` = :id ";
                $dbValues[':id'] = $qtype_id;
                break;
        }

        if ($showEmptyAns === false)
        {
            $tails[] = " `q_ans` <> '' ";
        }

        $SQL .= !empty($tails) ? " WHERE  " . implode(" AND ",  $tails) : "";
        $SQL .= " ORDER BY `id` DESC ";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues($dbValues, $stmt);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;

    }

    /**
     * @param $userId
     * @param int $limit
     * @param bool $isCount
     * @return array
     */
    public function getQuestionsByUser( $userId, $limit = 0, $isCount = false )
    {
        $COLUMN =  $isCount ? " COUNT(*) AS `count` " : " * ";

        $SQL = "SELECT {$COLUMN} FROM `{$this->_question_answerTable}` 
                WHERE `userid` = :uId ORDER BY `id` ";
        $dbValues = array(
            ':uId' => $userId
        );

        if ( $limit > 0 )
        {
            $SQL .= " LIMIT 0, :l ";
            $dbVars[':l'] = $limit;
        }

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues($dbValues, $stmt);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $isCount ? $result[0]['count'] : $result;
    }
}
