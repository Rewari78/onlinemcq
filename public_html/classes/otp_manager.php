<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 3/10/18
 * Time: 4:55 PM
 */

class OtpManager
{
    const OBJECT_TYPE_REGISTER = 'register';
    const OBJECT_TYPE_LOGIN = 'login';

    private $_otpObjectTable;
    private $_db;

    private static $_classInstance;

    public static function getInstance()
    {
        if ( self::$_classInstance == null )
        {
            self::$_classInstance = new self;
        }

        return self::$_classInstance;
    }

    private function __construct()
    {
        $this->_otpObjectTable = DB_TABLE_OTP_OBJECTS;
        $this->_db = Db::getInstance();
    }

    public function createAnOtpObject( $objectType, array $object )
    {
        // generate an otp number.
        $OTP = Util::genRandomNumber(Util::MAX_OTP_CHARS);

        $SQL = "INSERT INTO `{$this->_otpObjectTable}` 
                ( `objectType`, `object`, `otp`, `timeStamp` ) 
                VALUES (?, ?, ?, ?)";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            $objectType,
            json_encode($object),
            $OTP,
            time()
        ), $stmt);

        $stmt->execute();

        return ( (bool) $stmt->rowCount() ) === true ?
            Db::getInstance()->lastInsertId() : 0;
    }

    /**
     * Get the otp object.
     *
     * @param $id
     * @return array|mixed
     */
    public function getAnOtpObjectById( $id )
    {
        $SQL = "SELECT * FROM `{$this->_otpObjectTable}`
                WHERE `id` = :id ";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':id' => $id
        ), $stmt);
        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        if ( empty($result) ) return array();

        return array(
            'id' => $result['id'],
            'objectType' => $result['objectType'],
            'object' => json_decode($result['object'], true),
            'otp' => $result['otp'],
            'timeStamp' => $result['timeStamp']
        );
    }

    /**
     * Delete otp.
     *
     * @param $id
     * @return bool
     */
    public function deleteById( $id )
    {
        $SQL = "DELETE FROM 
                `{$this->_otpObjectTable}` WHERE `id` = :id";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':id' => $id
        ), $stmt);
        $stmt->execute();

        return (bool) $stmt->rowCount();
    }
}