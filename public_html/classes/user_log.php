<?php

/**
 *
 * Description of UserLog
 *
 * @package
 * @subpackage
 * @author Akash Bose <bose@ullashnetwork.com>
 * @category
 * @since 04 13, 2019 [3:10 AM]
 */
class UserLog
{

    const TYPE_TEST = "test";

    private static $_classInstance;

    public static function getInstance()
    {
        if ( self::$_classInstance == null )
        {
            self::$_classInstance = new self;
        }

        return self::$_classInstance;
    }

    private $_db;
    private $_userLogTable;


    private function __construct()
    {
        $this->_db = Db::getInstance();
        $this->_userLogTable = DB_TABLE_USER_LOG;
    }

    public function createUserLog( $userId, $logType, $log )
    {
        $SQL = "INSERT INTO `{$this->_userLogTable}`
                ( `userId`, `logType`, `text`, `timeStamp` )
                VALUES ( :uId, :lT, :t, :tS ) ";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':uId' => $userId,
            ':lT' => $logType,
            ':t' => $log,
            ':tS' => time()
        ), $stmt);

        $stmt->execute();

        return (bool) $stmt->rowCount();
    }

    public function getUserLog( $userId, $logType = null, $limit = null )
    {
        $SQL = "SELECT * FROM `{$this->_userLogTable}`
              WHERE `userId` = :id ";

        $dbValues = array(
            ':id' => $userId
        );

        if ( is_string($logType) )
        {
            $SQL .= " AND `logType` = :lt ";
            $dbValues[':lt'] = $logType;
        }

        $SQL .= " ORDER BY `timeStamp` DESC ";

        if ( is_numeric($limit) )
        {
            $SQL .= " LIMIT 0, $limit ";
        }


        $stmt = $this->_db->prepare($SQL);
        Db::bindValues($dbValues, $stmt);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function getUserLogById( $logId )
    {
        $SQL = "SELECT * FROM `{$this->_userLogTable}`
              WHERE `id` = :id ";

        $dbValues = array(
            ':id' => $logId
        );



        $stmt = $this->_db->prepare($SQL);
        Db::bindValues($dbValues, $stmt);
        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public function getUserLogByType( $logType, $limit = null )
    {
        $SQL = "SELECT * FROM `{$this->_userLogTable}`
              WHERE `logType` = :lt ";

        $dbValues = array(
            ':lt' => $logType
        );

        $SQL .= " ORDER BY `timeStamp` DESC ";

        if ( is_numeric($limit) )
        {
            $SQL .= " LIMIT 0, $limit ";
        }


        $stmt = $this->_db->prepare($SQL);
        Db::bindValues($dbValues, $stmt);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
}