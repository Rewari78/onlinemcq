<?php

/**
 * Created by PhpStorm.
 * User: akash
 * Date: 28/6/18
 * Time: 9:34 AM
 */

class UserManager
{
    // const USER_LEVEL_PROVIDER = 'provider';

    // const USER_TYPE_SUBSCRIBER = 'subscriber';
    const USER_TYPE_TEACHER = 'teacher';
    const USER_TYPE_STUDENT = 'student';

    const STUDY_12_APPEARED = 1;
    const STUDY_12_PASSOUT = 2;

    private static $_classInstance;

    public static function getInstance()
    {
        if (self::$_classInstance == null) {
            self::$_classInstance = new self;
        }

        return self::$_classInstance;
    }

    public function loginUser($userId, $userType, $sessionData)
    {

        if (isset($_SESSION['userId']) && isset($_SESSION['userType'])) return true;

        $_SESSION['userId'] = $userId;
        $_SESSION['userType'] = $userType;

        foreach ($sessionData as $key => $value) {
            $_SESSION[$key] = $value;
        }

        return true;
    }

    public function get($key)
    {
        return $_SESSION[$key];
    }

    public function logout()
    {
        if (isset($_SESSION['userId'])) unset($_SESSION['userId']);
        if (isset($_SESSION['userType'])) unset($_SESSION['userType']);
        return true;
    }

    public function isLoggedIn()
    {
        return isset($_SESSION['userId']) && isset($_SESSION['userType']);
    }

    /**
     * Check if this is the teacher.
     *
     * @return bool
     */
    public function isTeacher()
    {
        return ($this->isLoggedIn() && $_SESSION['userType'] == self::USER_TYPE_TEACHER);
    }

    /**
     * Check if this is an student.
     *
     * @return bool
     */
    public function isStudent()
    {
        return ($this->isLoggedIn() && $_SESSION['userType'] == self::USER_TYPE_STUDENT);
    }

    public function getUserId()
    {
        return isset($_SESSION['userId']) ? $_SESSION['userId'] : null;
    }

    public function getUserType()
    {
        return isset($_SESSION['userType']) ? $_SESSION['userType'] : null;
    }
}
