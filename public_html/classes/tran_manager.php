<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 3/10/18
 * Time: 4:55 PM
 */

class TranManager
{
    const TRAN_TYPE_REGISTER = 'register';
    const TRAN_TYPE_UPGRADE_PACKAGE = 'upgrade_package';

    private $_tranTable;
    private $_db;

    private static $_classInstance;

    public static function getInstance()
    {
        if ( self::$_classInstance == null )
        {
            self::$_classInstance = new self;
        }

        return self::$_classInstance;
    }

    private function __construct()
    {
        $this->_tranTable = DB_TABLE_TRANS;
        $this->_db = Db::getInstance();
    }

    public function createTran($hash, $type, $amount, $tranText, array $object )
    {
        $SQL = "INSERT INTO `{$this->_tranTable}` 
                ( `hash`, `type`, `amount`, `tranText`, `object`, `isSuccess`, `timeStamp` ) 
                VALUES (?, ?, ?, ?, ?, ?, ?)";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            $hash,
            $type,
            $amount,
            $tranText,
            json_encode($object),
            0,
            time()
        ), $stmt);

        $stmt->execute();

        return ( (bool) $stmt->rowCount() ) === true ?
            Db::getInstance()->lastInsertId() : 0;
    }

    /**
     * @param $hash
     * @return bool
     */
    public function markATranAsSuccess( $hash )
    {
        $SQL = "UPDATE `{$this->_tranTable}` SET `isSuccess` = 1 WHERE `hash` = $hash";
        $stmt = $this->_db->prepare($SQL);
        $stmt->execute();

        return (bool) $stmt->rowCount();
    }

    /**
     * Get the tran.
     *
     * @param $id
     * @return array|mixed
     */
    public function getAnTranById( $id )
    {
        $SQL = "SELECT * FROM `{$this->_tranTable}`
                WHERE `id` = :id ";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':id' => $id
        ), $stmt);
        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        if ( empty($result) ) return array();

        return array(
            'hash' => $result['hash'],
            'type' => $result['type'],
            'amount' => $result['amount'],
            'tranText' => $result['tranText'],
            'object' => json_decode($result['object'], true),
            'timeStamp' => $result['timeStamp']
        );
    }

    /**
     * Get the tran.
     *
     * @param $hash
     * @return array|mixed
     */
    public function getAnTranByHash( $hash )
    {
        $SQL = "SELECT * FROM `{$this->_tranTable}`
                WHERE `hash` = :id ";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':id' => $hash
        ), $stmt);
        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        if ( empty($result) ) return array();

        return array(
            'hash' => $result['hash'],
            'type' => $result['type'],
            'amount' => $result['amount'],
            'tranText' => $result['name'],
            'object' => json_decode($result['object'], true),
            'timeStamp' => $result['timeStamp']
        );
    }

    /**
     * Delete otp.
     *
     * @param $hash
     * @return bool
     */
    public function deleteByHash( $hash )
    {
        $SQL = "DELETE FROM 
                `{$this->_tranTable}` WHERE `hash` = :hash";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':hash' => $hash
        ), $stmt);
        $stmt->execute();

        return (bool) $stmt->rowCount();
    }
}