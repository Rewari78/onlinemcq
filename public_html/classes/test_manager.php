<?php

/**
 *
 * Description of TestManager
 *
 * @package
 * @subpackage
 * @author Akash Bose <bose@ullashnetwork.com>
 * @category
 * @since 04 12, 2019 [11:08 PM]
 */
class TestManager
{

    private static $_classInstance;

    public static function getInstance()
    {
        if (self::$_classInstance == null) {
            self::$_classInstance = new self;
        }

        return self::$_classInstance;
    }

    private $_db;
    private $_testTable;
    private $_questionAnsTable;
    private $_studentTestResultsTable;

    private function __construct()
    {
        $this->_db = Db::getInstance();
        $this->_testTable = DB_TABLE_TESTS;
        $this->_questionAnsTable = DB_TABLE_TEST_QUESTIONS;
        $this->_studentTestResultsTable = DB_TABLE_STUDENT_TEST_RESULTS;
    }

    /**
     * Create a test.
     *
     * @param $title
     * @param $subject
     * @param $packageId
     * @param $totalMarks
     * @param $totalTime
     * @return bool
     */
    public function createTest(
        $schoolId,
        $teacherId,
        $subject,
        $class,
        $divisions,
        $description,
        $start,
        $createdBy,
        $totalTime
    ) {
        $SQL = "INSERT INTO `{$this->_testTable}`
                ( `subject`, `school_id`, `class`, `divisions`, `description`, `teacherId`, `startAt`, `createdBy`, `totalTime`, `createdAt` )
                VALUES ( :sub, :schl, :cls, :divs, :dsc, :tId, :st, :cb, :tt, :ca )";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':sub' => $subject,
            ':cls' => $class,
            ':schl' => $schoolId,
            ':divs' => $divisions,
            ':dsc' => $description,
            ':tId' => $teacherId,
            ':st' => $start,
            ':cb' => $createdBy,
            ':tt' => $totalTime,
            ':ca' => time(),
        ), $stmt);

        $stmt->execute();

        return (int) $this->_db->lastInsertId();
    }

    public function updateTest(
        $id,
        $subject,
        $class,
        $divisions,
        $description,
        $start,
        $totalTime,
        $published
    ) {
        $SQL = "UPDATE `{$this->_testTable}`
                SET 
                `subject` = :s,
                `class` = :cls,
                `divisions` = :divs,
                `description` = :dsc,
                `totalTime` = :totalTime,
                `startAt` = :st,
                `published` = :pbl
                WHERE `id` = :id";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':id' => $id,
            ':s' => $subject,
            ':cls' => $class,
            ':divs' => $divisions,
            ':dsc' => $description,
            ':st' => $start,
            ':totalTime' => $totalTime,
            ':pbl' => $published,
        ), $stmt);

        $stmt->execute();

        return (bool) $stmt->rowCount();
    }


    public function getAllTests()
    {
        $SQL = "SELECT * FROM `{$this->_testTable}` ORDER BY stringExp2Int(score) DESC";

        $stmt = $this->_db->prepare($SQL);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    public function getAllTestBySchool( $schoolId )
    {
        $SQL = "SELECT * FROM `{$this->_testTable}` WHERE `school_id` = :id ORDER BY `id` DESC";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':id' => $schoolId
        ), $stmt);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }


    public function deleteTest($id)
    {
        $SQL = "DELETE FROM `{$this->_testTable}`
              WHERE `id` = :id";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':id' => $id
        ), $stmt);

        $stmt->execute();
        $isDeleted =  (bool) $stmt->rowCount();

        if ($isDeleted) {
            $this->deleteQuestions($id);
        }

        return $isDeleted;
    }

    public function getTestInfo($id)
    {
        $SQL = "SELECT * FROM `{$this->_testTable}`
              WHERE `id` = :id";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':id' => $id
        ), $stmt);

        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        return empty($result) ? array() : $result;
    }


    public function createQuestion(
        $testId,
        $questionNo,
        $qs,
        $ans1,
        $ans2,
        $ans3,
        $ans4,
        $ans5,
        $corr,
        $exp,
        $photo,
        $weight
    ) {
        $SQL = "INSERT INTO `{$this->_questionAnsTable}`
              ( `testId`, `questionNo`, `question`, `ans1`, `ans2`, `ans3`, `ans4`, `ans5`, `correctAns`, `exp`, `photo`, `weight` )
              VALUES ( :ti, :qn, :q, :ans1, :ans2, :ans3, :ans4, :ans5, :ca, :exp, :pht, :w )";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':ti' => $testId,
            ':qn' => $questionNo,
            ':q' => $qs,
            ':ans1' => $ans1,
            ':ans2' => $ans2,
            ':ans3' => $ans3,
            ':ans4' => $ans4,
            ':ans5' => $ans5,
            ':ca' => $corr,
            ':exp' => $exp,
            ':pht' => $photo,
            ':w' => $weight
        ), $stmt);
        $stmt->execute();
        return (bool) $stmt->rowCount();
    }


    public function updateQuestion(
        $id,
        $testId,
        $questionNo,
        $qs,
        $ans1,
        $ans2,
        $ans3,
        $ans4,
        $ans5,
        $corr,
        $exp,
        $photo,
        $weight
    ) {
        $SQL = "REPLACE INTO `{$this->_questionAnsTable}`
              ( `id`, `testId`, `questionNo`, `question`, `ans1`, `ans2`, `ans3`, `ans4`, `ans5`, `correctAns`, `exp`, `photo`, `weight`  )
              VALUES ( :id, :ti, :qn, :q, :ans1, :ans2, :ans3, :ans4, :ans5, :ca, :exp, :pht, :w  )";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':id' => $id,
            ':ti' => $testId,
            ':qn' => $questionNo,
            ':q' => $qs,
            ':ans1' => $ans1,
            ':ans2' => $ans2,
            ':ans3' => $ans3,
            ':ans4' => $ans4,
            ':ans5' => $ans5,
            ':ca' => $corr,
            ':exp' => $exp,
            ':pht' => $photo,
            ':w' => $weight
        ), $stmt);

        $stmt->execute();
        return (bool) $stmt->rowCount();
    }

    public function getQuestion($testId, $questionNum)
    {
        $SQL = "SELECT  * FROM `{$this->_questionAnsTable}` 
                WHERE `testId` = :ti AND `questionNo` = :qn";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':ti' => $testId,
            ':qn' => $questionNum
        ), $stmt);

        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        return empty($result) ? array() : $result;
    }


    public function getAllQuestions($testId)
    {
        $SQL = "SELECT  * FROM `{$this->_questionAnsTable}` 
                WHERE `testId` = :ti";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':ti' => $testId
        ), $stmt);

        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }


    public function deleteQuestions($testId)
    {
        $SQL = "DELETE FROM `{$this->_questionAnsTable}`
              WHERE `testId` = :id";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':id' => $testId
        ), $stmt);

        $stmt->execute();
        $isDeleted =  (bool) $stmt->rowCount();
        return $isDeleted;
    }

    public function questionExists($testId, $questionNum)
    {
        $SQL = "SELECT  * FROM `{$this->_questionAnsTable}` 
                WHERE `testId` = :ti AND  `questionNo` = :qno";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':ti' => $testId,
            ':qno' => $questionNum
        ), $stmt);

        $stmt->execute();
        $isQuestionsExists =  (bool) $stmt->rowCount();
        return $isQuestionsExists;
    }


    public function createTestResult(
        $ip,
        $userBrowser,
        $studentId,
        $testId,
        $rollNo,
        $firstName,
        $lastName,
        $className,
        $divName,
        $score,
        $questionsAttempted,
        $data
    ) {
        $SQL = "INSERT INTO `{$this->_studentTestResultsTable}`
                ( `ip`, `userAgent`, `studentId`, `testId`, `rollNo`, `firstName`, `lastName`, `className`, `divName`, `score`, `questionsAttempted`, `data` )
                VALUES ( :ipadd, :userag, :stuId, :ti, :rNo, :fn, :ln, :cn, :dn, :s, :qa, :d )";


        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':ipadd' =>   $ip,
            ':userag' =>  $userBrowser,
            ':stuId' => $studentId,
            ':ti' => $testId,
            ':rNo' => $rollNo,
            ':fn' => $firstName,
            ':ln' => $lastName,
            ':cn' => $className,
            ':dn' => $divName,
            ':s' => $score,
            ':qa' => $questionsAttempted,
            ':d' => $data
        ), $stmt);

        $stmt->execute();

        return (int) $this->_db->lastInsertId();
    }


    public function getTestResultById($id)
    {
        $SQL = "SELECT  * FROM `{$this->_studentTestResultsTable}`
                WHERE `id` = :ri";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':ri' => $id,
        ), $stmt);

        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        return empty($result) ? array() : $result;
    }


    public function getTestResult($testId, $studentId)
    {
        $SQL = "SELECT  * FROM `{$this->_studentTestResultsTable}` 
                WHERE `studentId` = :si AND `testId` = :ti";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':ti' => $testId,
            ':si' => $studentId,
        ), $stmt);

        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        return empty($result) ? array() : $result;
    }


    public function getStudentTestsExcel($testId)
	{
        $SQL = "SELECT  * FROM `{$this->_studentTestResultsTable}` 
                WHERE `testId` = :ti ORDER BY divName,className,rollNo, stringExp2Int(score) DESC";

		$stmt = $this->_db->prepare($SQL);
		Db::bindValues(array(
			':ti' => $testId,
		), $stmt);

		$stmt->execute();

		$allresult = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$result = [];
		$student_ids = [];

		foreach( $allresult as $item ) {
			if ( ! in_array( $item['studentId'], $student_ids ) ) {
				$student_ids[] = $item['studentId'];
				$result[] = $item;
			}
		}

		return empty($result) ? array() : $result;
    }
    
	public function getStudentTestsByTestId($testId)
	{
		$SQL = "SELECT  * FROM `{$this->_studentTestResultsTable}` 
                WHERE `testId` = :ti ORDER BY id ASC";

		$stmt = $this->_db->prepare($SQL);
		Db::bindValues(array(
			':ti' => $testId,
		), $stmt);

		$stmt->execute();

		$allresult = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$result = [];
		$student_ids = [];

		foreach( $allresult as $item ) {
			if ( ! in_array( $item['studentId'], $student_ids ) ) {
				$student_ids[] = $item['studentId'];
				$result[] = $item;
			}
		}

		return empty($result) ? array() : $result;
	}
}
