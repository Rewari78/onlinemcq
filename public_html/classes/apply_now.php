<?php

/**
 *
 * Description of ApplyNow
 *
 * @package
 * @subpackage
 * @author Akash Bose <bose@ullashnetwork.com>
 * @category
 * @since 03 03, 2019 [4:39 PM]
 */
class ApplyNow
{
    private $_applyNeetTable;
    private $_applyAIIMSTable;
    private $_applyJipmerTable;
    private $_db;

    private static $_classInstance;

    public static function getInstance()
    {
        if ( self::$_classInstance == null )
        {
            self::$_classInstance = new self;
        }

        return self::$_classInstance;
    }

    private function __construct()
    {
        $this->_applyNeetTable = DB_TABLE_APPLY_NOW_NEET;
        $this->_applyAIIMSTable = DB_TABLE_APPLY_NOW_AIIMS;
        $this->_applyJipmerTable = DB_TABLE_APPLY_NOW_JIPMER;
        $this->_db = Db::getInstance();
    }

    public function insertApplyNeet(
        $stateId, $collegeId, $collegeTypeId, $desc,
        $b1L, $b1Start, $b1End, $b1Url,
        $b2L, $b2Start, $b2End, $b2Url,
        $b3L, $b3Start, $b3End, $b3Url
    ) {
        $SQL = "REPLACE INTO `{$this->_applyNeetTable}`
                ( `stateId`, `collegeId`, `collegeTypeId`, `desc`, 
                 `button1Label`, `button1StartTime`, `button1EndTime`, `button1Url`,
                  `button2Label`, `button2StartTime`, `button2EndTime`, `button2Url`,
                  `button3Label`, `button3StartTime`, `button3EndTime`, `button3Url`)
                VALUES ( :sId, :cId,  :cTI, :desc, :b1l, :b1s, :b1e, :b1u,
                :b2l, :b2s, :b2e, :b2u, :b2l, :b2s, :b2e, :b2u) ";

        $stmt = $this->_db->prepare($SQL);
        $stmt->execute(array(
            ':sId' => $stateId,
            ':cId' => $collegeId,
            ':cTI' => $collegeTypeId,
            ':desc' => $desc,
            ':b1l' => $b1L,
            ':b1s' => $b1Start,
            ':b1e' => $b1End,
            ':b1u' => $b1Url,
            ':b2l' => $b2L,
            ':b2s' => $b2Start,
            ':b2e' => $b2End,
            ':b2u' => $b2Url,
            ':b3l' => $b3L,
            ':b3s' => $b3Start,
            ':b3e' => $b3End,
            ':b3u' => $b3Url,
         ));

        return (bool) $stmt->rowCount();
    }

    public function deleteApplyNeet( $id )
    {
        $SQL = "DELETE FROM `{$this->_applyNeetTable}` WHERE `id` = :id";

        $stmt = $this->_db->prepare($SQL);
        $stmt->execute(array(
            ':id' => $id
        ));

        return (bool) $stmt->rowCount();
    }

    public function getApplyNeet( $stateId = null ) {

        $SQL = "SELECT * FROM `{$this->_applyNeetTable}`";

        $dbparams = array();
        if ( is_numeric($stateId) )
        {
            $SQL .= " WHERE `stateId` = :sId ";
            $dbparams[':sId'] = $stateId;
        }

        $stmt = $this->_db->prepare($SQL);
        $stmt->execute($dbparams);

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function insertApplyAIIMS( $desc,
        $b1L, $b1Start, $b1End, $b1Url,
        $b2L, $b2Start, $b2End, $b2Url,
        $b3L, $b3Start, $b3End, $b3Url
    ) {

        // First delete all
        $SQL = "DELETE FROM `{$this->_applyAIIMSTable}`";
        $stmt = $this->_db->prepare($SQL);
        $stmt->execute();

        // NOW INSERT
        $SQL = "INSERT INTO `{$this->_applyAIIMSTable}`
                ( `desc`, 
                 `button1Label`, `button1StartTime`, `button1EndTime`, `button1Url`,
                  `button2Label`, `button2StartTime`, `button2EndTime`, `button2Url`,
                  `button3Label`, `button3StartTime`, `button3EndTime`, `button3Url`)
                VALUES (  :desc, :b1l, :b1s, :b1e, :b1u,
                :b2l, :b2s, :b2e, :b2u, :b2l, :b2s, :b2e, :b2u) ";

        $stmt = $this->_db->prepare($SQL);
        $stmt->execute(array(
            ':desc' => $desc,
            ':b1l' => $b1L,
            ':b1s' => $b1Start,
            ':b1e' => $b1End,
            ':b1u' => $b1Url,
            ':b2l' => $b2L,
            ':b2s' => $b2Start,
            ':b2e' => $b2End,
            ':b2u' => $b2Url,
            ':b3l' => $b3L,
            ':b3s' => $b3Start,
            ':b3e' => $b3End,
            ':b3u' => $b3Url,
        ));

        return (bool) $stmt->rowCount();
    }


    public function getApplyAiims( ) {

        $SQL = "SELECT * FROM `{$this->_applyAIIMSTable}`";

        $dbparams = array();
        $stmt = $this->_db->prepare($SQL);
        $stmt->execute($dbparams);

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }


    public function insertApplyJipmer( $desc,
                                      $b1L, $b1Start, $b1End, $b1Url,
                                      $b2L, $b2Start, $b2End, $b2Url,
                                      $b3L, $b3Start, $b3End, $b3Url
    ) {

        // First delete all
        $SQL = "DELETE FROM `{$this->_applyJipmerTable}`";
        $stmt = $this->_db->prepare($SQL);
        $stmt->execute();

        // NOW INSERT
        $SQL = "INSERT INTO `{$this->_applyJipmerTable}`
                ( `desc`, 
                 `button1Label`, `button1StartTime`, `button1EndTime`, `button1Url`,
                  `button2Label`, `button2StartTime`, `button2EndTime`, `button2Url`,
                  `button3Label`, `button3StartTime`, `button3EndTime`, `button3Url`)
                VALUES (  :desc, :b1l, :b1s, :b1e, :b1u,
                :b2l, :b2s, :b2e, :b2u, :b2l, :b2s, :b2e, :b2u) ";

        $stmt = $this->_db->prepare($SQL);
        $stmt->execute(array(
            ':desc' => $desc,
            ':b1l' => $b1L,
            ':b1s' => $b1Start,
            ':b1e' => $b1End,
            ':b1u' => $b1Url,
            ':b2l' => $b2L,
            ':b2s' => $b2Start,
            ':b2e' => $b2End,
            ':b2u' => $b2Url,
            ':b3l' => $b3L,
            ':b3s' => $b3Start,
            ':b3e' => $b3End,
            ':b3u' => $b3Url,
        ));

        return (bool) $stmt->rowCount();
    }

    public function getApplyJipmer( ) {

        $SQL = "SELECT * FROM `{$this->_applyJipmerTable}`";

        $dbparams = array();
        $stmt = $this->_db->prepare($SQL);
        $stmt->execute($dbparams);

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

}