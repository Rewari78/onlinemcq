<?php

/**
 * Created by PhpStorm.
 * User: akash
 * Date: 14/11/17
 * Time: 8:15 PM
 */
class Util
{
    const MAX_OTP_CHARS = 6;

    public static function genRandomNumber( $length = 5 )
    {
        if ( $length <= 0 ) return 0;

        $i = 0;
        $numbers = array();
        for( ; $i < $length; $i++ )
        {
            $numbers[] = mt_rand(1, 9);
        }

        return implode('', $numbers);
    }

    public static function convertPHPSizeToBytes($sSize)
    {
        if ( is_numeric( $sSize) ) {
            return $sSize;
        }
        $sSuffix = substr($sSize, -1);
        $iValue = substr($sSize, 0, -1);
        switch(strtoupper($sSuffix)){
            case 'P':
                $iValue *= 1024;
            case 'T':
                $iValue *= 1024;
            case 'G':
                $iValue *= 1024;
            case 'M':
                $iValue *= 1024;
            case 'K':
                $iValue *= 1024;
                break;
        }
        return $iValue;
    }

    public static function getMaximumFileUploadSize()
    {
        return min(self::convertPHPSizeToBytes(ini_get('post_max_size')),
            self::convertPHPSizeToBytes(ini_get('upload_max_filesize')));
    }

    public static function htmlEncode( $string )
    {
        return htmlentities($string, null, 'UTF-8');
    }

    public static function explode($delimiter, $string )
    {
        $s = explode($delimiter, $string);
        $output = array();
        foreach ( $s as $v )
        {
            if ( empty($v) ) continue;
            $output[] = trim($v);
        }

        return $output;
    }

    public static function passwordEncrypt( $password )
    {
        return password_hash($password, PASSWORD_BCRYPT, array(
            'cost' => 11
        ));
    }

}