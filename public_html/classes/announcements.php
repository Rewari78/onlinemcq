<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 3/11/18
 * Time: 12:32 PM
 */

class Announcements
{
    private $_stateAnnouncementTable = null;
    private $_collegeAnnouncementTable = null;
    private $_aioAnnouncementTable = null;

    private $_db = null;

    private static $_classInstance;

    public static function getInstance()
    {
        if ( self::$_classInstance == null )
        {
            self::$_classInstance = new self;
        }

        return self::$_classInstance;
    }

    private function __construct()
    {
        $this->_stateAnnouncementTable = DB_TABLE_ANNOUNCEMENT_COUNTER_STATES;
        $this->_collegeAnnouncementTable = DB_TABLE_ANNOUNCEMENT_COUNTER_COLLEGES;
        $this->_aioAnnouncementTable = DB_TABLE_ANNOUNCEMENT_COUNTER_AIO;
        $this->_db = Db::getInstance();
    }

    /**
     * Some more work is done.
     *
     * @param int       $stateId
     * @param int       $collegeTypeId
     * @param int       $studyTypeId
     * @param int       $courseId
     * @param int       $branchId
     * @param string    $title
     * @param int       $start
     * @param int       $end
     * @param string    $message
     * @param string    $link
     * @param string    $customText
     *
     * @return bool
     */
    public function addStateCounterAnnouncement($stateId, $collegeTypeId, $studyTypeId, $courseId, $branchId,
                                                $title, $start, $end, $message, $link, $customText )
    {

        $SQL = "INSERT INTO `{$this->_stateAnnouncementTable}` 
              (`stateId`, `collegeTypeId`, `studyTypeId`, `courseId`, 
              `branchId`, `title`, `start`, `end`, `message`, `link`, `custom_text`, `isActive` ) 
              VALUES ( :stateId, :collegeId, :studyId, :courseId, :branchId, :title, :start, 
              :end, :message, :link, :custom_text, :isActive ) ";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':stateId' => $stateId,
            ':collegeId' => $collegeTypeId,
            ':studyId' => $studyTypeId,
            ':courseId' => $courseId,
            ':branchId' => $branchId,
            ':title' => $title,
            ':start' => $start,
            ':end' => $end,
            ':message' => $message,
            ':link' => $link,
            ':custom_text'=> $customText,
            ':isActive' => 1
        ), $stmt);
        $stmt->execute();

        return (bool) $stmt->rowCount();
    }

    /**
     * Activate or Deactivate state counter announcement.
     *
     * @param $id
     * @param $doActivate
     * @return bool
     */
    public function activateOrDeactivateStateCounter( $id, $doActivate )
    {
        $SQL = "UPDATE `{$this->_stateAnnouncementTable}` SET `isActive` = :iA 
                WHERE `id` = :id";


        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':id' => $id,
            ':iA' => (bool) $doActivate ? 1 : 0
        ), $stmt);
        $stmt->execute();

        return (bool) $stmt->rowCount();
    }

    /**
     * Some more work is done.
     *
     * @param $id
     * @return bool
     */
    public function deleteStateCounterAnnouncement( $id )
    {
        $SQL = "DELETE FROM `{$this->_stateAnnouncementTable}`
                WHERE `id` = :id";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':id' => $id
        ), $stmt);
        $stmt->execute();

        return (bool) $stmt->rowCount();
    }

    /**
     * Get all announcements.
     *
     * @return array
     */
    public function getAllAnnouncements()
    {
        $SQL = "SELECT * FROM `{$this->_stateAnnouncementTable}`";

        $stmt = $this->_db->prepare($SQL);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function getActiveStateAnnouncementsByStateId( $stateIdList )
    {
        if ( empty($stateIdList) ) return array();

        $stateIdList = (array) $stateIdList;
        $time = time();

        $SQL = "SELECT * FROM `{$this->_stateAnnouncementTable}` 
                WHERE `stateId` IN ";

        $placeHolders = array_fill(0, count($stateIdList), '?');
        $values = array_values($stateIdList);

        $SQL .= " (" . implode(',', $placeHolders) . ") ";
        $SQL .= " AND `start` <= ? AND `end` >= ?";

        $values[] = $time;
        $values[] = $time;

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues($values, $stmt);

        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $output = array();
        foreach ( $result as $row )
        {
            $stateId = $row['stateId'];
            unset($row['stateId']);
            $output[$stateId][] = $row;
        }

        return $result;
    }

    /**
     * @param $stateId
     * @param $collegeTypeId
     * @param $collegeId
     * @param $studyTypeId
     * @param $courseId
     * @param $branchId
     * @param $title
     * @param $start
     * @param $end
     * @param $message
     * @param $link
     * @param $customText
     *
     * @return bool
     */
    public function addCollegeCounterAnnouncement($stateId, $collegeTypeId, $collegeId,
                                                  $studyTypeId, $courseId, $branchId,
                                                  $title, $start, $end, $message, $link, $customText )
    {

        $SQL = "INSERT INTO `{$this->_collegeAnnouncementTable}` 
              (`stateId`, `collegeTypeId`, `collegeId`, `studyTypeId`, `courseId`, 
              `branchId`, `title`, `start`, `end`, `message`, `link`, `custom_text`, `isActive` ) 
              VALUES ( :stateId, :collegeTId, :collegeId, :studyId, :courseId, :branchId, :title, :start, 
              :end, :message, :link, :custom_text, :isActive ) ";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':stateId' => $stateId,
            ':collegeTId' => $collegeTypeId,
            ':collegeId' => $collegeId,
            ':studyId' => $studyTypeId,
            ':courseId' => $courseId,
            ':branchId' => $branchId,
            ':title' => $title,
            ':start' => $start,
            ':end' => $end,
            ':message' => $message,
            ':link' => $link,
            ':custom_text'=> $customText,
            ':isActive' => 1
        ), $stmt);
        $stmt->execute();

        return (bool) $stmt->rowCount();
    }

    /**
     * Activate or Deactivate college counter announcement.
     *
     * @param $id
     * @param $doActivate
     * @return bool
     */
    public function activateOrDeactivateCollegeCounter( $id, $doActivate )
    {
        $SQL = "UPDATE `{$this->_collegeAnnouncementTable}` SET `isActive` = :iA 
                WHERE `id` = :id";


        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':id' => $id,
            ':iA' => (bool) $doActivate ? 1 : 0
        ), $stmt);
        $stmt->execute();

        return (bool) $stmt->rowCount();
    }

    /**
     * Delete college counter announcement
     *
     * @param $id
     * @return bool
     */
    public function deleteCollegeCounterAnnouncement( $id )
    {
        $SQL = "DELETE FROM `{$this->_collegeAnnouncementTable}`
                WHERE `id` = :id";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':id' => $id
        ), $stmt);
        $stmt->execute();

        return (bool) $stmt->rowCount();
    }

    /**
     * Get all College announcements.
     *
     * @return array
     */
    public function getAllCollegeAnnouncements()
    {
        $SQL = "SELECT * FROM `{$this->_collegeAnnouncementTable}`";

        $stmt = $this->_db->prepare($SQL);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    /**
     * Some more work is done.
     *
     * @param int       $stateId
     * @param int       $collegeTypeId
     * @param int       $studyTypeId
     * @param int       $courseId
     * @param int       $branchId
     * @param string    $title
     * @param int       $start
     * @param int       $end
     * @param string    $message
     * @param string    $link
     * @param string    $customText
     *
     * @return bool
     */
    public function addAIOCounterAnnouncement($stateId, $collegeTypeId, $studyTypeId, $courseId, $branchId,
                                                $title, $start, $end, $message, $link, $customText )
    {

        $SQL = "INSERT INTO `{$this->_aioAnnouncementTable}` 
              (`stateId`, `collegeTypeId`, `studyTypeId`, `courseId`, 
              `branchId`, `title`, `start`, `end`, `message`, `link`, `custom_text`, `isActive` ) 
              VALUES ( :stateId, :collegeId, :studyId, :courseId, :branchId, :title, :start, 
              :end, :message, :link, :custom_text, :isActive ) ";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':stateId' => $stateId,
            ':collegeId' => $collegeTypeId,
            ':studyId' => $studyTypeId,
            ':courseId' => $courseId,
            ':branchId' => $branchId,
            ':title' => $title,
            ':start' => $start,
            ':end' => $end,
            ':message' => $message,
            ':link' => $link,
            ':custom_text'=> $customText,
            ':isActive' => 1
        ), $stmt);
        $stmt->execute();

        return (bool) $stmt->rowCount();
    }

    /**
     * Activate or Deactivate state counter announcement.
     *
     * @param $id
     * @param $doActivate
     * @return bool
     */
    public function activateOrDeactivateAIOCounter( $id, $doActivate )
    {
        $SQL = "UPDATE `{$this->_aioAnnouncementTable}` SET `isActive` = :iA 
                WHERE `id` = :id";


        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':id' => $id,
            ':iA' => (bool) $doActivate ? 1 : 0
        ), $stmt);
        $stmt->execute();

        return (bool) $stmt->rowCount();
    }

    /**
     * Some more work is done.
     *
     * @param $id
     * @return bool
     */
    public function deleteAIOCounterAnnouncement( $id )
    {
        $SQL = "DELETE FROM `{$this->_aioAnnouncementTable}`
                WHERE `id` = :id";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':id' => $id
        ), $stmt);
        $stmt->execute();

        return (bool) $stmt->rowCount();
    }

    /**
     * Get all announcements.
     *
     * @return array
     */
    public function getAllAIOAnnouncements()
    {
        $SQL = "SELECT * FROM `{$this->_aioAnnouncementTable}`";

        $stmt = $this->_db->prepare($SQL);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

}