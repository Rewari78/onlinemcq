<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 30/10/18
 * Time: 12:40 PM
 */

class Quotas
{
    private $_quotaTable;
    private $_quotaPercentTable;
    private $_db;

    private static $_classInstance;

    public static function getInstance()
    {
        if ( self::$_classInstance == null )
        {
            self::$_classInstance = new self;
        }

        return self::$_classInstance;
    }

    private function __construct()
    {
        $this->_quotaTable = DB_TABLE_QUOTAS;
        $this->_quotaPercentTable = DB_TABLE_QUOTA_PERCENTAGE;

        $this->_db = Db::getInstance();
    }

    /**
     * Add the quotas.
     *
     * @param $quotaName
     * @return bool
     */
    public function addQuota( $quotaName )
    {
        $SQL = "INSERT INTO `{$this->_quotaTable}`
                ( `quota` )
                VALUES ( :quota )";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':quota' => $quotaName
        ), $stmt);
        $stmt->execute();

        return (bool) $stmt->rowCount();
    }

    /**
     * Some more work is done.
     *
     * @param $quotaId
     * @param $quota
     * @return bool
     */
    public function updateQuota( $quotaId, $quota )
    {
        $SQL = "UPDATE `{$this->_quotaTable}` 
                SET `quota` = :q WHERE `id` = :i";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':i' => $quotaId,
            ':q' => $quota
        ), $stmt);
        $stmt->execute();

        return (bool) $stmt->rowCount();
    }

    /**
     * Add quota percent, on the dashboard.
     *
     * @param $stateId
     * @param $collegeTypeId
     * @param $studyTypeId
     * @param $courseTypeId
     * @param $quotaTypeId
     * @param $quotaPercent
     * @return bool
     */
    public function addQuotaPercentage( $stateId, $collegeTypeId, $studyTypeId,
                                        $courseTypeId, $quotaTypeId, $quotaPercent )
    {
        $SQL = "REPLACE INTO `{$this->_quotaPercentTable}` 
                ( `stateId`, `collegeTypeId`, `studyTypeId`, 
                `courseTypeId`, `quotaTypeId`, `percent` )
                VALUES ( :stateId, :collegeTypeId, :studyTypeId, 
                :courseTypeId, :quotaTypeId, :percent ) ";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':stateId' => $stateId,
            ':collegeTypeId' => $collegeTypeId,
            ':studyTypeId' => $studyTypeId,
            ':courseTypeId' => $courseTypeId,
            ':quotaTypeId' => $quotaTypeId,
            ':percent' => $quotaPercent
        ), $stmt);
        $stmt->execute();

        return (bool) $stmt->rowCount();
    }

    /**
     * Update the quota percentage.
     *
     * @param $id
     * @param $percent
     * @return bool
     */
    public function updateQuotaPercentage( $id, $percent )
    {
        $SQL = "UPDATE `{$this->_quotaPercentTable}` SET `percent` = :p
                WHERE `id` = :id";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':id' => $id,
            ':p' => $percent
        ), $stmt);
        $stmt->execute();

        return (bool) $stmt->rowCount();
    }

    /**
     * Get quota by Id.
     *
     * @param $quotaId
     * @return array|mixed
     */
    public function getQuotaById( $quotaId )
    {
        $SQL = "SELECT * FROM `{$this->_quotaTable}`
                WHERE `id` = :i";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':i' => $quotaId
        ), $stmt);
        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        return empty($result) ? array() : $result;

    }

    /**
     * Get quota types by ids list.
     *
     * @param $idList
     * @return array
     */
    public function getQuotaByIdList( $idList )
    {
        if ( empty($idList) ) return array();
        $idList = (array) $idList;

        $SQL = "SELECT * FROM `{$this->_quotaTable}` 
                WHERE `id` IN ";

        $placeHolders = array_fill(0, count($idList), '?');
        $values = array_values($idList);

        $SQL .= "( " . implode(',', $placeHolders) . " )";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues($values, $stmt);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $output = array();
        foreach ( $result as $row )
        {
            $id = $row['id'];
            unset($row['id']);
            $output[$id] = $row;
        }

        return $output;
    }

    /**
     * Get all the quotas.
     *
     * @return array
     */
    public function getAllQuotas()
    {
        $SQL = "SELECT * FROM `{$this->_quotaTable}`";

        $stmt = $this->_db->prepare($SQL);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    /**
     * Get all quota Percentage.
     *
     * @return array
     */
    public function getAllQuotaPercentage()
    {
        $SQL = "SELECT * FROM 
                `{$this->_quotaPercentTable}` ";

        $stmt = $this->_db->prepare($SQL);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

}