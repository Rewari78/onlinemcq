<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 11/10/18
 * Time: 10:05 AM
 */

class StateManager
{
    private static $_classInstance;

    private $_stateTable = null;
    private $_citiesTable = null;
    private $_db = null;

    public static function getInstance()
    {
        if ( self::$_classInstance == null )
        {
            self::$_classInstance = new self;
        }

        return self::$_classInstance;
    }

    private function __construct()
    {
        $this->_stateTable = DB_TABLE_STATES;
        $this->_citiesTable = DB_TABLE_CITIES;

        $this->_db = Db::getInstance();
    }


    /**
     * Add the states on database.
     *
     * @param $stateName
     * @param $shortName
     * @return bool
     */
    public function addState( $stateName, $shortName )
    {
        $SQL = "INSERT INTO `{$this->_stateTable}` ( `name`, `shortName`, `timeStamp` ) 
                VALUES ( :name, :sn, :time )";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':name' => $stateName,
            ':sn' => $shortName,
            ':time' => time()
        ), $stmt);

        $stmt->execute();
        return (bool) $stmt->rowCount();
    }

    /**
     * Update state details.
     *
     * @param $stateId
     * @param $stateName
     * @param $shortName
     * @return bool
     */
    public function updateState( $stateId, $stateName, $shortName )
    {
        $SQL = "UPDATE `{$this->_stateTable}` 
                SET `name` = :name, `shortName` = :sn 
                WHERE `id` = :id";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':name' => $stateName,
            ':sn' => $shortName,
            ':id' => $stateId
        ), $stmt);

        $stmt->execute();
        return (bool) $stmt->rowCount();
    }

    public function addACity( $stateId, $cityName, $pinCode = '000000')
    {
        $SQL = "INSERT INTO `{$this->_citiesTable}` 
                ( `stateId`, `cityName`, `pinCode` )
                VALUES ( :stateId, :name, :pin )";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':stateId' => $stateId,
            ':name' => $cityName,
            ':pin' => $pinCode
        ), $stmt);

        $stmt->execute();

        return $stmt->rowCount();
    }

    /**
     * Update a city based on Id.
     *
     * @param $cityId
     * @param $stateId
     * @param $cityName
     * @param $pinCode
     * @return bool
     */
    public function updateACity( $cityId, $stateId, $cityName, $pinCode = '000000' )
    {
        $SQL = "UPDATE `{$this->_citiesTable}` 
                SET `stateId` = :s,
                `cityName` = :cn,
                `pinCode` = :pin
                WHERE `id` = :id ";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':id' => $cityId,
            ':s' => $stateId,
            ':cn' => $cityName,
            ':pin' => $pinCode
        ), $stmt);
        $stmt->execute();

        return (bool) $stmt->rowCount();

    }

    /**
     * Get the list of all states.
     *
     * @return array
     */
    public function getAllStates()
    {
        $SQL = "SELECT * FROM `{$this->_stateTable}` ORDER BY `name` ASC";
        $stmt = $this->_db->prepare($SQL);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    /**
     * Some more work is done by stateId.
     *
     * @param $stateId
     * @return array
     */
    public function getCityListByStateId( $stateId )
    {
        $SQL = "SELECT * FROM `{$this->_citiesTable}` 
                WHERE `stateId` = :stateId";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':stateId' => $stateId,
        ), $stmt);

        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $output = array();
        foreach ( $result as $row )
        {
            $output[] = array(
                'id' => $row['id'],
                'name' => $row['cityName'],
                'pinCode' => $row['pinCode']
            );
        }

        return $output;
    }

    /**
     *
     * Get the state by state id.
     *
     * @param $stateId
     * @return array|mixed
     */
    public function getStateById( $stateId )
    {
        $SQL = "SELECT * FROM `{$this->_stateTable}`
                WHERE `id` = :id";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':id' => $stateId
        ), $stmt);
        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if ( empty($result) ) return array();

        return $result;
    }

    public function getStateByIdList( $stateIds )
    {
        if ( empty($stateIds) ) return array();

        $stateIds = (array) $stateIds;

        $SQL = "SELECT * FROM `{$this->_stateTable}`
                WHERE `id` IN ";

        $placeHolders = array_fill(0, count($stateIds), '?');
        $values = array_values($stateIds);

        $SQL .= "( " . implode(',', $placeHolders) . " )";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues($values, $stmt);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $output = array();
        foreach ( $result as $row )
        {
            $id = $row['id'];
            unset($row['id']);
            $output[$id] = $row;
        }

        return $output;
    }

    public function getCityById( $cityId )
    {
        $SQL = "SELECT * FROM `{$this->_citiesTable}` 
                WHERE `id` = :id";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':id' => $cityId
        ), $stmt);
        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if ( empty($result) ) return array();

        return array(
            'id' => $result['id'],
            'stateId' => $result['stateId'],
            'name' => $result['cityName'],
            'pinCode' => $result['pinCode']
        );
    }

    public function getCityByIdList($cityIds )
    {
        if ( empty($cityIds) ) return array();

        $cityIds = (array) $cityIds;

        $SQL = "SELECT * FROM `{$this->_citiesTable}`
                WHERE `id` IN ";

        $placeHolders = array_fill(0, count($cityIds), '?');
        $values = array_values($cityIds);

        $SQL .= "( " . implode(',', $placeHolders) . " )";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues($values, $stmt);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $output = array();
        foreach ( $result as $row )
        {
            $id = $row['id'];
            unset($row['id']);
            $output[$id] = $row;
        }

        return $output;
    }

}