<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 26/10/18
 * Time: 6:28 PM
 */

class CollegeManager
{
    const CUTOFF_GEN = 1;
    const CUTOFF_GEN_PH = 2;
    const CUTOFF_OBC = 3;
    const CUTOFF_OBC_PH = 4;
    const CUTOFF_SC = 5;
    const CUTOFF_SC_PH = 6;
    const CUTOFF_ST = 7;
    const CUTOFF_ST_PH = 8;

    const ORDER_BY_ALPHA = 1;
    const ORDER_BY_LTH = 2;
    const ORDER_BY_HTL = 3;

    private $_collegesTable;
    private $_cityTable;
    private $_collegeTypeTable;
    private $_studyTypeTable;
    private $_courseTypeTable;
    private $_branchTypeTable;
    private $_universitiesTable;
    private $_universityTypesTables;
    private $_recognition = DB_TABLE_RECOGNITION;
    private $_db;

    private static $_classInstance;

    public static function getInstance()
    {
        if ( self::$_classInstance == null )
        {
            self::$_classInstance = new self;
        }

        return self::$_classInstance;
    }

    private function __construct()
    {
        $this->_collegeTypeTable = DB_TABLE_COLLEGE_TYPES;
        $this->_studyTypeTable = DB_TABLE_STUDY_TYPES;
        $this->_courseTypeTable = DB_TABLE_COURSE_TYPES;
        $this->_branchTypeTable = DB_TABLE_BRANCH_TYPES;

        $this->_universitiesTable = DB_TABLE_UNIVERSITIES;
        $this->_universityTypesTables = DB_TABLE_UNIVERSITY_TYPES;
        $this->_recognition = DB_TABLE_RECOGNITION;

        $this->_collegesTable = DB_TABLE_COLLEGES;
        $this->_cityTable = DB_TABLE_CITIES;

        $this->_db = Db::getInstance();
    }

    /**
     * Add A college type.
     *
     * @param $collegeType
     * @return bool
     */
    public function addCollegeType( $collegeType )
    {
        $SQL = "INSERT INTO `{$this->_collegeTypeTable}` 
                ( `collegeType` ) 
                VALUES ( :cType ) ";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':cType' => $collegeType
        ), $stmt);
        $stmt->execute();

        return (bool) $stmt->rowCount();
    }

    /**
     * Update college based on id.
     *
     * @param $id
     * @param $collegeType
     * @return bool
     */
    public function updateCollegeType( $id, $collegeType )
    {
        $SQL = "UPDATE `{$this->_collegeTypeTable}` 
                SET `collegeType` = :cT WHERE `id` = :id";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':cT' => $collegeType,
            ':id' => $id
        ), $stmt);
        $stmt->execute();

        return (bool) $stmt->rowCount();
    }

    /**
     * Add new study type in database.
     *
     * @param $collegeTypeId
     * @param $studyType
     * @return bool
     */
    public function addStudyType( $studyType )
    {
        $SQL = "INSERT INTO `{$this->_studyTypeTable}` 
                ( `studyType` ) 
                VALUES ( :studyType ) ";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':studyType' => $studyType
        ), $stmt);
        $stmt->execute();

        return (bool) $stmt->rowCount();
    }

    /**
     * Update the study type
     *
     * @param $id
     * @param $studyType
     * @return bool
     */
    public function updateStudyType( $id, $studyType )
    {
        $SQL = "UPDATE `{$this->_studyTypeTable}` 
                SET `studyType` = :sT
                WHERE `id` = :id";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':sT' => $studyType,
            ':id' => $id
        ), $stmt);

        $stmt->execute();

        return (bool) $stmt->rowCount();
    }

    public function addCourseType( $studyTypeId, $courseType )
    {
        $SQL = "INSERT INTO `{$this->_courseTypeTable}` 
                ( `studyTypeId`, `courseType` )
                VALUES ( :studyTI, :courseT ) ";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':studyTI' => $studyTypeId,
            ':courseT' => $courseType
        ), $stmt);

        $stmt->execute();

        return (bool) $stmt->rowCount();
    }

    public function updateCourseType( $id, $studyTypeId, $courseType )
    {
        $SQL = "UPDATE `{$this->_courseTypeTable}` 
                SET `studyTypeId` = :sTI, `courseType` = :cT 
                WHERE `id` = :id";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':sTI' => $studyTypeId,
            ':cT' => $courseType,
            ':id' => $id
        ), $stmt);
        $stmt->execute();

        return (bool) $stmt->rowCount();
    }

    public function addBranchType( $courseId, $branchType )
    {
        $SQL = "INSERT INTO `{$this->_branchTypeTable}`
                ( `courseTypeId`, `branchType` )
                VALUES ( :cTI, :bT ) ";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':cTI' => $courseId,
            ':bT' => $branchType
        ), $stmt);
        $stmt->execute();

        return (bool) $stmt->rowCount();

    }

    /**
     *
     * Update the branch type.
     *
     * @param $id
     * @param $branchType
     * @return bool
     */
    public function updateBranchType( $id, $branchType )
    {
        $SQL = "UPDATE `{$this->_branchTypeTable}` SET
                `branchType` = :bT WHERE `id` = :id";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':bT' => $branchType,
            ':id' => $id
        ), $stmt);
        $stmt->execute();

        return (bool) $stmt->rowCount();

    }

    /**
     * Add university type.
     *
     * @param $type
     * @return bool
     */
    public function addUniversityType( $type )
    {
        $SQL = "INSERT INTO `{$this->_universityTypesTables}`
                ( `universityType` )
                VALUES ( :uT )";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':uT' => $type
        ), $stmt);
        $stmt->execute();

        return (bool) $stmt->rowCount();
    }

    /**
     * Update university type.
     *
     * @param $id
     * @param $type
     * @return bool
     */
    public function updateUniversityType( $id, $type )
    {
        $SQL = "UPDATE `{$this->_universityTypesTables}` 
                SET `universityType` = :t
                WHERE `id` = :id";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':t' => $type,
            ':id' => $id
        ), $stmt);
        $stmt->execute();

        return (bool) $stmt->rowCount();
    }

    /**
     * Add university.
     *
     * @param $stateId
     * @param $universityTypeId
     * @param $name
     * @return bool
     */
    public function addUniversity( $stateId, $universityTypeId, $name )
    {
        $SQL = "INSERT INTO `{$this->_universitiesTable}` 
                ( `stateId`, `universityTypeId`, `name` )
                VALUES ( :stateId, :uTable, :name ) ";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':stateId' => $stateId,
            ':uTable' => $universityTypeId,
            ':name' => $name
        ), $stmt);
        $stmt->execute();

        return (bool) $stmt->rowCount();
    }

    /**
     * Update universities.
     *
     * @param int $id
     * @param int $stateId
     * @param int $universityTypeId
     * @param string $name
     * @return bool
     */
    public function updateUniversity( $id, $stateId, $universityTypeId, $name )
    {
        $SQL = "UPDATE `{$this->_universitiesTable}` 
                SET `stateId` = :s, `universityTypeId` = :uti, 
                `name` = :n WHERE `id` = :id";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':s' => $stateId,
            ':uti' => $universityTypeId,
            ':n' => $name,
            ':id' => $id
        ), $stmt);
        $stmt->execute();

        return (bool) $stmt->rowCount();
    }

    public function addCollege( $collegeName, $estYear, $stateId, $universityId, $collegeTypeId,
                                $recognitionId, $govFee, $nriFree, $privateFee, $otherFree,
                                $cutGen, $cutGenPh, $cutObc, $cutObcPh, $cutSc, $cutScPh, $cutSt, $cutStPh,
                                $cityId, $websiteUrl, $totalSeats )
    {
        $SQL = "INSERT INTO `{$this->_collegesTable}` 
                ( `collegeName`, `established`, `stateId`, `affUniversityId`, 
                `collegeTypeId`, `recognitionId`, `govFee`, `nriFee`, `privateFee`, 
                `otherFee`, `cutGen`, `cutGenPh`, `cutObc`, `cutObcPh`, `cutSc`, `cutScPh`, `cutSt`, `cutStPh`,
                 `cityId`, `websiteUrl`, `totalSeats` ) 
                VALUES ( :colName, :est, :stateId, :affUI,
                  :colTId, :recogId, :govFee, :nriFee, :privateFee, 
                  :otherFee, :cutGen, :cutGenPh, :cutObc, :cutObcPh, :cutSc, 
                  :cutScPh, :cutSt, :cutStPh, :cityId, :websiteUrl, :totalSeats)";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':colName' => $collegeName,
            ':est' => $estYear,
            ':stateId' => $stateId,
            ':affUI' => $universityId,
            ':colTId' => $collegeTypeId,
            ':recogId' => $recognitionId,
            ':govFee' => $govFee,
            ':nriFee' => $nriFree,
            ':privateFee' => $privateFee,
            ':otherFee' => $otherFree,
            ':cutGen' => $cutGen,
            ':cutGenPh' => $cutGenPh,
            ':cutObc' => $cutObc,
            ':cutObcPh' => $cutObcPh,
            ':cutSc' => $cutSc,
            ':cutScPh' => $cutScPh,
            ':cutSt' => $cutSt,
            ':cutStPh' => $cutStPh,
            ':cityId' => $cityId,
            ':websiteUrl' => $websiteUrl,
            ':totalSeats' => $totalSeats
        ), $stmt);
        $stmt->execute();

        return (bool) $stmt->rowCount();
    }

    public function updateCollege( $collegeId, $collegeName, $estYear, $stateId, $universityId, $collegeTypeId,
                                $recognitionId, $govFee, $nriFree, $privateFee, $otherFree,
                                $cutGen, $cutGenPh, $cutObc, $cutObcPh, $cutSc, $cutScPh, $cutSt, $cutStPh,
                                $cityId, $websiteUrl, $totalSeats )
    {

        $SQL = "UPDATE `{$this->_collegesTable}` 
                SET `collegeName` = :colName,
                `established` = :est,
                `stateId` = :stateId,
                `affUniversityId` = :affUI,
                `collegeTypeId` = :colTId,
                `recognitionId` = :recogId,
                `govFee` = :govFee,
                `nriFee` = :nriFee,
                `privateFee` = :privateFee,
                `otherFee` = :otherFee,
                `cutGen` = :cutGen,
                `cutGenPh` = :cutGenPh,
                `cutObc` = :cutObc,
                `cutObcPh` = :cutObcPh,
                `cutSc` = :cutSc,
                `cutScPh` = :cutScPh,
                `cutSt` = :cutSt,
                `cutStPh` = :cutStPh,
                `cityId` = :cityId,
                `websiteUrl` = :websiteUrl,
                `totalSeats` = :totalSeats
                WHERE `id` = :id";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':id' => $collegeId,
            ':colName' => $collegeName,
            ':est' => $estYear,
            ':stateId' => $stateId,
            ':affUI' => $universityId,
            ':colTId' => $collegeTypeId,
            ':recogId' => $recognitionId,
            ':govFee' => $govFee,
            ':nriFee' => $nriFree,
            ':privateFee' => $privateFee,
            ':otherFee' => $otherFree,
            ':cutGen' => $cutGen,
            ':cutGenPh' => $cutGenPh,
            ':cutObc' => $cutObc,
            ':cutObcPh' => $cutObcPh,
            ':cutSc' => $cutSc,
            ':cutScPh' => $cutScPh,
            ':cutSt' => $cutSt,
            ':cutStPh' => $cutStPh,
            ':cityId' => $cityId,
            ':websiteUrl' => $websiteUrl,
            ':totalSeats' => $totalSeats
        ), $stmt);
        $stmt->execute();

        return (bool) $stmt->rowCount();
    }


    /**
     *
     * @return array
     */
    public function getAvlStudyType()
    {
        $SQL = "SELECT * 
                FROM `{$this->_studyTypeTable}`";

        $stmt = $this->_db->prepare($SQL);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    public function getAvlCourseType( $studyTypeId )
    {
        $SQL = "SELECT * FROM `{$this->_courseTypeTable}` 
                WHERE `studyTypeId` = :sTI";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':sTI' => $studyTypeId
        ), $stmt);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getAvlBranchType( $courseId )
    {
        $SQL = "SELECT * FROM `{$this->_branchTypeTable}` 
                WHERE `courseTypeId` = :cTI";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':cTI' => $courseId
        ), $stmt);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getAvlUniversityTypes()
    {
        $SQL = "SELECT * FROM `{$this->_universityTypesTables}`";

        $stmt = $this->_db->prepare($SQL);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    /**
     * Get all the list of universities.
     *
     * @return array
     */
    public function getAvlUniversities()
    {
        $SQL = "SELECT * FROM `{$this->_universitiesTable}`";

        $stmt = $this->_db->prepare($SQL);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $result;

    }

    /**
     * Get available colleges.
     *
     * @return array
     */
    public function getAvlCollegeTypes()
    {
        $SQL = "SELECT * FROM `{$this->_collegeTypeTable}` ";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(), $stmt);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    public function getCollege(
        $stateId = null, $cityId = null,
        $recognitionId = null, $collegeType = null,
        $courseType = null, $orderBy = null )
    {
        $SQL = "SELECT *,
                ( `govFee` + `nriFee` + `privateFee` + `otherFee` ) AS `totalFee`
                FROM `{$this->_collegesTable}` ";

        $dbValues = array();
        $tails = array();
        if ( is_numeric($stateId) && $stateId > 0 )
        {
            $tails[] = " `stateId` = :stateId ";
            $dbValues[':stateId'] = $stateId;
        }

        if ( is_numeric($cityId) && $cityId > 0 )
        {
            $tails[] = " `cityId` = :cityId ";
            $dbValues[':cityId'] = $cityId;
        }

        if ( is_numeric($recognitionId) && $recognitionId > 0 )
        {
            $tails[] = " `recognitionId` = :rId ";
            $dbValues[':rId'] = $recognitionId;
        }

        if ( is_numeric($collegeType) && $collegeType > 0 )
        {
            $tails[] = " `collegeTypeId` = :cT ";
            $dbValues[':cT'] = $collegeType;
        }

        if ( is_numeric($courseType) && $courseType > 0 )
        {
            $tails[] = " `courseTypeId` = :courseT ";
            $dbValues[':courseT'] = $courseType;
        }

        if ( !empty($tails) )
        {
            $SQL .= " WHERE  " . implode(" AND ", $tails);
        }

        switch ( $orderBy )
        {
            case self::ORDER_BY_ALPHA:
                $SQL .= " ORDER BY `collegeName` ASC ";
                break;
            case self::ORDER_BY_HTL:
                $SQL .= " ORDER BY `totalFee` DESC ";
                break;
            case self::ORDER_BY_LTH:
                $SQL .= " ORDER BY `totalFee` ASC ";
                break;
        }

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues($dbValues, $stmt);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;

    }

    public function predictCollege($stateId, $cutoffType, $score )
    {
        $SQL = "SELECT * FROM `{$this->_collegesTable}` 
                WHERE `stateId` = :stateId AND ";

        switch ( $cutoffType )
        {
            case self::CUTOFF_GEN:
                $SQL .= " `cutGen` <= :number ";
                break;
            case self::CUTOFF_GEN_PH:
                $SQL .= " `cutGenPh` <= :number ";
                break;
            case self::CUTOFF_OBC:
                $SQL .= " `cutObc` <= :number ";
                break;
            case self::CUTOFF_OBC_PH:
                $SQL .= " `cutObcPh` >= :number ";
                break;
            case self::CUTOFF_SC:
                $SQL .= " `cutSc` <= :number ";
                break;
            case self::CUTOFF_SC_PH:
                $SQL .= " `cutScPh` <= :number ";
                break;
            case self::CUTOFF_ST:
                $SQL .= " `cutSt` <= :number ";
                break;
            default:
                $SQL .= " `cutStPh` <= :number ";
        }

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':stateId' => $stateId,
            ':number' => $score
        ), $stmt);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;

    }

    /**
     * Get  college type by id.
     *
     * @param $id
     * @return array
     */
    public function getCollegeTypeById( $id )
    {
        $SQL = "SELECT * 
                FROM `{$this->_collegeTypeTable}`
                WHERE `id` = :id";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':id' => $id
        ), $stmt);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Some more work is done.
     *
     * @param $idList
     * @return array
     */
    public function getCollegeTypeIdList( $idList )
    {
        if ( empty($idList) ) return array();
        $idList = (array) $idList;

        $SQL = "SELECT * FROM `{$this->_collegeTypeTable}` 
                WHERE `id` IN ";

        $placeHolders = array_fill(0, count($idList), '?');
        $values = array_values($idList);

        $SQL .= "( " . implode(',', $placeHolders) . " )";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues($values, $stmt);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $output = array();
        foreach ( $result as $row )
        {86%
            $id = $row['id'];
            unset($row['id']);
            $output[$id] = $row;
        }

        return $output;
    }

    /**
     * Get study type by id.
     *
     * @param $id
     * @return array|mixed
     */
    public function getStudyTypeById( $id )
    {
        $SQL = "SELECT * FROM `{$this->_studyTypeTable}`
                WHERE `id` = :id";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':id' => $id
        ), $stmt);
        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        return empty($result) ? array() : $result;
    }

    public function getStudyTypeByIdList( $idList )
    {
        if ( empty($idList) ) return array();
        $idList = (array) $idList;

        $SQL = "SELECT * FROM `{$this->_studyTypeTable}` 
                WHERE `id` IN ";

        $placeHolders = array_fill(0, count($idList), '?');
        $values = array_values($idList);

        $SQL .= "( " . implode(',', $placeHolders) . " )";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues($values, $stmt);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $output = array();
        foreach ( $result as $row )
        {
            $id = $row['id'];
            unset($row['id']);
            $output[$id] = $row;
        }

        return $output;
    }

    /**
     * Get course by id.
     *
     * @param $id
     * @return array|mixed
     */
    public function getCourseTypeById( $id )
    {
        $SQL = "SELECT * FROM `{$this->_courseTypeTable}`
                WHERE `id` = :id";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':id' => $id
        ), $stmt);
        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        return empty($result) ? array() : $result;
    }

    public function getCourseTypeByIdList( $idList )
    {
        if ( empty($idList) ) return array();
        $idList = (array) $idList;

        $SQL = "SELECT * FROM `{$this->_courseTypeTable}` 
                WHERE `id` IN ";

        $placeHolders = array_fill(0, count($idList), '?');
        $values = array_values($idList);

        $SQL .= "( " . implode(',', $placeHolders) . " )";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues($values, $stmt);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $output = array();
        foreach ( $result as $row )
        {
            $id = $row['id'];
            unset($row['id']);
            $output[$id] = $row;
        }

        return $output;
    }

    /**
     * Some more work is done.
     *
     * @param $branchId
     * @return array|mixed
     */
    public function getBranchTypeById( $branchId )
    {
        $SQL = "SELECT * FROM `{$this->_branchTypeTable}` 
                WHERE `id` = :id";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':id' => $branchId
        ), $stmt);
        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        return empty($result) ? array() : $result;

    }

    /**
     * Some get branch type by id list.
     *
     * @param $idList
     * @return array
     */
    public function getBranchTypeByIdList( $idList )
    {
       if ( empty($idList) ) return array();

        $idList = (array) $idList;
        $SQL = "SELECT * FROM `{$this->_branchTypeTable}` 
                WHERE `id` IN ";

        $placeHolders = array_fill(0, count($idList), '?');
        $values = array_values($idList);

        $SQL .= " (" . implode(',', $placeHolders) . ") ";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues($values, $stmt);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $output = array();
        foreach ( $result as $row )
        {
            $id = $row['id'];
            unset($row['id']);
            $output[$id] = $row;
        }

        return $output;
    }

    public function getUniversityTypeById( $id )
    {
        $SQL = "SELECT * FROM `{$this->_universityTypesTables}` 
                WHERE `id` = :id";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':id' => $id
        ), $stmt);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    /**
     * Some more work is done.
     *
     * @param int       $id
     *
     * @return array
     */
    public function getUniversityById( $id )
    {
        $SQL = "SELECT * FROM `{$this->_universitiesTable}`
                WHERE `id` = :id";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':id' => $id
        ), $stmt);
        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        
        if ( empty($result) ) return array();
        return $result;
    }

    /**
     * Some more work is done.
     *
     * @param $stateId
     * @return array
     */
    public function getUniversitiesByStateId( $stateId )
    {
        $SQL = "SELECT * FROM `{$this->_universitiesTable}` 
                WHERE `stateId` = :id";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':id' => $stateId
        ), $stmt);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function getAllRecognitions()
    {
        $SQL = "SELECT * FROM `{$this->_recognition}`";

        $stmt = $this->_db->prepare($SQL);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    /**
     * Some more work is done.
     *
     * @param array     $idList
     * @return array|bool
     */
    public function getRecognitionsByIdList( $idList )
    {
        if ( empty($idList) ) return array();

        $idList = (array) $idList;
        $SQL = "SELECT * FROM `{$this->_recognition}` 
                WHERE `id` IN ";

        $placeHolders = array_fill(0, count($idList), '?');
        $values = array_values($idList);

        $SQL .= " (" . implode(',', $placeHolders) . ") ";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues($values, $stmt);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $output = array();
        foreach ( $result as $row )
        {
            $id = $row['id'];
            unset($row['id']);
            $output[$id] = $row;
        }

        return $output;
    }

    /**
     * Get college by id.
     *
     * @param int       $id
     *
     * @return array
     */
    public function getCollegeById( $id )
    {
        $SQL = "SELECT * FROM `{$this->_collegesTable}`
                WHERE `id` = :id";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':id' => $id
        ), $stmt);
        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        if ( empty($result) ) return array();
        return $result;
    }

    public function getCityWiseCollegeAbility( $stateId )
    {
        $SQL = "SELECT `city`.`id`, `ct`.`collegeName`                
                FROM `{$this->_cityTable}` AS `city`
                LEFT JOIN `{$this->_collegesTable}` AS `ct`
                ON ( `city`.`id` = `ct`.`cityId` ) 
                WHERE `city`.`stateId` = :stateId 
                GROUP BY `city`.`id` ";


        $stmt = $this->_db->prepare($SQL);
        $stmt->execute(array(
            ':stateId' => $stateId
        ));

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $output = array();
        foreach ( $result as $row )
        {
            $output[$row['id']] = $row['collegeName'] !== null ? true : false;
        }

        return $output;
    }

    /**
     * Some more work is done.
     *
     * @param array     $idList
     * @return array|bool
     */
    public function getCollegeByIdList( $idList )
    {
        if ( empty($idList) ) return array();

        $idList = (array) $idList;
        $SQL = "SELECT * FROM `{$this->_collegesTable}` 
                WHERE `id` IN ";

        $placeHolders = array_fill(0, count($idList), '?');
        $values = array_values($idList);

        $SQL .= " (" . implode(',', $placeHolders) . ") ";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues($values, $stmt);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $output = array();
        foreach ( $result as $row )
        {
            $id = $row['id'];
            unset($row['id']);
            $output[$id] = $row;
        }

        return $output;
    }


    /**
     * Some more work is done.
     *
     * @param array     $idList
     * @return array|bool
     */
    public function getUniversitiesByIdList( $idList )
    {
        if ( empty($idList) ) return array();

        $idList = (array) $idList;
        $SQL = "SELECT * FROM `{$this->_universitiesTable}` 
                WHERE `id` IN ";

        $placeHolders = array_fill(0, count($idList), '?');
        $values = array_values($idList);

        $SQL .= " (" . implode(',', $placeHolders) . ") ";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues($values, $stmt);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $output = array();
        foreach ( $result as $row )
        {
            $id = $row['id'];
            unset($row['id']);
            $output[$id] = $row;
        }

        return $output;
    }

}