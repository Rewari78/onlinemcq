<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 29/6/18
 * Time: 9:00 AM
 */

class Validator
{
    const EMAIL_PATTERN = '/^([\w\-\.\+\%]*[\w])@((?:[A-Za-z0-9\-]+\.)+[A-Za-z]{2,})$/';
    const PHONE_PATTERN = '/^\d{10}$/';

    public static function isValidEmail( $string )
    {
        $string = trim($string);

        if ( !preg_match(self::EMAIL_PATTERN, $string) ) return false;

        return true;
    }

    public static function isValidPhone( $string )
    {
        $string = trim($string);

        if ( !preg_match(self::PHONE_PATTERN, $string) ) return false;

        return true;
    }

    public static function isPasswordValid( $password, $hash )
    {
        return password_verify($password, $hash);
    }
}