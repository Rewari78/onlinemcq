<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 18/12/18
 * Time: 9:42 PM
 */

class VideoLists
{
    const PAGE_PHYSICS = 'physics';
    const PAGE_BIO = 'bio';
    const PAGE_CHEM = 'chem';

    private $_currentPath = null;
    private $_currentUrl = null;
    private $_videoUrl = null;

    public function __construct( $baseUrl )
    {
        $this->_currentPath = FLD_VIDEO_DIR;
        $this->_currentUrl = $baseUrl;
        $this->_videoUrl = SITE_URL . DS . 'pages' . DS . 'normal' . DS . 'videos';
    }

    public function getPageContent( array $resourcePath )
    {
        if ( is_file($this->_currentPath) ) return $this->_getVideo($this->_videoUrl, $this->_currentPath);

        // Els it is a dir
        $dirs = @scandir($this->_currentPath);
        if ( empty($resourcePath) ) return $this->_dirToHTML($this->_currentUrl, $dirs);

        //else get the first element.
        $pop = rawurldecode(array_shift($resourcePath));
        foreach ( $dirs as $item )
        {
            if ( $item == '.' || $item == '..' ) continue;

            if ( $item == $pop ) {
                $this->_currentPath  = $this->_currentPath . DS . $item;
                $this->_currentUrl = $this->_currentUrl . DS . $item;
                $this->_videoUrl = $this->_videoUrl . DS . $item;
                return $this->getPageContent($resourcePath);
            }
        }

        return false;
    }


    private function _dirToHTML( $currentUrl, $dirs, $ignoreDir = null )
    {
        $html = "<ul>";
        foreach ( $dirs as $dir )
        {
            if ( $dir == '.' || $dir == '..' ) continue;

            if ( !is_string($ignoreDir) && $ignoreDir == $dir ) continue;

            $html .= '<li><a href="' . $currentUrl . DS . $dir . '">' . $dir . '</a></li>';
        }

        $html .= "</ul>";

        return $html;
    }

    private function _getVideo( $filePath, $currentPath )
    {
        $html = " <video  class='video-js' controls preload='auto' width='640' height='480' data-setup='{}'>
                    <source src=\"{$filePath}\" type=\"video/mp4\">
                    Your browser does not support the video tag.
                    </video> ";

        $rootPath = @scandir(dirname($currentPath));
        if ( empty($rootPath) ) return $html;
        $fileName = basename($currentPath);

        $html .= "<h3 class=\"mt-5\">Other related videos</h3>";
        $currentUrl = dirname($filePath);
        $html .= $this->_dirToHTML($currentUrl, $rootPath, $fileName);

        return $html;
    }
}