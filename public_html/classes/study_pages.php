<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 18/12/18
 * Time: 9:42 PM
 */

class StudyPages
{
    const PAGE_PHYSICS = 'physics';
    const PAGE_BIO = 'bio';
    const PAGE_CHEM = 'chem';

    private $_currentPath = null;
    private $_currentUrl = null;

    public function __construct( $subject, $baseUrl )
    {
        $this->_currentPath = FLD_STUDY_DIR . DS . $subject;
        $this->_currentUrl = $baseUrl . DS . $subject;

    }

    public function getPageContent( array $resourcePath )
    {
        if ( is_file($this->_currentPath) ) return file_get_contents($this->_currentPath);

        // Els it is a dir
        $dirs = @scandir($this->_currentPath);
        if ( empty($resourcePath) ) return $this->_dirToHTML($this->_currentUrl, $dirs);

        //else get the first element.
        $pop = rawurldecode(array_shift($resourcePath));
        foreach ( $dirs as $item )
        {
            if ( $item == '.' || $item == '..' ) continue;

            if ( $item == $pop ) {
                $this->_currentPath  = $this->_currentPath . DS . $item;
                $this->_currentUrl = $this->_currentUrl . DS . $item;
                return $this->getPageContent($resourcePath);
            }
        }

        return false;
    }


    private function _dirToHTML( $currentUrl, $dirs )
    {
        $html = "<ul>";
        foreach ( $dirs as $dir )
        {
            if ( $dir == '.' || $dir == '..' ) continue;

            $html .= '<li><a href="' . $currentUrl . DS . $dir . '"> ' . $dir . '</a></li>';
        }

        $html .= "</ul>";

        return $html;
    }
}