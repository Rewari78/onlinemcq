<?php

/**
 * Created by PhpStorm.
 * User: akash
 * Date: 30/9/18
 * Time: 5:37 PM
 */

/**
 * This class will be used with
 * Page direct connections.
 *
 * Class User
 */
class User
{
    private $_info = null;
    private $_packageInfo = array();
    private $_packageFeatures = array();

    public function __construct($userId)
    {
        $userInfo = UserDatabase::getInstance()->getUserInfoByIdList($userId);
        $userInfo = $userInfo[$userId];

        $extraInfo = UserDatabase::getInstance()->getExtraInfo($userId);

        // Set the info.
        $this->_info = array(

            // 'isModerator' => $userInfo['userType'] == UserManager::USER_TYPE_ADMIN ||
            //     $userInfo['userType'] == UserManager::USER_TYPE_MODERATOR,

            // 'isAdmin' => $userInfo['userType'] == UserManager::USER_TYPE_ADMIN,

            'userId' => $userId,
            'firstName' => $userInfo['firstName'],
            'lastName' => $userInfo['lastName'],
            'email' => $userInfo['email'],
            'phone' => $userInfo['phone'],
            'password' => $userInfo['password'],
            'join' => $userInfo['join'],
            'isActive' => $userInfo['isActive'],
            'userType' => $userInfo['userType'],
            'extraInfo' => $extraInfo
        );

        // Now its time to get the package for user.
        $packageId = UserPackageController::getInstance()->getAUserPackage($userId);
        $packages = PackageManager::getInstance()->getAPackageById($packageId);

        foreach ($packages['features'] as $feature) {
            $this->_packageFeatures[$feature['featureId']] = $feature['variables'];
        }

        $this->_packageInfo = $packages['packageInfo'];
    }

    public function isModerator()
    {
        return $this->_info['isModerator'];
    }

    public function isAdmin()
    {
        return $this->_info['isAdmin'];
    }

    public function getId()
    {
        return $this->_info['userId'];
    }

    public function getType()
    {
        return $this->_info['userType'];
    }

    public function getFirstName()
    {
        return $this->_info['firstName'];
    }

    public function getLastName()
    {
        return $this->_info['lastName'];
    }

    public function getEmail()
    {
        return $this->_info['email'];
    }

    public function getPhone()
    {
        return $this->_info['phone'];
    }

    public function getPasswordHash()
    {
        return $this->_info['password'];
    }

    public function getJoinDate()
    {
        return $this->_info['join'];
    }

    public function isActive()
    {
        return $this->_info['isActive'] == '1';
    }

    public function getUserType()
    {
        return $this->_info['userType'];
    }

    public function getExtraInfo()
    {
        return $this->_info['extraInfo'];
    }

    public function getPackageDetails()
    {
        return $this->_packageInfo;
    }

    public function isFeatureEnabled($feature)
    {
        return array_key_exists($feature, $this->_packageFeatures);
    }

    public function getFeatureDetails($feature)
    {
        if (!$this->isFeatureEnabled($feature)) return array();

        return $this->_packageFeatures[$feature];
    }

    public function getFeatureVariables($feature)
    {
        return isset($this->_packageFeatures[$feature]) ? $this->_packageFeatures[$feature] : array();
    }
}
