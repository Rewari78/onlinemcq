<?php

class Config
{
    private $_db;
    private $_table;

    private static $instance;

    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function __construct()
    {
        $this->_db = Db::getInstance();
        $this->_table = DB_TABLE_CONFIG;
    }

    public function setConfig($name, $value)
    {

        $SQL = "REPLACE INTO `{$this->_table}` ( `configName`, `configValue` )
              VALUE (?, ?)";

        $stmt = $this->_db->prepare($SQL);
        $stmt->execute(array($name, $value));
        return (bool)$stmt->rowCount();
    }

    /**
     * @param $name
     * @return null|string
     */
    public function getConfig($name)
    {
        $SQL = "SELECT * FROM `{$this->_table}` WHERE `configName` = ?";
        $stmt = $this->_db->prepare($SQL);
        $stmt->execute(array($name));
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        if ( empty($data) ) return null;

        return $data['configValue'];
    }

}
