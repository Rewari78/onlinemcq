<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 29/9/18
 * Time: 11:54 AM
 */

class PackageManager
{
    const FEATURE_LIST_FOREIGN_MBBS_COL = 1;
    const FEATURE_LIST_DOMICILE_MBBS_COL = 1;
    const FEATURE_BASIC_BLOG = 3;
    const FEATURE_NEET_UPDATES = 4;
    const FEATURE_QNA_R_ONLY = 5;
    const FEATURE_QNA_RW = 6;
    const FEATURE_ALL_INDIA_QUOTA_REG_NOTI = 7;
    const FEATURE_EMAIL_NOTI = 8;
    const FEATURE_STATE_WISE_REG_ALERTS = 9;
    const FEATURE_STATE_WISE_DOC_VERIFY_ALERTS = 10;
    const FEATURE_STATE_WISE_COUNSL_ALERTS = 11;
    const FEATURE_SMS_NOTI = 12;
    const FEATURE_TEST_SERIES = 13;
    const FEATURE_STUDY_MATERIAL = 14;
    const FEATURE_LIVE_CHAT = 15;
    const FEATURE_COLLEGE_PREDICTOR = 16;
    const FEATURE_PHONE_CALL_ALERT = 17;
    const FEATURE_MCC_CHOICE_FILLING = 18;
    const FEATURE_STATE_WISE_CHOICE_FILLING_GUIDANCE = 19;
    const FEATURE_VIDEO_LESSIONS = 20;

    const MAX_TEST_LIMIT = 5;

    private $_db;
    private $_packageTable = null;
    private $_packageFeatureRelationTable = null;
    private $_packageStateRelations = null;
    private $_packageDisabled = null;
    private $_featureTable =  null;

    private $_allAvailableFeatures = array();

    private static $_classInstance;

    public static function getInstance()
    {
        if ( self::$_classInstance == null )
        {
            self::$_classInstance = new self;
        }

        return self::$_classInstance;
    }

    private function __construct() {

        $this->_db = Db::getInstance();
        $this->_packageTable = DB_TABLE_PACKAGES;
        $this->_packageFeatureRelationTable = DB_TABLE_PACKAGE_FEATURE_REL;
        $this->_packageStateRelations = DB_TABLE_PACKAGE_STATE_RELATIONS;
        $this->_packageDisabled = DB_TABLE_PACKAGES_DISABLED;
        $this->_featureTable = DB_TABLE_FEATURES;

        $this->_allAvailableFeatures = $this->_getAllFeatures();
    }

    /**
     * Add a package name.
     *
     * @param $name
     * @param $price
     * @param int $offPrice
     * @return bool
     */
    public function addAPackage( $name, $price, $offPrice = 0 )
    {
        if ( !is_string($name) ||
            !is_numeric($price) ||
            !is_numeric($offPrice) )
        {
            trigger_error("Please provide a valid parameters.");
            return false;
        }

        $SQL = "INSERT INTO `{$this->_packageTable}` 
                ( `packageName`, `price`, `offPrice` )
                VALUES (:pn, :price, :off_price) ";

        $stmt = $this->_db->prepare($SQL);

        Db::bindValues(array(
            ':pn' => $name,
            ':price' => $price,
            ':off_price' => $offPrice
        ), $stmt);
        $stmt->execute();

        return (bool) $stmt->rowCount();
    }

    /**
     * Update a package based on package Id.
     *
     * @param $packageId
     * @param $name
     * @param $price
     * @param $offPrice
     * @return bool
     */
    public function updatePackage( $packageId, $name, $price, $offPrice )
    {
        $SQL = "UPDATE `{$this->_packageTable}` 
                SET `packageName` = :name, `price` = :p, `offPrice` = :op
                WHERE `id` = :id";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':name' => $name,
            ':p' => $price,
            ':op' => $offPrice,
            ':id' => $packageId
        ), $stmt);
        $stmt->execute();

        return (bool) $stmt->rowCount();

    }

    /**
     * Some more work is needed.
     *
     * @param int       $packageId                    Provide package id.
     * @param array|int $featureIdList                Provide feature id with variables. Eg. array (  featureId /int => array('variable') );
     *
     * @return bool
     */
    public function addFeaturesToPackage( $packageId, $featureIdList )
    {

        if ( !is_numeric($packageId) )
        {
            trigger_error("Please provide valid package id.");
            return false;
        }

        // First we need to remove.
        // Then we need to reimport all the feature
        // I think this is the easier method.
        $this->_deleteAllFeatures($packageId);

        $featureIdList = (array) $featureIdList;

        if ( empty($featureIdList) ) return true;

        $SQL = "INSERT INTO `{$this->_packageFeatureRelationTable}` (`packageId`, `featureId`, `variables`) 
                VALUES ";

        $placeHolders = array();
        $values = array();

        foreach ( $featureIdList as $featureId => $variables )
        {
            $placeHolders[] = '(?, ?, ?)';
            $values[] = $packageId;
            $values[] = $featureId;
            $values[] = !empty($variables) && is_array($variables) ? json_encode($variables) : json_encode(array());
        }

        $SQL .= implode(',', $placeHolders);

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues($values, $stmt);

        $stmt->execute();

        return (bool) $stmt->rowCount();

    }

    /**
     * Add states to a package.
     *
     * @param $packageId
     * @param $stateIds
     * @return bool
     */
    public function addStatesToPackage( $packageId, $stateIds )
    {

        $this->_deleteAllStates($packageId);

        if ( empty($stateIds) ) return true;

        $stateIds = (array) $stateIds;
        $SQL = "INSERT INTO `{$this->_packageStateRelations}` ( `packageId`, `stateId` ) 
                VALUES ";

        $placeHolders = array();
        $values = array();
        foreach ( $stateIds as $id )
        {
            $placeHolders[] = '(?, ?)';
            $values[] = $packageId;
            $values[] = $id;
        }

        $SQL .= implode(',', $placeHolders);

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues($values, $stmt);
        $stmt->execute();

        return (bool) $stmt->rowCount();
    }

    /**
     *
     * Disable a package.
     *
     * @param $packageId
     * @return bool
     */
    public function disableAPackage( $packageId )
    {
        $SQL = "INSERT INTO `{$this->_packageDisabled}` 
                ( `packageId` ) VALUES ( :pId )";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':pId' => $packageId
        ), $stmt);
        $stmt->execute();

        return (bool) $stmt->rowCount();
    }

    /**
     * Enable a package.
     *
     * @param $packageId
     * @return bool
     */
    public function enableAPackage( $packageId )
    {
        $SQL = "DELETE FROM `{$this->_packageDisabled}` 
                WHERE `packageId` = :pId";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':pId' => $packageId
        ), $stmt);
        $stmt->execute();

        return (bool) $stmt->rowCount();
    }

    /**
     * Get disabled package Ids.
     *
     * @return array
     */
    public function getDisabledPackageIds()
    {
        $SQL = "SELECT `packageId` FROM `{$this->_packageDisabled}` ";

        $stmt = $this->_db->prepare($SQL);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $output = array();
        foreach ( $result as $row )
        {
            $output[$row['packageId']] = $row['packageId'];
        }

        return $output;
    }

    /**
     * @return array
     */
    public function getAvlPackageLists()
    {
        $SQL = "SELECT
                  `pt`.`id`,
                  `pt`.`packageName`,
                  `pt`.`offPrice`,
                  `pt`.`price`                  
                FROM `{$this->_packageTable}` AS `pt`
                " ;

        $stmt = $this->_db->prepare($SQL);

        $stmt->execute();
        $packages = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ( empty($packages) ) return array();

        $packageIds = array();
        foreach ( $packages as $row )
        {
            $packageIds[] = $row['id'];
        }

        // Now it is time to find the features.
        $SQL = "SELECT                  
                  `pf`.`packageId`,                  
                  `ft`.`id` AS `featureId`,
                  `ft`.`featureName`,
                  `ft`.`desc`,
                  `pf`.`variables`            
                FROM `{$this->_packageFeatureRelationTable}` AS `pf`
                INNER JOIN `{$this->_featureTable}` AS `ft`
                ON ( `ft`.`id` = `pf`.`featureId` )
                WHERE `pf`.`packageId` IN ";

        $placeHolders = array_fill(0, count($packageIds), '?');
        $values = array_values($packageIds);

        $SQL .= "(" . implode(',', $placeHolders) . ") ";
        $stmt = $this->_db->prepare($SQL);
        Db::bindValues($values, $stmt);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $formattedOutFeature = array();
        foreach ( $result as $row )
        {
            $formattedOutFeature[$row['packageId']][] = array(
                'featureId' => $row['featureId'],
                'featureName' => $row['featureName'],
                'desc' => $row['desc'],
                'variables' => json_decode($row['variables'], true)
            );
        }

        // generate final output.
        $output = array();
        foreach ( $packages as $key => $package )
        {
            $output[$key]['packageInfo'] = $package;
            $output[$key]['features'] = isset($formattedOutFeature[$package['id']]) ?
                $formattedOutFeature[$package['id']] : array();
        }

        return $output;

    }

    /**
     *
     * @param $packageId
     * @return array|mixed
     */
    public function getAPackageById( $packageId )
    {
        $SQL = "SELECT
                  `pt`.`id`,
                  `pt`.`packageName`,
                  `pt`.`offPrice`,
                  `pt`.`price`        
                FROM `{$this->_packageTable}` AS `pt` WHERE `id` = :id";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':id' => $packageId
        ), $stmt);

        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if ( empty($result) ) return array();

        $package = $result;

        // Now it is time to find the features.
        $SQL = "SELECT              
                  `ft`.`id` AS `featureId`,
                  `ft`.`featureName`,
                  `ft`.`desc`,
                  `pf`.`variables`            
                FROM `{$this->_packageFeatureRelationTable}` AS `pf`
                INNER JOIN `{$this->_featureTable}` AS `ft`
                ON ( `ft`.`id` = `pf`.`featureId` )
                WHERE `pf`.`packageId` IN (:p)";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array( ':p' => $packageId ), $stmt);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ( empty($result) ) $result = array();

        foreach ( $result as &$row )
        {
            $row['variables'] = json_decode($row['variables'], true);
        }

        return array(
            'packageInfo' => $package,
            'features' => $result
        );
    }

    public function getStateIdsForPackages( $packageIds )
    {
        if ( empty($packageIds) ) return array();

        $packageIds = (array) $packageIds;

        $SQL = "SELECT * FROM `{$this->_packageStateRelations}` 
                WHERE `packageId` IN ";

        $placeHolders = array_fill(0, count($packageIds), '?');
        $values = array_values($packageIds);

        $SQL .= "( " . implode(',', $placeHolders) . " )";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues($values, $stmt);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $output = array();
        foreach ( $result as $row )
        {
            $output[$row['packageId']][$row['stateId']] = $row['stateId'];
        }

        return $output;
    }

    public function getAllFeatures()
    {
        return $this->_allAvailableFeatures;
    }

    public function getFeaturesById( $featureId )
    {
        $SQL = "SELECT * FROM `{$this->_featureTable}` WHERE `id` = :id";
        $stmt = $this->_db->prepare($SQL);
        $stmt->execute(array(
            ':id' => $featureId
        ));

        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    /**
     *
     * @used-by PackageManager::addAPackage()
     *
     * @param $packageId
     * @return bool
     */
    public function _deleteAllFeatures( $packageId )
    {
        if ( !is_numeric($packageId) )
        {
            trigger_error("Please provide the valid package id.");
            return false;
        }

        $SQL = "DELETE FROM `{$this->_packageFeatureRelationTable}` WHERE 
                `packageId` = :pId";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':pId' => $packageId
        ), $stmt);

        $stmt->execute();
        return (bool) $stmt->rowCount();
    }

    public function _deleteAllStates( $packageId )
    {
        $SQL = "DELETE FROM `{$this->_packageStateRelations}` 
                WHERE `packageId` = :pId";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':pId' => $packageId,
        ), $stmt);

        $stmt->execute();

        return (bool) $stmt->rowCount();
    }

    private function _getAllFeatures()
    {
        $SQL = "SELECT * FROM `{$this->_featureTable}`";
        $stmt = $this->_db->prepare($SQL);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

}