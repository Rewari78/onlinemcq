<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 3/23/19
 * Time: 12:23 PM
 */

class EmailSmsManager
{
    const QUEUE_TYPE_SMS = 'sms';
    const QUEUE_TYPE_EMAIL = 'email';

    private $_db;
    private $_smsStatusTable;
    private $_emailStatusTable;
    private $_emailSMSQueueTable;
    private $_templateTable;

    private static $_classInstance;

    public static function getInstance()
    {
        if ( self::$_classInstance == null )
        {
            self::$_classInstance = new self;
        }

        return self::$_classInstance;
    }

    private function __construct()
    {
        $this->_db = Db::getInstance();
        $this->_smsStatusTable = DB_TABLE_SMS_STATUS;
        $this->_emailStatusTable = DB_TABLE_EMAIL_STATUS;
        $this->_emailSMSQueueTable = DB_TABLE_EMAIL_SMS_QUEUE;
    }

    /**
     *
     * Switch alert option.
     *
     * @param $userId
     * @param $alertValue
     * @return bool
     */
    public function changeSmsStatus($userId, $alertValue )
    {
        $SQL = "REPLACE INTO `{$this->_smsStatusTable}` 
                ( `userId`, `values` ) 
                VALUES ( :uId, :v ) ";

        $stmt = $this->_db->prepare($SQL);
        $stmt->execute(array(
            ':uId' => $userId,
            ':v' => json_encode($alertValue)
        ));

        return (bool) $stmt->rowCount();
    }

    /**
     *
     * @param $idList
     * @return array
     */
    public function getSmsStatus($idList )
    {
        if ( empty($idList) ) return array();

        $idList = (array) $idList;

        $SQL = "SELECT * FROM `{$this->_smsStatusTable}`
                WHERE `userId` IN ";

        $placeHolders = array_fill(0, count($idList), '?');
        $SQL .= " ( " . implode(',', $placeHolders) . ") ";
        $values = array_values($idList);


        $stmt = $this->_db->prepare($SQL);
        $stmt->execute($values);

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $output = array();
        foreach ( $result as $row )
        {
            $output[$row['userId']] = $row['values'];
        }

        return $output;
    }

    /**
     *
     * Switch alert option.
     *
     * @param $userId
     * @param $alertValue
     * @return bool
     */
    public function changeEmailStatus($userId, $alertValue )
    {
        $SQL = "REPLACE INTO `{$this->_emailStatusTable}` 
                ( `userId`, `values` ) 
                VALUES ( :uId, :v ) ";

        $stmt = $this->_db->prepare($SQL);
        $stmt->execute(array(
            ':uId' => $userId,
            ':v' => json_encode($alertValue)
        ));

        return (bool) $stmt->rowCount();
    }

    /**
     *
     * @param $idList
     * @return array
     */
    public function getEmailStatus($idList )
    {
        if ( empty($idList) ) return array();

        $idList = (array) $idList;

        $SQL = "SELECT * FROM `{$this->_emailStatusTable}`
                WHERE `userId` IN ";

        $placeHolders = array_fill(0, count($idList), '?');
        $SQL .= " ( " . implode(',', $placeHolders) . ") ";
        $values = array_values($idList);


        $stmt = $this->_db->prepare($SQL);
        $stmt->execute($values);

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $output = array();
        foreach ( $result as $row )
        {
            $output[$row['userId']] = $row['values'];
        }

        return $output;
    }


    public function createATemplate( $templateKey, $text, array $avlVars = null )
    {
        $SQL = "INSERT INTO `{$this->_templateTable}` 
                ( `template_key`, `template_format`, `template_variables` )
                ( :tk, :tf, :tv ) ";

        $stmt = $this->_db->prepare($SQL);
        $stmt->execute(array(
            ':tk' => $templateKey,
            ':tf' => $text,
            ':tv' => json_encode($avlVars)
        ));

        return (bool) $stmt->rowCount();
    }

    public function deleteTemplate( $id )
    {
        $SQL = "DELETE FROM `{$this->_templateTable}` 
                WHERE `id` = :id";

        $stmt = $this->_db->prepare($SQL);
        $stmt->execute(array(
            ':id' => $id
        ));

        return (bool) $stmt->rowCount();
    }

    public function insertQueue( $type, $schedule, array $obj, array $to )
    {
        $SQL = "INSERT INTO `{$this->_emailSMSQueueTable}` 
                ( `type`, `scheduleStart`, `object`, `to`, `timeStamp` )                
                VALUES ( :type, :schedule, :obj, :to, :ts )";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':type' => $type,
            ':schedule' => $schedule,
            ':obj' => json_encode($obj),
            ':to' => json_encode($to),
            ':ts' => time()
        ), $stmt);
        $stmt->execute();

        return (bool) $stmt->rowCount();
    }

    public function getQueue( $type )
    {
        $SQL = "SELECT * FROM `{$this->_emailSMSQueueTable}` 
        WHERE `type` = :t";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':t' => $type
        ), $stmt);
        $stmt->execute();

//        $result = $stmt->fetchAll();
    }

}