<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 29/9/18
 * Time: 9:16 PM
 */

class UserPackageController
{
    const DEFAULT_PACKAGE_ID = 1; //This is a free Id.

    private $_db;
    private $_packageUserRel = null;

    private static $_classInstance;

    public static function getInstance()
    {
        if ( self::$_classInstance == null )
        {
            self::$_classInstance = new self;
        }
        return self::$_classInstance;
    }

    private function __construct()
    {
        // We need to understand how everything
        // will work.
        $this->_db = Db::getInstance();
        $this->_packageUserRel = DB_TABLE_PACKAGE_USER_REL;
    }

    /**
     * Give user a package.
     *
     * @param $userId
     * @param $packageId
     * @return bool
     */
    public function givePackageToAUser( $userId, $packageId )
    {
        $SQL = "REPLACE INTO `{$this->_packageUserRel}` 
                ( `userId`, `packageId`, `timeStamp` ) 
                VALUES ( :uId, :pId, :t ) ";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':uId' => $userId,
            ':pId' => $packageId,
            ':t' => time()
        ), $stmt);
        $stmt->execute();

        return (bool) $stmt->rowCount();
    }

    public function getAUserPackage( $userId )
    {
        $SQL = "SELECT * FROM `{$this->_packageUserRel}` 
                WHERE `userId` = :uId ";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':uId' => $userId
        ), $stmt);
        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if ( empty($result) ) return 0;

        return (int) $result['packageId'];
    }

    /**
     * Disabling a package.
     *
     *
     * @param $packageId
     * @return int
     */
    public function disableAPackage( $packageId )
    {
        $SQL = "INSERT INTO `{$this->_packageUserRel}` 
                ( `packageId` ) VALUES ( ? )";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array($packageId), $stmt);
        $stmt->execute();

        return $stmt->rowCount();
    }

    /**
     * Enable a package.
     *
     * @param $packageId
     * @return int
     */
    public function enableAPackage( $packageId )
    {
        $SQL = "DELETE FROM `{$this->_}` 
WHERE `packageId` = :pId";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array($packageId), $stmt);
        $stmt->execute();

        return $stmt->rowCount();
    }
}