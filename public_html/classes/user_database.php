<?php
/**
 * Created by PhpStorm.
 * User: akash
 * Date: 29/6/18
 * Time: 9:18 AM
 */

class UserDatabase
{
    private static $_classInstance;

    private $_userTable = null;
    private $_userExtraTable = null;
    private $_userPackageRelationTable = null;
    private $_db = null;

    public static function getInstance()
    {
        if ( self::$_classInstance == null )
        {
            self::$_classInstance = new self;
        }

        return self::$_classInstance;
    }

    private function __construct()
    {
        $this->_userTable = DB_TABLE_USERS;
        $this->_userExtraTable = DB_TABLE_USER_EXTRA_INFO;
        $this->_userPackageRelationTable = DB_TABLE_PACKAGE_USER_REL;
        $this->_db = Db::getInstance();
    }

    /**
     *
     *
     * @param $firstName
     * @param $lastName
     * @param $email
     * @param $phone
     * @param $password
     * @param $userType
     * @return bool
     */
    public function createUser($firstName, $lastName, $email, $phone, $password, $userType )
    {

        $SQL = "INSERT INTO `{$this->_userTable}` 
        ( `firstName`, `lastName`, `email`, `phone`,
         `password`, `join`, `isActive`, `userType` ) 
        VALUES ( ?, ?, ?, ?,
                ?, ?, ?, ? ) ";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            $firstName, $lastName, $email,
            $phone, $password, time(), 1, $userType
        ), $stmt);
        $stmt->execute();
        
        return (bool) $stmt->rowCount();
    }

    /**
     * This is a user Id list.
     *
     * @param $userIdList
     * @return array
     */
    public function getUserInfoByIdList( $userIdList )
    {
        $idList = (array) $userIdList;

        if ( empty($idList) ) return array();

        $SQL = "SELECT * FROM `{$this->_userTable}` 
                WHERE `id` IN ";

        $placeHolders = array_fill(0, count($idList), '?');
        $values = array_values($idList);

        $SQL .= "(" . implode(',', $placeHolders) . ")";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues($values, $stmt);

        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $output = array();
        foreach ( $result as $row )
        {
            $output[$row['id']] = array(
                'id' => $row['id'],
                'firstName' => $row['firstName'],
                'lastName' => $row['lastName'],
                'email' => $row['email'],
                'phone' => $row['phone'],
                'password' => $row['password'],
                'join' => $row['join'],
                'isActive' => $row['isActive'],
                'userType' => $row['userType']
            );
        }

        return $output;
    }

    public function getUserInfoByEmail( $email )
    {
        $SQL = "SELECT * FROM `{$this->_userTable}` 
                WHERE `email` = :email";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':email' => $email
        ), $stmt);
        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if ( empty($result) ) return array();

        $output = array();
        $output = array(
            'id' => $result['id'],
            'firstName' => $result['firstName'],
            'lastName' => $result['lastName'],
            'email' => $result['email'],
            'phone' => $result['phone'],
            'password' => $result['password'],
            'join' => $result['join'],
            'isActive' => $result['isActive'],
            'userType' => $result['userType']
        );

        return $output;
    }

    /**
     * Get user info by Phone.
     *
     * @param $phone
     * @return array
     */
    public function getUserInfoByPhone( $phone )
    {
        $SQL = "SELECT * FROM `{$this->_userTable}` 
                WHERE `phone` = :phone";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':phone' => $phone
        ), $stmt);
        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if ( empty($result) ) return array();

        $output = array(
            'id' => $result['id'],
            'firstName' => $result['firstName'],
            'lastName' => $result['lastName'],
            'email' => $result['email'],
            'phone' => $result['phone'],
            'password' => $result['password'],
            'join' => $result['join'],
            'isActive' => $result['isActive'],
            'userType' => $result['userType']
        );

        return $output;
    }

    /**
     * Update user.
     *
     * @param int     $userId
     * @param string  $firstName
     * @param string  $lastName
     * @param string  $email
     * @param string  $phone
     * @param string  $password
     * @param string  $userType
     *
     * @return bool
     */
    public function updateUser($userId, $firstName, $lastName, $email,
                               $phone, $password, $userType )
    {
        $SQL = "UPDATE `{$this->_userTable}` 
                SET `firstName` = :firstName, `lastName` = :lastName, `email` = :email, 
                `phone` = :phone, `password` = :pass, `userType` = :type
                WHERE `id` = :id";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':firstName' => $firstName,
            ':lastName' => $lastName,
            ':email' => $email,
            ':phone' => $phone,
            ':pass' => $password,
            ':type' => $userType,
            ':id' => $userId
        ), $stmt);

        $stmt->execute();
        return (bool) $stmt->rowCount();
    }

    /**
     * Activate or deactivate an user account.
     *
     * @param int           $userId
     * @param bool          $doActive
     *
     * @return bool
     */
    public function activeOrDeactivateUser(  $userId, $doActive )
    {
        $SQL = "UPDATE `{$this->_userTable}` SET `isActive` = :iA
                WHERE `id` = :id";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':id' => $userId,
            ':iA' => $doActive === true ? 1 : 0
        ), $stmt);
        $stmt->execute();

        return (bool) $stmt->rowCount();
    }

    /**
     * Replace the extra infos for the date of birth.
     *
     * @param $userId
     * @param $state
     * @param $study12
     * @param $pcbP
     * @param $course
     * @param $DOB
     * @return bool
     */
    public function saveExtraInfo( $userId, $state, $study12, $pcbP, $course, $DOB )
    {
        $SQL = "REPLACE INTO `{$this->_userExtraTable}` 
                ( `userId`, `state`, `study12`, `pcbPercent`, `course`, `dob` )
                VALUES (:uId, :state, :study, :pcb, :course, :dob)";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':uId' => $userId,
            ':state' => $state,
            ':study' => $study12,
            ':pcb' => $pcbP,
            ':course' => $course,
            ':dob' => $DOB
        ), $stmt);
        $stmt->execute();

        return (bool) $stmt->rowCount();
    }

    /**
     * Get the extra info.
     *
     * @param $userId
     * @return array
     */
    public function getExtraInfo( $userId )
    {
        $SQL = "SELECT * FROM `{$this->_userExtraTable}` 
              WHERE `userId` = :id";

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues(array(
            ':id' => $userId
        ), $stmt);
        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        if ( empty($result) ) return array();

        // else filter before return data.
        return array(
            'stateId' => $result['state'],
            'study12' => $result['study12'],
            'pcbPercent' => !empty($result['pcbPercent']) ? $result['pcbPercent'] : "0",
            'course' => $result['course'],
            'dob' => $result['dob']
        );
    }


    public function getAllUsers( $skip = null, $limit = null )
    {
        $SQL = "SELECT * FROM `{$this->_userTable}` ";

        $dbParams = array();
        if ( $skip >= 0 && $limit >= 1 )
        {
            $SQL .= " LIMIT :s, :l ";
            $dbParams[':s'] = $skip;
            $dbParams[':l'] = $limit;
        }

        $stmt = $this->_db->prepare($SQL);
        Db::bindValues($dbParams, $stmt);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }


    public function getUserCount( )
    {
        // Get total user
        $SQL = "SELECT count(*) as `totalUser` FROM `{$this->_userTable}`";

        $stmt = $this->_db->prepare($SQL);
        $stmt->execute();
        $result1 = $stmt->fetch(PDO::FETCH_ASSOC);

        // get user count based on packages.
        $SQL = "SELECT count(*) as `count`, `packageId` FROM `{$this->_userTable}`  AS `ut`
                INNER JOIN `{$this->_userPackageRelationTable}` AS `upr` 
                ON ( `ut`.`id` = `upr`.`userId` ) 
                GROUP BY `upr`.`packageId` ";

        $stmt = $this->_db->prepare($SQL);
        $stmt->execute();
        $result2 = $stmt->fetchAll(PDO::FETCH_ASSOC);


        return array(
            'totalUser' => $result1['totalUser'],
            'packageUserCount' => $result2
        );
    }

}