<?php

/**
 * Created by PhpStorm.
 * User: akash
 * Date: 27/7/16
 * Time: 7:33 AM
 */

class Db
{
    private static $db;

    public static function getInstance()
    {
        if ( self::$db === null )
        {
            self::$db = new \PDO('mysql:dbname=' . DB_B . ';host=' . DB_HOST, DB_USR, DB_PASS, array(
                PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true,
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8mb4' COLLATE 'utf8mb4_unicode_ci'"
            ));
            self::$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
        }

        return self::$db;
    }

    public static function bindValues( $valuesWithKeys, PDOStatement $stmt )
    {
        foreach ( $valuesWithKeys as $key => $value ) {
            $valueType = PDO::PARAM_STR;
            if ( is_int($value) ) $valueType = PDO::PARAM_INT;

            $stmt->bindValue( is_int($key) ? $key + 1 : $key, $value, $valueType);
        }
    }

}